# coding=utf-8
import sys
from socdata import SOCXMLData

class Settings():
    RootDir = './'
    SOC = None
    Files = None

def sls(s, k=None):
    return sorted(list(set(s)), key=k)

class Classes():
    @staticmethod
    def generateID(text, upper=None, lower=None, camel=None, camel1=None, sep="_", inv=""):
        result = []
        for t in text.split():
            r = ""
            for c in t:
                valid = (c >= '0' and c <= '9') or (c >= 'A' and c <= 'Z') or (c >= 'a' and c <= 'z') or (c in ['_'])
                if not valid:
                    c = inv
                r += c
            if r:
                result.append(r)
                
        if camel1 == True:
            result = [r[0].upper() + r[1:] for r in result]
        if camel == True:
            result = [r[0].upper() + r[1:].lower() for r in result]
        result = sep.join(result)
        if upper == True:
            result = result.upper();
        if lower == True:
            result = result.lower();
        
        return result

    class F_Macros():
        @classmethod
        def header_guard(self):
            return "__%s_MACROS_INCLUDED__" % Settings.SOC.name

    class D_SOC():
        def __init__(self):
            self.name = None
            self.C_typename = None
            self.devices = []
            self.source_soc = None
            
        def __str__(self):
            return self.name
        
        def header_guard(self):
            return "__%s_HEADER_INCLUDED__" % self.name

    class D_Device():
        def __init__(self, soc, name):
            self.name = name
            self.C_typename = None
            self.base = None
            self.base_device = None
            self.registers = []
            self.source_device = None
            soc.devices.append(self)
            
        def header_guard(self):
            return "__%s_HEADER_INCLUDED__" % Classes.generateID(self.source_device.name, upper=True, sep="")
            
        def __str__(self):
            return "[%s] %s%s" % ("BASE" if self.base == None else "0x%08X" % self.base, self.name, " (%s)" % self.base_device.name if self.base_device else "")
        
    class D_UnionField():
        def __init__(self, union, name):
            self.name = name
            self.position = None
            self.size = None
            self.type = None
            union.fields.append(self)
        
        def __str__(self):
            if self.size == 1:
                return "[%s] %s" % (self.position, self.name if self.name else "")
            else:
                return "[%s:%s] %s" % (self.position, self.position + self.size - 1, self.name if self.name else "")

    class D_RegisterUnion():

        def __init__(self, typ, name, union_def):
            typ.unions.append(self)
            self.name = name
            self.fields = []
            for field_def in union_def:
                union = Classes.D_UnionField(self, field_def['name'])
                union.size = field_def['size']
                union.position = field_def['bit']
                # TODO: field type
                class t():
                    name = "uint32_t"
                union.type = t
        
        def __str__(self):
            return self.name or ""

    class D_RegisterType():
    
        def __init__(self, name, defs):
            self.name = name
            self.C_typename = "REG_%s" % self.name
            self.size = defs[1]
            self.unions = []
            for union_name, union_def in defs[0].items():
                Classes.D_RegisterUnion(self, union_name, union_def)
        
        @classmethod
        def header_guard(self):
            return "__%s_TYPES_HEADER_INCLUDED__" % Settings.SOC.name

        def __str__(self):
            return "%s, %s bits" % (self.name, self.size)
            
    class D_Register():
        def __init__(self, dev, name, typ):
            self.name = name
            self.type = typ
            self.offset = None
            self.count = 1
            self.source_registers = []
            dev.registers.append(self)
            
        def __str__(self):
            return "[%04X] %s %s%s" % (self.offset, self.type.name, self.name, ("[%s]" % self.count if self.count > 1 else ""))
    
    class F_File():
        def __init__(self, cpp=None, h=None, cpp_data=(None, None, None), h_data=(None, None, None)):
            class ff():
                def __init__(self, fname, data=(None, None, None)):
                    self.__data_start = data[0]
                    self.__data_end = data[1]
                    self.__data_param = data[2]
                    self.__file = open(Settings.RootDir + fname, 'wb')
                    self.fullname = self.__file.name
                    self.name = fname
                
                def init(self):
                    if callable(self.__data_start):
                        self.__data_start(self, self.__data_param)
                    elif self.__data_start != None:
                        self.write(self.__data_start)                
                    
                def write(self, s):
                    self.__file.write(s)
    
                def writeln(self, s=""):
                    self.__file.write(s + "\n")
            
                def writelines(self, sequence_of_strings):
                    self.__file.writelines(sequence_of_strings)
                    
                def close(self):
                    if callable(self.__data_end):
                        self.__data_end(self, self.__data_param)
                    elif self.__data_end != None:
                        self.write(self.__data_end)                
                    self.__file.close()
                    
                def __str__(self):
                    return self.name
            
            self.source = None
            self.header = None
            if cpp:
                self.source = ff(cpp, cpp_data)
            if h:
                self.header = ff(h, h_data)
            
        def init(self):
            if self.source:
                self.source.init()
            if self.header:
                self.header.init()
            
        def close(self):
            if self.source:
                self.source.close()
            if self.header:
                self.header.close()
                
        def __str__(self):
            s = []
            if self.source:
                s.append("source: %s" % self.source.name)
            if self.header:
                s.append("header: %s" % self.header.name)
            return ", ".join(s)
    
    class F_Files(dict):
        def __start_device_cpp(self, source, device):
            header = self[device].header
            macros = self[Classes.F_Macros()]
            source.writeln('#include "%s"' % header.name)
            source.writeln('#include "%s"' % macros.header.name)
            source.writeln()
            source.writeln('namespace %s' % Settings.SOC.name)
            source.writeln('{')
            source.writeln()

        def __end_device_cpp(self, source, device):
            source.writeln('} /* %s namespace */' % Settings.SOC.name)
        
        def __start_device_h(self, header, device):
            types = self[device.registers[0]].header
            guard_name = device.header_guard()
            header.writeln('#ifndef %s' % guard_name)
            header.writeln('#define %s' % guard_name)
            header.writeln()
            header.writeln('#include <soc.h>')
            header.writeln('#include "%s"' % types.name)
            header.writeln()
            header.writeln('namespace %s' % Settings.SOC.name)
            header.writeln('{')
            header.writeln()

        def __end_device_h(self, header, device):
            guard_name = device.header_guard()
            header.writeln('} /* %s namespace */' % Settings.SOC.name)
            header.writeln()
            header.writeln('#endif /* %s */' % guard_name)
        
        def __start_types_cpp(self, source, t):
            header = self[t].header
            source.writeln('#include "%s"' % header.name)
            source.writeln()
            source.writeln('namespace %s' % Settings.SOC.name)
            source.writeln('{')
            source.writeln()
            source.writeln('const char* const %s_DeviceNames[] = {%s};' % 
                           (Settings.SOC.name, ", ".join(['"%s"' % device.name for device in Settings.SOC.devices])))
            source.writeln()            

        def __end_types_cpp(self, source, t):
            source.writeln('} /* %s namespace */' % Settings.SOC.name)
        
        def __start_types_h(self, header, t):
            guard_name = t.header_guard()
            header.writeln('#ifndef %s' % guard_name)
            header.writeln('#define %s' % guard_name)
            header.writeln()
            header.writeln('#include <soc.h>')
            header.writeln()
            header.writeln('namespace %s' % Settings.SOC.name)
            header.writeln('{')
            header.writeln('extern const char* const %s_DeviceNames[];' % Settings.SOC.name)
            header.writeln()

        def __end_types_h(self, header, t):
            guard_name = t.header_guard()
            header.writeln('} /* %s namespace */' % Settings.SOC.name)
            header.writeln()
            header.writeln('#endif /* %s */' % guard_name)

        def __start_macros_cpp(self, source, t):
            header = self[t].header
            source.writeln('#include "%s"' % header.name)
            source.writeln()
            source.writeln('namespace %s' % Settings.SOC.name)
            source.writeln('{')
            source.writeln()

        def __end_macros_cpp(self, source, t):
            source.writeln('} /* %s namespace */' % Settings.SOC.name)
        
        def __start_macros_h(self, header, m):
            guard_name = m.header_guard()
            header.writeln('#ifdef %s' % guard_name)
            header.writeln('#error This file can be inclued only once!!!')
            header.writeln('#endif')
            header.writeln('#define %s' % guard_name)
            header.writeln()
            header.writeln('#include <soc.h>')
            header.writeln('#define ___SOC_NAME %s' % Settings.SOC.name)
            header.writeln()
            
            for device in Settings.SOC.devices:
                if device.base != None:
                    header.writeln('#define __%s_%s_class %s' % 
                                    (Settings.SOC.name, device.name, device.C_typename))
            idx = 0
            for device in Settings.SOC.devices:
                header.writeln('#define __%s_%s_id %s' % 
                                (Settings.SOC.name, device.name, idx))
                idx += 1
            
            header.writeln('#include <soctypes.h>')
            header.writeln()
            header.writeln('namespace %s' % Settings.SOC.name)
            header.writeln('{')
            header.writeln()

        def __end_macros_h(self, header, m):
            header.writeln('} /* %s namespace */' % Settings.SOC.name)

        def __start_soc_cpp(self, source, soc):
            header = self[soc].header
            source.writeln('#include "%s"' % header.name)
            source.writeln('#include "%s"' % self[Classes.F_Macros()].header.name)
            source.writeln()
            source.writeln('namespace %s' % soc.name)
            source.writeln('{')
            source.writeln()

        def __end_soc_cpp(self, source, soc):
            source.writeln('} /* %s namespace */' % soc.name)
        
        def __start_soc_h(self, header, soc):
            types = self[soc.devices[0].registers[0]]
            guard_name = soc.header_guard()
            header.writeln('#ifndef %s' % guard_name)
            header.writeln('#define %s' % guard_name)
            header.writeln()
            header.writeln('#include "%s"' % types.header.name)
            # TODO: CPU core support
            header.writeln('#include <armcpu.h>')
            header.writeln()
            # include all devices
            for device in soc.devices:
                if device.base_device == None:
                    files = self[device]
                    header.writeln('#include "%s"' % files.header.name)
                    
            header.writeln()
            header.writeln('// %s supported devices' % soc.name)
            header.writeln('#include <Memory/RAMDevice.h>')
            header.writeln()
            header.writeln('namespace %s' % soc.name)
            header.writeln('{')
            header.writeln()

        def __end_soc_h(self, header, soc):
            guard_name = soc.header_guard()
            header.writeln('} /* %s namespace */' % soc.name)
            header.writeln()
            header.writeln('#endif /* %s */' % guard_name)
        
        def __getitem__(self, idx):
            if isinstance(idx, Classes.D_Device):
                if idx.base_device:
                    return self[idx.base_device]
                if not idx in self:
                    _name = Classes.generateID(idx.source_device.name, sep="")
                    cpp_name = "%s.cpp" % _name 
                    h_name = "%s.h" % _name
                    self[idx] = Classes.F_File(h=h_name, cpp=cpp_name, 
                                               cpp_data=(self.__start_device_cpp, self.__end_device_cpp, idx),
                                               h_data=(self.__start_device_h, self.__end_device_h, idx))
                    self[idx].init()
            if isinstance(idx, Classes.D_RegisterType) or isinstance(idx, Classes.D_Register):
                idx = Classes.D_RegisterType
                if not idx in self:
                    cpp_name = "%stypes.cpp" % (Settings.SOC.name)
                    h_name = "%stypes.h" % (Settings.SOC.name)
                    self[idx] = Classes.F_File(h=h_name, cpp=cpp_name,
                                               cpp_data=(self.__start_types_cpp, self.__end_types_cpp, idx),
                                               h_data=(self.__start_types_h, self.__end_types_h, idx))
                    self[idx].init()
            if isinstance(idx, Classes.F_Macros):
                idx = Classes.F_Macros
                if not idx in self:
                    cpp_name = "%smacros.cpp" % (Settings.SOC.name)
                    h_name = "%smacros.h" % (Settings.SOC.name)
                    self[idx] = Classes.F_File(h=h_name, cpp=cpp_name,
                                               cpp_data=(self.__start_macros_cpp, self.__end_macros_cpp, idx),
                                               h_data=(self.__start_macros_h, self.__end_macros_h, idx))
                    self[idx].init()
            if isinstance(idx, Classes.D_SOC):
                if not idx in self:
                    cpp_name = "%s.cpp" % (Settings.SOC.name)
                    h_name = "%s.h" % (Settings.SOC.name)
                    self[idx] = Classes.F_File(h=h_name, cpp=cpp_name,
                                               cpp_data=(self.__start_soc_cpp, self.__end_soc_cpp, idx),
                                               h_data=(self.__start_soc_h, self.__end_soc_h, idx))
                    self[idx].init()

            return dict.__getitem__(self, idx)
            
        def close(self):
            for f in self.values():
                f.close()
            self.clear()

class SourceGenerator():

    def write_SOC(self, soc):
        header = Settings.Files[soc].header
        source = Settings.Files[soc].source

        def write_constructor():
            header.writeln('\t%s();' % soc.C_typename);
            
            source.writeln('%s::%s(): SOCBase()' % (soc.C_typename, soc.C_typename))
            source.writeln('{')
            # TODO: CPU core support
            source.writeln('\tINIT_SOC_CORE(ARM_CPU_ARM926EJS);')
            source.writeln()
            source.writeln('\t// initialize %s devices' % soc.name)
            for device in soc.devices:
                if device.base != None:
                    source.writeln('\tINIT_SOC_DEVICE(%s);' % device.name)
            source.writeln('}')
            source.writeln()
            
        def write_Reset():
            header.writeln('\tSOC_METHOD virtual bool Reset();');
            
            source.writeln('bool %s::Reset()' % soc.C_typename)
            source.writeln('{')
            # TODO: CPU core support
            source.writeln('\tCore->Reset();')
            source.writeln('\treturn SOCBase::Reset();')
            source.writeln('}')
            source.writeln()
            
        def write_AttachDevice():
            header.writeln('\tSOC_METHOD virtual void AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index);');
            
            source.writeln('void %s::AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index)' % soc.C_typename)
            source.writeln('{')
            source.writeln('\tswitch (type) {')
            source.writeln('\t\tcase EMULATED_RAM:')
            source.writeln('\t\t\t{')
            # TODO: CPU core support
            source.writeln('\t\t\t\tRAMMemory* ram = dynamic_cast<RAMMemory*>(device);')
            source.writeln('\t\t\t\tCore->AddPhysicalMemory(ram->GetRAW(), 0x30000000, ram->GetSize());')
            source.writeln('\t\t\t}')
            source.writeln('\t\t\tbreak;')
            source.writeln('\t\tdefault:')
            source.writeln('\t\t\tSOCBase::AttachDevice(device, type, index);')
            source.writeln('\t\t\tbreak;')
            source.writeln('\t}')
            source.writeln('}')
            source.writeln()
            
        def write_PowerOn():
            header.writeln('\tSOC_METHOD virtual bool PowerOn();');
            
            source.writeln('bool %s::PowerOn()' % soc.C_typename)
            source.writeln('{')
            source.writeln('\tif (!powered_on) {')
            source.writeln('\t\tif (SOCBase::PowerOn()) {')
            # TODO: CPU core support
            source.writeln('\t\t\tCore->PowerOn();')
            source.writeln('\t\t}')
            source.writeln('\t}')
            source.writeln('\treturn powered_on;')
            source.writeln('}')
            source.writeln()
            
        def write_PowerOff():
            header.writeln('\tSOC_METHOD virtual bool PowerOff();');
            
            source.writeln('bool %s::PowerOff()' % soc.C_typename)
            source.writeln('{')
            source.writeln('\tif (powered_on) {')
            source.writeln('\t\tif (SOCBase::PowerOff()) {')
            # TODO: CPU core support
            source.writeln('\t\t\tCore->PowerOff();')
            source.writeln('\t\t}')
            source.writeln('\t}')
            source.writeln('\treturn !powered_on;')
            source.writeln('}')
            source.writeln()

        header.writeln("class %s: public SOCBase" % soc.C_typename)
        header.writeln('{')
        
        # private declarations
        header.writeln('private:')
        header.writeln()
        
        # protected declarations
        header.writeln('protected:')
        header.writeln()

        # public declarations
        header.writeln('public:')
        # TODO: CPU core support
        header.writeln('\tARMCPU* Core;')
        header.writeln()
        for device in soc.devices:
            if device.base != None:
                header.writeln('\t%s* %s;' % (device.C_typename, device.name))
        header.writeln();
        write_constructor()
        write_PowerOn()
        write_PowerOff()
        write_Reset()
        header.writeln()
        write_AttachDevice()
        header.writeln()        
        header.writeln('};')
        header.writeln()

    def write_device(self, device):
        header = Settings.Files[device].header
        source = Settings.Files[device].source
        
        base_C_typename = device.base_device.C_typename if device.base_device else "SOCIODevice" 

        def write_register_type(register):
            hdr = Settings.Files[register].header
            
            def write_register_definition_base(size):
                if size == 64:
                    hdr.writeln('\tuint64_t _Quad;')
                    hdr.writeln('\tuint32_t _Word[2];')
                    hdr.writeln('\tuint16_t _HalfWord[4];')
                    hdr.writeln('\tuint8_t _Byte[8];')
                elif size == 32:
                    hdr.writeln('\tuint32_t _Word;')
                    hdr.writeln('\tuint16_t _HalfWord[2];')
                    hdr.writeln('\tuint8_t _Byte[4];')
                elif size == 16:
                    hdr.writeln('\tuint16_t _HalfWord;')
                    hdr.writeln('\tuint8_t _Byte[2];')
                elif size == 8:
                    hdr.writeln('\tuint8_t _Byte;')
                else:
                    assert(False)
                
            regtype = register.type
            if not regtype in self.types_written:
                self.types_written.append(regtype)
                hdr.writeln('union %s {' % regtype.C_typename)
                write_register_definition_base(regtype.size)
                for union in regtype.unions:
                    hdr.writeln('\tstruct {')
                    for field in union.fields:
                        hdr.writeln('\t\t%s %s:%s;' % (field.type.name, field.name or '', field.size))                        
                    hdr.writeln('\t}%s;' % ((" " + union.name) if union.name else ""))
                hdr.writeln('};')
                hdr.writeln()

        def write_device_types():
            hdr = Settings.Files[device.registers[0]].header
            if device.base != None:
                hdr.writeln('constexpr uint __%s_BASE = 0x%08X;' % (device.name, device.base))
                last_address = device.registers[-1].offset + device.registers[-1].count * (device.registers[-1].source_registers[0].get_size() / 8)
                hdr.writeln('constexpr uint __%s_SIZE = 0x%08X;' % (device.name, last_address))
                hdr.writeln()            
            
        def write_registers_type():
            hdr = Settings.Files[device.registers[0]].header
            macros = Settings.Files[Classes.F_Macros()].header
            
            # write register aliases
            for register in device.registers:
                if register.source_registers[0].name != register.name:
                    macros.writeln('#define __%s_%s_ALIAS %s' % (Settings.SOC.name, register.name, register.name))
                for sr in register.source_registers:
                    macros.writeln('#define __%s_%s_ALIAS %s' % (Settings.SOC.name, sr.name, register.name))

            hdr.writeln('struct %s_Registers {' % device.name)
            for register in device.registers:
                hdr.writeln('\t%s %s%s;' % (register.type.C_typename, register.name, 
                                            "[%s]" % register.count if register.count > 1 else ""))
            hdr.writeln('};')
            hdr.writeln()

        def write_register_flags(register):
            if not register.name in self.types_written:
                hdr = Settings.Files[register].header
                if device.base == None:
                    hdr.writeln('constexpr uint __%s_OFFSET = 0x%08X;' % (register.name, register.offset))
                if register.count > 1:
                    hdr.writeln('constexpr uint __%s_ARRAY_START = 0x%08X;' % (register.name, register.offset))
                    hdr.writeln('constexpr uint __%s_ARRAY_LENGTH = 0x%08X;' % (register.name, register.count))
                    hdr.writeln('constexpr uint __%s_ARRAY_ELEMENT_SIZE = sizeof(%s);' % (register.name, register.type.C_typename))
                    hdr.writeln('constexpr uint __%s_ARRAY_END = __%s_ARRAY_START + __%s_ARRAY_ELEMENT_SIZE * __%s_ARRAY_LENGTH;' % 
                                (register.name, register.name, register.name, register.name))
                for reg in register.source_registers:
                    if not reg.name in self.types_written:
                        self.types_written.append(reg.name)
                        flags = reg.get_flags()
                        hdr.writeln('constexpr uint __%s_OFFSET = 0x%08X;' % (reg.name, reg.offset))
                        hdr.writeln('constexpr uint __%s_SIZE = %u;' % (reg.name, reg.get_size()))
                        for flag_name in sorted(flags.keys()):
                            flag_value = flags[flag_name]
                            if flag_value != None:
                                hdr.writeln('constexpr uint __%s_%s = 0x%08X;' % (reg.name, flag_name.upper(), flag_value))
                self.types_written.append(register.name)
                hdr.writeln()
                
        def write_constructor():
            header.writeln('\t%s(SOCBase* soc);' % device.C_typename);
            
            source.writeln('%s::%s(SOCBase* soc): %s(soc)' % (device.C_typename, device.C_typename, base_C_typename))
            source.writeln('{')
            source.writeln()
            source.writeln('}')
            source.writeln()        
            
        def write_Reset():
            header.writeln('\tSOC_METHOD virtual bool Reset();');
            
            source.writeln('bool %s::Reset()' % device.C_typename)
            source.writeln('{')
            source.writeln('\treturn %s::Reset();' % base_C_typename)
            source.writeln('}')
            source.writeln()
            
        def write_ResetRegisters():
            header.writeln('\tSOC_METHOD virtual void ResetRegisters();');
            
            source.writeln('void %s::ResetRegisters()' % device.C_typename)
            source.writeln('{')
            if device.base != None:
                for register in device.registers:
                    if register.count == 1: 
                        if register.source_registers[0].reset_value != None:
                            source.writeln('\tIOREG_RESET(%s);' % register.source_registers[0].name)
                    else:
                        source.writeln('\tIOREG_ARRAY_RESET(%s);' % register.name)
            source.writeln('\t%s::ResetRegisters();' % base_C_typename)
            source.writeln('}')
            source.writeln()
            
        def write_PowerOn():
            header.writeln('\tSOC_METHOD virtual bool PowerOn();');
            
            source.writeln('bool %s::PowerOn()' % device.C_typename)
            source.writeln('{')
            source.writeln('\treturn %s::PowerOn();' % base_C_typename)
            source.writeln('}')
            source.writeln()
            
        def write_PowerOff():
            header.writeln('\tSOC_METHOD virtual bool PowerOff();');
            
            source.writeln('bool %s::PowerOff()' % device.C_typename)
            source.writeln('{')
            source.writeln('\treturn %s::PowerOff();' % base_C_typename)
            source.writeln('}')
            source.writeln()

        def write_reg_access():
            # required access type: quad, word, half, byte
            _required_access_types = []
            required_access_types = []
            for r in device.registers:
                for sr in r.source_registers:
                    _required_access_types += sr.get_access_type()
            _required_access_types = sls(_required_access_types)
            for access_type in _required_access_types:
                if access_type == 'quad':
                    required_access_types.append(("uint64_t", "Quad", access_type))
                elif access_type == 'word':
                    required_access_types.append(("uint32_t", "Word", access_type))
                elif access_type == 'half':
                    required_access_types.append(("uint16_t", "HalfWord", access_type))
                elif access_type == 'byte':
                    required_access_types.append(("uint8_t", "Byte", access_type))
                else:
                    self.generate_failed("invalid %s access type: %s" % (device.name, access_type))
                
            # required access: read, write
            access_required = []
            for r in device.registers:
                for sr in r.source_registers:
                    access_required += sr.get_access()
            access_required = sls(access_required)

            if "read" in access_required:
                for access_type in required_access_types:
                    header.writeln('\tSOC_METHOD virtual %s Read%s(uint32_t IOAddress);' % (access_type[0], access_type[1]));
                    
                    source.writeln('%s %s::Read%s(uint32_t IOAddress)' % (access_type[0], device.C_typename, access_type[1]))
                    source.writeln('{')
                    source.writeln('\tIOREG_BEGIN_READ(%s, %s);' % (device.name, access_type[2].upper()))
                    source.writeln('\tswitch(IOAddress) {')
                    has_arrays = any([r.count > 1 for r in device.registers])
                    for r in filter(lambda r: r.count == 1, device.registers):
                        can_read = any(filter(lambda r: 'read' in r.get_access(), r.source_registers))
                        real_r = r.source_registers[0] if device.base_device else r
                        real_register_name = " // %s" % real_r.name if r != real_r else ""
                        source.writeln("\t\tcase IOREG_OFFSET(%s): {%s" % (r.name, real_register_name))
                        if can_read:
                            if device.base != None:
                                source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_READ(%s);" % real_r.name)
                                source.writeln("\t\t\t\treturn IOREG_READ(%s);" % real_r.name)
                            else:
                                source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_READ(%s);" % real_r.name)
                                source.writeln("\t\t\t\treturn IOREG_BASE_READ(%s);" % real_r.name)
                        else:
                            source.writeln("\t\t\t\tIOREG_INVALID_READ_ACCESS(%s);" % real_r.name)
                            source.writeln("\t\t\t\treturn 0;")
                        source.writeln("\t\t\t}")
                        source.writeln('\t\t\tbreak;')
                    if has_arrays:
                        source.writeln()
                        source.writeln("\t\tdefault:")
                        source.writeln("\t\t\tIOREG_BEGIN_ARRAYS();")
                        for r in filter(lambda r: r.count > 1, device.registers):
                            source.writeln("\t\t\t// %s" % ", ".join([x.name for x in r.source_registers]))
                            source.writeln("\t\t\tIOREG_ARRAY(%s, index) {" % r.name)
                            source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_ARRAY_READ(%s, index);" % r.name)
                            source.writeln("\t\t\t\treturn IOREG_ARRAY_READ(%s, index);" % (r.name))
                            source.writeln("\t\t\t}")
                        source.writeln("\t\t\tIOREG_END_ARRAYS();")
                        source.writeln()
                        source.writeln('\t\t\tIOREG_INVALID_READ_ADDRESS();')
                        source.writeln('\t\t\tbreak;')
                    else:
                        source.writeln()
                        source.writeln('\t\tdefault:')
                        source.writeln('\t\t\tIOREG_INVALID_READ_ADDRESS();')
                        source.writeln('\t\t\tbreak;')
                    source.writeln('\t}')
                    source.writeln('\treturn %s::Read%s(IOAddress);' % (base_C_typename, access_type[1]))
                    source.writeln('}') # end switch
                    source.writeln()

            if "write" in access_required:
                for access_type in required_access_types:
                    header.writeln('\tSOC_METHOD virtual void Write%s(uint32_t IOAddress, %s Value);' % (access_type[1], access_type[0]));
                    
                    source.writeln('void %s::Write%s(uint32_t IOAddress, %s Value)' % (device.C_typename, access_type[1], access_type[0]))
                    source.writeln('{')
                    source.writeln('\tIOREG_BEGIN_WRITE(%s, %s);' % (device.name, access_type[2].upper()))
                    source.writeln('\tswitch(IOAddress) {')
                    has_arrays = any([r.count > 1 for r in device.registers])
                    for r in filter(lambda r: r.count == 1, device.registers):
                        can_write = any(filter(lambda r: 'write' in r.get_access(), r.source_registers))
                        real_r = r.source_registers[0] if device.base_device else r
                        real_register_name = " // %s" % real_r.name if r != real_r else ""
                        source.writeln("\t\tcase IOREG_OFFSET(%s): {%s" % (r.name, real_register_name))
                        if can_write:
                            if device.base != None:
                                source.writeln("\t\t\t\tIOREG_VERIFY_WRITE(%s, Value);" % real_r.name)
                                source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_WRITE(%s);" % real_r.name)
                                source.writeln("\t\t\t\tIOREG_WRITE(%s, Value);" % real_r.name)
                            else:
                                source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_WRITE(%s);" % real_r.name)
                                source.writeln("\t\t\t\tIOREG_BASE_WRITE(%s, Value);" % real_r.name)
                        else:
                            source.writeln("\t\t\t\tIOREG_INVALID_WRITE_ACCESS(%s);" % real_r.name)
                        source.writeln("\t\t\t}")
                        source.writeln('\t\t\t%s;' % ('break' if can_write else 'return'))
                    if has_arrays:
                        source.writeln()
                        source.writeln("\t\tdefault:")
                        source.writeln("\t\t\tIOREG_BEGIN_ARRAYS();")
                        for r in filter(lambda r: r.count > 1, device.registers):
                            source.writeln("\t\t\t// %s" % ", ".join([x.name for x in r.source_registers]))
                            source.writeln("\t\t\tIOREG_ARRAY(%s, index) {" % r.name)
                            source.writeln("\t\t\t\tIOREG_VERIFY_ARRAY_WRITE(%s, index, Value);" % r.name)
                            source.writeln("\t\t\t\tIOREG_NOT_IMPLEMENTED_ARRAY_WRITE(%s, index);" % r.name)
                            source.writeln("\t\t\t\tIOREG_ARRAY_WRITE(%s, index, Value);" % r.name)
                            source.writeln("\t\t\t}")
                        source.writeln("\t\t\tIOREG_END_ARRAYS();")
                        source.writeln()
                        source.writeln('\t\t\tIOREG_INVALID_WRITE_ADDRESS();')
                        source.writeln('\t\t\tbreak;')
                    else:
                        source.writeln()
                        source.writeln('\t\tdefault:')
                        source.writeln('\t\t\tIOREG_INVALID_WRITE_ADDRESS();')
                        source.writeln('\t\t\tbreak;')
                    source.writeln('\t}')
                    source.writeln('\t%s::Write%s(IOAddress, Value);' % (base_C_typename, access_type[1]))
                    source.writeln('}') # end switch
                    source.writeln()

        header.writeln('/*')
        header.writeln(' * %s declaration' % device.C_typename)
        header.writeln(' */')
        source.writeln('/*')
        source.writeln(' * %s implementation' % device.C_typename)
        source.writeln(' */')
        
        header.writeln("class %s: public %s" % (device.C_typename, base_C_typename))
        header.writeln('{')

        # private declarations
        header.writeln('private:')
        header.writeln()
        
        # protected declarations
        header.writeln('protected:')
        write_device_types()
        if not device.base_device:
            # register types required
            for register in device.registers:
                write_register_type(register)
            write_registers_type()
            header.writeln('\t%s_Registers reg;' % device.name)
        header.writeln()

        # public declarations
        header.writeln('public:')
        write_constructor()
        write_PowerOn()
        write_PowerOff()
        write_Reset()
        write_ResetRegisters()
        header.writeln()
        write_reg_access()        
        header.writeln('};')
        header.writeln()
        for register in device.registers:
            write_register_flags(register)
        
    def generate_failed(self, msg):
        print 'GENERATE FAILED!'
        print msg
        sys.exit(1)
    
    def get_register_definition(self, regs):
        def check_register(reg):
            # bits required in each union
            bits_needed = {}
            for field in reg.fields:
                bits_needed[field.union] = range(reg.get_size())
            # check if all bits are used in each union
            for field in reg.fields:
                try:
                    for bit in range(field.size):
                        bits_needed[field.union].remove(bit + field.bit)
                except:
                    self.generate_failed("field %s does not fit in %s.%s" % (unicode(field), reg._parent._parent.shortname, reg.name))
    
            for union, bits in bits_needed.items():
                if len(bits) > 0:
                    self.generate_failed("union '%s' is incomplete in %s.%s. bits missing: %s" % (union or '', reg._parent._parent.shortname, reg.name, bits))
        assert(len(regs) >= 1)
        result = {}
        reg_size = None
        for reg in regs:
            check_register(reg)
            if reg_size == None:
                reg_size = reg.get_size()
            else:
                if reg_size != reg.get_size():
                    self.generate_failed("register size mismatch: %s, should be: %s" % (reg.name, reg_size))

            for field in reg.fields:
                union_info = result.get(field.union)
                if not union_info:
                    # create union info
                    union_info = []
                    result[field.union] = union_info
                field_info = filter(lambda x: x['size'] == field.size and x['bit'] == field.bit, union_info)
                if not field_info:
                    # create field info
                    field_info = {
                        'name': None,
                        'bit': field.bit,
                        'size': field.size,
                        'fields': []
                    }
                    union_info.append(field_info)
                    union_info.sort(key=lambda x: x['bit'])
                else:
                    assert(len(field_info) == 1)
                    field_info = field_info[0]
                field_info['fields'].append(field)
                if field.name:
                    if field_info['name']:
                        if field_info['name'] != field.name:
                            self.generate_failed('two fields on the same position with different names for registers %s, bit: %s, size: %s (%s != %s)' %
                                                 ([str(x.name) for x in regs], field.bit, field.size, field.name, field_info['name']))
                    else:
                        field_info['name'] = field.name

        # check that all union has the same size, and field does not overlap
        for union_name, union_info in result.items():
            required_bits = range(reg_size)
            for field_info in union_info:
                try:
                    for b in range(field_info['size']):
                        required_bits.remove(field_info['bit'] + b)                        
                except:
                    self.generate_failed("union '%s' error in registers %s" % (union_name or '', [str(x.name) for x in regs]))
        return result, reg_size
    
    def generate_code(self, data_source, dest_dir):
        Settings.RootDir = dest_dir
        data = SOCXMLData()
        print "Loading SOC from %s..." % data_source
        data.load_from_file(data_source)
        Settings.source_soc = data.SOC
        Settings.SOC = Classes.D_SOC()
        Settings.Files = Classes.F_Files()
        
        print "Generating source code for %s..." % (data.SOC.title or data.SOC.name)
        self.prepare_data(Settings.source_soc)
        self.write_SOC(Settings.SOC)

        for device in Settings.SOC.devices:
            print "--> %s..." % device
            self.write_device(device)
            
        Settings.Files.close()
    
    def prepare_data(self, soc):
        self.types_written = []
        
        Settings.SOC.name = soc.name
        Settings.SOC.source_soc = soc
        Settings.SOC.C_typename = soc.name
        
        # parse all devices, and assign registers to them
        for device in soc.devices.sorted("base"):
            # init registers types
            reg_type = {}
            for typ in sls([r.get_type() for r in device.registers]):
                # for each type get registers of this type
                regs = filter(lambda r: r.get_type() == typ, device.registers)
                typ = Classes.D_RegisterType(typ, self.get_register_definition(regs))
                for reg in regs:
                    reg_type[reg.name] = typ
                    # name for array or name in virtual device
                    reg_type[reg.get_name()] = typ

            # get virtual devices list
            required_devices = sls([reg.device for reg in device.registers])
            base_device = None
            base_device_registers = {}
            if len(required_devices) > 1:
                # create base device
                base_device = Classes.D_Device(Settings.SOC, Classes.generateID(device.shortname or device.name, sep=""))
                base_device.C_typename = "IO" + base_device.name
                base_device.source_device = device
                registers_added = []
                for r in device.registers:
                    r_name = r.get_name()
                    if not r_name in registers_added:
                        r_type = reg_type[r_name]
                        reg = Classes.D_Register(base_device, r_name, r_type)
                        reg.offset = None # will be fixed later
                        # source registers for this virtual register
                        # TODO: check for array DEV0.REG0..DEV0.REGn, DEV1.REG0..DEV1.REGn
                        reg.source_registers = filter(lambda r: r.get_name() == r_name, device.registers.sorted("offset"))
                        registers_added.append(r_name)
                        base_device_registers[r_name] = reg

            # init registers list
            for d_name in required_devices:
                dev = Classes.D_Device(Settings.SOC, d_name or device.shortname)
                dev.C_typename = "IO" + Classes.generateID(d_name or device.name, sep="")
                dev.base_device = base_device
                dev.source_device = device
                # registers for this virtual device
                regs = filter(lambda r: r.device == d_name, device.registers.sorted("offset"))
                base_diff = min([r.offset for r in regs]) if len(required_devices) > 1 else 0 
                dev.base = device.registers.base + base_diff
                registers_added = []
                for r in regs:
                    r_name = r.get_name()
                    if not r_name in registers_added:
                        r_type = reg_type[r_name]
                        reg = Classes.D_Register(dev, r_name, r_type)
                        reg.offset = r.offset - base_diff
                        # source registers for this virtual register
                        reg.source_registers = filter(lambda r: r.get_name() == r_name, regs)
                        reg.count = len(reg.source_registers)
                        registers_added.append(r_name)
                        if base_device:
                            base_reg = base_device_registers[r_name]
                            if base_reg.offset == None:
                                base_reg.offset = reg.offset
                            else:
                                assert(base_reg.offset == reg.offset)
                    
def generate_code(data_source, dest_dir):
    sg = SourceGenerator()
    return sg.generate_code(data_source, dest_dir)
