# coding=utf-8

from gi.repository import Gtk
from xml.dom.minidom import Node, NodeList, parse, getDOMImplementation

class ACCESS():
    READ_WRITE = "read-write"
    READ_ONLY = "read-only"
    WRITE_ONLY = "write-only"    

class ACCESS_TYPE():
    BYTE = "byte"
    HALF = "half"
    WORD = "word"

def xmlChildNodesByName(xmlnode, name):
    rc = NodeList() 
    for node in xmlnode.childNodes:
        if node.nodeType == Node.ELEMENT_NODE and \
            (name == "*" or node.tagName == name):
            rc.append(node)
    return rc

def xmlChildNodeByName(xmlnode, name):
    rc = xmlChildNodesByName(xmlnode, name)
    if len(rc) == 1: 
        return rc[0]
    if len(rc) == 0:
        return None
    assert(False)
    return None

def xmlAttr(xmlnode, name, value=None):
    attr = xmlnode.attributes.get(name)
    if attr != None:
        return attr.value
    else:
        return value

def xmlSetAttr(xmlnode, name, value):
    if value != None:
        xmlnode.attributes[name] = unicode(value)

def xmlSetAttrInt(xmlnode, name, value, base=None, size=None, signed=None):
    if value != None:
        xmlnode.attributes[name] = format_num(value, base=base, size=size, signed=signed)

def xmlSetAttrBool(xmlnode, name, value):
    if value != None:
        xmlnode.attributes[name] = 'true' if value else 'false'

def xmlAddNode(xmlnode, name):
    node = xmlnode.ownerDocument.createElement(name)
    xmlnode.appendChild(node)
    return node

def xmlNodeText(xmlnode, name, value=None):
    xmlnode = xmlChildNodeByName(xmlnode, name)
    if xmlnode != None and xmlnode.childNodes.length >= 1:
        result = ""
        for node in xmlnode.childNodes:
            if node.nodeType == Node.TEXT_NODE:
                result += node.data.strip()
            elif node.nodeType == Node.CDATA_SECTION_NODE:
                result += node.data
            else:
                assert not ("unknown node type: %s" % node.nodeType)
            
        return result or None
    else:
        return value

def xmlSetNodeText(xmlnode, name, value, cdata=False):
    if value != None:
        value = unicode(value)
        node = xmlAddNode(xmlnode, name)
        if cdata:
            node.appendChild(xmlnode.ownerDocument.createCDATASection(value))
        else:
            node.appendChild(xmlnode.ownerDocument.createTextNode(value.strip()))

def format_num(value, base=None, size=None, signed=False):
    if isinstance(base, list):
        for v in base:
            if v != None:
                base = v
                break
    if isinstance(size, list):
        for v in size:
            if v != None:
                size = v
                break
    if isinstance(signed, list):
        for v in signed:
            if v != None:
                signed = v
                break
        
    if base == None or base == 10:
        if size != None:
            fmt = "%0" + str(size) + ("d" if signed else "u")
        else:
            fmt = "%" + ("d" if signed else "u")
        return fmt % value
    elif base == 16:
        if size != None:
            size = (size + 3) / 4
            fmt = "0x%0" + str(size) + "X"
        else:
            fmt = "0x%X"
        return fmt % value
    elif base == 8:
        v = oct(value)[1:]
        if size != None:
            size = (size + 2) / 3
            v = v.rjust(size, '0') 
        return "0o" + v        
    elif base == 2:
        v = bin(value)[2:]
        if size != None:
            v = v.rjust(size, '0') 
        return "0b" + v        

    assert not ("unknown number base: %s" % base)
    return str(value)

def xmlInt(v):
    try:
        if '0x' in v or '0X' in v:
            return int(v, 16)
        if '0o' in v or '0O' in v:
            return int(v, 8)
        if '0b' in v or '0B' in v:
            return int(v, 2)
        return int(v)
    except:
        return None

def xmlBool(v):
    if v != None:
        if v.upper() in ["1", "TRUE", "YES"]:
            return True
        if v.upper() in ["0", "FALSE", "NO"]:
            return False
    return None

class SOC_Item(object):
    def __init__(self, parent, xmlnode=None):
        self._parent = parent
        self.model = None
        if xmlnode != None:
            self.load(xmlnode)
        else:
            self.clean()
            
    def get_SOCData(self):
        result = self
        while not isinstance(result, SOCXMLData):
            result = result._parent
        return result
    
    def get_ui_sort_method(self):
        return self.get_SOCData().get_sorting_method(self.__class__.__name__)
        
    def clean(self):
        raise NotImplementedError, "%s.clean() not implemented" % (self.__class__.__name__)

    def load(self, xmlnode):
        raise NotImplementedError, "%s.load() not implemented" % (self.__class__.__name__)
        
    def save(self, xmlnode):
        raise NotImplementedError, "%s.save() not implemented" % (self.__class__.__name__)
    
    def add_to_model(self, model, parent):
        raise NotImplementedError, "%s.add_to_model() not implemented" % (self.__class__.__name__)
    
    def assign(self, other):
        raise NotImplementedError, "%s.assign() not implemented" % (self.__class__.__name__)
    
    def changed(self):
        pass
    
    def clone(self, new_parent=None):
        obj = self.__class__(new_parent if new_parent != None else self._parent)
        obj.assign(self)
        return obj
    
    def get_model_item(self):
        if self.model:
            return self.model[0][self.model[1]]
        return None
    
    def __str__(self):
        return unicode(self)

class SOC_List(SOC_Item, list):
    def sorted(self, sort_type="xml"):
        if sort_type == "ui":
            sort_type = self.get_ui_sort_method()

        fun = self.get_sort_fun(sort_type)
        return sorted(self, fun)
    
    def sort_model(self):
        itm_list = self.get_model_item()
        if itm_list:
            org_sort_fun = self.get_sort_fun(self.get_ui_sort_method())
            if org_sort_fun:
                i = 0
                l = []
                for c in itm_list.iterchildren():
                    l.append((i, c))
                    i += 1
                assert(len(l) == len(self))
                def sort_fun(itm1, itm2):
                    return org_sort_fun(itm1[1][0], itm2[1][0])
                l2 = [x[0] for x in sorted(l, sort_fun)]
                id2pos = {}
                pos2id = {}
                for i in range(len(l)):
                    id2pos[i] = i
                    pos2id[i] = i
                for i in range(len(l)):
                    if pos2id[i] != l2[i]:
                        pos1 = i
                        pos2 = id2pos[l2[i]]
                        id1 = pos2id[i]
                        id2 = l2[i]
                        el1 = l[id1][1].iter
                        el2 = l[id2][1].iter
                        self.model[0].swap(el1, el2)
                        pos2id[pos2] = id1
                        pos2id[pos1] = id2
                        id2pos[id1] = pos2
                        id2pos[id2] = pos1
        
    def resort_model(self):
        self.sort_model()
        for itm in self:
            itm.resort_model()

    def assign(self, other):
        del self[:]
        for itm in other:
            self.append(itm.clone(self))

    def _sort_by_name(self, item1, item2):
        item1 = item1.name.lower() if item1.name != None else ""
        item2 = item2.name.lower() if item2.name != None else ""
        return cmp(item1, item2)  

    def _sort_by_name_neg(self, item1, item2):
        item1 = item1.name.lower() if item1.name != None else ""
        item2 = item2.name.lower() if item2.name != None else ""
        return cmp(item2, item1)  
        
    def get_sort_fun(self, sort_type):
        raise NotImplementedError, "%s.get_sort_fun() not implemented" % (self.__class__.__name__)

class SOC_Type(SOC_Item):
    def clean(self):
        self.name = None
        self.prefix = None
        self.title = None
        self.description = None

    def load(self, xmlnode):
        self.name = xmlAttr(xmlnode, 'name')
        self.title = xmlAttr(xmlnode, 'title')
        self.prefix = xmlAttr(xmlnode, 'prefix')
        self.description = xmlNodeText(xmlnode, 'description')

    def save(self, xmlnode):
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'title', self.title)
        xmlSetAttr(xmlnode, 'prefix', self.prefix)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)

    def assign(self, other):
        self.name = other.name
        self.prefix = other.prefix
        self.title = other.title
        self.description = other.description

    def changed(self):
        self._parent.sort_model()

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])

class SOC_EnumValue(SOC_Item):
    def clean(self):
        self.value = None
        self.name = None
        self.title = None
        self.description = None
        
    def load(self, xmlnode):
        self.value = xmlInt(xmlAttr(xmlnode, 'val'))
        self.name = xmlAttr(xmlnode, 'name')
        self.title = xmlAttr(xmlnode, 'title')
        self.description = xmlNodeText(xmlnode, 'description')
        
    def save(self, xmlnode):
        xmlSetAttrInt(xmlnode, 'val', self.value, base=self._parent.value_base, size=self._parent.value_size)
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'title', self.title)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)
        
    def changed(self):
        self._parent.sort_model()
        
    def resort_model(self):
        pass
    
    def assign(self, other):
        self.value = other.value
        self.name = other.name
        self.title = other.title
        self.description = other.description

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])

    def __unicode__(self):
        if self.value != None:
            value = format_num(self.value, self._parent.value_base, self._parent.value_size)
            return "%s %s" % (value, (self.name or self.title))
        else:
            return "unknown enum value"

class SOC_EnumValueList(SOC_List):
    def clean(self):
        del self[:]
        self.value_base = None
        self.value_size = None
        
    def load(self, xmlnode):
        self.clean()
        self.value_base = xmlInt(xmlAttr(xmlnode, 'value-base'))
        self.value_size = xmlInt(xmlAttr(xmlnode, 'value-size'))
        for n in xmlChildNodesByName(xmlnode, 'value'):
            self.append(SOC_EnumValue(self, n))

    def save(self, xmlnode):
        xmlSetAttrInt(xmlnode, 'value-base', self.value_base, base=10)
        xmlSetAttrInt(xmlnode, 'value-size', self.value_size, base=10)
        for value in self.sorted():
            value_node = xmlAddNode(xmlnode, 'value')
            value.save(value_node)

    def add_to_model(self, model, parent):
        self.model = model, parent
        for value in self.sorted("ui"):
            value.add_to_model(model, self.model[1])

    def assign(self, other):
        SOC_List.assign(self, other)
        self.value_base = other.value_base
        self.value_size = other.value_size

    def get_sort_fun(self, sort_type):
        def sort_by_value(val1, val2):
            return cmp(val1.value, val2.value)  
        def sort_by_value_neg(val1, val2):
            return cmp(val2.value, val1.value)
        
        return {
            "xml": sort_by_value,
            
            "value": sort_by_value,
            "-value": sort_by_value_neg, 
            "name": self._sort_by_name,
            "-name": self._sort_by_name_neg 
        }[sort_type]

class SOC_Enum(SOC_Type):
    def __unicode__(self):
        return 'enum %s' % (self.title or self.name or '')
    
    def clean(self):
        SOC_Type.clean(self)
        self.values = SOC_EnumValueList(self)

    def load(self, xmlnode):
        SOC_Type.load(self, xmlnode)
        self.values = SOC_EnumValueList(self, xmlChildNodeByName(xmlnode, 'values'))

    def save(self, xmlnode):
        SOC_Type.save(self, xmlnode)
        if self.values:
            values_node = xmlAddNode(xmlnode, 'values')
            self.values.save(values_node)

    def resort_model(self):
        self.values.resort_model()

    def assign(self, other):
        SOC_Type.assign(self, other)
        self.values = other.values.clone(self._parent)

    def add_to_model(self, model, parent):
        SOC_Type.add_to_model(self, model, parent)
        self.values.add_to_model(model, self.model[1])

class SOC_TypeList(SOC_List):
    def clean(self):
        del self[:]
    
    def load(self, xmlnode):
        self.clean()
        for n in xmlChildNodesByName(xmlnode, '*'):
            if n.nodeName == 'enum':
                self.append(SOC_Enum(self, n))
            else:
                assert not ("unknown type: %s" % n.nodeName)
        
    def save(self, xmlnode):
        for t in self.sorted():
            if isinstance(t, SOC_Enum):
                type_node = xmlAddNode(xmlnode, 'enum')
            else:
                assert not ("unsupported type: %s" % t)
            
            t.save(type_node)
        
    def get_sort_fun(self, sort_type):
        return {
            "xml": self._sort_by_name,
            
            "name": self._sort_by_name,
            "-name": self._sort_by_name_neg 
        }[sort_type]

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        for tp in self.sorted("ui"):
            tp.add_to_model(model, self.model[1])

    def __unicode__(self):
        return 'Types'

class SOC_RegisterField(SOC_Item):
    def clean(self):
        self.union = None
        self.name = None
        self.title = None
        self.description = None
        self.bit = None
        self.size = None
        self.type = None
        self.read_as_value = None
        self.write_as_value = None
        self.should_be_value = None
        self.must_be_value = None
        self.recommended_value = None
        self.access = None
        self.read_undefined = None
        self.hardware_set = None
        
    def load(self, xmlnode):
        self.clean()
        self.union = xmlAttr(xmlnode, 'union')
        self.name = xmlAttr(xmlnode, 'name')
        self.title = xmlAttr(xmlnode, 'title')
        self.description = xmlNodeText(xmlnode, 'description')
        self.bit = xmlInt(xmlAttr(xmlnode, 'bit'))
        self.size = xmlInt(xmlAttr(xmlnode, 'size'))
        self.type = xmlAttr(xmlnode, 'type')
        self.read_as_value = xmlInt(xmlAttr(xmlnode, 'read-as-value'))
        self.write_as_value = xmlInt(xmlAttr(xmlnode, 'write-as-value'))
        self.should_be_value = xmlInt(xmlAttr(xmlnode, 'should-be-value'))
        self.must_be_value = xmlInt(xmlAttr(xmlnode, 'must-be-value'))
        self.recommended_value = xmlInt(xmlAttr(xmlnode, 'recommended-value'))
        self.access = xmlAttr(xmlnode, 'access')
        self.read_undefined = xmlBool(xmlAttr(xmlnode, 'read-undefined'))
        self.hardware_set = xmlBool(xmlAttr(xmlnode, 'hardware-set'))

    def save(self, xmlnode):
        xmlSetAttr(xmlnode, 'union', self.union)
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'title', self.title)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)
        xmlSetAttrInt(xmlnode, 'bit', self.bit, base=10)
        xmlSetAttrInt(xmlnode, 'size', self.size, base=10)
        xmlSetAttr(xmlnode, 'type', (self.type or "uint") if self.name else None)
        # TODO: get base from type value
        base = 16
        xmlSetAttrInt(xmlnode, 'read-as-value', self.read_as_value, base=base, size=self.size)
        xmlSetAttrInt(xmlnode, 'write-as-value', self.write_as_value, base=base, size=self.size)
        xmlSetAttrInt(xmlnode, 'should-be-value', self.should_be_value, base=base, size=self.size)
        xmlSetAttrInt(xmlnode, 'must-be-value', self.must_be_value, base=base, size=self.size)
        xmlSetAttrInt(xmlnode, 'recommended-value', self.recommended_value, base=base, size=self.size)
        xmlSetAttr(xmlnode, 'access', self.access)
        xmlSetAttrBool(xmlnode, 'read-undefined', self.read_undefined)
        xmlSetAttrBool(xmlnode, 'hardware-set', self.hardware_set)

    def changed(self):
        self._parent.sort_model()

    def assign(self, other):
        self.union = other.union
        self.name = other.name
        self.title = other.title
        self.description = other.description
        self.bit = other.bit
        self.size = other.size
        self.type = other.type
        self.read_as_value = other.read_as_value
        self.write_as_value = other.write_as_value
        self.should_be_value = other.should_be_value
        self.must_be_value = other.must_be_value
        self.recommended_value = other.recommended_value
        self.access = other.access
        self.read_undefined = other.read_undefined
        self.hardware_set = other.hardware_set

    def resort_model(self):
        pass

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])

    def get_access(self):
        return self.access or self._parent._parent.access or self._parent._parent._parent.access 

    def __unicode__(self):
        if self.union != None:
            u = "%s." % self.union
        else:
            u = ""
        if self.size > 1:
            if self._parent.get_ui_sort_method() == "-bit":
                s = "%s:%s" % (self.bit + self.size - 1, self.bit)
            else:
                s = "%s:%s" % (self.bit, self.bit + self.size - 1)
        else:
            s = "%s" % self.bit
        return "%s%s [%s]" % (u, self.name or 'reserved', s)

class SOC_RegisterFieldList(SOC_List):
    def clean(self):
        del self[:]
        
    def load(self, xmlnode):
        self.clean()
        for n in xmlChildNodesByName(xmlnode, 'field'):
            self.append(SOC_RegisterField(self, n))
        
    def save(self, xmlnode):
        for field in self.sorted():
            field_node = xmlAddNode(xmlnode, 'field')
            field.save(field_node)

    def get_sort_fun(self, sort_type):
        def cmp_union(field1, field2):
            if field1.union != None or field2.union != None:
                s1 = field1.union or "" 
                s2 = field2.union or ""
                return cmp(s1, s2)
            return 0 
        def sort_by_bit(field1, field2):
            return cmp_union(field1, field2) or cmp(field1.bit, field2.bit)
        def sort_by_bit_neg(field1, field2):
            return cmp_union(field1, field2) or cmp(field2.bit, field1.bit)
        def sort_by_name(field1, field2):
            return cmp_union(field1, field2) or self._sort_by_name(field1, field2)
        def sort_by_name_neg(field1, field2):
            return cmp_union(field1, field2) or self._sort_by_name_neg(field1, field2)
                  
        
        return {
            "xml": sort_by_bit,
            
            "bit": sort_by_bit,
            "-bit": sort_by_bit_neg, 
            "name": sort_by_name,
            "-name": sort_by_name_neg 
        }[sort_type]

    def add_to_model(self, model, parent):
        self.model = model, parent
        for field in self.sorted("ui"):
            field.add_to_model(model, self.model[1])

    def __unicode__(self):
        return 'Fields'

class SOC_Register(SOC_Item):
    def clean(self):
        self.name = None
        self.title = None
        self.description = None
        self.offset = None
        self.reset_value = None
        self.access = None
        self.access_type = None
        self.size = None
        self.device = None
        self.register_name = None
        self.type_name = None
        self.fields = SOC_RegisterFieldList(self)

    def load(self, xmlnode):
        self.clean()
        self.name = xmlAttr(xmlnode, 'name')
        self.title = xmlAttr(xmlnode, 'title')
        self.description = xmlNodeText(xmlnode, 'description')
        self.offset = xmlInt(xmlAttr(xmlnode, 'offset'))
        self.reset_value = xmlInt(xmlAttr(xmlnode, 'reset-value'))
        self.access = xmlAttr(xmlnode, 'access')
        self.access_type = xmlAttr(xmlnode, 'access-type')
        self.size = xmlInt(xmlAttr(xmlnode, 'size'))
        self.device = xmlAttr(xmlnode, 'device')
        self.register_name = xmlAttr(xmlnode, 'register-name')
        self.type_name = xmlAttr(xmlnode, 'type')
        self.fields = SOC_RegisterFieldList(self, xmlChildNodeByName(xmlnode, 'fields'))
        
    def save(self, xmlnode):
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'title', self.title)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)
        xmlSetAttrInt(xmlnode, 'offset', self.offset, base=16, size=16)
        xmlSetAttrInt(xmlnode, 'reset-value', self.reset_value, base=16, size=self.size or self._parent.size)
        xmlSetAttr(xmlnode, 'access', self.access)
        xmlSetAttr(xmlnode, 'access-type', self.access_type)
        xmlSetAttrInt(xmlnode, 'size', self.size, base=10)
        xmlSetAttr(xmlnode, 'device', self.device)
        xmlSetAttr(xmlnode, 'register-name', self.register_name)
        xmlSetAttr(xmlnode, 'type', self.type_name)
        if self.fields:
            fields_node = xmlAddNode(xmlnode, 'fields')
            self.fields.save(fields_node)
        
    def changed(self):
        self._parent.sort_model()

    def resort_model(self):
        self.fields.resort_model()

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        self.fields.add_to_model(model, self.model[1])

    def assign(self, other):
        self.name = other.name
        self.title = other.title
        self.description = other.description
        self.offset = other.offset
        self.reset_value = other.reset_value
        self.access = other.access
        self.access_type = other.access_type
        self.size = other.size
        self.device = other.device
        self.register_name = other.register_name
        self.type_name = other.type_name
        self.fields = other.fields.clone(self)

    def __unicode__(self):
        if self.offset != None:
            o = "[%s] " % format_num(self.offset, base=16, size=16)
        else:
            o = ""
        if self.device:
            name = self.register_name or self.name
            o += "%s." % self.device
        else:
            name = self.name 
        return "%s%s" % (o, name or 'unnamed register')
    
    def get_name(self):
        return self.register_name or self.name
        
    def get_type(self):
        return self.type_name or self.get_name()
    
    def get_size(self):
        return self.size or self._parent.size
    
    def get_access_type(self):
        at = []
        if self.access_type:
            at = self.access_type.split(';')
        elif self._parent.access_type:
            at = self._parent.access_type.split(';')
        return list(set(at))
    
    def get_access(self):
        a = ""
        if self.access:
            a = self.access
        elif self._parent.access:
            a = self._parent.access
        if a == "read-write":
            return ["read", "write"]
        if a == "read-only":
            return ["read"]
        if a == "write-only":
            return ["write"]
        return []
    
    def get_flags(self):
        # FIXME: calculate for multiple unions does not work properly
        # get access masks, types for all fields
        def get_mask(field):
            return ((2 ** field.size) - 1) << field.bit
        def get_value(field, value):
            return value << field.bit
        
        result = {
            'read_mask': 0,
            'write_mask': 0,
            'reset_value': self.reset_value,
            'reset_mask': 0,

            'read_as_mask': 0,
            'read_as_value': 0,
            'write_as_mask': 0,
            'write_as_value': 0,
            'should_be_mask': 0,
            'should_be_value': 0,
            'must_be_mask': 0,
            'must_be_value': 0,   
            'recommended_mask': 0,
            'recommended_value': 0,
            
            'reserved_mask': 0,
            'defined_mask': 0,
            'read_undefined_mask': 0,
            'hardware_set_mask': 0
        }

        for field in self.fields:
            mask = get_mask(field)
            if field.name == None:
                result['reserved_mask'] |= mask
            else:
                result['defined_mask'] |= mask
            # read-write access
            acc = field.get_access()
            if acc == 'read-write':
                result['read_mask'] |= mask
                result['write_mask'] |= mask
            elif acc == 'read-only':
                result['read_mask'] |= mask
            elif acc == 'write-only':
                result['write_mask'] |= mask
            else:
                print "Unknown read access type: %s" % acc
                assert(False)
            # other defined fields
            if field.read_as_value != None:
                result['read_as_mask'] |= mask
                result['read_as_value'] |= get_value(field, field.read_as_value)
            if field.write_as_value != None:
                result['write_as_mask'] |= mask
                result['write_as_value'] |= get_value(field, field.write_as_value)
            if field.should_be_value != None:
                result['should_be_mask'] |= mask
                result['should_be_value'] |= get_value(field, field.should_be_value)
            if field.must_be_value != None:
                result['must_be_mask'] |= mask
                result['must_be_value'] |= get_value(field, field.must_be_value)
            if field.recommended_value != None:
                result['recommended_mask'] |= mask
                result['recommended_value'] |= get_value(field, field.recommended_value)
            if field.read_undefined:
                result['read_undefined_mask'] |= mask
            if field.hardware_set:
                result['hardware_set_mask'] |= mask
            if not field.hardware_set and not field.read_undefined:
                result['reset_mask'] |= mask

        return result
            

class SOC_RegisterList(SOC_List):
    def clean(self):
        del self[:]
        self.base = None
        self.access = None
        self.access_type = None
        self.size = None

    def load(self, xmlnode):
        self.clean()
        self.base = xmlInt(xmlAttr(xmlnode, 'base'))
        self.access = xmlAttr(xmlnode, 'access')
        self.access_type = xmlAttr(xmlnode, 'access-type')
        self.size = xmlInt(xmlAttr(xmlnode, 'size'))
        for n in xmlChildNodesByName(xmlnode, 'register'):
            self.append(SOC_Register(self, n))
        
    def save(self, xmlnode):
        xmlSetAttrInt(xmlnode, 'base', self.base, base=16, size=32)
        xmlSetAttr(xmlnode, 'access', self.access)
        xmlSetAttr(xmlnode, 'access-type', self.access_type)
        xmlSetAttrInt(xmlnode, 'size', self.size, base=10)
        for reg in self.sorted():
            register_node = xmlAddNode(xmlnode, 'register')
            reg.save(register_node)
        
    def get_sort_fun(self, sort_type):
        def sort_by_offset(reg1, reg2):
            return cmp(reg1.offset, reg2.offset)  
        def sort_by_offset_neg(reg1, reg2):
            return cmp(reg2.offset, reg1.nameoffset)  
        
        return {
            "xml": sort_by_offset,
            
            "offset": sort_by_offset,
            "-offset": sort_by_offset_neg, 
            "name": self._sort_by_name,
            "-name": self._sort_by_name_neg 
        }[sort_type]

    def changed(self):
        self._parent._parent.sort_model()
        
    def assign(self, other):
        SOC_List.assign(self, other)
        self.base = other.base
        self.access = other.access
        self.access_type = other.access_type
        self.size = other.size

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        for reg in self.sorted("ui"):
            reg.add_to_model(model, self.model[1])

    def __unicode__(self):
        if self.base != None:
            s = " (0x%08X)" % self.base
        else:
            s = ""
        return "Registers%s" % s 

class SOC_Device(SOC_Item):
    def clean(self):
        self.name = None
        self.shortname = None
        self.description = None
        self.registers = SOC_RegisterList(self)
        self.types = SOC_TypeList(self)
        
    def load(self, xmlnode):
        self.clean()
        self.name = xmlAttr(xmlnode, 'name')
        self.shortname = xmlAttr(xmlnode, 'short-name')
        self.description = xmlNodeText(xmlnode, 'description')
        self.registers = SOC_RegisterList(self, xmlChildNodeByName(xmlnode, 'registers'))
        self.types = SOC_TypeList(self, xmlChildNodeByName(xmlnode, 'types'))

    def save(self, xmlnode):
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'short-name', self.shortname)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)
        if self.types:
            types_node = xmlAddNode(xmlnode, 'types')
            self.types.save(types_node)
        registers_node = xmlAddNode(xmlnode, 'registers')
        self.registers.save(registers_node)

    def changed(self):
        self._parent.sort_model()
        
    def assign(self, other):
        self.name = other.name
        self.shortname = other.shortname
        self.description = other.description
        self.registers = other.registers.clone(self)
        self.types = other.types.clone(self)

    def resort_model(self):
        self.registers.resort_model()
        self.types.resort_model()

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        self.registers.add_to_model(model, self.model[1])
        self.types.add_to_model(model, self.model[1])

    def __unicode__(self):
        return self.name or self.shortname or 'unknown device'

class SOC_DeviceList(SOC_List):
    def clean(self):
        del self[:]
        self.types = SOC_TypeList(self)
        
    def load(self, xmlnode):
        self.clean()
        devices = xmlChildNodesByName(xmlnode, 'device')
        for device in devices:
            self.append(SOC_Device(self, device))
        self.types = SOC_TypeList(self, xmlChildNodeByName(xmlnode, 'types'))
         
    def save(self, xmlnode):
        if self.types:
            types_node = xmlAddNode(xmlnode, 'types')
            self.types.save(types_node)
        for device in self.sorted():
            device_node = xmlAddNode(xmlnode, 'device')
            device.save(device_node)
         
    def get_sort_fun(self, sort_type):
        def sort_by_base(dev1, dev2):
            return cmp(dev1.registers.base, dev2.registers.base)  
        def sort_by_base_neg(dev1, dev2):
            return cmp(dev2.registers.base, dev1.registers.base)  
        
        return {
            "xml": sort_by_base,
            
            "base": sort_by_base,
            "-base": sort_by_base_neg, 
            "name": self._sort_by_name,
            "-name": self._sort_by_name_neg 
        }[sort_type]

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        for device in self.sorted("ui"):
            device.add_to_model(model, self.model[1])

    def __unicode__(self):
        return "Devices"

class SOC(SOC_Item):
    def clean(self):
        self.name = None
        self.title = None
        self.description = None
        self.devices = SOC_DeviceList(self)
    
    def load(self, xmlnode):
        self.clean()
        self.name = xmlAttr(xmlnode, 'name')
        self.title = xmlAttr(xmlnode, 'title')
        self.description = xmlNodeText(xmlnode, 'description')
        self.devices = SOC_DeviceList(self, xmlChildNodeByName(xmlnode, 'devices'))
        
    def save(self, xmlnode):
        xmlSetAttr(xmlnode, 'name', self.name)
        xmlSetAttr(xmlnode, 'title', self.title)
        xmlSetNodeText(xmlnode, 'description', self.description, cdata=True)
        if self.devices:
            devices_node = xmlAddNode(xmlnode, 'devices')
            self.devices.save(devices_node)
        
    def resort_model(self):
        self.devices.resort_model()

    def add_to_model(self, model, parent):
        self.model = model, model.append(parent, [self])
        self.devices.add_to_model(model, self.model[1])
        self.devices.types.add_to_model(model, self.model[1])

    def __unicode__(self):
        return self.name

class SOCXMLData():
    def __init__(self):
        self.sorting = {}
    
    def load_from_file(self, filename):
        tree = parse(filename)
        root = xmlChildNodeByName(tree, 'soc')
        self.model = None
        self.SOC = SOC(self, root)
    
    def save_to_file(self, filename):
        xmlDoc = getDOMImplementation().createDocument(None, "soc", None)
        xmlRoot = xmlDoc.documentElement
        xmlRoot.attributes['version'] = '1.0'
        self.SOC.save(xmlRoot)
        f = open(filename, "wb")
        f.write(xmlDoc.toprettyxml(encoding = "utf-8"))
        f.close()
    
    def get_sorting_method(self, elem_type):
        return self.sorting.get(elem_type, "xml")

    def get_tree_model(self):
        if not self.model:
            def get_types(parent, types):
                for t in types.sorted("ui"):
                    t.model = (self.model, self.model.append(parent[1], [t]))
                    if isinstance(t, SOC_Enum):
                        t.values.model = t.model
                        for v in t.values.sorted("ui"):
                            v.model = (self.model, self.model.append(t.model[1], [v]))
                    else:
                        assert not ("unknown type: %s" % t.__class__.__name__)
                
            self.model = Gtk.TreeStore(object)
            self.SOC.add_to_model(self.model, None)

        return self.model
    
    def sort_tree_model(self):
        self.SOC.resort_model()
    
class SOCTreeRenderer(Gtk.CellRendererText):
    def init_column(self, column):
        def gettext_func(column, cell, model, it, func_data):
            val = model.get_value(it, 0)
            val = unicode(val).encode('utf-8')            
            cell.set_property("text", val)
        column.set_cell_data_func(self, gettext_func)
