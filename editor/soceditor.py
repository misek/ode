#!/usr/bin/env python
# coding=utf-8

from gi.repository import Gtk
from socdata import SOCXMLData, SOCTreeRenderer, format_num, SOC_Device,\
    SOC_Register, SOC_RegisterField, SOC_Enum, SOC_EnumValue
import sys

class WindowBase(object):
    def __init__(self, builder, wndName):
        self.builder = builder
        self.wnd = builder.get_object(wndName)

    def show(self):
        self.wnd.show_all()

CLASS_to_TAB = {
    "SOC": 1,
    "SOC_DeviceList": 2,
    "SOC_Device": 3,
    "SOC_TypeList": 4,
    "SOC_Enum": 5,    
    "SOC_RegisterList": 6,
    "SOC_Register": 7,    
    "SOC_RegisterField": 8,    
    "SOC_EnumValue": 9,    
}

class WindowMain(WindowBase):
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("ui.glade")
        super(WindowMain, self).__init__(builder, "wndMain")
        self.wnd.connect("delete-event", Gtk.main_quit)
        
        self.apply_button = self.builder.get_object("apply_button")
        self.add_new_button = self.builder.get_object("btn_add_new")
        self.clone_button = self.builder.get_object("btn_clone")
        self.save_button = self.builder.get_object("tool_btn_save")
        self.save_as_button = self.builder.get_object("tool_btn_save_as")
        # tree
        self.current_item = None
        self.tree = self.builder.get_object("tree")
        self.notebook = self.builder.get_object("notebook")
        self.notebook.set_show_tabs(False)
        
        def add_text_renderer(cb):
            if isinstance(cb, basestring):
                cb = self.builder.get_object("cb_" + cb)
            renderer_text = Gtk.CellRendererText()
            cb.pack_start(renderer_text, True)
            cb.add_attribute(renderer_text, "text", 1)
        
        # register properties pages
        def add_access(cb_name):
            cb = self.builder.get_object("cb_" + cb_name)
            ls = Gtk.ListStore(object, str)
            items = [(None, '--------'),
                     ('read-write', 'read-write'),
                     ('read-only', 'read only'),
                     ('write-only', 'write only')]
            for i in items:
                ls.append([i[0], i[1]])
            add_text_renderer(cb)
            cb.set_model(ls)
        def add_access_type(cb_name):
            cb = self.builder.get_object("cb_" + cb_name)
            ls = Gtk.ListStore(object, str)
            items = [(None, '--------'),
                     ('byte', 'byte'),
                     ('byte;half', 'byte, half-word'),
                     ('byte;word', 'byte, word'),
                     ('half', 'half-word'),
                     ('half;word', 'half-word, word'),
                     ('word', 'word')]
            for i in items:
                ls.append([i[0], i[1]])
            add_text_renderer(cb)
            cb.set_model(ls)

        add_access("SOC_RegisterList_Access")
        add_access("SOC_Register_Access")
        add_access("SOC_RegisterField_Access")
        add_access_type("SOC_RegisterList_AccessType")
        add_access_type("SOC_Register_AccessType")
        
        add_text_renderer("SOC_RegisterField_Type")
        
        self.builder.connect_signals(self)
        self.soc_data = None
        self.set_new_command(None)
        self.set_clone_command(None)
        self.file_name = None
        self.init_tree()
        
        if len(sys.argv) == 2:
            self.load_data(sys.argv[1])
        
    def init_tree(self):
        renderer = SOCTreeRenderer()
        column = Gtk.TreeViewColumn("Element", renderer)
        column.set_min_width(260)
        column.set_max_width(260)
        renderer.init_column(column)
        self.tree.append_column(column)
        sel = self.tree.get_selection() 
        sel.connect("changed", self.tree_selection_changed)
        sel.set_select_function(self.before_tree_selection_change, None)

    def before_tree_selection_change(self, sel, model, path, selected, data):
        if self.apply_button.get_sensitive():
            dialog = Gtk.MessageDialog(self.wnd, Gtk.DialogFlags.MODAL,
                                       Gtk.MessageType.QUESTION, 
                                       Gtk.ButtonsType.OK_CANCEL,
                                       "Data has been changed.")
            dialog.format_secondary_text("New data will be lost. Continue?")
            response = dialog.run()
            dialog.destroy()
            if response == Gtk.ResponseType.OK:
                self.apply_button.set_sensitive(False)
                return True

            return False
        else:
            return True

    def main_wnd_show(self, wnd):
        self.set_new_command(None)
        self.set_clone_command(None)

    def log_error(self, err):
        dialog = Gtk.MessageDialog(self.wnd, Gtk.DialogFlags.MODAL,
                                   Gtk.MessageType.ERROR, 
                                   Gtk.ButtonsType.OK,
                                   "ERROR")
        dialog.format_secondary_text(err)
        dialog.run()
        dialog.destroy()

    def init_types(self, cb_name, current, field):
        cb = self.builder.get_object("cb_" + cb_name)
        ls = Gtk.ListStore(object, str)
        registers = field._parent._parent._parent._parent
        items = [("uint", "unsigned integer"),
                 ("int", "signed integer")]
        for t in registers._parent.types:
            items.append((t.name, t.title or t.name))
        for t in registers.types:
            items.append((t.name, t.title or t.name))
        for i in items:
            ls.append((i[0], i[1]))
        cb.set_model(ls)
        for i in range(len(items)):
            if items[i][0] == current:
                cb.set_active(i)
                return
        # defaults to unsigned integer
        if current:
            self.log_error("Unknown type: %s" % current)
        cb.set_active(0)

    def tree_selection_changed(self, sel):
        store, itr = sel.get_selected()
        if itr:
            self.current_item = store.get_value(itr, 0)
            cls = self.current_item.__class__.__name__
            tab = CLASS_to_TAB.get(cls, None)
            if tab != None:
                self.notebook.set_current_page(tab)
                getattr(self, "load_" + cls)(self.current_item) 
            else:
                self.notebook.set_current_page(0)
                print "Tab not found for class: %s" % cls
        else:
            self.notebook.set_current_page(0)
        self.apply_button.set_sensitive(False)
            
    def save_page_data(self, button):
        cls = self.current_item.__class__.__name__
        if getattr(self, "save_" + cls)(self.current_item):
            self.current_item.changed()
            getattr(self, "load_" + cls)(self.current_item)
            self.apply_button.set_sensitive(False)
            self.save_button.set_sensitive(True)

        self.tree.queue_draw()
        
    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("SOC definition files")
        filter_text.add_pattern("*.xml")
        dialog.add_filter(filter_text)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)
        
    def item_data_changed(self, *args):
        self.apply_button.set_sensitive(True)
        
    def clear_data(self):
        pass
    
    def load_data(self, filename):
        self.soc_data = SOCXMLData()
        self.soc_data.load_from_file(filename)
        self.soc_data_model = self.soc_data.get_tree_model()
        model = self.soc_data_model
        self.tree.set_model(model)
        self.tree.expand_row(model[0].path, False)
        for i in model[0].iterchildren():
            self.tree.expand_row(i.path, False)
        self.file_name = filename
        self.resort_tree(None)
        self.save_as_button.set_sensitive(True)
        self.set_new_command(None)
        self.set_clone_command(None)
        
    def save_data(self, filename):
        self.file_name = filename
        self.soc_data.save_to_file(self.file_name)
        self.save_button.set_sensitive(False)
        
    def file_open(self, button):
        dlg = Gtk.FileChooserDialog("Select SOC file definition", self.wnd, 
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_filters(dlg)
        
        response = dlg.run()
        fn = dlg.get_filename()
        dlg.destroy()
        
        if response == Gtk.ResponseType.OK:
            self.load_data(fn)
        
    def set_new_command(self, name, cmd=None):
        if name:
            self.new_command = cmd
            self.add_new_button.set_label("New %s" % name)
            self.add_new_button.set_visible(True)
        else:
            self.add_new_button.set_visible(False)
            
    def set_clone_command(self, name, cmd=None):
        if name:
            self.clone_command = cmd
            self.clone_button.set_label("Clone %s" % name)
            self.clone_button.set_visible(True)
        else:
            self.clone_button.set_visible(False)
            
    def add_new_element(self, button):
        new_type, parent = self.new_command
        model, it = parent.model
        obj = new_type(parent)
        parent.append(obj)
        obj.add_to_model(model, it)
        obj.changed()
        self.select_tree_element(it, True)
        self.select_tree_element(obj.model[1], True)
        self.save_button.set_sensitive(True)
             
    def clone_element(self, button):
        source = self.clone_command
        model, it = source._parent.model
        obj = source.clone()
        obj._parent.append(obj)
        obj.add_to_model(model, it)
        obj.changed()
        self.select_tree_element(it, True)
        self.select_tree_element(obj.model[1], True)
        self.save_button.set_sensitive(True)
             
    def select_tree_element(self, it, expand=False):
        sel = self.tree.get_selection()
        sel.select_iter(it)
        path = self.tree.get_model().get_path(it)
        self.tree.scroll_to_cell(path)
        self.tree.set_cursor(path)
        if expand:
            self.tree.expand_row(path, False)

    def file_save(self, button):
        self.save_data(self.file_name)
        
    def file_save_as(self, button):
        dlg = Gtk.FileChooserDialog("Select SOC file definition", self.wnd, 
            Gtk.FileChooserAction.SAVE,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        self.add_filters(dlg)
        
        response = dlg.run()
        fn = dlg.get_filename()
        dlg.destroy()
        
        if response == Gtk.ResponseType.OK:
            self.save_data(fn)
            
    def resort_tree(self, radio):
        if self.soc_data:
            if self.builder.get_object('registers_by_name').get_active():
                self.soc_data.sorting['SOC_RegisterList'] = 'name'
            if self.builder.get_object('registers_by_offset').get_active():
                self.soc_data.sorting['SOC_RegisterList'] = 'offset'
                
            if self.builder.get_object('devices_by_base').get_active():
                self.soc_data.sorting['SOC_DeviceList'] = 'base'
            if self.builder.get_object('devices_by_name').get_active():
                self.soc_data.sorting['SOC_DeviceList'] = 'name'

            if self.builder.get_object('register_fields_asc').get_active():
                self.soc_data.sorting['SOC_RegisterFieldList'] = 'bit'
            if self.builder.get_object('register_fields_desc').get_active():
                self.soc_data.sorting['SOC_RegisterFieldList'] = '-bit'
                
            self.soc_data.sort_tree_model()
            
        self.tree.queue_draw()
        
    # save & load routines
    def load_text(self, ctrl, v):
        self.builder.get_object('text_' + ctrl).set_text(v or '')
        
    def save_text(self, ctrl):
        txt = self.builder.get_object('text_' + ctrl).get_text()
        return txt or None

    def load_int(self, ctrl, v, inherited_v=None, base=None, size=None):
        if v == None:
            if inherited_v == None:
                s = ""
            else:
                s = "%s (inherited)" % format_num(inherited_v, base=base, size=size)
        else:
            s = format_num(v, base=base, size=size)
        self.builder.get_object('text_' + ctrl).set_text(s)

    def save_int(self, ctrl):
        txt = self.builder.get_object('text_' + ctrl).get_text()
        if not txt or "(inherited)" in txt:
            return None
        
        if "0x" in txt or "0X" in txt:
            return int(txt, 16)
        if "0o" in txt or "0O" in txt:
            return int(txt, 8)
        if "0b" in txt or "0B" in txt:
            return int(txt, 2)
        return int(txt)

    def load_bool(self, ctrl, v):
        ctrl = self.builder.get_object('check_' + ctrl)
        ctrl.set_active(bool(v))

    def save_bool(self, ctrl, default):
        ctrl = self.builder.get_object('check_' + ctrl)
        v = ctrl.get_active() 
        if v == default:
            return None
        else:
            return v

    def load_enum(self, ctrl, v, inherited_v=None):
        ctrl = self.builder.get_object('cb_' + ctrl)
        if v == None:
            ctrl.get_model()[0][1] = "--------"
            if inherited_v != None:
                if not isinstance(inherited_v, list):
                    inherited_v = [inherited_v]
                for inh_v in inherited_v:
                    if inh_v == None:
                        continue
                    sel = None
                    for i in ctrl.get_model():
                        if i[0] == inh_v:
                            sel = i
                            break
                    if sel != None:
                        ctrl.get_model()[0][1] = "%s (inherited)" % sel[1]
                        break
                
            ctrl.set_active(0)
            return True
        else:
            for i in ctrl.get_model():
                if i[0] == v:
                    ctrl.set_active_iter(i.iter)
                    return True
            
        ctrl.set_active(0)
        assert not ("invalid value: %s" % v)

    def save_enum(self, ctrl):
        ctrl = self.builder.get_object('cb_' + ctrl)
        it = ctrl.get_active_iter()
        item = ctrl.get_model()[it]
        return item[0]

    def load_set(self, ctrl, v, inherited_v=None):
        return self.load_enum(ctrl, v, inherited_v)

    def save_set(self, ctrl):
        return self.save_enum(ctrl)

    def load_buffer(self, ctrl, v):
        buf = Gtk.TextBuffer()
        buf.insert_at_cursor(v or '')
        buf.connect("changed", self.item_data_changed)
        self.builder.get_object('text_' + ctrl).set_buffer(buf)
    
    def save_buffer(self, ctrl):
        buf = self.builder.get_object('text_' + ctrl).get_buffer()
        return buf.get_text(buf.get_start_iter(), buf.get_end_iter(), True) or None
    
    def load_SOC(self, soc):
        self.load_text('SOC_Name', soc.name)
        self.load_text('SOC_Title', soc.title)
        self.load_buffer('SOC_Description', soc.description)
        self.set_new_command(None)
        self.set_clone_command(None)
        return True
        
    def save_SOC(self, soc):
        soc.name = self.save_text('SOC_Name')
        soc.title = self.save_text('SOC_Title')
        soc.description = self.save_buffer('SOC_Description')
        return True
    
    def load_SOC_Device(self, dev):
        self.load_text('SOC_Device_Name', dev.name)
        self.load_text('SOC_Device_ShortName', dev.shortname)
        self.load_buffer('SOC_Device_Description', dev.description)
        self.set_new_command("device", (SOC_Device, dev._parent))
        self.set_clone_command("device", dev)
        return True
        
    def save_SOC_Device(self, dev):
        dev.name = self.save_text('SOC_Device_Name')
        dev.shortname = self.save_text('SOC_Device_ShortName')
        dev.description = self.save_buffer('SOC_Device_Description')
        return True

    def load_SOC_DeviceList(self, devices):
        self.set_new_command("device", (SOC_Device, devices))
        self.set_clone_command(None)
        return True
        
    def save_SOC_DeviceList(self, devices):
        return True
        
    def load_SOC_TypeList(self, types):
        self.set_new_command("enum", (SOC_Enum, types))
        self.set_clone_command(None)
        return True
        
    def save_SOC_TypeList(self, types):
        return True

    def load_SOC_RegisterList(self, rl):
        self.load_int('SOC_RegisterList_Size', rl.size)
        self.load_int('SOC_RegisterList_Base', rl.base, base=16, size=32)
        self.load_set('SOC_RegisterList_AccessType', rl.access_type)
        self.load_enum('SOC_RegisterList_Access', rl.access)
        self.set_new_command("register", (SOC_Register, rl))
        self.set_clone_command(None)
        return True

    def save_SOC_RegisterList(self, rl):
        rl.size = self.save_int('SOC_RegisterList_Size')
        rl.base = self.save_int('SOC_RegisterList_Base')
        rl.access_type = self.save_set('SOC_RegisterList_AccessType')
        rl.access = self.save_enum('SOC_RegisterList_Access')
        return True

    def load_SOC_Register(self, reg):
        self.load_text('SOC_Register_Name', reg.name)
        self.load_text('SOC_Register_Title', reg.title)
        self.load_int('SOC_Register_Offset', reg.offset, base=16, size=16)
        self.load_int('SOC_Register_Size', reg.size, reg._parent.size)
        self.load_int('SOC_Register_ResetValue', reg.reset_value, base=16, size=[reg.size, reg._parent.size, 32])
        self.load_set('SOC_Register_AccessType', reg.access_type, reg._parent.access_type)
        self.load_enum('SOC_Register_Access', reg.access, reg._parent.access)
        self.load_buffer('SOC_Register_Description', reg.description)
        self.load_text('SOC_Register_Device', reg.device)
        self.load_text('SOC_Register_RegisterName', reg.register_name)
        self.load_text('SOC_Register_Type', reg.type_name)
        if reg.fields:
            self.set_new_command("register", (SOC_Register, reg._parent))
        else:
            self.set_new_command("field", (SOC_RegisterField, reg.fields))
        self.set_clone_command("register", reg)
        return True

    def save_SOC_Register(self, reg):
        reg.name = self.save_text('SOC_Register_Name')
        reg.title = self.save_text('SOC_Register_Title')
        reg.offset = self.save_int('SOC_Register_Offset')
        reg.size = self.save_int('SOC_Register_Size')
        reg.reset_value = self.save_int('SOC_Register_ResetValue')
        reg.access_type = self.save_set('SOC_Register_AccessType')
        reg.access = self.save_enum('SOC_Register_Access')
        reg.description = self.save_buffer('SOC_Register_Description')
        reg.device = self.save_text('SOC_Register_Device')
        reg.register_name = self.save_text('SOC_Register_RegisterName')
        reg.type_name = self.save_text('SOC_Register_Type')
        return True

    def load_SOC_RegisterField(self, field):
        self.load_text('SOC_RegisterField_Name', field.name)
        self.load_text('SOC_RegisterField_Title', field.title)
        self.load_text('SOC_RegisterField_Union', field.union)
        self.load_int('SOC_RegisterField_Bit', field.bit)
        self.load_int('SOC_RegisterField_Size', field.size)
        self.load_enum('SOC_RegisterField_Access', field.access, [field._parent._parent.access, field._parent._parent._parent.access])
        self.load_buffer('SOC_RegisterField_Description', field.description)
        self.load_int('SOC_RegisterField_ReadAs', field.read_as_value, base=16, size=field.size or 32)
        self.load_int('SOC_RegisterField_WriteAs', field.write_as_value, base=16, size=field.size or 32)
        self.load_int('SOC_RegisterField_ShouldBe', field.should_be_value, base=16, size=field.size or 32)
        self.load_int('SOC_RegisterField_MustBe', field.must_be_value, base=16, size=field.size or 32)
        self.load_int('SOC_RegisterField_Recommended', field.recommended_value, base=16, size=field.size or 32)
        self.load_bool('SOC_RegisterField_ReadUndefined', field.read_undefined)
        self.load_bool('SOC_RegisterField_HWSet', field.hardware_set)
        self.init_types('SOC_RegisterField_Type', field.type, field)
        self.set_new_command("field", (SOC_RegisterField, field._parent))
        self.set_clone_command("field", field)
        return True

    def save_SOC_RegisterField(self, field):
        field.name = self.save_text('SOC_RegisterField_Name')
        field.title = self.save_text('SOC_RegisterField_Title')
        field.union = self.save_text('SOC_RegisterField_Union')
        field.bit = self.save_int('SOC_RegisterField_Bit')
        field.size = self.save_int('SOC_RegisterField_Size')
        field.access = self.save_enum('SOC_RegisterField_Access')
        field.description = self.save_buffer('SOC_RegisterField_Description')
        field.read_as_value = self.save_int('SOC_RegisterField_ReadAs')
        field.write_as_value = self.save_int('SOC_RegisterField_WriteAs')
        field.should_be_value = self.save_int('SOC_RegisterField_ShouldBe')
        field.must_be_value = self.save_int('SOC_RegisterField_MustBe')
        field.recommended_value = self.save_int('SOC_RegisterField_Recommended')
        field.read_undefined = self.save_bool('SOC_RegisterField_ReadUndefined', False)
        field.hardware_set = self.save_bool('SOC_RegisterField_HWSet', False)
        field.type = self.save_enum('SOC_RegisterField_Type')
        return True

    def load_SOC_TypeCommon(self, typename, t):
        self.load_text('SOC_%s_Name' % typename, t.name)
        self.load_text('SOC_%s_Title' % typename, t.title)
        self.load_text('SOC_%s_Prefix' % typename, t.prefix)
        self.load_buffer('SOC_%s_Description' % typename, t.description)
        return True

    def save_SOC_TypeCommon(self, typename, t):
        t.name = self.save_text('SOC_%s_Name' % typename)
        t.title = self.save_text('SOC_%s_Title' % typename)
        t.prefix = self.save_text('SOC_%s_Prefix' % typename)
        t.description = self.save_buffer('SOC_%s_Description' % typename)
        return True

    def load_SOC_Enum(self, enum):
        self.load_SOC_TypeCommon("Enum", enum)
        self.load_int('SOC_Enum_ValueSize', enum.values.value_size)
        self.load_int('SOC_Enum_ValueBase', enum.values.value_base)
        if enum.values:
            self.set_new_command("enum", (SOC_Enum, enum._parent))
        else:
            self.set_new_command("value", (SOC_EnumValue, enum.values))
        self.set_clone_command("enum", enum)
        return True

    def save_SOC_Enum(self, enum):
        self.save_SOC_TypeCommon("Enum", enum)
        enum.values.value_size = self.save_int('SOC_Enum_ValueSize')
        enum.values.value_base = self.save_int('SOC_Enum_ValueBase')
        return True

    def load_SOC_EnumValue(self, value):
        self.load_text('SOC_EnumValue_Name', value.name)
        self.load_text('SOC_EnumValue_Title', value.title)
        self.load_int('SOC_EnumValue_Value', value.value, base=value._parent.value_base, size=value._parent.value_size)
        self.load_buffer('SOC_EnumValue_Description', value.description)
        self.set_new_command("value", (SOC_EnumValue, value._parent))
        self.set_clone_command("value", value)
        return True

    def save_SOC_EnumValue(self, value):
        value.name = self.save_text('SOC_EnumValue_Name')
        value.title = self.save_text('SOC_EnumValue_Title')
        value.value = self.save_int('SOC_EnumValue_Value')
        value.description = self.save_buffer('SOC_EnumValue_Description')
        return True

def main():
    if len(sys.argv) >= 2:
        if sys.argv[1] == 'generate':
            if len(sys.argv) == 4:
                from socgenerate import generate_code
                return generate_code(sys.argv[2], sys.argv[3])
        if sys.argv[1] == 's3c2412x':
            if len(sys.argv) == 3:
                from socutils import process_S3C2412X
                return process_S3C2412X(sys.argv[2])
    wndMain = WindowMain()
    wndMain.show()
    Gtk.main()    

if __name__ == '__main__':
    main()
