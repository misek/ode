# encoding: utf-8
'''
Created on 07-05-2013

@author: Łukasz Misek
'''

def process_S3C2412X(fn):
    f = open(fn)
    lines = f.readlines()
    f.close()
    items = []
    items_port = {}
    for _l in lines:
        l = []
        for i in _l.split():
            x = i.strip()
            if x:
                l.append(x)
        try:
            # pin bits (xx = description)
            itm = {
                'name': l[0],
                'size': 2 if ':' in l[1] else 1,
                'values': [],
                'values_all': [],
            }
            port = l[0][:3]
            if not port in items_port:
                items_port[port] = []
            l = l[2:]
            if len(l) % 3 != 0:
                print "unrecognized values list: %s" % l
                raise
            for i in range(len(l) / 3):
                v = int(l[0], 2)
                if l[1] != '=':
                    print "unrecognized values list: %s" % l
                    raise
                n = l[2]
                
                safe_n = n.replace('[', '').replace(']', '').replace(')', '').replace('(', '')
                if not "reserved" in n.lower():
                    itm['values'].append((v, safe_n, n))
                itm['values_all'].append((v, safe_n, n))
                    
                l = l[3:]
            items.append(itm)
            items_port[port].append(itm)
        except:
            pass
        
    # print types
    if 0:
        for item in items:
            print '<enum name="%s-function" prefix="%s_" title="%s function">' % (item['name'].lower(), item['name'].lower(), item['name'])
            print '    <values value-base="2" value-size="%s">' % item['size']
            for v, safe_n, n in item['values']:
                print '        <value name="%s" title="%s" val="%s"/>' % (safe_n, n, v)
            print '    </values>'
            print '</enum>'
        
    # print portCON     
    if 0:
        for port, fields in items_port.items():
            p_no = ord(port[2]) - ord('A')
            print '<register name="%sCON" offset="%s">' % (port, hex(p_no * 16 + 0))
            print '    <description>Configure the pins of port %s</description>' % port[2]
            print '    <fields>'
            for field in fields:
                no = int(field['name'][3:])
                print '        <field bit="%s" size="%s" type="%s-function" name="%s">' % (
                    no * field['size'], 
                    field['size'], 
                    field['name'].lower(),
                    field['name'])
                print '            <description>'
                s = '<![CDATA['
                for value in field['values_all']:
                    v = bin(value[0])[2:]
                    while len(v) < field['size']:
                        v = '0' + v
                    s += '@%s: %s\n' % (v, value[2])
                s = s[:-1] + ']]>'
                print s
                print '            </description>'
                print '        </field>'
            print '    </fields>'
            print '</register>'
            
    # print portDAT  
    if 0:
        for port, fields in items_port.items():
            p_no = ord(port[2]) - ord('A')
            print '<register name="%sDAT" offset="%s">' % (port, hex(p_no * 16 + 4))
            print '    <description>The data register for port %s</description>' % port[2]
            print '    <fields>'
            for field in fields:
                no = int(field['name'][3:])
                print '        <field bit="%s" size="1" type="uint" name="%s">' % (
                    no,
                    field['name'])
                print '            <description>'
                s = '<![CDATA['
                if field['size'] == 1:
                    s += 'When the port is configured as output port, the pin state is the same as the that of the corresponding bit.\n' + \
                         'When the port is configured as functional pin, undefined value will be read.'
                else:
                    s += 'When the port is configured as input port, data from external sources can be read ' + \
                         'to the corresponding pin. When the port is configured as output port, data written ' + \
                         'in this register can be sent to the corresponding pin. When the port is configured ' + \
                         'as functional pin, undefined value will be read.'
                s += ']]>'
                print s
                print '            </description>'
                print '        </field>'
            print '    </fields>'
            print '</register>'
            
    # print portDN
    if 0:
        for port, fields in items_port.items():
            p_no = ord(port[2]) - ord('A')
            if fields[0]['size'] == 1:
                continue
            print '<register name="%sDN" offset="%s" reset-value="0">' % (port, hex(p_no * 16 + 8))
            print '    <description>Pull-down disable register for port %s</description>' % port[2]
            print '    <fields>'
            for field in fields:
                no = int(field['name'][3:])
                print '        <field bit="%s" size="1" type="bool" name="%s">' % (
                    no,
                    field['name'])
                print '            <description>'
                s = '<![CDATA['
                s += '@0: The pull-down function is enabled\n' + \
                     '@1: The pull-down function is disabled'
                s += ']]>'
                print s
                print '            </description>'
                print '        </field>'
            print '    </fields>'
            print '</register>'
            
    # print portSLPCON
    if 0:
        for port, fields in items_port.items():
            p_no = ord(port[2]) - ord('A')
            if fields[0]['size'] == 1:
                continue
            print '<register name="%sSLPCON" offset="%s" reset-value="0">' % (port, hex(p_no * 16 + 12))
            print '    <description>Sleep mode configuration register for port %s</description>' % port[2]
            print '    <fields>'
            for field in fields:
                no = int(field['name'][3:])
                print '        <field bit="%s" size="2" type="slp-control-pdn" name="%s">' % (no * 2, field['name'])
                print '            <description>'
                s = '<![CDATA['
                s += "@00: Output '0'\n" + \
                     "@01: Output '1'\n" + \
                     "@10: input, pull-down disable\n" + \
                     "@11: input, pull-down enable"                     
                s += ']]>'
                print s
                print '            </description>'
                print '        </field>'
            print '    </fields>'
            print '</register>'
            
    # print EXTINTx
    if 0:
        for no in range(24):
            if no in [0, 8, 16]:
                print '<register name="EXTINT%s" offset="%s" reset-value="0">' % (no / 8, hex(0x98 + no / 2))
                print '    <description>External interrupt control register %s</description>' % (no / 8)
                print '    <fields>'
            
            print '        <field bit="%s" size="1" type="bool" name="FLTEN%s">' % ((4 * (no % 8) + 3), no) 
            print '            <description>'
            s = '<![CDATA['
            s += 'Filter Enable for EINT%s\n' % no
            s += "@0: Disable\n" + \
                 "@1: Enable"                     
            s += ']]>'
            print s
            print '            </description>'
            print '        </field>'
            print '        <field bit="%s" size="3" type="eint-signal-method" name="EINT%s">' % ((4 * (no % 8)), no) 
            print '            <description>'
            s = '<![CDATA['
            s += 'Set the signaling method of the EINT%s\n' % no
            s += "@000: Low Level\n" + \
                 "@001: High level\n" + \
                 "@01x: Falling edge triggered\n" + \
                 "@10x: Rising edge triggered\n" + \
                 "@11x: Both edge triggered"
            s += ']]>'
            print s
            print '            </description>'
            print '        </field>'
                
            if no in [7, 15, 23]:
                print '    </fields>'
                print '</register>'

    # print EINTFLTx
    if 0:
        for _no in range(8):
            no = _no + 16
            if no in [16, 20]:
                print '<register name="EINTFLT%s" offset="%s" reset-value="0">' % (no / 4 - 2, hex(0xa4 + no - 8))
                print '    <description>External interrupt control register %s</description>' % (no / 4 - 2)
                print '    <fields>'
            print '        <field bit="%s" size="7" type="uint" name="EINTFLT%s">' % ((8 * (no % 4)), no) 
            print '            <description>Filter width of EINT%s</description>' % no
            print '        </field>'
            print '        <field bit="%s" size="3" type="eint-filter-clock" name="EINT%s">' % ((8 * (no % 4) + 7), no)
            print '            <description>'
            s = '<![CDATA['
            s += 'Filter clock of EINT%s\n' % no
            s += "@0: PCLK\n" + \
                 "@1: EXTCLK/OSC_CLK (Selected by OM pin)"
            s += ']]>'
            print s
            print '            </description>'
            print '        </field>'
                
            if no in [19, 23]:
                print '    </fields>'
                print '</register>'

    # print EINTPND/EINTMASK
    if 0:
        print '<register name="EINTMASK" offset="0xb4" reset-value="0xFFFFF0">'
        print '    <description>External interrupt mask register</description>'
        print '    <fields>'
        
        for _no in range(20):
            no = _no + 4
            print '        <field bit="%s" size="1" type="bool" name="EINT%s">' % (no, no) 
            print '            <description>'
            s = '<![CDATA['
            s += "@0: Enable interrupt\n" + \
                 "@1: Masked"
            s += ']]>'
            print s
            print '            </description>'
            print '        </field>'
        print '    </fields>'
        print '</register>'

        print '<register name="EINTPEND" offset="0xb8" reset-value="0">'
        print '    <description>External interrupt pending register</description>'
        print '    <fields>'
        
        for _no in range(20):
            no = _no + 4
            print '        <field bit="%s" size="1" type="bool" name="EINT%s">' % (no, no) 
            print '            <description>'
            s = '<![CDATA['
            s += "@0: Not requested\n" + \
                 "@1: Requested\n" + \
                 "@!1: clear bit"
            s += ']]>'
            print s
            print '            </description>'
            print '        </field>'
        print '    </fields>'
        print '</register>'

    # print REDLUTx
    if 0:
        for n in range(7):
            print '<register name="REDLUT%s" offset="%s" reset-value="0">' % (n, hex(0x44 + n * 4))
            print '    <description>Red Lookup table[%s:%s]</description>' % (32 + 32 * n - 1, 32 * n) 
            print '    <fields>'
            print '        <field bit="0" size="32" type="uint" name="Value" />'
            print '    </fields>'
            print '</register>'

    # print GREENLUTx
    if 0:
        for n in range(14):
            print '<register name="GREENLUT%s" offset="%s" reset-value="0">' % (n, hex(0x60 + n * 4))
            print '    <description>Green Lookup table[%s:%s]</description>' % (32 + 32 * n - 1, 32 * n) 
            print '    <fields>'
            print '        <field bit="0" size="32" type="uint" name="Value" />'
            print '    </fields>'
            print '</register>'

    # print BLUELUTx
    if 0:
        for n in range(7):
            print '<register name="BLUELUT%s" offset="%s" reset-value="0">' % (n, hex(0x98 + n * 4))
            print '    <description>Blue Lookup table[%s:%s]</description>' % (32 + 32 * n - 1, 32 * n) 
            print '    <fields>'
            print '        <field bit="0" size="32" type="uint" name="Value" />'
            print '    </fields>'
            print '</register>'

    # print FRCPATx
    if 0:
        for n in range(64):
            print '<register name="FRCPAT%s" offset="%s" reset-value="0">' % (n, hex(0xb4 + n * 4))
            print '    <description>FRC Pattern Register</description>' 
            print '    <fields>'
            print '        <field bit="0" size="32" type="uint" name="Value" />'
            print '    </fields>'
            print '</register>'

