/*
 * main.cpp
 *
 *  Created on: 12-04-2012
 *      Author: Łukasz Misek
 */

#include <gtk/gtk.h>

#include <1000A_MSD_035TCboard.h>

#define DUMP_PATH "/projects/data/_system/"

int main(int argc, char** argv) {
	pthread_setname_np(pthread_self(), "main");
	setvbuf(stdout, __null, _IONBF, 0);

	gtk_init(&argc, &argv);

	GtkWidget* window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Lark Voyage 35.5c (S3C2412X)");
	gtk_window_set_default_size(GTK_WINDOW(window), 320, 240);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_widget_show(window);

	const char* flashName = DUMP_PATH"lark_voyage_355c/flash.bin";
	const char* AT88SCFileName = "../../wince/DeviceEmulator/boards/_S3C2412X/resources/AT88SC0104C.bin";
	const char* SDfileName = DUMP_PATH"SD/SD_512MB.bin";

	Board1000A_MSD_035TC board;
	board.Initialize();
	board.SetConfig("nand", "file", flashName);
	board.SetConfig("at88sc", "file", AT88SCFileName);
	board.SetConfig("sdmmc", "file", SDfileName);
	board.PowerOn();

	GtkWidget* lcd = board.GetWidget("lcd");
	if (lcd) {
		gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(lcd));
		gtk_widget_show(GTK_WIDGET(lcd));
	}

	gtk_main();

	return 0;
}

