/*
 * utils.h
 *
 *  Created on: 13-04-2012
 *      Author: Łukasz Misek
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <cstdlib>
#include <algorithm>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <time.h>
#include <fcntl.h>
#include <poll.h>

#include "config.h"

#define FIXME(...) { \
	DEBUG_PRINT_COLOR(5); \
	DEBUG_PRINT("\n!!!! Halted at: %s, line: %d\n", __FILE__, __LINE__); \
	DEBUG_PRINT_COLOR(7); \
	fflush(stdout); \
	asm("int 3"); \
}

#define ASSERT(condition, ...) { if (!(condition)) FIXME(); }

#define DEBUG_PRINT(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define DEBUG_PRINT_CHAR(chr) putc(chr, stdout)
#ifdef __WIN32
#define DEBUG_PRINT_COLOR(col) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), col)
#else
#define DEBUG_PRINT_COLOR(col)
//DEBUG_PRINT("\033[3%u;%um", col & 0x7, col & 8 ? 1 : 21)
#endif

#ifdef __WIN32
	#ifndef pthread_yield
		#define pthread_yield() Sleep(0)
	#endif
	#ifndef memalign
		// memory can not be freed
		#define memalign(al, size) (void*)(((int)malloc(size + al) + al) & ~(al - 1))
	#endif
#endif

bool kbhit();
int kbread();

void reverse_bytes(void* src, uint len);
uint8_t reverse_byte_bits(uint8_t b);
void reverse_bits(void* src, uint len);

#define STRUCT_REVERSE(s) reverse_bytes(&s, sizeof(s))
#define STRUCT_REVERSE_BITS(s) reverse_bits(&s, sizeof(s))

#define CODEGEN_CALL __attribute__((regparm(3)))
#define TOOL_CALL __attribute__((regparm(3)))
#define CALLBACK __attribute__((regparm(3)))
#define FORCE_INLINE __attribute__((always_inline))

class AutoResetEvent {
	pollfd pfd;
	int pipefd[2];
public:
	AutoResetEvent();
	~AutoResetEvent();

	TOOL_CALL void Signal();
	TOOL_CALL bool WaitFor(int timeout = -1);
	TOOL_CALL void Reset();
	TOOL_CALL int GetReadHandle() { return pipefd[0]; };
};

class FIFOQueue;

enum FIFO_EVENT {evEmpty, evNotEmpty, evFull};

typedef void (TOOL_CALL *pFIFOQueueCallback)(FIFOQueue* queue, FIFO_EVENT ev, void* user);

class FIFOQueue {
private:
	pollfd pfd;
	int pipefd[2];
	pthread_mutex_t mutex;
	uint size;
	uint cap;
	AutoResetEvent emptyEvent;
	pFIFOQueueCallback callback[16];
	void* callback_user[16];

	void UpdateSize(int diff);

	TOOL_CALL void DoEvent(FIFO_EVENT ev);
	TOOL_CALL void DoEmpty();
	TOOL_CALL void DoFull();
	TOOL_CALL void DoNotEmpty();

public:
	FIFOQueue();
	~FIFOQueue();

	TOOL_CALL void SetCapacity(uint capacity);
	TOOL_CALL void Reset();
	TOOL_CALL void Lock();
	TOOL_CALL void Unlock();

	TOOL_CALL void AddCallback(pFIFOQueueCallback cb, void* user);

	TOOL_CALL uint8_t ReadByte();
	TOOL_CALL uint16_t ReadHalf();
	TOOL_CALL uint32_t ReadWord();
	TOOL_CALL uint Read(void* data, uint len);

	TOOL_CALL void WriteByte(uint8_t value);
	TOOL_CALL void WriteHalf(uint16_t value);
	TOOL_CALL void WriteWord(uint32_t value);
	TOOL_CALL void Write(const void* data, uint len);

	TOOL_CALL uint GetSize();
	TOOL_CALL bool IsEmpty();
	TOOL_CALL void WaitForEmpty();
	TOOL_CALL void WaitForNotEmpty();
};

class Timer;

typedef void (CALLBACK *pTimerCallback)(Timer* timer, void* user);

class Timer {
private:
	int hTimer;
	pTimerCallback callback;
	void* userdata;
public:
	Timer();
	~Timer();

	TOOL_CALL void SetCallback(pTimerCallback cb);
	TOOL_CALL void SetUserData(void* user);

	TOOL_CALL void Cancel();
	TOOL_CALL void SetTimeout(uint usecs);
	TOOL_CALL void Signal();
};

class Mutex {
private:
	pthread_mutex_t m_mutex;
	pthread_mutexattr_t m_mutex_attr;
public:
	Mutex(bool recursive = false);
	~Mutex();

#ifdef DEBUG
	TOOL_CALL void Lock();
#else
	FORCE_INLINE void Lock() { pthread_mutex_lock(&m_mutex); }
#endif
	FORCE_INLINE void Unlock() { pthread_mutex_unlock(&m_mutex); }
	FORCE_INLINE bool TryLock() { return pthread_mutex_trylock(&m_mutex) == 0; }
};

class Thread;

typedef bool (CALLBACK *pThreadCallback)(Thread* thread, void* user);

class Thread {
private:
	pthread_t handle;
	pThreadCallback callback;
	void* userdata;
	bool terminated;
	bool do_terminate;
	char name[16];
	static void* ThreadFun(void* user);
public:
	Thread();
	~Thread();

	TOOL_CALL void Start(pThreadCallback function, void* user = __null, const char* thread_name = __null);
	TOOL_CALL void Terminate();
	FORCE_INLINE bool IsTerminated() { return terminated; };
};

#endif /* UTILS_H_ */
