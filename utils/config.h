/*
 * config.h
 *
 *  Created on: 15-05-2012
 *      Author: Łukasz Misek
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#ifdef DEBUG

// deassembled code will be printed
#define DEBUG_DEASSEMBLED_CODE
// compiled code will be printed
//#define DEBUG_COMPILED_CODE
// if defined, PC will be update on every instruction,
// otherwise PC is update only on branches
#define DEBUG_VALID_PC
// use DEBUG_ADDRESs macro to debug instructions
#define DEBUG_INSTRUCTIONS

// main flag, debug all IO methods, registers
#define DEBUG_IO

// debug GPIO, show changes to port CONs
//#define DEBUG_GPIO

// NAND flash debug
//#define DEBUG_NAND
// NAND flash commands output
//#define DEBUG_NAND_COMMAND

// debug SD interface
//#define DEBUG_SD

#endif

#endif /* CONFIG_H_ */
