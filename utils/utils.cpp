/*
 * utils.cpp
 *
 *  Created on: 15-05-2012
 *      Author: Łukasz Misek
 */

#include "utils.h"

#include <termios.h>
#include <unistd.h>
#include <stropts.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <limits.h>

bool kb_initialized = false;

void kb_initialize()
{
#ifdef linux
	termios t;
	tcgetattr(STDIN_FILENO, &t);
	t.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &t);
#else
#endif
}

bool kbhit()
{
	if (!kb_initialized) {
		kb_initialize();
	}
#ifdef linux
	int bytesWaiting;
	ioctl(STDIN_FILENO, FIONREAD, &bytesWaiting);
	return bytesWaiting;
#else
	// FIXME:
	return false;
#endif
}

int kbread()
{
	if (!kb_initialized) {
		kb_initialize();
	}
	int ret = getchar();
	if (ret == 10) {
		ret = 13;
	}
	return ret;
}

void reverse_bytes(void* _src, uint len)
{
	char* src = (char*)_src;
	char* dest = src + len;
	for (uint i = 0; i < len / 2; i++) {
		char x = *--dest;
		*dest = *src;
		*src++ = x;
	}
}

uint8_t reverse_byte_bits(uint8_t b)
{
	uint8_t res = 0;
	for (uint i = 0; i < 8; i++) {
		res |= (((b >> i) & 1) << (8 - i - 1)) ;
	}
	return res;
}

void reverse_bits(void* _src, int len)
{
	char* src = (char*)_src;
	char* dest = src + len;
	for (int i = 0; i < len / 2; i++) {
		char x = reverse_byte_bits(*--dest);
		*dest = reverse_byte_bits(*src);
		*src++ = x;
	}
}

char static__buf[256];

AutoResetEvent::AutoResetEvent()
{
	pipe2(pipefd, O_NONBLOCK);
	pfd.fd = pipefd[0];
	pfd.events = POLLIN;
}

AutoResetEvent::~AutoResetEvent()
{
	close(pipefd[0]);
	close(pipefd[1]);
}

void AutoResetEvent::Signal()
{
	write(pipefd[1], this, 1);
}

void AutoResetEvent::Reset()
{
	while(read(pipefd[0], static__buf, sizeof(static__buf)) > 0) {
	}
}

bool AutoResetEvent::WaitFor(int timeout)
{
	bool result = poll(&pfd, 1, timeout) == 1;
	Reset();
	return result;
}

FIFOQueue::FIFOQueue() : size(0), cap(PIPE_BUF)
{
	pipe2(pipefd, 0);
	pfd.fd = pipefd[0];
	pfd.events = POLLIN;
	pthread_mutex_init(&mutex, __null);
}

FIFOQueue::~FIFOQueue()
{
	close(pipefd[0]);
	close(pipefd[1]);
	pthread_mutex_destroy(&mutex);
}

void FIFOQueue::SetCapacity(uint capacity)
{
	cap = capacity;
}

void FIFOQueue::Reset()
{
	Lock();
	while (size) {
		size -= read(pipefd[0], static__buf, sizeof(static__buf));
	}
	Unlock();
}

void FIFOQueue::Lock()
{
	pthread_mutex_lock(&mutex);
}

void FIFOQueue::Unlock()
{
	pthread_mutex_unlock(&mutex);
}

void FIFOQueue::UpdateSize(int diff)
{
	Lock();
	bool wasEmpty = size == 0;
	bool wasFull = size >= cap;
	size += diff;
	bool isEmpty = size == 0;
	bool isFull = size >= 0;
	Unlock();
	// do FIFO Queue events
	if (isEmpty) {
		DoEmpty();
	} else {
		if (wasEmpty) {
			DoNotEmpty();
		}
	}
	if (isFull) {
		if (!wasFull) {
			DoFull();
		}
	}
}

void FIFOQueue::AddCallback(pFIFOQueueCallback cb, void* user)
{
	uint i = 0;
	while (callback[i]) {
		i++;
	}
	callback[i] = cb;
	callback_user[i] = user;
}

void FIFOQueue::DoEvent(FIFO_EVENT ev)
{
	uint i = 0;
	while (callback[i]) {
		callback[i](this, ev, callback_user[i]);
		i++;
	}
}

void FIFOQueue::DoEmpty()
{
	emptyEvent.Signal();
	DoEvent(evEmpty);
}

void FIFOQueue::DoNotEmpty()
{
	emptyEvent.Reset();
	DoEvent(evNotEmpty);
}

void FIFOQueue::DoFull()
{
	DoEvent(evFull);
}

uint8_t FIFOQueue::ReadByte()
{
	uint8_t value;
	read(pipefd[0], &value, sizeof(value));
	UpdateSize(-sizeof(value));
	return value;
}

uint16_t FIFOQueue::ReadHalf()
{
	uint16_t value;
	read(pipefd[0], &value, sizeof(value));
	UpdateSize(-sizeof(value));
	return value;
}

uint32_t FIFOQueue::ReadWord()
{
	uint32_t value;
	read(pipefd[0], &value, sizeof(value));
	UpdateSize(-sizeof(value));
	return value;
}

uint FIFOQueue::Read(void* data, uint len)
{
	uint res = read(pipefd[0], data, len);
	UpdateSize(-res);
	return res;
}

void FIFOQueue::WriteByte(uint8_t value)
{
	write(pipefd[1], &value, sizeof (value));
	UpdateSize(sizeof(value));
}

void FIFOQueue::WriteHalf(uint16_t value)
{
	write(pipefd[1], &value, sizeof (value));
	UpdateSize(sizeof(value));
}

void FIFOQueue::WriteWord(uint32_t value)
{
	write(pipefd[1], &value, sizeof (value));
	UpdateSize(sizeof(value));
}

void FIFOQueue::Write(const void* data, uint len)
{
	write(pipefd[1], data, len);
	UpdateSize(len);
}

uint FIFOQueue::GetSize()
{
	return size;
}

bool FIFOQueue::IsEmpty()
{
	return poll(&pfd, 1, 0) == 0;
}

void FIFOQueue::WaitForEmpty()
{
	if (size) {
		emptyEvent.WaitFor();
	}
}

void FIFOQueue::WaitForNotEmpty()
{
	poll(&pfd, 1, -1);
}

#define MAX_TIMER_TIMEOUT 64

struct TimerInfo {
	int handle;
	Timer* timer;
	uint usecs;
};

struct TimerCommand {
	enum { cmdSetTime, cmdCancel } command;
	int handle;
	Timer* timer;
	uint usecs;
};

class TimerTimeoutThread {
	TimerInfo timers[MAX_TIMER_TIMEOUT];
	TimerCommand commands[MAX_TIMER_TIMEOUT];
	int timers_count, commands_count;
	Mutex lockCommands;
	AutoResetEvent command_pending;
	pthread_t thread;

	void ThreadRoutine();
	static void* __thread_routine(void* usr);
	void InternalRemoveTimers(int* indexes, int count);

public:
	TimerTimeoutThread();
	void AddTimer(int handle, Timer* timer_obj, uint usecs);
	void RemoveTimer(int handle);

};

TimerTimeoutThread::TimerTimeoutThread() : timers_count(0)
{
	pthread_create(&thread, __null, &__thread_routine, this);
}

void* TimerTimeoutThread::__thread_routine(void* usr)
{
	TimerTimeoutThread* tmr = (TimerTimeoutThread*)usr;
	pthread_setname_np(pthread_self(), "utl/Timers");
	tmr->ThreadRoutine();
	return __null;
}

void TimerTimeoutThread::ThreadRoutine()
{
	int t = 0;
	pollfd poll_list[MAX_TIMER_TIMEOUT + 1];
	uint64_t timer_cnt;
	uint usecs;
	itimerspec tspec;
	int toremove[MAX_TIMER_TIMEOUT];
	int toremove_count = 0;

	command_pending.WaitFor();
	while (true) {
		// remove old timers
		if (toremove_count) {
			InternalRemoveTimers(toremove, toremove_count);
			toremove_count = 0;
		}

		// process commands
		lockCommands.Lock();
		if (commands_count > 0) {
			TimerCommand* cmd = commands;
			TimerInfo* tmr;
			for (int i = 0; i < commands_count; i++, cmd++) {
				// search timer info
				t = 0;
				tmr = timers;
				while (t < timers_count) {
					if (tmr->handle == cmd->handle) {
						break;
					}
					t++;
					tmr++;
				}
				switch (cmd->command) {
					case TimerCommand::cmdSetTime:
						// set time for timer
						if (t == timers_count) {
							tmr->handle = cmd->handle;
							tmr->timer = cmd->timer;
							timers_count++;
						}
						tmr->usecs = cmd->usecs;
						// start/restart timer
						usecs = cmd->usecs;
						tspec.it_value.tv_sec = usecs / 1000000;
						usecs %= 1000000;
						tspec.it_value.tv_nsec = usecs * 1000;
						timerfd_settime(tmr->handle, 0, &tspec, __null);
						break;
					case TimerCommand::cmdCancel:
						// remove timer
						if (t < timers_count) {
							tspec.it_value.tv_sec = 0;
							tspec.it_value.tv_nsec = 0;
							toremove[toremove_count++] = t;
							timerfd_settime(tmr->handle, 0, &tspec, __null);
						}
						break;
				}
				commands_count = 0;
			}

			// recreate list of timers
			for (int i = 0; i < timers_count; i++) {
				poll_list[i].fd = timers[i].handle;
				poll_list[i].events = POLLIN;
				poll_list[i].revents = 0;
			}
			poll_list[timers_count].fd = command_pending.GetReadHandle();
			poll_list[timers_count].events = POLLIN;
			poll_list[timers_count].revents = 0;
		}
		command_pending.Reset();
		lockCommands.Unlock();

		if (poll(poll_list, timers_count + 1, 1000) > 0) {
			if (poll_list[timers_count].revents) {
				// command pending, process it first
				continue;
			}

			// process timeout events
			TimerInfo* tmr = timers;
			pollfd* poll_item = poll_list;
			for (int i = 0; i < timers_count; i++) {
				if (poll_item->revents) {
					read(tmr->handle, &timer_cnt, sizeof(uint64_t));
					toremove[toremove_count++] = i;
					tmr->timer->Signal();
				}
				tmr++;
				poll_item++;
			}
		}
	}
}

void TimerTimeoutThread::AddTimer(int handle, Timer* timer_obj, uint usecs)
{
	lockCommands.Lock();
	commands[commands_count].command = TimerCommand::cmdSetTime;
	commands[commands_count].handle = handle;
	commands[commands_count].timer = timer_obj;
	commands[commands_count].usecs = usecs;
	++commands_count;
	command_pending.Signal();
	lockCommands.Unlock();
}

void TimerTimeoutThread::InternalRemoveTimers(int* indexes, int count)
{
	TimerInfo* src = timers;
	TimerInfo* dest = timers;
	int removed = 0;
	for (int i = 0; i < timers_count; i++) {
		if (count) {
			if (i == *indexes) {
				indexes++;
				src++;
				removed++;
				count--;
				continue;
			}
		} else {
			*dest = *src++;
		}
	}
	timers_count -= removed;
}

void TimerTimeoutThread::RemoveTimer(int handle)
{
	lockCommands.Lock();
	commands[commands_count].command = TimerCommand::cmdCancel;
	commands[commands_count].handle = handle;
	++commands_count;
	command_pending.Signal();
	lockCommands.Unlock();
}

TimerTimeoutThread timeout_thread;

Timer::Timer(): callback(__null), userdata(__null)
{
	hTimer = timerfd_create(CLOCK_REALTIME, 0);
}

Timer::~Timer()
{
	close(hTimer);
}

void Timer::SetCallback(pTimerCallback cb)
{
	callback = cb;
}

void Timer::SetUserData(void* user)
{
	userdata = user;
}

void Timer::Cancel()
{
	timeout_thread.RemoveTimer(hTimer);
}

void Timer::SetTimeout(uint usecs)
{
	timeout_thread.AddTimer(hTimer, this, usecs);
}

void Timer::Signal()
{
	if (callback) {
		callback(this, userdata);
	}
}

Thread::Thread() : handle(0), callback(__null), userdata(__null), terminated(true), do_terminate(false)
{

}

Thread::~Thread()
{
	Terminate();
}

void* Thread::ThreadFun(void* user)
{
	Thread* t = (Thread*)user;
	if (t->name[0]) {
		pthread_setname_np(pthread_self(), t->name);
	}
	while (!t->do_terminate) {
		if (!t->callback(t, t->userdata)) {
			break;
		}
	}
	return __null;
}

void Thread::Start(pThreadCallback function, void* user, const char* thread_name)
{
	//ASSERT(terminated && !do_terminate);
	callback = function;
	userdata = user;
	pthread_create(&handle, __null, Thread::ThreadFun, this);
	if (thread_name) {
		if (strlen(thread_name) < 16) {
			strcpy(name, thread_name);
		} else {
			strncpy(name, thread_name, 15);
			name[15] = 0;
		}
	} else {
		name[0] = 0;
	}
	pthread_setname_np(handle, name);
}

void Thread::Terminate()
{
	if (!IsTerminated()) {
		do_terminate = true;
		while (!IsTerminated()) {
			usleep(25 * 1000);	// 25 ms
		}
		terminated = true;
		do_terminate = false;
	}
}

Mutex::Mutex(bool recursive)
{
	pthread_mutexattr_init(&m_mutex_attr);
	if (recursive) {
		pthread_mutexattr_settype(&m_mutex_attr, PTHREAD_MUTEX_RECURSIVE_NP);
	} else {
#ifdef DEBUG
		pthread_mutexattr_settype(&m_mutex_attr, PTHREAD_MUTEX_ERRORCHECK);
#else
		pthread_mutexattr_settype(&m_mutex_attr, PTHREAD_MUTEX_NORMAL);
#endif
	}
	pthread_mutex_init(&m_mutex, &m_mutex_attr);
}

Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_mutex);
	pthread_mutexattr_destroy(&m_mutex_attr);
}

#ifdef DEBUG

void Mutex::Lock()
{
	if (pthread_mutex_lock(&m_mutex)) {
		DEBUG_PRINT("Mutex dead-lock detected");
		ASSERT(false);
	}
}

#endif
