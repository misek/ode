#include "MobileDRAMController.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOMobileDRAMController implementation
 */
IOMobileDRAMController::IOMobileDRAMController(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOMobileDRAMController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOMobileDRAMController::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOMobileDRAMController::Reset()
{
	return SOCIODevice::Reset();
}

void IOMobileDRAMController::ResetRegisters()
{
	IOREG_RESET(BANKCFG);
	IOREG_RESET(BANKCON1);
	IOREG_RESET(BANKCON2);
	IOREG_RESET(BANKCON3);
	IOREG_RESET(REFRESH);
	IOREG_RESET(TIMEOUT);
	SOCIODevice::ResetRegisters();
}

uint32_t IOMobileDRAMController::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(DRAMController, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(BANKCFG): {
				IOREG_NOT_IMPLEMENTED_READ(BANKCFG);
				return IOREG_READ(BANKCFG);
			}
			break;
		case IOREG_OFFSET(BANKCON1): {
				IOREG_NOT_IMPLEMENTED_READ(BANKCON1);
				return IOREG_READ(BANKCON1);
			}
			break;
		case IOREG_OFFSET(BANKCON2): {
				IOREG_NOT_IMPLEMENTED_READ(BANKCON2);
				return IOREG_READ(BANKCON2);
			}
			break;
		case IOREG_OFFSET(BANKCON3): {
				IOREG_NOT_IMPLEMENTED_READ(BANKCON3);
				return IOREG_READ(BANKCON3);
			}
			break;
		case IOREG_OFFSET(REFRESH): {
				IOREG_NOT_IMPLEMENTED_READ(REFRESH);
				return IOREG_READ(REFRESH);
			}
			break;
		case IOREG_OFFSET(TIMEOUT): {
				IOREG_NOT_IMPLEMENTED_READ(TIMEOUT);
				return IOREG_READ(TIMEOUT);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOMobileDRAMController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(DRAMController, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(BANKCFG): {
				IOREG_VERIFY_WRITE(BANKCFG, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(BANKCFG);
				IOREG_WRITE(BANKCFG, Value);
			}
			break;
		case IOREG_OFFSET(BANKCON1): {
				IOREG_VERIFY_WRITE(BANKCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(BANKCON1);
				IOREG_WRITE(BANKCON1, Value);
			}
			break;
		case IOREG_OFFSET(BANKCON2): {
				IOREG_VERIFY_WRITE(BANKCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(BANKCON2);
				IOREG_WRITE(BANKCON2, Value);
			}
			break;
		case IOREG_OFFSET(BANKCON3): {
				IOREG_VERIFY_WRITE(BANKCON3, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(BANKCON3);
				IOREG_WRITE(BANKCON3, Value);
			}
			break;
		case IOREG_OFFSET(REFRESH): {
				IOREG_VERIFY_WRITE(REFRESH, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(REFRESH);
				IOREG_WRITE(REFRESH, Value);
			}
			break;
		case IOREG_OFFSET(TIMEOUT): {
				IOREG_VERIFY_WRITE(TIMEOUT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(TIMEOUT);
				IOREG_WRITE(TIMEOUT, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
