<<<<<<< HEAD
#ifndef GPIOCONTROLLER_H_INCLUDED
#define GPIOCONTROLLER_H_INCLUDED

#define GPIOCONTROLLER_BASE		0x56000000

#define rGPACON					0x0000		// Port A Control
#define rGPADAT					0x0004		// Port A Data
#define rGPBCON					0x0010		// Port B Control
#define rGPBDAT					0x0014		// Port B Data
#define rGPBDN					0x0018		// Pull-down Control B
#define rGPBSLPCON				0x001C		// Port B sleep mode configuration
#define rGPCCON					0x0020		// Port C Control
#define rGPCDAT					0x0024		// Port C Data
#define rGPCDN					0x0028		// Pull-down Control C
#define rGPCSLPCON				0x002C		// Port C sleep mode configuration
#define rGPDCON					0x0030		// Port D Control
#define rGPDDAT					0x0034		// Port D Data
#define rGPDDN					0x0038		// Pull-down Control D
#define rGPDSLPCON				0x003C		// Port D sleep mode configuration
#define rGPECON					0x0040		// Port E Control
#define rGPEDAT					0x0044		// Port E Data
#define rGPEDN					0x0048		// Pull-down Control E
#define rGPESLPCON				0x004C		// Port E sleep mode configuration
#define rGPFCON					0x0050		// Port F Control
#define rGPFDAT					0x0054		// Port F Data
#define rGPFDN					0x0058		// Pull-down Control F
#define rGPGCON					0x0060		// Port G Control
#define rGPGDAT					0x0064		// Port G Data
#define rGPGDN					0x0068		// Pull-down Control G
#define rGPGSLPCON				0x006C		// Port G sleep mode configuration
#define rGPHCON					0x0070		// Port H Control
#define rGPHDAT					0x0074		// Port H Data
#define rGPHDN					0x0078		// Pull-down Control H
#define rGPHSLPCON				0x007C		// Port H sleep mode configuration
#define rMISCCR					0x0090		// Miscellaneous Control
#define rDCLKCON				0x0094		// DCLK0/1 Control
#define rEXTINT0				0x0098		// External Interrupt Control Register 0
#define rEXTINT1				0x009C		// External Interrupt Control Register 1
#define rEXTINT2				0x00A0		// External Interrupt Control Register 2
#define rEINTFLT0				0x00A4		// Reserved
#define rEINTFLT1				0x00A8		// Reserved
#define rEINTFLT2				0x00AC		// External Interrupt Filter Control Register 2
#define rEINTFLT3				0x00B0		// External Interrupt Filter Control Register 3
#define rEINTMASK				0x00B4		// External Interrupt Mask
#define rEINTPEND				0x00B8		// External Interrupt Pending
#define rGSTATUS0				0x00BC		// External Pin Status
#define rGSTATUS1				0x00C0		// Chip ID
#define rGSTATUS2				0x00C4		// R/W Reset Status
#define rGSTATUS3				0x00C8		// Inform Register
#define rGSTATUS4				0x00CC		// Inform Register
#define rGSTATUS5				0x00D0		// Inform Register
#define rMSTCON					0x00D4		// Memory port Stop control
#define rMSLCON					0x00D8		// Memory port Sleep control
#define rDSC0					0x00DC		// Strength control 0
#define rDSC1					0x00E0		// Strength control 1

#define GPIOCONTROLLER_LEN		0x00E4

#include "soc.h"

#include "InterruptController.h"

#ifdef DEBUG_IO
typedef const char* GPIO_FUNCTION[32][4];
#endif

#ifdef DEBUG_GPIO

#define DEBUG_GPIO_CON(port) {\
	int i;\
	DEBUG_PRINT("0x%08X:\n", CPU_GetInstructionPointer());\
	for (i = 0; i < GP##port##_functions_count; i++) {\
		int fn = (reg.GP##port##CON.Word >> (i * 2)) & 3;\
		char* fun = GP##port##_function[i][fn];\
		if (fun) {\
			DEBUG_PRINT("GP"#port##"%-2d: ", i);\
			DEBUG_PRINT_COLOR(0x0B);\
			DEBUG_PRINT("%-10s", fun);\
			DEBUG_PRINT_COLOR(0x07);\
		} else {\
			DEBUG_PRINT("%17s", "");\
		}\
		if ((i+1)%4 == 0)\
			DEBUG_PRINT("\n"); else DEBUG_PRINT(" ");\
	}\
	if (i%4) DEBUG_PRINT("\n");\
}

#else

#define DEBUG_GPIO_CON(port)

#endif

enum GPIOPort {
	GPA, GPB, GPC, GPD, GPE, GPF, GPG, GPH
};
#define GPIOPort_count 8

class IOGPIOController;

struct GPIO_cb_param {
	IOGPIOController* gpio;
	GPIOPort port;
	union {
		struct {
			uint32_t Pin0:1;
			uint32_t Pin1:1;
			uint32_t Pin2:1;
			uint32_t Pin3:1;
			uint32_t Pin4:1;
			uint32_t Pin5:1;
			uint32_t Pin6:1;
			uint32_t Pin7:1;
			uint32_t Pin8:1;
			uint32_t Pin9:1;
			uint32_t Pin10:1;
			uint32_t Pin11:1;
			uint32_t Pin12:1;
			uint32_t Pin13:1;
			uint32_t Pin14:1;
			uint32_t Pin15:1;
			uint32_t Pin16:1;
			uint32_t Pin17:1;
			uint32_t Pin18:1;
			uint32_t Pin19:1;
			uint32_t Pin20:1;
			uint32_t Pin21:1;
			uint32_t Pin22:1;
			uint32_t Pin23:1;
			uint32_t Pin24:1;
			uint32_t Pin25:1;
			uint32_t Pin26:1;
			uint32_t Pin27:1;
			uint32_t Pin28:1;
			uint32_t Pin29:1;
			uint32_t Pin30:1;
			uint32_t Pin31:1;
		};
		struct {
			uint8_t Byte0;
			uint8_t Byte1;
			uint8_t Byte2;
			uint8_t Byte3;
		};
		struct {
			uint16_t Half0;
			uint16_t Half1;
		};
		uint32_t Word;
	} value, mask;
};

typedef void (SOC_METHOD *GPIO_read_callback)(GPIO_cb_param* params, void* user);
typedef void (SOC_METHOD *GPIO_write_callback)(GPIO_cb_param* params, void* user);

class IOGPIOController: public SOCIODevice {
private:
	uint32_t EINTLevel;			// collection of level-triggered interrupts that are pending
	uint32_t EINTBits;			// previous EINT states (to determine rising/falling edge)
	uint32_t EINTLevelFloat;	// collection of floating level-triggered interrupts that are pending

	uint32_t EINT_MaskF;		// mask for GPxDAT
	uint32_t EINT_MaskG;

	GPIO_read_callback read_cb[GPIOPort_count];
	GPIO_write_callback write_cb[GPIOPort_count];
	GPIO_cb_param read_cb_param[GPIOPort_count];
	GPIO_cb_param write_cb_param[GPIOPort_count];
	void* read_cb_user[GPIOPort_count];
	void* write_cb_user[GPIOPort_count];

	uint32_t GPA_input_mask, GPA_output_mask;
	uint32_t GPB_input_mask, GPB_output_mask;
	uint32_t GPC_input_mask, GPC_output_mask;
	uint32_t GPD_input_mask, GPD_output_mask;
	uint32_t GPE_input_mask, GPE_output_mask;
	uint32_t GPF_input_mask, GPF_output_mask;
	uint32_t GPG_input_mask, GPG_output_mask;
	uint32_t GPH_input_mask, GPH_output_mask;

public:
	IOGPIOController(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool Reset();

	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
	SOC_METHOD IOInterruptController::InterruptSource GetLevelInterruptsPending();

	SOC_METHOD void SetReadCallback(GPIO_read_callback cb, GPIOPort port, void* user = __null);
	SOC_METHOD void SetWriteCallback(GPIO_write_callback cb, GPIOPort port, void* user = __null);

	SOC_METHOD void SetEINTLevel(unsigned int EINTx, int level);

protected:
	struct {
		union {
			uint32_t Word;
			struct {
				uint32_t GPA0:1;
				uint32_t GPA1:1;
				uint32_t GPA2:1;
				uint32_t GPA3:1;
				uint32_t GPA4:1;
				uint32_t GPA5:1;
				uint32_t GPA6:1;
				uint32_t GPA7:1;
				uint32_t GPA8:1;
				uint32_t GPA9:1;
				uint32_t GPA10:1;
				uint32_t GPA11:1;
				uint32_t GPA12:1;
				uint32_t GPA13:1;
				uint32_t GPA14:1;
				uint32_t GPA15:1;
				uint32_t GPA16:1;
				uint32_t GPA17:1;
				uint32_t GPA18:1;
				uint32_t GPA19:1;
				uint32_t GPA20:1;
				uint32_t GPA21:1;
			};
		} GPACON, GPADAT;
		union {
			uint32_t Word;
			struct {
				uint32_t GPB0:2;
				uint32_t GPB1:2;
				uint32_t GPB2:2;
				uint32_t GPB3:2;
				uint32_t GPB4:2;
				uint32_t GPB5:2;
				uint32_t GPB6:2;
				uint32_t GPB7:2;
				uint32_t GPB8:2;
				uint32_t GPB9:2;
				uint32_t GPB10:2;
			};
		} GPBCON, GPBSLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPB0:1;
				uint32_t GPB1:1;
				uint32_t GPB2:1;
				uint32_t GPB3:1;
				uint32_t GPB4:1;
				uint32_t GPB5:1;
				uint32_t GPB6:1;
				uint32_t GPB7:1;
				uint32_t GPB8:1;
				uint32_t GPB9:1;
				uint32_t GPB10:1;
			};
		} GPBDAT, GPBDN;
		union {
			uint32_t Word;
			struct {
				uint32_t :2;
				uint32_t GPC1:2;
				uint32_t GPC2:2;
				uint32_t GPC3:2;
				uint32_t GPC4:2;
				uint32_t GPC5:2;
				uint32_t GPC6:2;
				uint32_t GPC7:2;
				uint32_t GPC8:2;
				uint32_t GPC9:2;
				uint32_t GPC10:2;
				uint32_t GPC11:2;
				uint32_t GPC12:2;
				uint32_t GPC13:2;
				uint32_t GPC14:2;
				uint32_t GPC15:2;
			};
		} GPCCON, GPCSLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t :1;
				uint32_t GPC1:1;
				uint32_t GPC2:1;
				uint32_t GPC3:1;
				uint32_t GPC4:1;
				uint32_t GPC5:1;
				uint32_t GPC6:1;
				uint32_t GPC7:1;
				uint32_t GPC8:1;
				uint32_t GPC9:1;
				uint32_t GPC10:1;
				uint32_t GPC11:1;
				uint32_t GPC12:1;
				uint32_t GPC13:1;
				uint32_t GPC14:1;
				uint32_t GPC15:1;
			};
		} GPCDAT, GPCDN;
		union {
			uint32_t Word;
			struct {
				uint32_t GPD0:2;
				uint32_t GPD1:2;
				uint32_t GPD2:2;
				uint32_t GPD3:2;
				uint32_t GPD4:2;
				uint32_t GPD5:2;
				uint32_t GPD6:2;
				uint32_t GPD7:2;
				uint32_t GPD8:2;
				uint32_t GPD9:2;
				uint32_t GPD10:2;
				uint32_t GPD11:2;
				uint32_t GPD12:2;
				uint32_t GPD13:2;
				uint32_t GPD14:2;
				uint32_t GPD15:2;
			};
		} GPDCON, GPDSLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPD0:1;
				uint32_t GPD1:1;
				uint32_t GPD2:1;
				uint32_t GPD3:1;
				uint32_t GPD4:1;
				uint32_t GPD5:1;
				uint32_t GPD6:1;
				uint32_t GPD7:1;
				uint32_t GPD8:1;
				uint32_t GPD9:1;
				uint32_t GPD10:1;
				uint32_t GPD11:1;
				uint32_t GPD12:1;
				uint32_t GPD13:1;
				uint32_t GPD14:1;
				uint32_t GPD15:1;
			};
		} GPDDAT, GPDDN;
		union {
			uint32_t Word;
			struct {
				uint32_t GPE0:2;
				uint32_t GPE1:2;
				uint32_t GPE2:2;
				uint32_t GPE3:2;
				uint32_t GPE4:2;
				uint32_t GPE5:2;
				uint32_t GPE6:2;
				uint32_t GPE7:2;
				uint32_t GPE8:2;
				uint32_t GPE9:2;
				uint32_t GPE10:2;
				uint32_t GPE11:2;
				uint32_t GPE12:2;
				uint32_t GPE13:2;
				uint32_t GPE14:2;
				uint32_t GPE15:2;
			};
		} GPECON, GPESLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPE0:1;
				uint32_t GPE1:1;
				uint32_t GPE2:1;
				uint32_t GPE3:1;
				uint32_t GPE4:1;
				uint32_t GPE5:1;
				uint32_t GPE6:1;
				uint32_t GPE7:1;
				uint32_t GPE8:1;
				uint32_t GPE9:1;
				uint32_t GPE10:1;
				uint32_t GPE11:1;
				uint32_t GPE12:1;
				uint32_t GPE13:1;
				uint32_t GPE14:1;
				uint32_t GPE15:1;
			};
		} GPEDAT, GPEDN;
		union {
			uint32_t Word;
			struct {
				uint32_t GPF0:2;
				uint32_t GPF1:2;
				uint32_t GPF2:2;
				uint32_t GPF3:2;
				uint32_t GPF4:2;
				uint32_t GPF5:2;
				uint32_t GPF6:2;
				uint32_t GPF7:2;
			};
		} GPFCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPF0:1;
				uint32_t GPF1:1;
				uint32_t GPF2:1;
				uint32_t GPF3:1;
				uint32_t GPF4:1;
				uint32_t GPF5:1;
				uint32_t GPF6:1;
				uint32_t GPF7:1;
			};
		} GPFDAT, GPFDN;
		union {
			uint32_t Word;
			struct {
				uint32_t GPG0:2;
				uint32_t GPG1:2;
				uint32_t GPG2:2;
				uint32_t GPG3:2;
				uint32_t GPG4:2;
				uint32_t GPG5:2;
				uint32_t GPG6:2;
				uint32_t GPG7:2;
				uint32_t GPG8:2;
				uint32_t GPG9:2;
				uint32_t GPG10:2;
				uint32_t GPG11:2;
				uint32_t GPG12:2;
				uint32_t GPG13:2;
				uint32_t GPG14:2;
				uint32_t GPG15:2;
			};
		} GPGCON, GPGSLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPG0:1;
				uint32_t GPG1:1;
				uint32_t GPG2:1;
				uint32_t GPG3:1;
				uint32_t GPG4:1;
				uint32_t GPG5:1;
				uint32_t GPG6:1;
				uint32_t GPG7:1;
				uint32_t GPG8:1;
				uint32_t GPG9:1;
				uint32_t GPG10:1;
				uint32_t GPG11:1;
				uint32_t GPG12:1;
				uint32_t GPG13:1;
				uint32_t GPG14:1;
				uint32_t GPG15:1;
			};
		} GPGDAT, GPGDN;
		union {
			uint32_t Word;
			struct {
				uint32_t GPH0:2;
				uint32_t GPH1:2;
				uint32_t GPH2:2;
				uint32_t GPH3:2;
				uint32_t GPH4:2;
				uint32_t GPH5:2;
				uint32_t GPH6:2;
				uint32_t GPH7:2;
				uint32_t GPH8:2;
				uint32_t GPH9:2;
				uint32_t GPH10:2;
			};
		} GPHCON, GPHSLPCON;
		union {
			uint32_t Word;
			struct {
				uint32_t GPH0:1;
				uint32_t GPH1:1;
				uint32_t GPH2:1;
				uint32_t GPH3:1;
				uint32_t GPH4:1;
				uint32_t GPH5:1;
				uint32_t GPH6:1;
				uint32_t GPH7:1;
				uint32_t GPH8:1;
				uint32_t GPH9:1;
				uint32_t GPH10:1;
			};
		} GPHDAT, GPHDN;
		union {
			uint32_t Word;
			struct {
				uint32_t SPUCR_L:1;
				uint32_t SPUCR_H:1;
				uint32_t SPUCR2:1;
				uint32_t USBPAD:1;
				uint32_t CLKSEL0:3;
				uint32_t :1;
				uint32_t CLKSEL1:3;
				uint32_t :1;
				uint32_t USBSUSPND0:1;
				uint32_t USBSUSPND1:1;
				uint32_t :3;
				uint32_t :3;
				uint32_t :12;
			};
		} MISCCR;
		union {
			uint32_t Word;
			struct {
				uint32_t DCLK0EN:1;
				uint32_t DCLK0SelCK:1;
				uint32_t :2;
				uint32_t DCLK0DIV:4;
				uint32_t DCLK0CMP:4;
				uint32_t :4;
				uint32_t DCLK1EN:1;
				uint32_t DCLK1SelCK:1;
				uint32_t :2;
				uint32_t DCLK1DIV:4;
				uint32_t DCLK1CMP:4;
				uint32_t :4;
			};
		} DCLKCON;
		union {
			uint32_t Word;
			struct {
				uint32_t EINT0:3;
				uint32_t FLTEN0:1;
				uint32_t EINT1:3;
				uint32_t FLTEN1:1;
				uint32_t EINT2:3;
				uint32_t FLTEN2:1;
				uint32_t EINT3:3;
				uint32_t FLTEN3:1;
				uint32_t EINT4:3;
				uint32_t FLTEN4:1;
				uint32_t EINT5:3;
				uint32_t FLTEN5:1;
				uint32_t EINT6:3;
				uint32_t FLTEN6:1;
				uint32_t EINT7:3;
				uint32_t FLTEN7:1;
			};
		} EXTINT0;
		union {
			uint32_t Word;
			struct {
				uint32_t EINT8:3;
				uint32_t FLTEN8:1;
				uint32_t EINT9:3;
				uint32_t FLTEN9:1;
				uint32_t EINT10:3;
				uint32_t FLTEN10:1;
				uint32_t EINT11:3;
				uint32_t FLTEN11:1;
				uint32_t EINT12:3;
				uint32_t FLTEN12:1;
				uint32_t EINT13:3;
				uint32_t FLTEN13:1;
				uint32_t EINT14:3;
				uint32_t FLTEN14:1;
				uint32_t EINT15:3;
				uint32_t FLTEN15:1;
			};
		} EXTINT1;
		union {
			uint32_t Word;
			struct {
				uint32_t EINT16:3;
				uint32_t FLTEN16:1;
				uint32_t EINT17:3;
				uint32_t FLTEN17:1;
				uint32_t EINT18:3;
				uint32_t FLTEN18:1;
				uint32_t EINT19:3;
				uint32_t FLTEN19:1;
				uint32_t EINT20:3;
				uint32_t FLTEN20:1;
				uint32_t EINT21:3;
				uint32_t FLTEN21:1;
				uint32_t EINT22:3;
				uint32_t FLTEN22:1;
				uint32_t EINT23:3;
				uint32_t FLTEN23:1;
			};
		} EXTINT2;
		RESERVED_REGISTER EINTFLT0;
		RESERVED_REGISTER EINTFLT1;
		union {
			uint32_t Word;
			struct {
				uint32_t EINTFLT16:6;
				uint32_t FLTCLK16:1;
				uint32_t EINTFLT17:6;
				uint32_t FLTCLK17:1;
				uint32_t EINTFLT18:6;
				uint32_t FLTCLK18:1;
				uint32_t EINTFLT19:6;
				uint32_t FLTCLK19:1;
			};
		} EINTFLT2;
		union {
			uint32_t Word;
			struct {
				uint32_t EINTFLT20:6;
				uint32_t FLTCLK20:1;
				uint32_t EINTFLT21:6;
				uint32_t FLTCLK21:1;
				uint32_t EINTFLT22:6;
				uint32_t FLTCLK22:1;
				uint32_t EINTFLT23:6;
				uint32_t FLTCLK23:1;
			};
		} EINTFLT3;
		union {
			uint32_t Word;
			struct {
				uint32_t EINT0:1;
				uint32_t EINT1:1;
				uint32_t EINT2:1;
				uint32_t EINT3:1;
				uint32_t EINT4:1;
				uint32_t EINT5:1;
				uint32_t EINT6:1;
				uint32_t EINT7:1;
				uint32_t EINT8:1;
				uint32_t EINT9:1;
				uint32_t EINT10:1;
				uint32_t EINT11:1;
				uint32_t EINT12:1;
				uint32_t EINT13:1;
				uint32_t EINT14:1;
				uint32_t EINT15:1;
				uint32_t EINT16:1;
				uint32_t EINT17:1;
				uint32_t EINT18:1;
				uint32_t EINT19:1;
				uint32_t EINT20:1;
				uint32_t EINT21:1;
				uint32_t EINT22:1;
				uint32_t EINT23:1;
			};
		} EINTMASK, EINTPEND;
		union {
			uint32_t Word;
			struct {
				uint32_t nBATT_FLT:1;
				uint32_t RnB:1;
				uint32_t :1;
				uint32_t nWAIT:1;
			};
		} GSTATUS0;
		union {
			uint32_t Word;
			struct {
				uint32_t CHIPID:1;
			};
		} GSTATUS1;
		union {
			uint32_t Word;
			struct {
				uint32_t Inform:1;
			};
		} GSTATUS2, GSTATUS3, GSTATUS4, GSTATUS5;
		union {
			uint32_t Word;
			struct {
				uint32_t MST_DATA:2;
				uint32_t :2;
				uint32_t MST_ADDR:1;
				uint32_t MST_CS:1;
				uint32_t MST_SDR:1;
				uint32_t MST_BE:1;
				uint32_t MST_nWE:1;
				uint32_t MST_nOE:1;
				uint32_t MST_NFC:1;
				uint32_t MST_RnB:1;
				uint32_t MST_WAIT:1;
				uint32_t :1;
				uint32_t MST_nRSTOUT:1;
				uint32_t :1;
				uint32_t :1;
				uint32_t MST_SCK:1;
				uint32_t :1;
				uint32_t MST_SCKE:1;
			};
		} MSTCON;
		union {
			uint32_t Word;
			struct {
				uint32_t MST_DATA:2;
				uint32_t :2;
				uint32_t MST_ADDR:2;
				uint32_t MST_CS:2;
				uint32_t MST_SDR:2;
				uint32_t MST_BE:2;
				uint32_t MST_nWE:2;
				uint32_t MST_nOE:2;
				uint32_t MST_NFC:2;
				uint32_t MST_RnB:1;
				uint32_t MST_WAIT:1;
				uint32_t :1;
				uint32_t MST_nRSTOUT:1;
				uint32_t :2;
				uint32_t :2;
				uint32_t MST_SCK:2;
				uint32_t :2;
				uint32_t MST_SCKE:2;
			};
		} MSLCON;
		union {
			uint32_t Word;
			struct {
				uint32_t DSC_DATA0:2;
				uint32_t DSC_DATA1:2;
				uint32_t DSC_DATA2:2;
				uint32_t DSC_DATA3:2;
				uint32_t DSC_ADR:2;
				uint32_t DSC_CS0:2;
				uint32_t DSC_CS1:2;
				uint32_t DSC_CS2:2;
				uint32_t DSC_CS3:2;
				uint32_t DSC_CS4:2;
				uint32_t DSC_CS5:2;
				uint32_t DSC_CS6:2;
				uint32_t DSC_CS7:2;
				uint32_t :5;
				uint32_t nEN_DSC:1;
			};
		} DSC0;
		union {
			uint32_t Word;
			struct {
				uint32_t DSC_SCK:2;
				uint32_t :2;
				uint32_t :2;
				uint32_t DSC_SCKE:2;
				uint32_t DSC_SDR:2;
				uint32_t DSC_NFC:2;
				uint32_t DSC_BE:2;
				uint32_t DSC_WOE:2;
				uint32_t :2;
				uint32_t :2;
				uint32_t :12;
			};
		} DSC1;
	} reg;
};

extern IOGPIOController* GPIO;

#endif
=======
#ifndef __GPIOCONTROLLER_HEADER_INCLUDED__
#define __GPIOCONTROLLER_HEADER_INCLUDED__

#include <soc.h>
#include "S3C2412Xtypes.h"

namespace S3C2412X
{

/*
 * IOGPIOController declaration
 */
class IOGPIOController: public SOCIODevice
{
private:

protected:
	GPIO_Registers reg;

public:
	IOGPIOController(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

} /* S3C2412X namespace */

#endif /* __GPIOCONTROLLER_HEADER_INCLUDED__ */
>>>>>>> soc-editor
