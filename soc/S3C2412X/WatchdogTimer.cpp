#include "WatchdogTimer.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOWatchdogTimer implementation
 */
IOWatchdogTimer::IOWatchdogTimer(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOWatchdogTimer::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOWatchdogTimer::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOWatchdogTimer::Reset()
{
	return SOCIODevice::Reset();
}

void IOWatchdogTimer::ResetRegisters()
{
	IOREG_RESET(WTCON);
	IOREG_RESET(WTDAT);
	IOREG_RESET(WTCNT);
	SOCIODevice::ResetRegisters();
}

uint32_t IOWatchdogTimer::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(Watchdog, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(WTCON): {
				IOREG_NOT_IMPLEMENTED_READ(WTCON);
				return IOREG_READ(WTCON);
			}
			break;
		case IOREG_OFFSET(WTDAT): {
				IOREG_NOT_IMPLEMENTED_READ(WTDAT);
				return IOREG_READ(WTDAT);
			}
			break;
		case IOREG_OFFSET(WTCNT): {
				IOREG_NOT_IMPLEMENTED_READ(WTCNT);
				return IOREG_READ(WTCNT);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOWatchdogTimer::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(Watchdog, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(WTCON): {
				IOREG_VERIFY_WRITE(WTCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(WTCON);
				IOREG_WRITE(WTCON, Value);
			}
			break;
		case IOREG_OFFSET(WTDAT): {
				IOREG_VERIFY_WRITE(WTDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(WTDAT);
				IOREG_WRITE(WTDAT, Value);
			}
			break;
		case IOREG_OFFSET(WTCNT): {
				IOREG_VERIFY_WRITE(WTCNT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(WTCNT);
				IOREG_WRITE(WTCNT, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
