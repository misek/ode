#include "InterruptController.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOInterruptController implementation
 */
IOInterruptController::IOInterruptController(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOInterruptController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOInterruptController::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOInterruptController::Reset()
{
	return SOCIODevice::Reset();
}

void IOInterruptController::ResetRegisters()
{
	IOREG_RESET(SRCPND);
	IOREG_RESET(INTMOD);
	IOREG_RESET(INTMSK);
	IOREG_RESET(PRIORITY);
	IOREG_RESET(INTPND);
	IOREG_RESET(INTOFFSET);
	IOREG_RESET(SUBSRCPND);
	IOREG_RESET(INTSUBMSK);
	SOCIODevice::ResetRegisters();
}

uint32_t IOInterruptController::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(InterruptController, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(SRCPND): {
				IOREG_NOT_IMPLEMENTED_READ(SRCPND);
				return IOREG_READ(SRCPND);
			}
			break;
		case IOREG_OFFSET(INTMOD): {
				IOREG_NOT_IMPLEMENTED_READ(INTMOD);
				return IOREG_READ(INTMOD);
			}
			break;
		case IOREG_OFFSET(INTMSK): {
				IOREG_NOT_IMPLEMENTED_READ(INTMSK);
				return IOREG_READ(INTMSK);
			}
			break;
		case IOREG_OFFSET(PRIORITY): {
				IOREG_NOT_IMPLEMENTED_READ(PRIORITY);
				return IOREG_READ(PRIORITY);
			}
			break;
		case IOREG_OFFSET(INTPND): {
				IOREG_NOT_IMPLEMENTED_READ(INTPND);
				return IOREG_READ(INTPND);
			}
			break;
		case IOREG_OFFSET(INTOFFSET): {
				IOREG_NOT_IMPLEMENTED_READ(INTOFFSET);
				return IOREG_READ(INTOFFSET);
			}
			break;
		case IOREG_OFFSET(SUBSRCPND): {
				IOREG_NOT_IMPLEMENTED_READ(SUBSRCPND);
				return IOREG_READ(SUBSRCPND);
			}
			break;
		case IOREG_OFFSET(INTSUBMSK): {
				IOREG_NOT_IMPLEMENTED_READ(INTSUBMSK);
				return IOREG_READ(INTSUBMSK);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOInterruptController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(InterruptController, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(SRCPND): {
				IOREG_VERIFY_WRITE(SRCPND, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SRCPND);
				IOREG_WRITE(SRCPND, Value);
			}
			break;
		case IOREG_OFFSET(INTMOD): {
				IOREG_VERIFY_WRITE(INTMOD, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(INTMOD);
				IOREG_WRITE(INTMOD, Value);
			}
			break;
		case IOREG_OFFSET(INTMSK): {
				IOREG_VERIFY_WRITE(INTMSK, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(INTMSK);
				IOREG_WRITE(INTMSK, Value);
			}
			break;
		case IOREG_OFFSET(PRIORITY): {
				IOREG_VERIFY_WRITE(PRIORITY, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(PRIORITY);
				IOREG_WRITE(PRIORITY, Value);
			}
			break;
		case IOREG_OFFSET(INTPND): {
				IOREG_VERIFY_WRITE(INTPND, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(INTPND);
				IOREG_WRITE(INTPND, Value);
			}
			break;
		case IOREG_OFFSET(INTOFFSET): {
				IOREG_INVALID_WRITE_ACCESS(INTOFFSET);
			}
			return;
		case IOREG_OFFSET(SUBSRCPND): {
				IOREG_VERIFY_WRITE(SUBSRCPND, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SUBSRCPND);
				IOREG_WRITE(SUBSRCPND, Value);
			}
			break;
		case IOREG_OFFSET(INTSUBMSK): {
				IOREG_VERIFY_WRITE(INTSUBMSK, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(INTSUBMSK);
				IOREG_WRITE(INTSUBMSK, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
