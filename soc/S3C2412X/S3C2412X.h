#ifndef __S3C2412X_HEADER_INCLUDED__
#define __S3C2412X_HEADER_INCLUDED__

#include "S3C2412Xtypes.h"
#include <armcpu.h>

#include "MobileDRAMController.h"
#include "ExternalBusInterface.h"
#include "USBHostController.h"
#include "InterruptController.h"
#include "DMAController.h"
#include "ATAController.h"
#include "SystemController.h"
#include "LCDController.h"
#include "NANDFlashController.h"
#include "SSMCController.h"
#include "UART.h"
#include "PWMTimer.h"
#include "USBDeviceController.h"
#include "WatchdogTimer.h"
#include "IICBusInterface.h"
#include "IISBusInterface.h"
#include "GPIOController.h"
#include "RealTimeClock.h"
#include "ADCTouchScreenInterface.h"
#include "SPIInterface.h"
#include "MMCSDSDIOController.h"

// S3C2412X supported devices
#include <Memory/RAMDevice.h>

namespace S3C2412X
{

class S3C2412X: public SOCBase
{
private:

protected:

public:
	ARMCPU* Core;

	IOMobileDRAMController* DRAMController;
	IOExternalBusInterface* EBI;
	IOUSBHostController* USBHost;
	IOInterruptController* InterruptController;
	IODMA0* DMA0;
	IODMA1* DMA1;
	IODMA2* DMA2;
	IODMA3* DMA3;
	IOATAController* ATA;
	IOSystemController* SystemController;
	IOLCDController* LCD;
	IONANDFlashController* NANDFlash;
	IOSSMCController* SSMC;
	IOUART0* UART0;
	IOUART1* UART1;
	IOUART2* UART2;
	IOPWMTimer* PWM;
	IOUSBDeviceController* USBDevice;
	IOWatchdogTimer* Watchdog;
	IOIICBusInterface* IIC;
	IOIISBusInterface* IIS;
	IOGPIOController* GPIO;
	IORealTimeClock* RTC;
	IOADCTouchScreenInterface* ADC;
	IOSPI0* SPI0;
	IOSPI1* SPI1;
	IOMMCSDSDIOController* SDMMC;

	S3C2412X();
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();

	SOC_METHOD virtual void AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index);

};

} /* S3C2412X namespace */

#endif /* __S3C2412X_HEADER_INCLUDED__ */
