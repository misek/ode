#include "IICBusInterface.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOIICBusInterface implementation
 */
IOIICBusInterface::IOIICBusInterface(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOIICBusInterface::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOIICBusInterface::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOIICBusInterface::Reset()
{
	return SOCIODevice::Reset();
}

void IOIICBusInterface::ResetRegisters()
{
	IOREG_RESET(IICCON);
	IOREG_RESET(IICSTAT);
	IOREG_RESET(IICLC);
	SOCIODevice::ResetRegisters();
}

uint32_t IOIICBusInterface::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(IIC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(IICCON): {
				IOREG_NOT_IMPLEMENTED_READ(IICCON);
				return IOREG_READ(IICCON);
			}
			break;
		case IOREG_OFFSET(IICSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(IICSTAT);
				return IOREG_READ(IICSTAT);
			}
			break;
		case IOREG_OFFSET(IICADD): {
				IOREG_NOT_IMPLEMENTED_READ(IICADD);
				return IOREG_READ(IICADD);
			}
			break;
		case IOREG_OFFSET(IICDS): {
				IOREG_NOT_IMPLEMENTED_READ(IICDS);
				return IOREG_READ(IICDS);
			}
			break;
		case IOREG_OFFSET(IICLC): {
				IOREG_NOT_IMPLEMENTED_READ(IICLC);
				return IOREG_READ(IICLC);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOIICBusInterface::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(IIC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(IICCON): {
				IOREG_VERIFY_WRITE(IICCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IICCON);
				IOREG_WRITE(IICCON, Value);
			}
			break;
		case IOREG_OFFSET(IICSTAT): {
				IOREG_VERIFY_WRITE(IICSTAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IICSTAT);
				IOREG_WRITE(IICSTAT, Value);
			}
			break;
		case IOREG_OFFSET(IICADD): {
				IOREG_VERIFY_WRITE(IICADD, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IICADD);
				IOREG_WRITE(IICADD, Value);
			}
			break;
		case IOREG_OFFSET(IICDS): {
				IOREG_VERIFY_WRITE(IICDS, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IICDS);
				IOREG_WRITE(IICDS, Value);
			}
			break;
		case IOREG_OFFSET(IICLC): {
				IOREG_VERIFY_WRITE(IICLC, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IICLC);
				IOREG_WRITE(IICLC, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
