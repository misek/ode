#include "MMCSDSDIOController.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOMMCSDSDIOController implementation
 */
IOMMCSDSDIOController::IOMMCSDSDIOController(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOMMCSDSDIOController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOMMCSDSDIOController::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOMMCSDSDIOController::Reset()
{
	return SOCIODevice::Reset();
}

void IOMMCSDSDIOController::ResetRegisters()
{
	IOREG_RESET(SDICON);
	IOREG_RESET(SDIPRE);
	IOREG_RESET(SDICmdArg);
	IOREG_RESET(SDICmdCon);
	IOREG_RESET(SDICmdSta);
	IOREG_RESET(SDIRSP0);
	IOREG_RESET(SDIRSP1);
	IOREG_RESET(SDIRSP2);
	IOREG_RESET(SDIRSP3);
	IOREG_RESET(SDIDTimer);
	IOREG_RESET(SDIBSize);
	IOREG_RESET(SDIDatCon);
	IOREG_RESET(SDIDatCnt);
	IOREG_RESET(SDIDatSta);
	IOREG_RESET(SDIFSTA);
	IOREG_RESET(SDIIntMsk);
	IOREG_RESET(SDIDAT);
	IOREG_RESET(SDIDAT_BE_HalfWord);
	IOREG_RESET(SDIDAT_BE_Byte);
	SOCIODevice::ResetRegisters();
}

uint32_t IOMMCSDSDIOController::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(SDMMC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(SDICON): {
				IOREG_NOT_IMPLEMENTED_READ(SDICON);
				return IOREG_READ(SDICON);
			}
			break;
		case IOREG_OFFSET(SDIPRE): {
				IOREG_NOT_IMPLEMENTED_READ(SDIPRE);
				return IOREG_READ(SDIPRE);
			}
			break;
		case IOREG_OFFSET(SDICmdArg): {
				IOREG_NOT_IMPLEMENTED_READ(SDICmdArg);
				return IOREG_READ(SDICmdArg);
			}
			break;
		case IOREG_OFFSET(SDICmdCon): {
				IOREG_NOT_IMPLEMENTED_READ(SDICmdCon);
				return IOREG_READ(SDICmdCon);
			}
			break;
		case IOREG_OFFSET(SDICmdSta): {
				IOREG_NOT_IMPLEMENTED_READ(SDICmdSta);
				return IOREG_READ(SDICmdSta);
			}
			break;
		case IOREG_OFFSET(SDIRSP0): {
				IOREG_NOT_IMPLEMENTED_READ(SDIRSP0);
				return IOREG_READ(SDIRSP0);
			}
			break;
		case IOREG_OFFSET(SDIRSP1): {
				IOREG_NOT_IMPLEMENTED_READ(SDIRSP1);
				return IOREG_READ(SDIRSP1);
			}
			break;
		case IOREG_OFFSET(SDIRSP2): {
				IOREG_NOT_IMPLEMENTED_READ(SDIRSP2);
				return IOREG_READ(SDIRSP2);
			}
			break;
		case IOREG_OFFSET(SDIRSP3): {
				IOREG_NOT_IMPLEMENTED_READ(SDIRSP3);
				return IOREG_READ(SDIRSP3);
			}
			break;
		case IOREG_OFFSET(SDIDTimer): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDTimer);
				return IOREG_READ(SDIDTimer);
			}
			break;
		case IOREG_OFFSET(SDIBSize): {
				IOREG_NOT_IMPLEMENTED_READ(SDIBSize);
				return IOREG_READ(SDIBSize);
			}
			break;
		case IOREG_OFFSET(SDIDatCon): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDatCon);
				return IOREG_READ(SDIDatCon);
			}
			break;
		case IOREG_OFFSET(SDIDatCnt): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDatCnt);
				return IOREG_READ(SDIDatCnt);
			}
			break;
		case IOREG_OFFSET(SDIDatSta): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDatSta);
				return IOREG_READ(SDIDatSta);
			}
			break;
		case IOREG_OFFSET(SDIFSTA): {
				IOREG_NOT_IMPLEMENTED_READ(SDIFSTA);
				return IOREG_READ(SDIFSTA);
			}
			break;
		case IOREG_OFFSET(SDIIntMsk): {
				IOREG_NOT_IMPLEMENTED_READ(SDIIntMsk);
				return IOREG_READ(SDIIntMsk);
			}
			break;
		case IOREG_OFFSET(SDIDAT): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDAT);
				return IOREG_READ(SDIDAT);
			}
			break;
		case IOREG_OFFSET(SDIDAT_BE_HalfWord): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDAT_BE_HalfWord);
				return IOREG_READ(SDIDAT_BE_HalfWord);
			}
			break;
		case IOREG_OFFSET(SDIDAT_BE_Byte): {
				IOREG_NOT_IMPLEMENTED_READ(SDIDAT_BE_Byte);
				return IOREG_READ(SDIDAT_BE_Byte);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOMMCSDSDIOController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(SDMMC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(SDICON): {
				IOREG_VERIFY_WRITE(SDICON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDICON);
				IOREG_WRITE(SDICON, Value);
			}
			break;
		case IOREG_OFFSET(SDIPRE): {
				IOREG_VERIFY_WRITE(SDIPRE, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIPRE);
				IOREG_WRITE(SDIPRE, Value);
			}
			break;
		case IOREG_OFFSET(SDICmdArg): {
				IOREG_VERIFY_WRITE(SDICmdArg, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDICmdArg);
				IOREG_WRITE(SDICmdArg, Value);
			}
			break;
		case IOREG_OFFSET(SDICmdCon): {
				IOREG_VERIFY_WRITE(SDICmdCon, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDICmdCon);
				IOREG_WRITE(SDICmdCon, Value);
			}
			break;
		case IOREG_OFFSET(SDICmdSta): {
				IOREG_INVALID_WRITE_ACCESS(SDICmdSta);
			}
			return;
		case IOREG_OFFSET(SDIRSP0): {
				IOREG_INVALID_WRITE_ACCESS(SDIRSP0);
			}
			return;
		case IOREG_OFFSET(SDIRSP1): {
				IOREG_INVALID_WRITE_ACCESS(SDIRSP1);
			}
			return;
		case IOREG_OFFSET(SDIRSP2): {
				IOREG_INVALID_WRITE_ACCESS(SDIRSP2);
			}
			return;
		case IOREG_OFFSET(SDIRSP3): {
				IOREG_INVALID_WRITE_ACCESS(SDIRSP3);
			}
			return;
		case IOREG_OFFSET(SDIDTimer): {
				IOREG_VERIFY_WRITE(SDIDTimer, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIDTimer);
				IOREG_WRITE(SDIDTimer, Value);
			}
			break;
		case IOREG_OFFSET(SDIBSize): {
				IOREG_VERIFY_WRITE(SDIBSize, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIBSize);
				IOREG_WRITE(SDIBSize, Value);
			}
			break;
		case IOREG_OFFSET(SDIDatCon): {
				IOREG_VERIFY_WRITE(SDIDatCon, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIDatCon);
				IOREG_WRITE(SDIDatCon, Value);
			}
			break;
		case IOREG_OFFSET(SDIDatCnt): {
				IOREG_INVALID_WRITE_ACCESS(SDIDatCnt);
			}
			return;
		case IOREG_OFFSET(SDIDatSta): {
				IOREG_INVALID_WRITE_ACCESS(SDIDatSta);
			}
			return;
		case IOREG_OFFSET(SDIFSTA): {
				IOREG_INVALID_WRITE_ACCESS(SDIFSTA);
			}
			return;
		case IOREG_OFFSET(SDIIntMsk): {
				IOREG_VERIFY_WRITE(SDIIntMsk, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIIntMsk);
				IOREG_WRITE(SDIIntMsk, Value);
			}
			break;
		case IOREG_OFFSET(SDIDAT): {
				IOREG_VERIFY_WRITE(SDIDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIDAT);
				IOREG_WRITE(SDIDAT, Value);
			}
			break;
		case IOREG_OFFSET(SDIDAT_BE_HalfWord): {
				IOREG_VERIFY_WRITE(SDIDAT_BE_HalfWord, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIDAT_BE_HalfWord);
				IOREG_WRITE(SDIDAT_BE_HalfWord, Value);
			}
			break;
		case IOREG_OFFSET(SDIDAT_BE_Byte): {
				IOREG_VERIFY_WRITE(SDIDAT_BE_Byte, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(SDIDAT_BE_Byte);
				IOREG_WRITE(SDIDAT_BE_Byte, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
