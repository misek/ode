<<<<<<< HEAD
#include "GPIOController.h"

#ifdef DEBUG_GPIO

#define GPA_functions_count 23
static const GPIO_FUNCTION GPA_function = {
	{"out", "ADDR0"},
	{"out", "ADDR16"},
	{"out", "ADDR17"},
	{"out", "ADDR18"},
	{"out", "ADDR19"},
	{"out", "ADDR20"},
	{"out", "ADDR21"},
	{"out", "ADDR22"},
	{"out", "ADDR23"},
	{"out", "ADDR24"},
	{"out", "ADDR25"},
	{"out", "ADDR26"},
	{"out", "nGCS1"},
	{"out", "nGCS2"},
	{"out", "nGCS3"},
	{"out", "nGCS4"},
	{"out", "nGCS5"},
	{"out", "CLE"},
	{"out", "ALE"},
	{"out", "nFWE"},
	{"out", "nFRE"},
	{"out", "nRSTOUT"}
};

#define GPB_functions_count 11
static const GPIO_FUNCTION GPB_function = {
	{"in", "out", "TOUT0"},
	{"in", "out", "TOUT1"},
	{"in", "out", "TOUT2"},
	{"in", "out", "TOUT3"},
	{"in", "out", "TCLK0"},
	{"in", "out"},
	{"in", "out"},
	{"in", "out", "nXDACK1"},
	{"in", "out", "nXDREQ1"},
	{"in", "out", "nXDACK0"},
	{"in", "out", "nXDREQ0"}
};

#define GPC_functions_count 16
static const GPIO_FUNCTION GPC_function = {
	{},
	{"in", "out", "VCLK"},
	{"in", "out", "VLINE"},
	{"in", "out", "VFRAME"},
	{"in", "out", "VM"},
	{"in", "out", "LCDVF0"},
	{"in", "out", "LCDVF1"},
	{"in", "out", "LCDVF2"},
	{"in", "out", "VD[0]"},
	{"in", "out", "VD[1]"},
	{"in", "out", "VD[2]"},
	{"in", "out", "VD[3]"},
	{"in", "out", "VD[4]"},
	{"in", "out", "VD[5]"},
	{"in", "out", "VD[6]"},
	{"in", "out", "VD[7]"},
};

#define GPD_functions_count 16
static const GPIO_FUNCTION GPD_function = {
	{"in", "out", "VD[8]"},
	{"in", "out", "VD[9]"},
	{"in", "out", "VD[10]"},
	{"in", "out", "VD[11]"},
	{"in", "out", "VD[12]"},
	{"in", "out", "VD[13]"},
	{"in", "out", "VD[14]"},
	{"in", "out", "VD[15]"},
	{"in", "out", "VD[16]", "SPIMISO1"},
	{"in", "out", "VD[17]", "SPIMOSI1"},
	{"in", "out", "VD[18]", "SPICLK1"},
	{"in", "out", "VD[19]"},
	{"in", "out", "VD[20]"},
	{"in", "out", "VD[21]"},
	{"in", "out", "VD[22]", "nSS1"},
	{"in", "out", "VD[23]", "nSS0"},
};

#define GPE_functions_count 16
static const GPIO_FUNCTION GPE_function = {
	{"in", "out", "I2SLRCK"},
	{"in", "out", "I2SSCLK"},
	{"in", "out", "CDCLK"},
	{"in", "out", "I2SSDI", "nSS0"},
	{"in", "out", "I2SSDO", "I2SSDI"},
	{"in", "out", "SDCLK"},
	{"in", "out", "SDCMD"},
	{"in", "out", "SDDAT0"},
	{"in", "out", "SDDAT1"},
	{"in", "out", "SDDAT2"},
	{"in", "out", "SDDAT3"},
	{"in", "out", "SPIMISO0"},
	{"in", "out", "SPIMOSI0"},
	{"in", "out", "SPICLK0"},
	{"in", __null, "IICSCL"},
	{"in", __null, "IICSDA"},
};

#define GPF_functions_count 8
static const GPIO_FUNCTION GPF_function = {
	{"in", "out", "EINT0"},
	{"in", "out", "EINT1"},
	{"in", "out", "EINT2"},
	{"in", "out", "EINT3"},
	{"in", "out", "EINT4"},
	{"in", "out", "EINT5"},
	{"in", "out", "EINT6"},
	{"in", "out", "EINT7"},
};

#define GPG_functions_count 16
static const GPIO_FUNCTION GPG_function = {
	{"in", "out", "EINT8", "ATA_INTRQ"},
	{"in", "out", "EINT9", "ATA_NRESET"},
	{"in", "out", "EINT10", "nSS0"},
	{"in", "out", "EINT11", "nSS1"},
	{"in", "out", "EINT12"},
	{"in", "out", "EINT13", "SPIMISO1"},
	{"in", "out", "EINT14", "SPIMOSI1"},
	{"in", "out", "EINT15", "SPICLK1"},
	{"in", "out", "EINT16"},
	{"in", "out", "EINT17", "nRTS1"},
	{"in", "out", "EINT18", "nCTS1"},
	{"in", "out", "EINT19", "TCLK1"},
	{"in", "out", "EINT20"},
	{"in", "out", "EINT21"},
	{"in", "out", "EINT22"},
	{"in", "out", "EINT23"},
};

#define GPH_functions_count 11
static const GPIO_FUNCTION GPH_function = {
	{"in", "out", "nCTS0"},
	{"in", "out", "nRTS0"},
	{"in", "out", "TXD0"},
	{"in", "out", "RXD0"},
	{"in", "out", "TXD1"},
	{"in", "out", "RXD1"},
	{"in", "out", "TXD2", "nRTS1"},
	{"in", "out", "RXD2", "nCTS1"},
	{"in", "out", "UEXTCLK"},
	{"in", "out", "CLKOUT0"},
	{"in", "out", "CLKOUT1"},
};

#endif

IOGPIOController::IOGPIOController(SOCBase* soc): SOCIODevice(soc)
{
	memset(read_cb, 0, sizeof(read_cb));
	memset(write_cb, 0, sizeof(write_cb));

	GPA_input_mask = 0x00000000;
	GPB_input_mask = 0x000007FF;
	GPC_input_mask = 0x0000FFFE;
	GPD_input_mask = 0x0000FFFF;
	GPE_input_mask = 0x0000FFFF;
	GPF_input_mask = 0x000000FF;
	GPG_input_mask = 0x0000FFFF;
	GPH_input_mask = 0x000007FF;
}

bool IOGPIOController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOGPIOController::Reset()
{
	// set default register values
	reg.GPACON.Word = 0x007FFFFF;
	reg.GPBCON.Word = 0x00000000;
	reg.GPBDN.Word = 0x00000000;
	reg.GPBSLPCON.Word = 0x00000000;
	reg.GPCCON.Word = 0x00000000;
	reg.GPCDN.Word = 0x00000000;
	reg.GPCSLPCON.Word = 0x00000000;
	reg.GPDCON.Word = 0x00000000;
	reg.GPDDN.Word = 0x00000000;
	reg.GPDSLPCON.Word = 0x00000000;
	reg.GPECON.Word = 0x00000000;
	reg.GPEDN.Word = 0x00000000;
	reg.GPESLPCON.Word = 0x00000000;
	reg.GPFCON.Word = 0x00000000;
	reg.GPFDN.Word = 0x00000000;
	reg.GPGCON.Word = 0x00000000;
	reg.GPGDN.Word = 0x00000000;
	reg.GPGSLPCON.Word = 0x00000000;
	reg.GPHCON.Word = 0x00000000;
	reg.GPHDN.Word = 0x00000000;
	reg.GPHSLPCON.Word = 0x00000000;
	reg.MISCCR.Word = 0x00000330;
	reg.DCLKCON.Word = 0x00000000;
	reg.EXTINT0.Word = 0x00000000;
	reg.EXTINT1.Word = 0x00000000;
	reg.EXTINT2.Word = 0x00000000;
	reg.EINTFLT2.Word = 0x00000000;
	reg.EINTFLT3.Word = 0x00000000;
	reg.EINTMASK.Word = 0x00FFFFF0;
	reg.EINTPEND.Word = 0x00000000;
	reg.GSTATUS0.Word = 0x00000000;
	reg.GSTATUS1.Word = 0x32412003;
	reg.GSTATUS2.Word = 0x00000000;
	reg.GSTATUS3.Word = 0x00000000;
	reg.GSTATUS4.Word = 0x00000000;
	reg.GSTATUS5.Word = 0x00000000;
	reg.MSTCON.Word = 0x00000000;
	reg.MSLCON.Word = 0x00000000;
	reg.DSC0.Word = 0x00000000;
	reg.DSC1.Word = 0x00000000;

	// set default values for EINTx & inputs for GPF & GPG
	EINTBits = 0;

	return SOCIODevice::Reset();
}

void IOGPIOController::SetReadCallback(GPIO_read_callback cb, GPIOPort port, void* user)
{
	read_cb_param[port].gpio = this;
	read_cb_param[port].port = port;
	read_cb_user[port] = user;
	read_cb[port] = cb;
}

void IOGPIOController::SetWriteCallback(GPIO_read_callback cb, GPIOPort port, void* user)
{
	write_cb_param[port].gpio = this;
	write_cb_param[port].port = port;
	write_cb_user[port] = user;
	write_cb[port] = cb;
}

void IOGPIOController::SetEINTLevel(unsigned int EINTx, int level)
{
	Lock();

	ASSERT(EINTx <= 23);
	uint32_t bitValue = 0; // default to 0
	uint32_t InterruptBit = 1 << EINTx;

	switch (level)
	{
		case -1:
			EINTLevelFloat |= InterruptBit;
			if (EINTx < 8)
				bitValue = reg.GPFDN.Word & InterruptBit;
			else 
				bitValue = reg.GPGDN.Word & (InterruptBit >> 8);
			break;
		case 0:
			EINTLevelFloat &= ~InterruptBit;
			bitValue = 0;
			break;
		case 1:
			EINTLevelFloat &= ~InterruptBit;
			bitValue = InterruptBit;
			break;
		default:
			ASSERT(false);
			break;
	}

	if ((EINTBits & InterruptBit) != bitValue) {
		// EINTx level has changed
		if (bitValue)
			EINTBits |= InterruptBit; else
			EINTBits &= ~InterruptBit;

		// check for valid GPIO function
		unsigned int pin_fun;
		if (EINTx < 8) {
			unsigned int shift = 2 * EINTx;
			pin_fun = (reg.GPFCON.Word >> shift) & 3;
		} else {
			unsigned int shift = 2 * (EINTx - 8);
			pin_fun = (reg.GPGCON.Word >> shift) & 3;
		}
		
		// check if interrupt is required if valid GPIO function is set
		if (pin_fun == 2) {
			// get EINTx signal method
			unsigned int signal_method;
			if (EINTx < 8) {
				signal_method = (reg.EXTINT0.Word >> (4 * EINTx)) & 0x07;
			} else if (EINTx < 16) {
				signal_method = (reg.EXTINT1.Word >> (4 * (EINTx - 8))) & 0x07;
			} else {
				signal_method = (reg.EXTINT2.Word >> (4 * (EINTx - 16))) & 0x07;
			}

			// filter is enabled, check for correct change
			bool filter_passed;
			switch (signal_method) {
				case 0:
					// low level
					ASSERT(false);
					filter_passed = bitValue == 0;
					EINTLevel &= ~InterruptBit;
					break;
				case 1:
					// high level
					ASSERT(false);
					filter_passed = bitValue != 0;
					EINTLevel |= InterruptBit;
					break;
				case 2:
				case 3:
					// falling edge
					filter_passed = bitValue == 0;
					break;
				case 4:
				case 5:
					// rising edge
					filter_passed = bitValue != 0;
					break;
				case 6:
				case 7:
					// both edge
					filter_passed = true;
					break;
				default:
					// we should never get here
					filter_passed = false;
					ASSERT(false);
					break;
			}

			if (filter_passed) {
				// filter has passed, raise interrupt
				IOInterruptController::InterruptSource int_no = IOInterruptController::Invalid;
				if (EINTx >= 4) {
					reg.EINTPEND.Word |= InterruptBit;
					// check if interrupt is masked
					if (InterruptBit && ~reg.EINTMASK.Word) {
						if (EINTx < 8)
							int_no = IOInterruptController::SourceEINT4_7; else
							int_no = IOInterruptController::SourceEINT8_23;
					}
				} else {
					// for EINT0..3 do not check mask.
					// It is checked by Interrupt Controller
					switch (EINTx) {
						case 0:
							int_no = IOInterruptController::SourceEINT0;
							break;
						case 1:
							int_no = IOInterruptController::SourceEINT1;
							break;
						case 2:
							int_no = IOInterruptController::SourceEINT2;
							break;
						case 3:
							int_no = IOInterruptController::SourceEINT3;
							break;
					}
				}

				InterruptController->RaiseInterrupt(int_no);
			}
		}
	}
	Unlock();
}

IOInterruptController::InterruptSource IOGPIOController::GetLevelInterruptsPending()
{
	uint32_t Ret;
	uint32_t Pending = EINTLevel & ~reg.EINTMASK.Word;

	Ret = IOInterruptController::Invalid;
	ASSERT((Pending & 0xF) == 0); // EINT0...3 are controlled directly via the InterruptController, not via GPIO
	reg.EINTPEND.Word |= Pending;
	if (Pending & 0xF0) {
		Ret = IOInterruptController::SourceEINT4_7;
	}
	if (Pending & 0xFFF00) {
		Ret |= IOInterruptController::SourceEINT8_23;
	}
	return (IOInterruptController::InterruptSource)Ret;
}

uint32_t IOGPIOController::ReadWord(uint32_t IOAddress)
{
#define MERGE_INPUT_PINS(port) \
	if (read_cb[port] && port##_input_mask) { \
		GPIO_cb_param* param = &read_cb_param[port]; \
		param->value.Word = reg.port##DAT.Word; \
		param->mask.Word = port##_input_mask; \
		read_cb[port](param, read_cb_user[port]); \
		reg.port##DAT.Word = (reg.port##DAT.Word & ~port##_input_mask) | (param->value.Word & port##_input_mask); \
	}

	IO_DEBUG_DEVICE("GPIO", "rW");

	switch (IOAddress) {
		case rGPACON:
			IO_DEBUG_REG("GPACON");
			return reg.GPACON.Word;
		case rGPADAT:
			IO_DEBUG_REG("GPADAT");
			MERGE_INPUT_PINS(GPA);
			return reg.GPADAT.Word;
		case rGPBCON:
			IO_DEBUG_REG("GPBCON");
			return reg.GPBCON.Word;
		case rGPBDAT:
			IO_DEBUG_REG("GPBDAT");
			MERGE_INPUT_PINS(GPB);
			return reg.GPBDAT.Word;
		case rGPBDN:
			IO_DEBUG_REG("GPBDN");
			return reg.GPBDN.Word;
		case rGPBSLPCON:
			IO_DEBUG_REG("GPBSLPCON");
			break;
			return reg.GPBSLPCON.Word;
		case rGPCCON:
			IO_DEBUG_REG("GPCCON");
			return reg.GPCCON.Word;
		case rGPCDAT:
			IO_DEBUG_REG("GPCDAT");
			MERGE_INPUT_PINS(GPC);
			return reg.GPCDAT.Word;
		case rGPCDN:
			IO_DEBUG_REG("GPCDN");
			return reg.GPCDN.Word;
		case rGPCSLPCON:
			IO_DEBUG_REG("GPCSLPCON");
			break;
			return reg.GPCSLPCON.Word;
		case rGPDCON:
			IO_DEBUG_REG("GPDCON");
			return reg.GPDCON.Word;
		case rGPDDAT:
			IO_DEBUG_REG("GPDDAT");
			MERGE_INPUT_PINS(GPD);
			return reg.GPDDAT.Word;
		case rGPDDN:
			IO_DEBUG_REG("GPDDN");
			return reg.GPDDN.Word;
		case rGPDSLPCON:
			IO_DEBUG_REG("GPDSLPCON");
			break;
			return reg.GPDSLPCON.Word;
		case rGPECON:
			IO_DEBUG_REG("GPECON");
			return reg.GPECON.Word;
		case rGPEDAT:
			IO_DEBUG_REG("GPEDAT");
			MERGE_INPUT_PINS(GPE);
			return reg.GPEDAT.Word;
		case rGPEDN:
			IO_DEBUG_REG("GPEDN");
			return reg.GPEDN.Word;
		case rGPESLPCON:
			IO_DEBUG_REG("GPESLPCON");
			break;
			return reg.GPESLPCON.Word;
		case rGPFCON:
			IO_DEBUG_REG("GPFCON");
			return reg.GPFCON.Word;
		case rGPFDAT:
			IO_DEBUG_REG("GPFDAT");
			// merge in EINTx values
			// TODO: make use of pulldown when floating
			reg.GPFDAT.Word = (reg.GPFDAT.Word & ~EINT_MaskF) | (EINTBits & EINT_MaskF);
			MERGE_INPUT_PINS(GPF);
			return reg.GPFDAT.Word;
		case rGPFDN:
			IO_DEBUG_REG("GPFDN");
			return reg.GPFDN.Word;
		case rGPGCON:
			IO_DEBUG_REG("GPGCON");
			return reg.GPGCON.Word;
		case rGPGDAT:
			IO_DEBUG_REG("GPGDAT");
			// merge in EINTx values
			// TODO: make use of pulldown when floating
			reg.GPGDAT.Word = (reg.GPGDAT.Word & ~EINT_MaskG) | ((EINTBits >> 8) & EINT_MaskG);
			MERGE_INPUT_PINS(GPG);
			return reg.GPGDAT.Word;
		case rGPGDN:
			IO_DEBUG_REG("GPGDN");
			return reg.GPGDN.Word;
		case rGPGSLPCON:
			IO_DEBUG_REG("GPGSLPCON");
			break;
			return reg.GPGSLPCON.Word;
		case rGPHCON:
			IO_DEBUG_REG("GPHCON");
			return reg.GPHCON.Word;
		case rGPHDAT:
			IO_DEBUG_REG("GPHDAT");
			MERGE_INPUT_PINS(GPH);
			return reg.GPHDAT.Word;
		case rGPHDN:
			IO_DEBUG_REG("GPHDN");
			return reg.GPHDN.Word;
		case rGPHSLPCON:
			IO_DEBUG_REG("GPHSLPCON");
			break;
			return reg.GPHSLPCON.Word;
		case rMISCCR:
			IO_DEBUG_REG("MISCCR");
			return reg.MISCCR.Word;
		case rDCLKCON:
			IO_DEBUG_REG("DCLKCON");
			break;
			return reg.DCLKCON.Word;
		case rEXTINT0:
			IO_DEBUG_REG("EXTINT0");
			return reg.EXTINT0.Word;
		case rEXTINT1:
			IO_DEBUG_REG("EXTINT1");
			return reg.EXTINT1.Word;
		case rEXTINT2:
			IO_DEBUG_REG("EXTINT2");
			return reg.EXTINT2.Word;
		case rEINTFLT0:
			IO_DEBUG_REG("EINTFLT0");
			break;
			return reg.EINTFLT0.Word;
		case rEINTFLT1:
			IO_DEBUG_REG("EINTFLT1");
			break;
			return reg.EINTFLT1.Word;
		case rEINTFLT2:
			IO_DEBUG_REG("EINTFLT2");
			break;
			return reg.EINTFLT2.Word;
		case rEINTFLT3:
			IO_DEBUG_REG("EINTFLT3");
			break;
			return reg.EINTFLT3.Word;
		case rEINTMASK:
			IO_DEBUG_REG("EINTMASK");
			return reg.EINTMASK.Word;
		case rEINTPEND:
			IO_DEBUG_REG("EINTPEND");
			return reg.EINTPEND.Word;
		case rGSTATUS0:
			IO_DEBUG_REG("GSTATUS0");
			// ready & battery power valid
			// TODO: implement better
			return 0x00000003;
		case rGSTATUS1:
			IO_DEBUG_REG("GSTATUS1");
			return reg.GSTATUS1.Word;
		case rGSTATUS2:
			IO_DEBUG_REG("GSTATUS2");
			return reg.GSTATUS2.Word;
		case rGSTATUS3:
			IO_DEBUG_REG("GSTATUS3");
			return reg.GSTATUS3.Word;
		case rGSTATUS4:
			IO_DEBUG_REG("GSTATUS4");
			return reg.GSTATUS4.Word;
		case rGSTATUS5:
			IO_DEBUG_REG("GSTATUS5");
			return reg.GSTATUS5.Word;
		case rMSTCON:
			IO_DEBUG_REG("MSTCON");
			break;
			return reg.MSTCON.Word;
		case rMSLCON:
			IO_DEBUG_REG("MSLCON");
			break;
			return reg.MSLCON.Word;
		case rDSC0:
			IO_DEBUG_REG("DSC0");
			break;
			return reg.DSC0.Word;
		case rDSC1:
			IO_DEBUG_REG("DSC1");
			break;
			return reg.DSC1.Word;
	}

	IO_NOTIMPLEMENTED(IOAddress);
	IO_DEBUG_END();

	return 0;
#undef MERGE_INPUT_PINS
}

void IOGPIOController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
#define OUTPUT_PINS(port) \
	if (write_cb[port] && port##_output_mask) { \
		GPIO_cb_param* param = &write_cb_param[port]; \
		param->value.Word = reg.port##DAT.Word; \
		param->mask.Word = port##_output_mask; \
		write_cb[port](param, write_cb_user[port]); \
	}
	IO_DEBUG_DEVICE("GPIO", "wW");

	switch (IOAddress) {
		case rGPACON:
			IO_DEBUG_REG("GPACON");
			reg.GPACON.Word = Value;
			GPA_input_mask = 0;
			GPA_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 21; i++)
				{
					switch (v & 1) {
						case 0b0:
							// output
							GPA_output_mask |= b;
							break;
						case 0b1:
							// misc
							break;
					}
					v >>= 1;
					b <<= 1;
				}
			}
			return;
		case rGPADAT:
			IO_DEBUG_REG("GPADAT");
			reg.GPADAT.Word = Value;
			OUTPUT_PINS(GPA);
			return;
		case rGPBCON:
			IO_DEBUG_REG("GPBCON");
			reg.GPBCON.Word = Value;
			DEBUG_GPIO_CON(B);
			GPB_input_mask = 0;
			GPB_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 10; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPB_input_mask |= b;
							break;
						case 0b01:
							// output
							GPB_output_mask |= b;
							break;
						case 0b10:
							// misc
							break;
						case 0b11:
							// reserved
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPBDAT:
			IO_DEBUG_REG("GPBDAT");
			reg.GPBDAT.Word = Value;
			OUTPUT_PINS(GPB);
			return;
		case rGPBDN:
			IO_DEBUG_REG("GPBDN");
			reg.GPBDN.Word = Value;
			return;
		case rGPBSLPCON:
			IO_DEBUG_REG("GPBSLPCON");
			reg.GPBSLPCON.Word = Value;
			break;
		case rGPCCON:
			IO_DEBUG_REG("GPCCON");
			reg.GPCCON.Word = Value;
			DEBUG_GPIO_CON(C);
			GPC_input_mask = 0;
			GPC_output_mask = 0;
			{
				register int v = Value >> 2;
				register uint32_t b = 1 << 1;
				for (int i = 1; i <= 15; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPC_input_mask |= b;
							break;
						case 0b01:
							// output
							GPC_output_mask |= b;
							break;
						case 0b10:
							// misc
							break;
						case 0b11:
							// reserved
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPCDAT:
			IO_DEBUG_REG("GPCDAT");
			reg.GPCDAT.Word = Value;
			OUTPUT_PINS(GPC);
			return;
		case rGPCDN:
			IO_DEBUG_REG("GPCDN");
			reg.GPCDN.Word = Value;
			return;
		case rGPCSLPCON:
			IO_DEBUG_REG("GPCSLPCON");
			reg.GPCSLPCON.Word = Value;
			break;
		case rGPDCON:
			IO_DEBUG_REG("GPDCON");
			reg.GPDCON.Word = Value;
			DEBUG_GPIO_CON(D);
			GPD_input_mask = 0;
			GPD_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 15; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPD_input_mask |= b;
							break;
						case 0b01:
							// output
							GPD_output_mask |= b;
							break;
						case 0b10:
							// misc
							break;
						case 0b11:
							// reserved
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPDDAT:
			IO_DEBUG_REG("GPDDAT");
			reg.GPDDAT.Word = Value;
			OUTPUT_PINS(GPD);
			break;
		case rGPDDN:
			IO_DEBUG_REG("GPDDN");
			reg.GPDDN.Word = Value;
			return;
		case rGPDSLPCON:
			IO_DEBUG_REG("GPDSLPCON");
			reg.GPDSLPCON.Word = Value;
			break;
		case rGPECON:
			IO_DEBUG_REG("GPECON");
			reg.GPECON.Word = Value;
			DEBUG_GPIO_CON(E);
			GPE_input_mask = 0;
			GPE_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 15; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPE_input_mask |= b;
							break;
						case 0b01:
							// output
							GPE_output_mask |= b;
							break;
						case 0b10:
							// misc
							break;
						case 0b11:
							// reserved
							break;
					}
					v >>= 2;
					b <<= 1;
				}
				// GPE14 & GPE15 are input only
				GPE_output_mask &= 0x00003FFF;
			}
			return;
		case rGPEDAT:
			IO_DEBUG_REG("GPEDAT");
			reg.GPEDAT.Word = Value;
			OUTPUT_PINS(GPE);
			return;
		case rGPEDN:
			IO_DEBUG_REG("GPEDN");
			reg.GPEDN.Word = Value;
			return;
		case rGPESLPCON:
			IO_DEBUG_REG("GPESLPCON");
			reg.GPESLPCON.Word = Value;
			break;
		case rGPFCON:
			IO_DEBUG_REG("GPFCON");
			reg.GPFCON.Word = Value;
			DEBUG_GPIO_CON(F);
			// set flags for EINTx merging
			EINT_MaskF = 0;
			GPF_input_mask = 0;
			GPF_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 7; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPF_input_mask |= b;
							break;
						case 0b01:
							// output
							GPF_output_mask |= b;
							break;
						case 0b10:
							// EINTx
							EINT_MaskF |= b;
							break;
						case 0b11:
							// misc
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPFDAT:
			IO_DEBUG_REG("GPFDAT");
			reg.GPFDAT.Word = Value;
			OUTPUT_PINS(GPF);
			return;
		case rGPFDN:
			IO_DEBUG_REG("GPFDN");
			reg.GPFDN.Word = Value;
			return;
		case rGPGCON:
			IO_DEBUG_REG("GPGCON");
			reg.GPGCON.Word = Value;
			DEBUG_GPIO_CON(G);
			// set flags for EINTx merging
			EINT_MaskG = 0;
			GPG_input_mask = 0;
			GPG_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 15; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPG_input_mask |= b;
							break;
						case 0b01:
							// output
							GPG_output_mask |= b;
							break;
						case 0b10:
							// EINTx
							EINT_MaskG |= b;
							break;
						case 0b11:
							// misc
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPGDAT:
			IO_DEBUG_REG("GPGDAT");
			reg.GPGDAT.Word = Value;
			OUTPUT_PINS(GPG);
			return;
		case rGPGDN:
			IO_DEBUG_REG("GPGDN");
			reg.GPGDN.Word = Value;
			return;
		case rGPGSLPCON:
			IO_DEBUG_REG("GPGSLPCON");
			reg.GPGSLPCON.Word = Value;
			break;
		case rGPHCON:
			IO_DEBUG_REG("GPHCON");
			reg.GPHCON.Word = Value;
			DEBUG_GPIO_CON(H);
			GPH_input_mask = 0;
			GPH_output_mask = 0;
			{
				register int v = Value;
				register uint32_t b = 1;
				for (int i = 0; i <= 10; i++)
				{
					switch (v & 3) {
						case 0b00:
							// input
							GPH_input_mask |= b;
							break;
						case 0b01:
							// output
							GPH_output_mask |= b;
							break;
						case 0b10:
							// misc
							break;
						case 0b11:
							// reserved
							break;
					}
					v >>= 2;
					b <<= 1;
				}
			}
			return;
		case rGPHDAT:
			IO_DEBUG_REG("GPHDAT");
			reg.GPHDAT.Word = Value;
			OUTPUT_PINS(GPH);
			return;
		case rGPHDN:
			IO_DEBUG_REG("GPHDN");
			reg.GPHDN.Word = Value;
			return;
		case rGPHSLPCON:
			IO_DEBUG_REG("GPHSLPCON");
			reg.GPHSLPCON.Word = Value;
			break;
		case rMISCCR:
			IO_DEBUG_REG("MISCCR");
			reg.MISCCR.Word = Value;
			return;
		case rDCLKCON:
			IO_DEBUG_REG("DCLKCON");
			reg.DCLKCON.Word = Value;
			break;
		case rEXTINT0:
			IO_DEBUG_REG("EXTINT0");
			reg.EXTINT0.Word = Value;
			return;
		case rEXTINT1:
			IO_DEBUG_REG("EXTINT1");
			reg.EXTINT1.Word = Value;
			return;
		case rEXTINT2:
			IO_DEBUG_REG("EXTINT2");
			reg.EXTINT2.Word = Value;
			return;
		case rEINTFLT0:
			IO_DEBUG_REG("EINTFLT0");
			reg.EINTFLT0.Word = Value;
			break;
		case rEINTFLT1:
			IO_DEBUG_REG("EINTFLT1");
			reg.EINTFLT1.Word = Value;
			break;
		case rEINTFLT2:
			IO_DEBUG_REG("EINTFLT2");
			reg.EINTFLT2.Word = Value;
			break;
		case rEINTFLT3:
			IO_DEBUG_REG("EINTFLT3");
			reg.EINTFLT3.Word = Value;
			break;
		case rEINTMASK:
			IO_DEBUG_REG("EINTMASK");
			reg.EINTMASK.Word = Value;
			if ((reg.EINTMASK.Word & ~reg.EINTPEND.Word) & 0x00FFFF00) { // an interrupt has been unmasked
				InterruptController->RaiseInterrupt(IOInterruptController::SourceEINT8_23);
			}
			if ((reg.EINTMASK.Word & ~reg.EINTPEND.Word) & 0x000000F0) { // an interrupt has been unmasked
				InterruptController->RaiseInterrupt(IOInterruptController::SourceEINT4_7);
			}
			return;
		case rEINTPEND:
			IO_DEBUG_REG("EINTPEND");
			reg.EINTPEND.Word &= ~Value;
			return;
		case rGSTATUS0:
			IO_DEBUG_REG("GSTATUS0");
			reg.GSTATUS0.Word = Value;
			break;
		case rGSTATUS1:
			IO_DEBUG_REG("GSTATUS1");
			reg.GSTATUS1.Word = Value;
			break;
		case rGSTATUS2:
			IO_DEBUG_REG("GSTATUS2");
			reg.GSTATUS2.Word = Value;
			return;
		case rGSTATUS3:
			IO_DEBUG_REG("GSTATUS3");
			reg.GSTATUS3.Word = Value;
			return;
		case rGSTATUS4:
			IO_DEBUG_REG("GSTATUS4");
			reg.GSTATUS4.Word = Value;
			return;
		case rGSTATUS5:
			IO_DEBUG_REG("GSTATUS5");
			reg.GSTATUS5.Word = Value;
			return;
		case rMSTCON:
			IO_DEBUG_REG("MSTCON");
			reg.MSTCON.Word = Value;
			break;
		case rMSLCON:
			IO_DEBUG_REG("MSLCON");
			reg.MSLCON.Word = Value;
			break;
		case rDSC0:
			IO_DEBUG_REG("DSC0");
			reg.DSC0.Word = Value;
			break;
		case rDSC1:
			IO_DEBUG_REG("DSC1");
			reg.DSC1.Word = Value;
			break;
	}

	IO_NOTIMPLEMENTED_WRITE(IOAddress, Value);
	IO_DEBUG_END();
#undef OUTPUT_PINS
}
=======
#include "GPIOController.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOGPIOController implementation
 */
IOGPIOController::IOGPIOController(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOGPIOController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOGPIOController::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOGPIOController::Reset()
{
	return SOCIODevice::Reset();
}

void IOGPIOController::ResetRegisters()
{
	IOREG_RESET(GPACON);
	IOREG_RESET(GPBCON);
	IOREG_RESET(GPBDN);
	IOREG_RESET(GPBSLPCON);
	IOREG_RESET(GPCCON);
	IOREG_RESET(GPCDN);
	IOREG_RESET(GPCSLPCON);
	IOREG_RESET(GPDCON);
	IOREG_RESET(GPDDN);
	IOREG_RESET(GPDSLPCON);
	IOREG_RESET(GPECON);
	IOREG_RESET(GPEDN);
	IOREG_RESET(GPESLPCON);
	IOREG_RESET(GPFCON);
	IOREG_RESET(GPFDN);
	IOREG_RESET(GPGCON);
	IOREG_RESET(GPGDN);
	IOREG_RESET(GPGSLPCON);
	IOREG_RESET(GPHCON);
	IOREG_RESET(GPHDN);
	IOREG_RESET(GPHSLPCON);
	IOREG_RESET(MISCCR);
	IOREG_RESET(DCLKCON);
	IOREG_RESET(EXTINT0);
	IOREG_RESET(EXTINT1);
	IOREG_RESET(EXTINT2);
	IOREG_RESET(EINTFLT2);
	IOREG_RESET(EINTFLT3);
	IOREG_RESET(EINTMASK);
	IOREG_RESET(EINTPEND);
	IOREG_RESET(GSTATUS1);
	IOREG_RESET(GSTATUS2);
	IOREG_RESET(GSTATUS3);
	IOREG_RESET(GSTATUS4);
	IOREG_RESET(GSTATUS5);
	IOREG_RESET(MSTCON);
	IOREG_RESET(MSLCON);
	IOREG_RESET(DSC0);
	IOREG_RESET(DSC1);
	SOCIODevice::ResetRegisters();
}

uint32_t IOGPIOController::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(GPIO, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(GPACON): {
				IOREG_NOT_IMPLEMENTED_READ(GPACON);
				return IOREG_READ(GPACON);
			}
			break;
		case IOREG_OFFSET(GPADAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPADAT);
				return IOREG_READ(GPADAT);
			}
			break;
		case IOREG_OFFSET(GPBCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPBCON);
				return IOREG_READ(GPBCON);
			}
			break;
		case IOREG_OFFSET(GPBDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPBDAT);
				return IOREG_READ(GPBDAT);
			}
			break;
		case IOREG_OFFSET(GPBDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPBDN);
				return IOREG_READ(GPBDN);
			}
			break;
		case IOREG_OFFSET(GPBSLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPBSLPCON);
				return IOREG_READ(GPBSLPCON);
			}
			break;
		case IOREG_OFFSET(GPCCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPCCON);
				return IOREG_READ(GPCCON);
			}
			break;
		case IOREG_OFFSET(GPCDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPCDAT);
				return IOREG_READ(GPCDAT);
			}
			break;
		case IOREG_OFFSET(GPCDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPCDN);
				return IOREG_READ(GPCDN);
			}
			break;
		case IOREG_OFFSET(GPCSLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPCSLPCON);
				return IOREG_READ(GPCSLPCON);
			}
			break;
		case IOREG_OFFSET(GPDCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPDCON);
				return IOREG_READ(GPDCON);
			}
			break;
		case IOREG_OFFSET(GPDDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPDDAT);
				return IOREG_READ(GPDDAT);
			}
			break;
		case IOREG_OFFSET(GPDDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPDDN);
				return IOREG_READ(GPDDN);
			}
			break;
		case IOREG_OFFSET(GPDSLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPDSLPCON);
				return IOREG_READ(GPDSLPCON);
			}
			break;
		case IOREG_OFFSET(GPECON): {
				IOREG_NOT_IMPLEMENTED_READ(GPECON);
				return IOREG_READ(GPECON);
			}
			break;
		case IOREG_OFFSET(GPEDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPEDAT);
				return IOREG_READ(GPEDAT);
			}
			break;
		case IOREG_OFFSET(GPEDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPEDN);
				return IOREG_READ(GPEDN);
			}
			break;
		case IOREG_OFFSET(GPESLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPESLPCON);
				return IOREG_READ(GPESLPCON);
			}
			break;
		case IOREG_OFFSET(GPFCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPFCON);
				return IOREG_READ(GPFCON);
			}
			break;
		case IOREG_OFFSET(GPFDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPFDAT);
				return IOREG_READ(GPFDAT);
			}
			break;
		case IOREG_OFFSET(GPFDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPFDN);
				return IOREG_READ(GPFDN);
			}
			break;
		case IOREG_OFFSET(GPGCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPGCON);
				return IOREG_READ(GPGCON);
			}
			break;
		case IOREG_OFFSET(GPGDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPGDAT);
				return IOREG_READ(GPGDAT);
			}
			break;
		case IOREG_OFFSET(GPGDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPGDN);
				return IOREG_READ(GPGDN);
			}
			break;
		case IOREG_OFFSET(GPGSLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPGSLPCON);
				return IOREG_READ(GPGSLPCON);
			}
			break;
		case IOREG_OFFSET(GPHCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPHCON);
				return IOREG_READ(GPHCON);
			}
			break;
		case IOREG_OFFSET(GPHDAT): {
				IOREG_NOT_IMPLEMENTED_READ(GPHDAT);
				return IOREG_READ(GPHDAT);
			}
			break;
		case IOREG_OFFSET(GPHDN): {
				IOREG_NOT_IMPLEMENTED_READ(GPHDN);
				return IOREG_READ(GPHDN);
			}
			break;
		case IOREG_OFFSET(GPHSLPCON): {
				IOREG_NOT_IMPLEMENTED_READ(GPHSLPCON);
				return IOREG_READ(GPHSLPCON);
			}
			break;
		case IOREG_OFFSET(MISCCR): {
				IOREG_NOT_IMPLEMENTED_READ(MISCCR);
				return IOREG_READ(MISCCR);
			}
			break;
		case IOREG_OFFSET(DCLKCON): {
				IOREG_NOT_IMPLEMENTED_READ(DCLKCON);
				return IOREG_READ(DCLKCON);
			}
			break;
		case IOREG_OFFSET(EXTINT0): {
				IOREG_NOT_IMPLEMENTED_READ(EXTINT0);
				return IOREG_READ(EXTINT0);
			}
			break;
		case IOREG_OFFSET(EXTINT1): {
				IOREG_NOT_IMPLEMENTED_READ(EXTINT1);
				return IOREG_READ(EXTINT1);
			}
			break;
		case IOREG_OFFSET(EXTINT2): {
				IOREG_NOT_IMPLEMENTED_READ(EXTINT2);
				return IOREG_READ(EXTINT2);
			}
			break;
		case IOREG_OFFSET(EINTFLT2): {
				IOREG_NOT_IMPLEMENTED_READ(EINTFLT2);
				return IOREG_READ(EINTFLT2);
			}
			break;
		case IOREG_OFFSET(EINTFLT3): {
				IOREG_NOT_IMPLEMENTED_READ(EINTFLT3);
				return IOREG_READ(EINTFLT3);
			}
			break;
		case IOREG_OFFSET(EINTMASK): {
				IOREG_NOT_IMPLEMENTED_READ(EINTMASK);
				return IOREG_READ(EINTMASK);
			}
			break;
		case IOREG_OFFSET(EINTPEND): {
				IOREG_NOT_IMPLEMENTED_READ(EINTPEND);
				return IOREG_READ(EINTPEND);
			}
			break;
		case IOREG_OFFSET(GSTATUS0): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS0);
				return IOREG_READ(GSTATUS0);
			}
			break;
		case IOREG_OFFSET(GSTATUS1): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS1);
				return IOREG_READ(GSTATUS1);
			}
			break;
		case IOREG_OFFSET(GSTATUS2): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS2);
				return IOREG_READ(GSTATUS2);
			}
			break;
		case IOREG_OFFSET(GSTATUS3): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS3);
				return IOREG_READ(GSTATUS3);
			}
			break;
		case IOREG_OFFSET(GSTATUS4): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS4);
				return IOREG_READ(GSTATUS4);
			}
			break;
		case IOREG_OFFSET(GSTATUS5): {
				IOREG_NOT_IMPLEMENTED_READ(GSTATUS5);
				return IOREG_READ(GSTATUS5);
			}
			break;
		case IOREG_OFFSET(MSTCON): {
				IOREG_NOT_IMPLEMENTED_READ(MSTCON);
				return IOREG_READ(MSTCON);
			}
			break;
		case IOREG_OFFSET(MSLCON): {
				IOREG_NOT_IMPLEMENTED_READ(MSLCON);
				return IOREG_READ(MSLCON);
			}
			break;
		case IOREG_OFFSET(DSC0): {
				IOREG_NOT_IMPLEMENTED_READ(DSC0);
				return IOREG_READ(DSC0);
			}
			break;
		case IOREG_OFFSET(DSC1): {
				IOREG_NOT_IMPLEMENTED_READ(DSC1);
				return IOREG_READ(DSC1);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOGPIOController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(GPIO, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(GPACON): {
				IOREG_VERIFY_WRITE(GPACON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPACON);
				IOREG_WRITE(GPACON, Value);
			}
			break;
		case IOREG_OFFSET(GPADAT): {
				IOREG_VERIFY_WRITE(GPADAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPADAT);
				IOREG_WRITE(GPADAT, Value);
			}
			break;
		case IOREG_OFFSET(GPBCON): {
				IOREG_VERIFY_WRITE(GPBCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPBCON);
				IOREG_WRITE(GPBCON, Value);
			}
			break;
		case IOREG_OFFSET(GPBDAT): {
				IOREG_VERIFY_WRITE(GPBDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPBDAT);
				IOREG_WRITE(GPBDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPBDN): {
				IOREG_VERIFY_WRITE(GPBDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPBDN);
				IOREG_WRITE(GPBDN, Value);
			}
			break;
		case IOREG_OFFSET(GPBSLPCON): {
				IOREG_VERIFY_WRITE(GPBSLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPBSLPCON);
				IOREG_WRITE(GPBSLPCON, Value);
			}
			break;
		case IOREG_OFFSET(GPCCON): {
				IOREG_VERIFY_WRITE(GPCCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPCCON);
				IOREG_WRITE(GPCCON, Value);
			}
			break;
		case IOREG_OFFSET(GPCDAT): {
				IOREG_VERIFY_WRITE(GPCDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPCDAT);
				IOREG_WRITE(GPCDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPCDN): {
				IOREG_VERIFY_WRITE(GPCDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPCDN);
				IOREG_WRITE(GPCDN, Value);
			}
			break;
		case IOREG_OFFSET(GPCSLPCON): {
				IOREG_VERIFY_WRITE(GPCSLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPCSLPCON);
				IOREG_WRITE(GPCSLPCON, Value);
			}
			break;
		case IOREG_OFFSET(GPDCON): {
				IOREG_VERIFY_WRITE(GPDCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPDCON);
				IOREG_WRITE(GPDCON, Value);
			}
			break;
		case IOREG_OFFSET(GPDDAT): {
				IOREG_VERIFY_WRITE(GPDDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPDDAT);
				IOREG_WRITE(GPDDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPDDN): {
				IOREG_VERIFY_WRITE(GPDDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPDDN);
				IOREG_WRITE(GPDDN, Value);
			}
			break;
		case IOREG_OFFSET(GPDSLPCON): {
				IOREG_VERIFY_WRITE(GPDSLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPDSLPCON);
				IOREG_WRITE(GPDSLPCON, Value);
			}
			break;
		case IOREG_OFFSET(GPECON): {
				IOREG_VERIFY_WRITE(GPECON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPECON);
				IOREG_WRITE(GPECON, Value);
			}
			break;
		case IOREG_OFFSET(GPEDAT): {
				IOREG_VERIFY_WRITE(GPEDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPEDAT);
				IOREG_WRITE(GPEDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPEDN): {
				IOREG_VERIFY_WRITE(GPEDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPEDN);
				IOREG_WRITE(GPEDN, Value);
			}
			break;
		case IOREG_OFFSET(GPESLPCON): {
				IOREG_VERIFY_WRITE(GPESLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPESLPCON);
				IOREG_WRITE(GPESLPCON, Value);
			}
			break;
		case IOREG_OFFSET(GPFCON): {
				IOREG_VERIFY_WRITE(GPFCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPFCON);
				IOREG_WRITE(GPFCON, Value);
			}
			break;
		case IOREG_OFFSET(GPFDAT): {
				IOREG_VERIFY_WRITE(GPFDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPFDAT);
				IOREG_WRITE(GPFDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPFDN): {
				IOREG_VERIFY_WRITE(GPFDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPFDN);
				IOREG_WRITE(GPFDN, Value);
			}
			break;
		case IOREG_OFFSET(GPGCON): {
				IOREG_VERIFY_WRITE(GPGCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPGCON);
				IOREG_WRITE(GPGCON, Value);
			}
			break;
		case IOREG_OFFSET(GPGDAT): {
				IOREG_VERIFY_WRITE(GPGDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPGDAT);
				IOREG_WRITE(GPGDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPGDN): {
				IOREG_VERIFY_WRITE(GPGDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPGDN);
				IOREG_WRITE(GPGDN, Value);
			}
			break;
		case IOREG_OFFSET(GPGSLPCON): {
				IOREG_VERIFY_WRITE(GPGSLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPGSLPCON);
				IOREG_WRITE(GPGSLPCON, Value);
			}
			break;
		case IOREG_OFFSET(GPHCON): {
				IOREG_VERIFY_WRITE(GPHCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPHCON);
				IOREG_WRITE(GPHCON, Value);
			}
			break;
		case IOREG_OFFSET(GPHDAT): {
				IOREG_VERIFY_WRITE(GPHDAT, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPHDAT);
				IOREG_WRITE(GPHDAT, Value);
			}
			break;
		case IOREG_OFFSET(GPHDN): {
				IOREG_VERIFY_WRITE(GPHDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPHDN);
				IOREG_WRITE(GPHDN, Value);
			}
			break;
		case IOREG_OFFSET(GPHSLPCON): {
				IOREG_VERIFY_WRITE(GPHSLPCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GPHSLPCON);
				IOREG_WRITE(GPHSLPCON, Value);
			}
			break;
		case IOREG_OFFSET(MISCCR): {
				IOREG_VERIFY_WRITE(MISCCR, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(MISCCR);
				IOREG_WRITE(MISCCR, Value);
			}
			break;
		case IOREG_OFFSET(DCLKCON): {
				IOREG_VERIFY_WRITE(DCLKCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(DCLKCON);
				IOREG_WRITE(DCLKCON, Value);
			}
			break;
		case IOREG_OFFSET(EXTINT0): {
				IOREG_VERIFY_WRITE(EXTINT0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EXTINT0);
				IOREG_WRITE(EXTINT0, Value);
			}
			break;
		case IOREG_OFFSET(EXTINT1): {
				IOREG_VERIFY_WRITE(EXTINT1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EXTINT1);
				IOREG_WRITE(EXTINT1, Value);
			}
			break;
		case IOREG_OFFSET(EXTINT2): {
				IOREG_VERIFY_WRITE(EXTINT2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EXTINT2);
				IOREG_WRITE(EXTINT2, Value);
			}
			break;
		case IOREG_OFFSET(EINTFLT2): {
				IOREG_VERIFY_WRITE(EINTFLT2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EINTFLT2);
				IOREG_WRITE(EINTFLT2, Value);
			}
			break;
		case IOREG_OFFSET(EINTFLT3): {
				IOREG_VERIFY_WRITE(EINTFLT3, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EINTFLT3);
				IOREG_WRITE(EINTFLT3, Value);
			}
			break;
		case IOREG_OFFSET(EINTMASK): {
				IOREG_VERIFY_WRITE(EINTMASK, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EINTMASK);
				IOREG_WRITE(EINTMASK, Value);
			}
			break;
		case IOREG_OFFSET(EINTPEND): {
				IOREG_VERIFY_WRITE(EINTPEND, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EINTPEND);
				IOREG_WRITE(EINTPEND, Value);
			}
			break;
		case IOREG_OFFSET(GSTATUS0): {
				IOREG_INVALID_WRITE_ACCESS(GSTATUS0);
			}
			return;
		case IOREG_OFFSET(GSTATUS1): {
				IOREG_INVALID_WRITE_ACCESS(GSTATUS1);
			}
			return;
		case IOREG_OFFSET(GSTATUS2): {
				IOREG_VERIFY_WRITE(GSTATUS2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GSTATUS2);
				IOREG_WRITE(GSTATUS2, Value);
			}
			break;
		case IOREG_OFFSET(GSTATUS3): {
				IOREG_VERIFY_WRITE(GSTATUS3, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GSTATUS3);
				IOREG_WRITE(GSTATUS3, Value);
			}
			break;
		case IOREG_OFFSET(GSTATUS4): {
				IOREG_VERIFY_WRITE(GSTATUS4, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GSTATUS4);
				IOREG_WRITE(GSTATUS4, Value);
			}
			break;
		case IOREG_OFFSET(GSTATUS5): {
				IOREG_VERIFY_WRITE(GSTATUS5, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(GSTATUS5);
				IOREG_WRITE(GSTATUS5, Value);
			}
			break;
		case IOREG_OFFSET(MSTCON): {
				IOREG_VERIFY_WRITE(MSTCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(MSTCON);
				IOREG_WRITE(MSTCON, Value);
			}
			break;
		case IOREG_OFFSET(MSLCON): {
				IOREG_VERIFY_WRITE(MSLCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(MSLCON);
				IOREG_WRITE(MSLCON, Value);
			}
			break;
		case IOREG_OFFSET(DSC0): {
				IOREG_VERIFY_WRITE(DSC0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(DSC0);
				IOREG_WRITE(DSC0, Value);
			}
			break;
		case IOREG_OFFSET(DSC1): {
				IOREG_VERIFY_WRITE(DSC1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(DSC1);
				IOREG_WRITE(DSC1, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
>>>>>>> soc-editor
