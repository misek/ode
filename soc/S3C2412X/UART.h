#ifndef __UART_HEADER_INCLUDED__
#define __UART_HEADER_INCLUDED__

#include <soc.h>
#include "S3C2412Xtypes.h"

namespace S3C2412X
{

/*
 * IOUART declaration
 */
class IOUART: public SOCIODevice
{
private:

protected:
	UART_Registers reg;

public:
	IOUART(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint8_t ReadByte(uint32_t IOAddress);
	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteByte(uint32_t IOAddress, uint8_t Value);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

/*
 * IOUART0 declaration
 */
class IOUART0: public IOUART
{
private:

protected:

public:
	IOUART0(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint8_t ReadByte(uint32_t IOAddress);
	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteByte(uint32_t IOAddress, uint8_t Value);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

/*
 * IOUART1 declaration
 */
class IOUART1: public IOUART
{
private:

protected:

public:
	IOUART1(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint8_t ReadByte(uint32_t IOAddress);
	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteByte(uint32_t IOAddress, uint8_t Value);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

/*
 * IOUART2 declaration
 */
class IOUART2: public IOUART
{
private:

protected:

public:
	IOUART2(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint8_t ReadByte(uint32_t IOAddress);
	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteByte(uint32_t IOAddress, uint8_t Value);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

} /* S3C2412X namespace */

#endif /* __UART_HEADER_INCLUDED__ */
