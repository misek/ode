#include "S3C2412X.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

S3C2412X::S3C2412X(): SOCBase()
{
	INIT_SOC_CORE(ARM_CPU_ARM926EJS);

	// initialize S3C2412X devices
	INIT_SOC_DEVICE(DRAMController);
	INIT_SOC_DEVICE(EBI);
	INIT_SOC_DEVICE(USBHost);
	INIT_SOC_DEVICE(InterruptController);
	INIT_SOC_DEVICE(DMA0);
	INIT_SOC_DEVICE(DMA1);
	INIT_SOC_DEVICE(DMA2);
	INIT_SOC_DEVICE(DMA3);
	INIT_SOC_DEVICE(ATA);
	INIT_SOC_DEVICE(SystemController);
	INIT_SOC_DEVICE(LCD);
	INIT_SOC_DEVICE(NANDFlash);
	INIT_SOC_DEVICE(SSMC);
	INIT_SOC_DEVICE(UART0);
	INIT_SOC_DEVICE(UART1);
	INIT_SOC_DEVICE(UART2);
	INIT_SOC_DEVICE(PWM);
	INIT_SOC_DEVICE(USBDevice);
	INIT_SOC_DEVICE(Watchdog);
	INIT_SOC_DEVICE(IIC);
	INIT_SOC_DEVICE(IIS);
	INIT_SOC_DEVICE(GPIO);
	INIT_SOC_DEVICE(RTC);
	INIT_SOC_DEVICE(ADC);
	INIT_SOC_DEVICE(SPI0);
	INIT_SOC_DEVICE(SPI1);
	INIT_SOC_DEVICE(SDMMC);
}

bool S3C2412X::PowerOn()
{
	if (!powered_on) {
		if (SOCBase::PowerOn()) {
			Core->PowerOn();
		}
	}
	return powered_on;
}

bool S3C2412X::PowerOff()
{
	if (powered_on) {
		if (SOCBase::PowerOff()) {
			Core->PowerOff();
		}
	}
	return !powered_on;
}

bool S3C2412X::Reset()
{
	Core->Reset();
	return SOCBase::Reset();
}

void S3C2412X::AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index)
{
	switch (type) {
		case EMULATED_RAM:
			{
				RAMMemory* ram = dynamic_cast<RAMMemory*>(device);
				Core->AddPhysicalMemory(ram->GetRAW(), 0x30000000, ram->GetSize());
			}
			break;
		default:
			SOCBase::AttachDevice(device, type, index);
			break;
	}
}

} /* S3C2412X namespace */
