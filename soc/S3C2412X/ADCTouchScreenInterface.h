#ifndef __ADCTOUCHSCREENINTERFACE_HEADER_INCLUDED__
#define __ADCTOUCHSCREENINTERFACE_HEADER_INCLUDED__

#include <soc.h>
#include "S3C2412Xtypes.h"

namespace S3C2412X
{

/*
 * IOADCTouchScreenInterface declaration
 */
class IOADCTouchScreenInterface: public SOCIODevice
{
private:

protected:
	ADC_Registers reg;

public:
	IOADCTouchScreenInterface(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

} /* S3C2412X namespace */

#endif /* __ADCTOUCHSCREENINTERFACE_HEADER_INCLUDED__ */
