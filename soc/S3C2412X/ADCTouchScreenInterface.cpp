#include "ADCTouchScreenInterface.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOADCTouchScreenInterface implementation
 */
IOADCTouchScreenInterface::IOADCTouchScreenInterface(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOADCTouchScreenInterface::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOADCTouchScreenInterface::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOADCTouchScreenInterface::Reset()
{
	return SOCIODevice::Reset();
}

void IOADCTouchScreenInterface::ResetRegisters()
{
	IOREG_RESET(ADCCON);
	IOREG_RESET(ADCTSC);
	IOREG_RESET(ADCDLY);
	IOREG_RESET(ADCUPDN);
	SOCIODevice::ResetRegisters();
}

uint32_t IOADCTouchScreenInterface::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(ADC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ADCCON): {
				IOREG_NOT_IMPLEMENTED_READ(ADCCON);
				return IOREG_READ(ADCCON);
			}
			break;
		case IOREG_OFFSET(ADCTSC): {
				IOREG_NOT_IMPLEMENTED_READ(ADCTSC);
				return IOREG_READ(ADCTSC);
			}
			break;
		case IOREG_OFFSET(ADCDLY): {
				IOREG_NOT_IMPLEMENTED_READ(ADCDLY);
				return IOREG_READ(ADCDLY);
			}
			break;
		case IOREG_OFFSET(ADCDAT0): {
				IOREG_NOT_IMPLEMENTED_READ(ADCDAT0);
				return IOREG_READ(ADCDAT0);
			}
			break;
		case IOREG_OFFSET(ADCDAT1): {
				IOREG_NOT_IMPLEMENTED_READ(ADCDAT1);
				return IOREG_READ(ADCDAT1);
			}
			break;
		case IOREG_OFFSET(ADCUPDN): {
				IOREG_NOT_IMPLEMENTED_READ(ADCUPDN);
				return IOREG_READ(ADCUPDN);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOADCTouchScreenInterface::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(ADC, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ADCCON): {
				IOREG_VERIFY_WRITE(ADCCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ADCCON);
				IOREG_WRITE(ADCCON, Value);
			}
			break;
		case IOREG_OFFSET(ADCTSC): {
				IOREG_VERIFY_WRITE(ADCTSC, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ADCTSC);
				IOREG_WRITE(ADCTSC, Value);
			}
			break;
		case IOREG_OFFSET(ADCDLY): {
				IOREG_VERIFY_WRITE(ADCDLY, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ADCDLY);
				IOREG_WRITE(ADCDLY, Value);
			}
			break;
		case IOREG_OFFSET(ADCDAT0): {
				IOREG_INVALID_WRITE_ACCESS(ADCDAT0);
			}
			return;
		case IOREG_OFFSET(ADCDAT1): {
				IOREG_INVALID_WRITE_ACCESS(ADCDAT1);
			}
			return;
		case IOREG_OFFSET(ADCUPDN): {
				IOREG_VERIFY_WRITE(ADCUPDN, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ADCUPDN);
				IOREG_WRITE(ADCUPDN, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
