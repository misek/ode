#ifndef __NANDFLASHCONTROLLER_HEADER_INCLUDED__
#define __NANDFLASHCONTROLLER_HEADER_INCLUDED__

#include <soc.h>
#include "S3C2412Xtypes.h"

namespace S3C2412X
{

/*
 * IONANDFlashController declaration
 */
class IONANDFlashController: public SOCIODevice
{
private:

protected:
	NANDFlash_Registers reg;

public:
	IONANDFlashController(SOCBase* soc);
	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);
};

} /* S3C2412X namespace */

#endif /* __NANDFLASHCONTROLLER_HEADER_INCLUDED__ */
