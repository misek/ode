#include "UART.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOUART implementation
 */
IOUART::IOUART(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOUART::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOUART::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOUART::Reset()
{
	return SOCIODevice::Reset();
}

void IOUART::ResetRegisters()
{
	SOCIODevice::ResetRegisters();
}

uint8_t IOUART::ReadByte(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): {
				IOREG_NOT_IMPLEMENTED_READ(ULCON);
				return IOREG_BASE_READ(ULCON);
			}
			break;
		case IOREG_OFFSET(UCON): {
				IOREG_NOT_IMPLEMENTED_READ(UCON);
				return IOREG_BASE_READ(UCON);
			}
			break;
		case IOREG_OFFSET(UFCON): {
				IOREG_NOT_IMPLEMENTED_READ(UFCON);
				return IOREG_BASE_READ(UFCON);
			}
			break;
		case IOREG_OFFSET(UMCON): {
				IOREG_NOT_IMPLEMENTED_READ(UMCON);
				return IOREG_BASE_READ(UMCON);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT);
				return IOREG_BASE_READ(UTRSTAT);
			}
			break;
		case IOREG_OFFSET(UERSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT);
				return IOREG_BASE_READ(UERSTAT);
			}
			break;
		case IOREG_OFFSET(UFSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT);
				return IOREG_BASE_READ(UFSTAT);
			}
			break;
		case IOREG_OFFSET(UMSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT);
				return IOREG_BASE_READ(UMSTAT);
			}
			break;
		case IOREG_OFFSET(UTXH): {
				IOREG_INVALID_READ_ACCESS(UTXH);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): {
				IOREG_NOT_IMPLEMENTED_READ(URXH);
				return IOREG_BASE_READ(URXH);
			}
			break;
		case IOREG_OFFSET(UBRDIV): {
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV);
				return IOREG_BASE_READ(UBRDIV);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): {
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT);
				return IOREG_BASE_READ(UDIVSLOT);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadByte(IOAddress);
}

uint32_t IOUART::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): {
				IOREG_NOT_IMPLEMENTED_READ(ULCON);
				return IOREG_BASE_READ(ULCON);
			}
			break;
		case IOREG_OFFSET(UCON): {
				IOREG_NOT_IMPLEMENTED_READ(UCON);
				return IOREG_BASE_READ(UCON);
			}
			break;
		case IOREG_OFFSET(UFCON): {
				IOREG_NOT_IMPLEMENTED_READ(UFCON);
				return IOREG_BASE_READ(UFCON);
			}
			break;
		case IOREG_OFFSET(UMCON): {
				IOREG_NOT_IMPLEMENTED_READ(UMCON);
				return IOREG_BASE_READ(UMCON);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT);
				return IOREG_BASE_READ(UTRSTAT);
			}
			break;
		case IOREG_OFFSET(UERSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT);
				return IOREG_BASE_READ(UERSTAT);
			}
			break;
		case IOREG_OFFSET(UFSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT);
				return IOREG_BASE_READ(UFSTAT);
			}
			break;
		case IOREG_OFFSET(UMSTAT): {
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT);
				return IOREG_BASE_READ(UMSTAT);
			}
			break;
		case IOREG_OFFSET(UTXH): {
				IOREG_INVALID_READ_ACCESS(UTXH);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): {
				IOREG_NOT_IMPLEMENTED_READ(URXH);
				return IOREG_BASE_READ(URXH);
			}
			break;
		case IOREG_OFFSET(UBRDIV): {
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV);
				return IOREG_BASE_READ(UBRDIV);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): {
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT);
				return IOREG_BASE_READ(UDIVSLOT);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOUART::WriteByte(uint32_t IOAddress, uint8_t Value)
{
	IOREG_BEGIN_WRITE(UART, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON);
				IOREG_BASE_WRITE(ULCON, Value);
			}
			break;
		case IOREG_OFFSET(UCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UCON);
				IOREG_BASE_WRITE(UCON, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON);
				IOREG_BASE_WRITE(UFCON, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON);
				IOREG_BASE_WRITE(UMCON, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT);
			}
			return;
		case IOREG_OFFSET(UERSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UERSTAT);
			}
			return;
		case IOREG_OFFSET(UFSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UFSTAT);
			}
			return;
		case IOREG_OFFSET(UMSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UMSTAT);
			}
			return;
		case IOREG_OFFSET(UTXH): {
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH);
				IOREG_BASE_WRITE(UTXH, Value);
			}
			break;
		case IOREG_OFFSET(URXH): {
				IOREG_INVALID_WRITE_ACCESS(URXH);
			}
			return;
		case IOREG_OFFSET(UBRDIV): {
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV);
				IOREG_BASE_WRITE(UBRDIV, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): {
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT);
				IOREG_BASE_WRITE(UDIVSLOT, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteByte(IOAddress, Value);
}

void IOUART::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(UART, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON);
				IOREG_BASE_WRITE(ULCON, Value);
			}
			break;
		case IOREG_OFFSET(UCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UCON);
				IOREG_BASE_WRITE(UCON, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON);
				IOREG_BASE_WRITE(UFCON, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): {
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON);
				IOREG_BASE_WRITE(UMCON, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT);
			}
			return;
		case IOREG_OFFSET(UERSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UERSTAT);
			}
			return;
		case IOREG_OFFSET(UFSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UFSTAT);
			}
			return;
		case IOREG_OFFSET(UMSTAT): {
				IOREG_INVALID_WRITE_ACCESS(UMSTAT);
			}
			return;
		case IOREG_OFFSET(UTXH): {
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH);
				IOREG_BASE_WRITE(UTXH, Value);
			}
			break;
		case IOREG_OFFSET(URXH): {
				IOREG_INVALID_WRITE_ACCESS(URXH);
			}
			return;
		case IOREG_OFFSET(UBRDIV): {
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV);
				IOREG_BASE_WRITE(UBRDIV, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): {
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT);
				IOREG_BASE_WRITE(UDIVSLOT, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

/*
 * IOUART0 implementation
 */
IOUART0::IOUART0(SOCBase* soc): IOUART(soc)
{

}

bool IOUART0::PowerOn()
{
	return IOUART::PowerOn();
}

bool IOUART0::PowerOff()
{
	return IOUART::PowerOff();
}

bool IOUART0::Reset()
{
	return IOUART::Reset();
}

void IOUART0::ResetRegisters()
{
	IOREG_RESET(ULCON0);
	IOREG_RESET(UCON0);
	IOREG_RESET(UFCON0);
	IOREG_RESET(UMCON0);
	IOREG_RESET(UTRSTAT0);
	IOREG_RESET(UERSTAT0);
	IOREG_RESET(UFSTAT0);
	IOREG_RESET(UMSTAT0);
	IOREG_RESET(UTXH0);
	IOREG_RESET(URXH0);
	IOREG_RESET(UBRDIV0);
	IOREG_RESET(UDIVSLOT0);
	IOUART::ResetRegisters();
}

uint8_t IOUART0::ReadByte(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART0, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON0
				IOREG_NOT_IMPLEMENTED_READ(ULCON0);
				return IOREG_READ(ULCON0);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON0
				IOREG_NOT_IMPLEMENTED_READ(UCON0);
				return IOREG_READ(UCON0);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON0
				IOREG_NOT_IMPLEMENTED_READ(UFCON0);
				return IOREG_READ(UFCON0);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON0
				IOREG_NOT_IMPLEMENTED_READ(UMCON0);
				return IOREG_READ(UMCON0);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT0);
				return IOREG_READ(UTRSTAT0);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT0);
				return IOREG_READ(UERSTAT0);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT0);
				return IOREG_READ(UFSTAT0);
			}
			break;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT0);
				return IOREG_READ(UMSTAT0);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH0
				IOREG_INVALID_READ_ACCESS(UTXH0);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH0
				IOREG_NOT_IMPLEMENTED_READ(URXH0);
				return IOREG_READ(URXH0);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV0
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV0);
				return IOREG_READ(UBRDIV0);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT0
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT0);
				return IOREG_READ(UDIVSLOT0);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadByte(IOAddress);
}

uint32_t IOUART0::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART0, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON0
				IOREG_NOT_IMPLEMENTED_READ(ULCON0);
				return IOREG_READ(ULCON0);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON0
				IOREG_NOT_IMPLEMENTED_READ(UCON0);
				return IOREG_READ(UCON0);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON0
				IOREG_NOT_IMPLEMENTED_READ(UFCON0);
				return IOREG_READ(UFCON0);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON0
				IOREG_NOT_IMPLEMENTED_READ(UMCON0);
				return IOREG_READ(UMCON0);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT0);
				return IOREG_READ(UTRSTAT0);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT0);
				return IOREG_READ(UERSTAT0);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT0);
				return IOREG_READ(UFSTAT0);
			}
			break;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT0
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT0);
				return IOREG_READ(UMSTAT0);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH0
				IOREG_INVALID_READ_ACCESS(UTXH0);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH0
				IOREG_NOT_IMPLEMENTED_READ(URXH0);
				return IOREG_READ(URXH0);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV0
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV0);
				return IOREG_READ(UBRDIV0);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT0
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT0);
				return IOREG_READ(UDIVSLOT0);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadWord(IOAddress);
}

void IOUART0::WriteByte(uint32_t IOAddress, uint8_t Value)
{
	IOREG_BEGIN_WRITE(UART0, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON0
				IOREG_VERIFY_WRITE(ULCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON0);
				IOREG_WRITE(ULCON0, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON0
				IOREG_VERIFY_WRITE(UCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON0);
				IOREG_WRITE(UCON0, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON0
				IOREG_VERIFY_WRITE(UFCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON0);
				IOREG_WRITE(UFCON0, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON0
				IOREG_VERIFY_WRITE(UMCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON0);
				IOREG_WRITE(UMCON0, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT0
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT0);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT0
				IOREG_INVALID_WRITE_ACCESS(UERSTAT0);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT0
				IOREG_INVALID_WRITE_ACCESS(UFSTAT0);
			}
			return;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT0
				IOREG_INVALID_WRITE_ACCESS(UMSTAT0);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH0
				IOREG_VERIFY_WRITE(UTXH0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH0);
				IOREG_WRITE(UTXH0, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH0
				IOREG_INVALID_WRITE_ACCESS(URXH0);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV0
				IOREG_VERIFY_WRITE(UBRDIV0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV0);
				IOREG_WRITE(UBRDIV0, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT0
				IOREG_VERIFY_WRITE(UDIVSLOT0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT0);
				IOREG_WRITE(UDIVSLOT0, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteByte(IOAddress, Value);
}

void IOUART0::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(UART0, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON0
				IOREG_VERIFY_WRITE(ULCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON0);
				IOREG_WRITE(ULCON0, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON0
				IOREG_VERIFY_WRITE(UCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON0);
				IOREG_WRITE(UCON0, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON0
				IOREG_VERIFY_WRITE(UFCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON0);
				IOREG_WRITE(UFCON0, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON0
				IOREG_VERIFY_WRITE(UMCON0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON0);
				IOREG_WRITE(UMCON0, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT0
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT0);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT0
				IOREG_INVALID_WRITE_ACCESS(UERSTAT0);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT0
				IOREG_INVALID_WRITE_ACCESS(UFSTAT0);
			}
			return;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT0
				IOREG_INVALID_WRITE_ACCESS(UMSTAT0);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH0
				IOREG_VERIFY_WRITE(UTXH0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH0);
				IOREG_WRITE(UTXH0, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH0
				IOREG_INVALID_WRITE_ACCESS(URXH0);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV0
				IOREG_VERIFY_WRITE(UBRDIV0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV0);
				IOREG_WRITE(UBRDIV0, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT0
				IOREG_VERIFY_WRITE(UDIVSLOT0, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT0);
				IOREG_WRITE(UDIVSLOT0, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteWord(IOAddress, Value);
}

/*
 * IOUART1 implementation
 */
IOUART1::IOUART1(SOCBase* soc): IOUART(soc)
{

}

bool IOUART1::PowerOn()
{
	return IOUART::PowerOn();
}

bool IOUART1::PowerOff()
{
	return IOUART::PowerOff();
}

bool IOUART1::Reset()
{
	return IOUART::Reset();
}

void IOUART1::ResetRegisters()
{
	IOREG_RESET(ULCON1);
	IOREG_RESET(UCON1);
	IOREG_RESET(UFCON1);
	IOREG_RESET(UMCON1);
	IOREG_RESET(UTRSTAT1);
	IOREG_RESET(UERSTAT1);
	IOREG_RESET(UFSTAT1);
	IOREG_RESET(UMSTAT1);
	IOREG_RESET(UTXH1);
	IOREG_RESET(URXH1);
	IOREG_RESET(UBRDIV1);
	IOREG_RESET(UDIVSLOT1);
	IOUART::ResetRegisters();
}

uint8_t IOUART1::ReadByte(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART1, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON1
				IOREG_NOT_IMPLEMENTED_READ(ULCON1);
				return IOREG_READ(ULCON1);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON1
				IOREG_NOT_IMPLEMENTED_READ(UCON1);
				return IOREG_READ(UCON1);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON1
				IOREG_NOT_IMPLEMENTED_READ(UFCON1);
				return IOREG_READ(UFCON1);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON1
				IOREG_NOT_IMPLEMENTED_READ(UMCON1);
				return IOREG_READ(UMCON1);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT1);
				return IOREG_READ(UTRSTAT1);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT1);
				return IOREG_READ(UERSTAT1);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT1);
				return IOREG_READ(UFSTAT1);
			}
			break;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT1);
				return IOREG_READ(UMSTAT1);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH1
				IOREG_INVALID_READ_ACCESS(UTXH1);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH1
				IOREG_NOT_IMPLEMENTED_READ(URXH1);
				return IOREG_READ(URXH1);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV1
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV1);
				return IOREG_READ(UBRDIV1);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT1
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT1);
				return IOREG_READ(UDIVSLOT1);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadByte(IOAddress);
}

uint32_t IOUART1::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART1, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON1
				IOREG_NOT_IMPLEMENTED_READ(ULCON1);
				return IOREG_READ(ULCON1);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON1
				IOREG_NOT_IMPLEMENTED_READ(UCON1);
				return IOREG_READ(UCON1);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON1
				IOREG_NOT_IMPLEMENTED_READ(UFCON1);
				return IOREG_READ(UFCON1);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON1
				IOREG_NOT_IMPLEMENTED_READ(UMCON1);
				return IOREG_READ(UMCON1);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT1);
				return IOREG_READ(UTRSTAT1);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT1);
				return IOREG_READ(UERSTAT1);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT1);
				return IOREG_READ(UFSTAT1);
			}
			break;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT1
				IOREG_NOT_IMPLEMENTED_READ(UMSTAT1);
				return IOREG_READ(UMSTAT1);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH1
				IOREG_INVALID_READ_ACCESS(UTXH1);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH1
				IOREG_NOT_IMPLEMENTED_READ(URXH1);
				return IOREG_READ(URXH1);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV1
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV1);
				return IOREG_READ(UBRDIV1);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT1
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT1);
				return IOREG_READ(UDIVSLOT1);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadWord(IOAddress);
}

void IOUART1::WriteByte(uint32_t IOAddress, uint8_t Value)
{
	IOREG_BEGIN_WRITE(UART1, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON1
				IOREG_VERIFY_WRITE(ULCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON1);
				IOREG_WRITE(ULCON1, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON1
				IOREG_VERIFY_WRITE(UCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON1);
				IOREG_WRITE(UCON1, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON1
				IOREG_VERIFY_WRITE(UFCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON1);
				IOREG_WRITE(UFCON1, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON1
				IOREG_VERIFY_WRITE(UMCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON1);
				IOREG_WRITE(UMCON1, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT1
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT1);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT1
				IOREG_INVALID_WRITE_ACCESS(UERSTAT1);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT1
				IOREG_INVALID_WRITE_ACCESS(UFSTAT1);
			}
			return;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT1
				IOREG_INVALID_WRITE_ACCESS(UMSTAT1);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH1
				IOREG_VERIFY_WRITE(UTXH1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH1);
				IOREG_WRITE(UTXH1, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH1
				IOREG_INVALID_WRITE_ACCESS(URXH1);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV1
				IOREG_VERIFY_WRITE(UBRDIV1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV1);
				IOREG_WRITE(UBRDIV1, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT1
				IOREG_VERIFY_WRITE(UDIVSLOT1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT1);
				IOREG_WRITE(UDIVSLOT1, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteByte(IOAddress, Value);
}

void IOUART1::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(UART1, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON1
				IOREG_VERIFY_WRITE(ULCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON1);
				IOREG_WRITE(ULCON1, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON1
				IOREG_VERIFY_WRITE(UCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON1);
				IOREG_WRITE(UCON1, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON1
				IOREG_VERIFY_WRITE(UFCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON1);
				IOREG_WRITE(UFCON1, Value);
			}
			break;
		case IOREG_OFFSET(UMCON): { // UMCON1
				IOREG_VERIFY_WRITE(UMCON1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UMCON1);
				IOREG_WRITE(UMCON1, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT1
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT1);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT1
				IOREG_INVALID_WRITE_ACCESS(UERSTAT1);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT1
				IOREG_INVALID_WRITE_ACCESS(UFSTAT1);
			}
			return;
		case IOREG_OFFSET(UMSTAT): { // UMSTAT1
				IOREG_INVALID_WRITE_ACCESS(UMSTAT1);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH1
				IOREG_VERIFY_WRITE(UTXH1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH1);
				IOREG_WRITE(UTXH1, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH1
				IOREG_INVALID_WRITE_ACCESS(URXH1);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV1
				IOREG_VERIFY_WRITE(UBRDIV1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV1);
				IOREG_WRITE(UBRDIV1, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT1
				IOREG_VERIFY_WRITE(UDIVSLOT1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT1);
				IOREG_WRITE(UDIVSLOT1, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteWord(IOAddress, Value);
}

/*
 * IOUART2 implementation
 */
IOUART2::IOUART2(SOCBase* soc): IOUART(soc)
{

}

bool IOUART2::PowerOn()
{
	return IOUART::PowerOn();
}

bool IOUART2::PowerOff()
{
	return IOUART::PowerOff();
}

bool IOUART2::Reset()
{
	return IOUART::Reset();
}

void IOUART2::ResetRegisters()
{
	IOREG_RESET(ULCON2);
	IOREG_RESET(UCON2);
	IOREG_RESET(UFCON2);
	IOREG_RESET(UTRSTAT2);
	IOREG_RESET(UERSTAT2);
	IOREG_RESET(UFSTAT2);
	IOREG_RESET(UTXH2);
	IOREG_RESET(URXH2);
	IOREG_RESET(UBRDIV2);
	IOREG_RESET(UDIVSLOT2);
	IOUART::ResetRegisters();
}

uint8_t IOUART2::ReadByte(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART2, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON2
				IOREG_NOT_IMPLEMENTED_READ(ULCON2);
				return IOREG_READ(ULCON2);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON2
				IOREG_NOT_IMPLEMENTED_READ(UCON2);
				return IOREG_READ(UCON2);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON2
				IOREG_NOT_IMPLEMENTED_READ(UFCON2);
				return IOREG_READ(UFCON2);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT2);
				return IOREG_READ(UTRSTAT2);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT2);
				return IOREG_READ(UERSTAT2);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT2);
				return IOREG_READ(UFSTAT2);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH2
				IOREG_INVALID_READ_ACCESS(UTXH2);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH2
				IOREG_NOT_IMPLEMENTED_READ(URXH2);
				return IOREG_READ(URXH2);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV2
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV2);
				return IOREG_READ(UBRDIV2);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT2
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT2);
				return IOREG_READ(UDIVSLOT2);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadByte(IOAddress);
}

uint32_t IOUART2::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(UART2, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON2
				IOREG_NOT_IMPLEMENTED_READ(ULCON2);
				return IOREG_READ(ULCON2);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON2
				IOREG_NOT_IMPLEMENTED_READ(UCON2);
				return IOREG_READ(UCON2);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON2
				IOREG_NOT_IMPLEMENTED_READ(UFCON2);
				return IOREG_READ(UFCON2);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UTRSTAT2);
				return IOREG_READ(UTRSTAT2);
			}
			break;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UERSTAT2);
				return IOREG_READ(UERSTAT2);
			}
			break;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT2
				IOREG_NOT_IMPLEMENTED_READ(UFSTAT2);
				return IOREG_READ(UFSTAT2);
			}
			break;
		case IOREG_OFFSET(UTXH): { // UTXH2
				IOREG_INVALID_READ_ACCESS(UTXH2);
				return 0;
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH2
				IOREG_NOT_IMPLEMENTED_READ(URXH2);
				return IOREG_READ(URXH2);
			}
			break;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV2
				IOREG_NOT_IMPLEMENTED_READ(UBRDIV2);
				return IOREG_READ(UBRDIV2);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT2
				IOREG_NOT_IMPLEMENTED_READ(UDIVSLOT2);
				return IOREG_READ(UDIVSLOT2);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return IOUART::ReadWord(IOAddress);
}

void IOUART2::WriteByte(uint32_t IOAddress, uint8_t Value)
{
	IOREG_BEGIN_WRITE(UART2, BYTE);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON2
				IOREG_VERIFY_WRITE(ULCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON2);
				IOREG_WRITE(ULCON2, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON2
				IOREG_VERIFY_WRITE(UCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON2);
				IOREG_WRITE(UCON2, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON2
				IOREG_VERIFY_WRITE(UFCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON2);
				IOREG_WRITE(UFCON2, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT2
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT2);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT2
				IOREG_INVALID_WRITE_ACCESS(UERSTAT2);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT2
				IOREG_INVALID_WRITE_ACCESS(UFSTAT2);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH2
				IOREG_VERIFY_WRITE(UTXH2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH2);
				IOREG_WRITE(UTXH2, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH2
				IOREG_INVALID_WRITE_ACCESS(URXH2);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV2
				IOREG_VERIFY_WRITE(UBRDIV2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV2);
				IOREG_WRITE(UBRDIV2, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT2
				IOREG_VERIFY_WRITE(UDIVSLOT2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT2);
				IOREG_WRITE(UDIVSLOT2, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteByte(IOAddress, Value);
}

void IOUART2::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(UART2, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(ULCON): { // ULCON2
				IOREG_VERIFY_WRITE(ULCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(ULCON2);
				IOREG_WRITE(ULCON2, Value);
			}
			break;
		case IOREG_OFFSET(UCON): { // UCON2
				IOREG_VERIFY_WRITE(UCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UCON2);
				IOREG_WRITE(UCON2, Value);
			}
			break;
		case IOREG_OFFSET(UFCON): { // UFCON2
				IOREG_VERIFY_WRITE(UFCON2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UFCON2);
				IOREG_WRITE(UFCON2, Value);
			}
			break;
		case IOREG_OFFSET(UTRSTAT): { // UTRSTAT2
				IOREG_INVALID_WRITE_ACCESS(UTRSTAT2);
			}
			return;
		case IOREG_OFFSET(UERSTAT): { // UERSTAT2
				IOREG_INVALID_WRITE_ACCESS(UERSTAT2);
			}
			return;
		case IOREG_OFFSET(UFSTAT): { // UFSTAT2
				IOREG_INVALID_WRITE_ACCESS(UFSTAT2);
			}
			return;
		case IOREG_OFFSET(UTXH): { // UTXH2
				IOREG_VERIFY_WRITE(UTXH2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UTXH2);
				IOREG_WRITE(UTXH2, Value);
			}
			break;
		case IOREG_OFFSET(URXH): { // URXH2
				IOREG_INVALID_WRITE_ACCESS(URXH2);
			}
			return;
		case IOREG_OFFSET(UBRDIV): { // UBRDIV2
				IOREG_VERIFY_WRITE(UBRDIV2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UBRDIV2);
				IOREG_WRITE(UBRDIV2, Value);
			}
			break;
		case IOREG_OFFSET(UDIVSLOT): { // UDIVSLOT2
				IOREG_VERIFY_WRITE(UDIVSLOT2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(UDIVSLOT2);
				IOREG_WRITE(UDIVSLOT2, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	IOUART::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
