#include "USBHostController.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOUSBHostController implementation
 */
IOUSBHostController::IOUSBHostController(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOUSBHostController::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOUSBHostController::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOUSBHostController::Reset()
{
	return SOCIODevice::Reset();
}

void IOUSBHostController::ResetRegisters()
{
	IOREG_RESET(HcRevision);
	IOREG_RESET(HcControl);
	IOREG_RESET(HcCommandStatus);
	IOREG_RESET(HcInterruptStatus);
	IOREG_RESET(HcInterruptEnable);
	IOREG_RESET(HcInterruptDisable);
	IOREG_RESET(HcHCCA);
	IOREG_RESET(HcPeriodCurrentED);
	IOREG_RESET(HcControlHeadED);
	IOREG_RESET(HcControlCurrentED);
	IOREG_RESET(HcBulkHeadED);
	IOREG_RESET(HcBulkCurrentED);
	IOREG_RESET(HcDoneHead);
	IOREG_RESET(HcFmInterval);
	IOREG_RESET(HcFmRemaining);
	IOREG_RESET(HcFmNumber);
	IOREG_RESET(HcPeriodicStart);
	IOREG_RESET(HcLSThreshold);
	IOREG_RESET(HcRhDescriptorA);
	IOREG_RESET(HcRhDescriptorB);
	IOREG_RESET(HcRhStatus);
	IOREG_RESET(HcPortStatus1);
	IOREG_RESET(HcPortStatus2);
	SOCIODevice::ResetRegisters();
}

uint32_t IOUSBHostController::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(USBHost, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(HcRevision): {
				IOREG_NOT_IMPLEMENTED_READ(HcRevision);
				return IOREG_READ(HcRevision);
			}
			break;
		case IOREG_OFFSET(HcControl): {
				IOREG_NOT_IMPLEMENTED_READ(HcControl);
				return IOREG_READ(HcControl);
			}
			break;
		case IOREG_OFFSET(HcCommandStatus): {
				IOREG_NOT_IMPLEMENTED_READ(HcCommandStatus);
				return IOREG_READ(HcCommandStatus);
			}
			break;
		case IOREG_OFFSET(HcInterruptStatus): {
				IOREG_NOT_IMPLEMENTED_READ(HcInterruptStatus);
				return IOREG_READ(HcInterruptStatus);
			}
			break;
		case IOREG_OFFSET(HcInterruptEnable): {
				IOREG_NOT_IMPLEMENTED_READ(HcInterruptEnable);
				return IOREG_READ(HcInterruptEnable);
			}
			break;
		case IOREG_OFFSET(HcInterruptDisable): {
				IOREG_NOT_IMPLEMENTED_READ(HcInterruptDisable);
				return IOREG_READ(HcInterruptDisable);
			}
			break;
		case IOREG_OFFSET(HcHCCA): {
				IOREG_NOT_IMPLEMENTED_READ(HcHCCA);
				return IOREG_READ(HcHCCA);
			}
			break;
		case IOREG_OFFSET(HcPeriodCurrentED): {
				IOREG_NOT_IMPLEMENTED_READ(HcPeriodCurrentED);
				return IOREG_READ(HcPeriodCurrentED);
			}
			break;
		case IOREG_OFFSET(HcControlHeadED): {
				IOREG_NOT_IMPLEMENTED_READ(HcControlHeadED);
				return IOREG_READ(HcControlHeadED);
			}
			break;
		case IOREG_OFFSET(HcControlCurrentED): {
				IOREG_NOT_IMPLEMENTED_READ(HcControlCurrentED);
				return IOREG_READ(HcControlCurrentED);
			}
			break;
		case IOREG_OFFSET(HcBulkHeadED): {
				IOREG_NOT_IMPLEMENTED_READ(HcBulkHeadED);
				return IOREG_READ(HcBulkHeadED);
			}
			break;
		case IOREG_OFFSET(HcBulkCurrentED): {
				IOREG_NOT_IMPLEMENTED_READ(HcBulkCurrentED);
				return IOREG_READ(HcBulkCurrentED);
			}
			break;
		case IOREG_OFFSET(HcDoneHead): {
				IOREG_NOT_IMPLEMENTED_READ(HcDoneHead);
				return IOREG_READ(HcDoneHead);
			}
			break;
		case IOREG_OFFSET(HcFmInterval): {
				IOREG_NOT_IMPLEMENTED_READ(HcFmInterval);
				return IOREG_READ(HcFmInterval);
			}
			break;
		case IOREG_OFFSET(HcFmRemaining): {
				IOREG_NOT_IMPLEMENTED_READ(HcFmRemaining);
				return IOREG_READ(HcFmRemaining);
			}
			break;
		case IOREG_OFFSET(HcFmNumber): {
				IOREG_NOT_IMPLEMENTED_READ(HcFmNumber);
				return IOREG_READ(HcFmNumber);
			}
			break;
		case IOREG_OFFSET(HcPeriodicStart): {
				IOREG_NOT_IMPLEMENTED_READ(HcPeriodicStart);
				return IOREG_READ(HcPeriodicStart);
			}
			break;
		case IOREG_OFFSET(HcLSThreshold): {
				IOREG_NOT_IMPLEMENTED_READ(HcLSThreshold);
				return IOREG_READ(HcLSThreshold);
			}
			break;
		case IOREG_OFFSET(HcRhDescriptorA): {
				IOREG_NOT_IMPLEMENTED_READ(HcRhDescriptorA);
				return IOREG_READ(HcRhDescriptorA);
			}
			break;
		case IOREG_OFFSET(HcRhDescriptorB): {
				IOREG_NOT_IMPLEMENTED_READ(HcRhDescriptorB);
				return IOREG_READ(HcRhDescriptorB);
			}
			break;
		case IOREG_OFFSET(HcRhStatus): {
				IOREG_NOT_IMPLEMENTED_READ(HcRhStatus);
				return IOREG_READ(HcRhStatus);
			}
			break;
		case IOREG_OFFSET(HcPortStatus1): {
				IOREG_NOT_IMPLEMENTED_READ(HcPortStatus1);
				return IOREG_READ(HcPortStatus1);
			}
			break;
		case IOREG_OFFSET(HcPortStatus2): {
				IOREG_NOT_IMPLEMENTED_READ(HcPortStatus2);
				return IOREG_READ(HcPortStatus2);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOUSBHostController::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(USBHost, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(HcRevision): {
				IOREG_INVALID_WRITE_ACCESS(HcRevision);
			}
			return;
		case IOREG_OFFSET(HcControl): {
				IOREG_VERIFY_WRITE(HcControl, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcControl);
				IOREG_WRITE(HcControl, Value);
			}
			break;
		case IOREG_OFFSET(HcCommandStatus): {
				IOREG_VERIFY_WRITE(HcCommandStatus, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcCommandStatus);
				IOREG_WRITE(HcCommandStatus, Value);
			}
			break;
		case IOREG_OFFSET(HcInterruptStatus): {
				IOREG_VERIFY_WRITE(HcInterruptStatus, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcInterruptStatus);
				IOREG_WRITE(HcInterruptStatus, Value);
			}
			break;
		case IOREG_OFFSET(HcInterruptEnable): {
				IOREG_VERIFY_WRITE(HcInterruptEnable, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcInterruptEnable);
				IOREG_WRITE(HcInterruptEnable, Value);
			}
			break;
		case IOREG_OFFSET(HcInterruptDisable): {
				IOREG_VERIFY_WRITE(HcInterruptDisable, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcInterruptDisable);
				IOREG_WRITE(HcInterruptDisable, Value);
			}
			break;
		case IOREG_OFFSET(HcHCCA): {
				IOREG_VERIFY_WRITE(HcHCCA, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcHCCA);
				IOREG_WRITE(HcHCCA, Value);
			}
			break;
		case IOREG_OFFSET(HcPeriodCurrentED): {
				IOREG_INVALID_WRITE_ACCESS(HcPeriodCurrentED);
			}
			return;
		case IOREG_OFFSET(HcControlHeadED): {
				IOREG_VERIFY_WRITE(HcControlHeadED, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcControlHeadED);
				IOREG_WRITE(HcControlHeadED, Value);
			}
			break;
		case IOREG_OFFSET(HcControlCurrentED): {
				IOREG_VERIFY_WRITE(HcControlCurrentED, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcControlCurrentED);
				IOREG_WRITE(HcControlCurrentED, Value);
			}
			break;
		case IOREG_OFFSET(HcBulkHeadED): {
				IOREG_VERIFY_WRITE(HcBulkHeadED, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcBulkHeadED);
				IOREG_WRITE(HcBulkHeadED, Value);
			}
			break;
		case IOREG_OFFSET(HcBulkCurrentED): {
				IOREG_VERIFY_WRITE(HcBulkCurrentED, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcBulkCurrentED);
				IOREG_WRITE(HcBulkCurrentED, Value);
			}
			break;
		case IOREG_OFFSET(HcDoneHead): {
				IOREG_VERIFY_WRITE(HcDoneHead, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcDoneHead);
				IOREG_WRITE(HcDoneHead, Value);
			}
			break;
		case IOREG_OFFSET(HcFmInterval): {
				IOREG_VERIFY_WRITE(HcFmInterval, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcFmInterval);
				IOREG_WRITE(HcFmInterval, Value);
			}
			break;
		case IOREG_OFFSET(HcFmRemaining): {
				IOREG_INVALID_WRITE_ACCESS(HcFmRemaining);
			}
			return;
		case IOREG_OFFSET(HcFmNumber): {
				IOREG_INVALID_WRITE_ACCESS(HcFmNumber);
			}
			return;
		case IOREG_OFFSET(HcPeriodicStart): {
				IOREG_VERIFY_WRITE(HcPeriodicStart, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcPeriodicStart);
				IOREG_WRITE(HcPeriodicStart, Value);
			}
			break;
		case IOREG_OFFSET(HcLSThreshold): {
				IOREG_VERIFY_WRITE(HcLSThreshold, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcLSThreshold);
				IOREG_WRITE(HcLSThreshold, Value);
			}
			break;
		case IOREG_OFFSET(HcRhDescriptorA): {
				IOREG_VERIFY_WRITE(HcRhDescriptorA, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcRhDescriptorA);
				IOREG_WRITE(HcRhDescriptorA, Value);
			}
			break;
		case IOREG_OFFSET(HcRhDescriptorB): {
				IOREG_VERIFY_WRITE(HcRhDescriptorB, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcRhDescriptorB);
				IOREG_WRITE(HcRhDescriptorB, Value);
			}
			break;
		case IOREG_OFFSET(HcRhStatus): {
				IOREG_VERIFY_WRITE(HcRhStatus, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcRhStatus);
				IOREG_WRITE(HcRhStatus, Value);
			}
			break;
		case IOREG_OFFSET(HcPortStatus1): {
				IOREG_VERIFY_WRITE(HcPortStatus1, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcPortStatus1);
				IOREG_WRITE(HcPortStatus1, Value);
			}
			break;
		case IOREG_OFFSET(HcPortStatus2): {
				IOREG_VERIFY_WRITE(HcPortStatus2, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(HcPortStatus2);
				IOREG_WRITE(HcPortStatus2, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
