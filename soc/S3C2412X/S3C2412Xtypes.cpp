#include "S3C2412Xtypes.h"

namespace S3C2412X
{

const char* const S3C2412X_DeviceNames[] = {"DRAMController", "EBI", "USBHost", "InterruptController", "DMA", "DMA0", "DMA1", "DMA2", "DMA3", "ATA", "SystemController", "LCD", "NANDFlash", "SSMC", "UART", "UART0", "UART1", "UART2", "PWM", "USBDevice", "Watchdog", "IIC", "IIS", "GPIO", "RTC", "ADC", "SPI", "SPI0", "SPI1", "SDMMC"};

} /* S3C2412X namespace */
