#include "IISBusInterface.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOIISBusInterface implementation
 */
IOIISBusInterface::IOIISBusInterface(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOIISBusInterface::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOIISBusInterface::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOIISBusInterface::Reset()
{
	return SOCIODevice::Reset();
}

void IOIISBusInterface::ResetRegisters()
{
	IOREG_RESET(IISCON);
	IOREG_RESET(IISMOD);
	IOREG_RESET(IISFIC);
	IOREG_RESET(IISPSR);
	IOREG_RESET(IISTXD);
	IOREG_RESET(IISRXD);
	SOCIODevice::ResetRegisters();
}

uint32_t IOIISBusInterface::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(IIS, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(IISCON): {
				IOREG_NOT_IMPLEMENTED_READ(IISCON);
				return IOREG_READ(IISCON);
			}
			break;
		case IOREG_OFFSET(IISMOD): {
				IOREG_NOT_IMPLEMENTED_READ(IISMOD);
				return IOREG_READ(IISMOD);
			}
			break;
		case IOREG_OFFSET(IISFIC): {
				IOREG_NOT_IMPLEMENTED_READ(IISFIC);
				return IOREG_READ(IISFIC);
			}
			break;
		case IOREG_OFFSET(IISPSR): {
				IOREG_NOT_IMPLEMENTED_READ(IISPSR);
				return IOREG_READ(IISPSR);
			}
			break;
		case IOREG_OFFSET(IISTXD): {
				IOREG_INVALID_READ_ACCESS(IISTXD);
				return 0;
			}
			break;
		case IOREG_OFFSET(IISRXD): {
				IOREG_NOT_IMPLEMENTED_READ(IISRXD);
				return IOREG_READ(IISRXD);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOIISBusInterface::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(IIS, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(IISCON): {
				IOREG_VERIFY_WRITE(IISCON, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IISCON);
				IOREG_WRITE(IISCON, Value);
			}
			break;
		case IOREG_OFFSET(IISMOD): {
				IOREG_VERIFY_WRITE(IISMOD, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IISMOD);
				IOREG_WRITE(IISMOD, Value);
			}
			break;
		case IOREG_OFFSET(IISFIC): {
				IOREG_VERIFY_WRITE(IISFIC, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IISFIC);
				IOREG_WRITE(IISFIC, Value);
			}
			break;
		case IOREG_OFFSET(IISPSR): {
				IOREG_VERIFY_WRITE(IISPSR, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IISPSR);
				IOREG_WRITE(IISPSR, Value);
			}
			break;
		case IOREG_OFFSET(IISTXD): {
				IOREG_VERIFY_WRITE(IISTXD, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(IISTXD);
				IOREG_WRITE(IISTXD, Value);
			}
			break;
		case IOREG_OFFSET(IISRXD): {
				IOREG_INVALID_WRITE_ACCESS(IISRXD);
			}
			return;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
