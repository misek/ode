#include "ExternalBusInterface.h"
#include "S3C2412Xmacros.h"

namespace S3C2412X
{

/*
 * IOExternalBusInterface implementation
 */
IOExternalBusInterface::IOExternalBusInterface(SOCBase* soc): SOCIODevice(soc)
{

}

bool IOExternalBusInterface::PowerOn()
{
	return SOCIODevice::PowerOn();
}

bool IOExternalBusInterface::PowerOff()
{
	return SOCIODevice::PowerOff();
}

bool IOExternalBusInterface::Reset()
{
	return SOCIODevice::Reset();
}

void IOExternalBusInterface::ResetRegisters()
{
	IOREG_RESET(EBIPR);
	IOREG_RESET(BANK_CFG);
	SOCIODevice::ResetRegisters();
}

uint32_t IOExternalBusInterface::ReadWord(uint32_t IOAddress)
{
	IOREG_BEGIN_READ(EBI, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(EBIPR): {
				IOREG_NOT_IMPLEMENTED_READ(EBIPR);
				return IOREG_READ(EBIPR);
			}
			break;
		case IOREG_OFFSET(BANK_CFG): {
				IOREG_NOT_IMPLEMENTED_READ(BANK_CFG);
				return IOREG_READ(BANK_CFG);
			}
			break;

		default:
			IOREG_INVALID_READ_ADDRESS();
			break;
	}
	return SOCIODevice::ReadWord(IOAddress);
}

void IOExternalBusInterface::WriteWord(uint32_t IOAddress, uint32_t Value)
{
	IOREG_BEGIN_WRITE(EBI, WORD);
	switch(IOAddress) {
		case IOREG_OFFSET(EBIPR): {
				IOREG_VERIFY_WRITE(EBIPR, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(EBIPR);
				IOREG_WRITE(EBIPR, Value);
			}
			break;
		case IOREG_OFFSET(BANK_CFG): {
				IOREG_VERIFY_WRITE(BANK_CFG, Value);
				IOREG_NOT_IMPLEMENTED_WRITE(BANK_CFG);
				IOREG_WRITE(BANK_CFG, Value);
			}
			break;

		default:
			IOREG_INVALID_WRITE_ADDRESS();
			break;
	}
	SOCIODevice::WriteWord(IOAddress, Value);
}

} /* S3C2412X namespace */
