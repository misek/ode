/*
 * soctypes.h
 *
 */

#ifdef __SOCTYPES_H_INCLUDED__
#error This file must be included only once!!!
#endif

#define __SOCTYPES_H_INCLUDED__

#include <inttypes.h>
#include <stdio.h>

#define ___IOREG_ACCESS_TYPE_BYTE	0
#define ___IOREG_ACCESS_TYPE_HALF	1
#define ___IOREG_ACCESS_TYPE_WORD	2
#define ___IOREG_ACCESS_TYPE_QUAD	3
#define ___IOREG_ACCESS_TYPE_BYTE_BYTES	(1 << ___IOREG_ACCESS_TYPE_BYTE)
#define ___IOREG_ACCESS_TYPE_HALF_BYTES	(1 << ___IOREG_ACCESS_TYPE_HALF)
#define ___IOREG_ACCESS_TYPE_WORD_BYTES	(1 << ___IOREG_ACCESS_TYPE_WORD)
#define ___IOREG_ACCESS_TYPE_QUAD_BYTES	(1 << ___IOREG_ACCESS_TYPE_QUAD)
#define ___IOREG_ACCESS_TYPE_BYTE_BITS	(8 * ___IOREG_ACCESS_TYPE_BYTE_BYTES)
#define ___IOREG_ACCESS_TYPE_HALF_BITS	(8 * ___IOREG_ACCESS_TYPE_HALF_BYTES)
#define ___IOREG_ACCESS_TYPE_WORD_BITS	(8 * ___IOREG_ACCESS_TYPE_WORD_BYTES)
#define ___IOREG_ACCESS_TYPE_QUAD_BITS	(8 * ___IOREG_ACCESS_TYPE_QUAD_BYTES)

#define CCAT_NX(a, b) a ## b
#define CCAT(a, b) CCAT_NX(a, b)

#define __CURRENT_DEVICE_NAME() CCAT(___SOC_NAME, _DeviceNames)[___device_id]

// general macros for simplification other macros
#define __IOREG_DEBUG_BITSBYTE	8
#define __IOREG_DEBUG_BITSHALF	16
#define __IOREG_DEBUG_BITSWORD	32
#define __IOREG_DEBUG_BITSQUAD	64
#define __IOREG_DEBUG_TYPEBYTE	uint8_t
#define __IOREG_DEBUG_TYPEHALF	uint16_t
#define __IOREG_DEBUG_TYPEWORD	uint32_t
#define __IOREG_DEBUG_TYPEQUAD	uint64_t
#define __IOREG_DEBUG_MASKBYTE	0xFF
#define __IOREG_DEBUG_MASKHALF	0xFFFF
#define __IOREG_DEBUG_MASKWORD	0xFFFFFFFF
#define __IOREG_DEBUG_MASKQUAD	0xFFFFFFFFFFFFFFFF
#define __IOREG_DEBUG_FUNCBYTE	"Byte"
#define __IOREG_DEBUG_FUNCHALF	"HalfWord"
#define __IOREG_DEBUG_FUNCWORD	"Word"
#define __IOREG_DEBUG_FUNCQUAD	"Quad"
#define __IOREG_DEBUG_FMTBYTE	"0x%02X"
#define __IOREG_DEBUG_FMTHALF	"0x%04X"
#define __IOREG_DEBUG_FMTWORD	"0x%08X"
#define __IOREG_DEBUG_FMTQUAD	"0x%016llX"
// perform action(...) on specified access_type
#define __IOREG_ACTION(action_name, ...) \
	if(___access_type == ___IOREG_ACCESS_TYPE_BYTE) action_name(BYTE, ##__VA_ARGS__) else \
	if(___access_type == ___IOREG_ACCESS_TYPE_HALF) action_name(HALF, ##__VA_ARGS__) else \
	if(___access_type == ___IOREG_ACCESS_TYPE_WORD) action_name(WORD, ##__VA_ARGS__) else \
	if(___access_type == ___IOREG_ACCESS_TYPE_QUAD) action_name(QUAD, ##__VA_ARGS__) else \
	ASSERT(false, "invalid access type: %u", __access_type)
// perform action(...) on specified register size
#define __IOREG_ACTION_SIZE(register_name, action, ...) \
	if (__##register_name##_SIZE == ___IOREG_ACCESS_TYPE_BYTE_BITS) action(BYTE, ##__VA_ARGS__) else \
	if (__##register_name##_SIZE == ___IOREG_ACCESS_TYPE_HALF_BITS) action(HALF, ##__VA_ARGS__) else \
	if (__##register_name##_SIZE == ___IOREG_ACCESS_TYPE_WORD_BITS) action(WORD, ##__VA_ARGS__) else \
	if (__##register_name##_SIZE == ___IOREG_ACCESS_TYPE_QUAD_BITS) action(QUAD, ##__VA_ARGS__) else \
	ASSERT(false, "invalid register size: %u bits", __##register_name##_SIZE)

// FIXME: redirect message to some other working debug interface
#define IOREG_DEBUG_DEVICE(device_name, msg, ...) \
	printf("%s: " msg "\n", device_name, ##__VA_ARGS__)/*; \
	printf("\t%s, line %u\n", __FILE__, __LINE__)*/

#define IOREG_DEBUG(msg, ...) IOREG_DEBUG_DEVICE(__CURRENT_DEVICE_NAME(), msg, ##__VA_ARGS__)

#define __IOREG_alias(register_name) CCAT(CCAT(__, ___SOC_NAME), _##register_name##_ALIAS)

#define IOREG_OFFSET(register_name) __##register_name##_OFFSET

#define __IOREG_ADDRESS(register_name, t) ((__IOREG_DEBUG_TYPE##t *)&reg.register_name)
#define __IOREG_ADDRESS_alias(register_name, t) __IOREG_ADDRESS(__IOREG_alias(register_name), t)



/*
 * IO REGISTERS GENERAL FUNCTION MACROS, DEFINITIONS
 */

// functions required to place at the beginning of Read<size>() and Write<size>() functions
#define __IOREG_BEGIN(io_device, access_size) \
	constexpr int ___device_id = CCAT(CCAT(__, ___SOC_NAME), _##io_device##_id); \
	constexpr int ___access_type = ___IOREG_ACCESS_TYPE_##access_size;
#define IOREG_BEGIN_WRITE(io_device, access_size) \
	__IOREG_BEGIN(io_device, access_size)
#define IOREG_BEGIN_READ(io_device, access_size) \
	__IOREG_BEGIN(io_device, access_size)

// functions to registers array support
#define IOREG_BEGIN_ARRAYS() if (0) {
#define IOREG_ARRAY(array_name, index_name) \
	} else if ((IOAddress >= __##array_name##_ARRAY_START) && (IOAddress >= __##array_name##_ARRAY_END)) { \
		const uint index_name = (IOAddress - __##array_name##_ARRAY_START) / __##array_name##_ARRAY_ELEMENT_SIZE; \
		ASSERT((IOAddress - __##array_name##_ARRAY_START) % __##array_name##_ARRAY_ELEMENT_SIZE == 0, "invalid " #array_name " element access alignment");
#define IOREG_END_ARRAYS() }



/*
 * IO REGISTERS MACROS USED WHEN READING/WRITING TO INVALID ADDRESS
 */

// read from invalid address
#define __IOREG_INVALID_READ_ADDRESS(t) { \
}
#define IOREG_INVALID_READ_ADDRESS() __IOREG_ACTION(__IOREG_INVALID_READ_ADDRESS)

// write to invalid address
#define __IOREG_INVALID_WRITE_ADDRESS(t) { \
}
#define IOREG_INVALID_WRITE_ADDRESS() __IOREG_ACTION(__IOREG_INVALID_WRITE_ADDRESS)



/*
 * IO REGISTERS MACROS USED ON INVALID REGISTER ACCESS (WRITE TO READONLY, READ FROM WRITEONLY)
 */

// invalid read access from write-only register
#define __IOREG_INVALID_READ_ACCESS(t, register_name) { \
	IOREG_DEBUG("INVALID ACCESS: Read" __IOREG_DEBUG_FUNC##t "(" #register_name ") - register is write-only"); \
}
#define IOREG_INVALID_READ_ACCESS(register_name) __IOREG_ACTION(__IOREG_INVALID_READ_ACCESS, register_name)

// invalid write access to read-only register
#define __IOREG_INVALID_WRITE_ACCESS(t, register_name) { \
	IOREG_DEBUG("INVALID ACCESS: Write" __IOREG_DEBUG_FUNC##t "(" #register_name ", " __IOREG_DEBUG_FMT##t ") - register is read-only", (__IOREG_DEBUG_TYPE##t)Value); \
}
#define IOREG_INVALID_WRITE_ACCESS(register_name) __IOREG_ACTION(__IOREG_INVALID_WRITE_ACCESS, register_name)



/*
 * IO REGISTERS NOT IMPLEMENTED MACROS
 */

// show NOT IMPLEMENTED information on register write
#define __IOREG_NOT_IMPLEMENTED_WRITE(t, register_name) { \
	IOREG_DEBUG("NOT IMPLEMENTED: Write" __IOREG_DEBUG_FUNC##t "(" #register_name ", " __IOREG_DEBUG_FMT##t ")", (__IOREG_DEBUG_TYPE##t)Value); \
}
#define IOREG_NOT_IMPLEMENTED_WRITE(register_name) __IOREG_ACTION(__IOREG_NOT_IMPLEMENTED_WRITE, register_name)

// show NOT IMPLEMENTED information on register write
#define __IOREG_NOT_IMPLEMENTED_READ(t, register_name) { \
	IOREG_DEBUG("NOT IMPLEMENTED: Read" __IOREG_DEBUG_FUNC##t "(" #register_name "): " __IOREG_DEBUG_FMT##t, *__IOREG_ADDRESS_alias(register_name, t)); \
}
#define IOREG_NOT_IMPLEMENTED_READ(register_name) __IOREG_ACTION(__IOREG_NOT_IMPLEMENTED_READ, register_name)

// show NOT IMPLEMENTED information on register array write
#define __IOREG_NOT_IMPLEMENTED_ARRAY_WRITE(t, array_name, index) { \
	IOREG_DEBUG("NOT IMPLEMENTED: Write" __IOREG_DEBUG_FUNC##t "(" #array_name "[%u], " __IOREG_DEBUG_FMT##t ")", (uint)index, (__IOREG_DEBUG_TYPE##t)Value); \
}
#define IOREG_NOT_IMPLEMENTED_ARRAY_WRITE(array_name, index) __IOREG_ACTION(__IOREG_NOT_IMPLEMENTED_ARRAY_WRITE, array_name, index)

// show NOT IMPLEMENTED information on register array read
#define __IOREG_NOT_IMPLEMENTED_ARRAY_READ(t, array_name, index) { \
	IOREG_DEBUG("NOT IMPLEMENTED: Read" __IOREG_DEBUG_FUNC##t "(" #array_name "[%u]): " __IOREG_DEBUG_FMT##t, (uint)index, *__IOREG_ADDRESS(array_name, t)); \
}
#define IOREG_NOT_IMPLEMENTED_ARRAY_READ(array_name, index) __IOREG_ACTION(__IOREG_NOT_IMPLEMENTED_ARRAY_READ, array_name, index)



/*
 * IO REGISTERS READ MACROS
 */

// write register value for base device (no read-as fixing, no aliasing)
#define IOREG_BASE_READ(register_name) \
	((___access_type == ___IOREG_ACCESS_TYPE_BYTE) ? *__IOREG_ADDRESS(register_name, BYTE) : \
	((___access_type == ___IOREG_ACCESS_TYPE_HALF) ? *__IOREG_ADDRESS(register_name, HALF) : \
	((___access_type == ___IOREG_ACCESS_TYPE_WORD) ? *__IOREG_ADDRESS(register_name, QUAD) : \
	((___access_type == ___IOREG_ACCESS_TYPE_QUAD) ? *__IOREG_ADDRESS(register_name, WORD) : 0))))

// write register value for base device (read-as fixing, register alias used)
#define IOREG_READ(register_name) \
	((___access_type == ___IOREG_ACCESS_TYPE_BYTE) ? *__IOREG_ADDRESS_alias(register_name, BYTE) : \
	((___access_type == ___IOREG_ACCESS_TYPE_HALF) ? *__IOREG_ADDRESS_alias(register_name, HALF) : \
	((___access_type == ___IOREG_ACCESS_TYPE_WORD) ? *__IOREG_ADDRESS_alias(register_name, WORD) : \
	((___access_type == ___IOREG_ACCESS_TYPE_QUAD) ? *__IOREG_ADDRESS_alias(register_name, QUAD) : 0))))

// read from register array
#define IOREG_ARRAY_READ(array_name, index) \
	((___access_type == ___IOREG_ACCESS_TYPE_BYTE) ? *__IOREG_ADDRESS(array_name[index], BYTE) : \
	((___access_type == ___IOREG_ACCESS_TYPE_HALF) ? *__IOREG_ADDRESS(array_name[index], HALF) : \
	((___access_type == ___IOREG_ACCESS_TYPE_WORD) ? *__IOREG_ADDRESS(array_name[index], WORD) : \
	((___access_type == ___IOREG_ACCESS_TYPE_QUAD) ? *__IOREG_ADDRESS(array_name[index], QUAD) : 0))))



/*
 * IO REGISTERS VERIFY WRITE MACROS
 */

// verify value written to register (check write-as, must-be and recommended values)
#define __IOREG_VERIFY_WRITE_flag(t, register_name, value, flag) \
	if ((__##register_name##_##flag##_MASK & __IOREG_DEBUG_MASK##t) != 0) { \
		if (((__IOREG_DEBUG_TYPE##t)value & (__##register_name##_##flag##_MASK & __IOREG_DEBUG_MASK##t)) != (__##register_name##_##flag##_VALUE & __IOREG_DEBUG_MASK##t)) { \
			IOREG_DEBUG(#register_name ": value " __IOREG_DEBUG_FMT##t " violates " #flag " value", (__IOREG_DEBUG_TYPE##t)value); \
		} \
	}
#define __IOREG_VERIFY_WRITE(t, register_name, value) { \
	__IOREG_VERIFY_WRITE_flag(t, register_name, value, WRITE_AS) \
	__IOREG_VERIFY_WRITE_flag(t, register_name, value, MUST_BE) \
	__IOREG_VERIFY_WRITE_flag(t, register_name, value, RECOMMENDED) \
}
#define IOREG_VERIFY_WRITE(register_name, value) __IOREG_ACTION(__IOREG_VERIFY_WRITE, register_name, value)

// TODO: NOT IMPLEMENTED
// verify value written to register array (check write-as and must-be values)
#define __IOREG_VERIFY_ARRAY_WRITE(t, array_name, index, value) { \
}
#define IOREG_VERIFY_ARRAY_WRITE(array_name, index, value) __IOREG_ACTION(__IOREG_VERIFY_ARRAY_WRITE, array_name, index, value)



/*
 * IO REGISTERS WRITE MACROS
 */

// write register value for base device (no write nor write-as checking, no aliasing)
#define __IOREG_BASE_WRITE(t, register_name, value) { \
	*__IOREG_ADDRESS(register_name, t) = (__IOREG_DEBUG_TYPE##t)value; \
}
#define IOREG_BASE_WRITE(register_name, value) __IOREG_ACTION(__IOREG_BASE_WRITE, register_name, value)

// write register value for device (check write & write-as masks, register alias used)
#define __IOREG_WRITE_AS_FIX(register_name, value, t) \
	(__IOREG_DEBUG_TYPE##t)(((__##register_name##_WRITE_AS_MASK & __IOREG_DEBUG_MASK##t) == 0) ? value : \
	(((__IOREG_DEBUG_TYPE##t)value & ~(__IOREG_DEBUG_TYPE##t)(__##register_name##_WRITE_AS_MASK & __IOREG_DEBUG_MASK##t)) | (__IOREG_DEBUG_TYPE##t)((__##register_name##_WRITE_AS_VALUE & __##register_name##_WRITE_AS_MASK) & __IOREG_DEBUG_MASK##t)))
#define __IOREG_WRITE(t, register_name, value) { \
	if ((__##register_name##_WRITE_MASK & __IOREG_DEBUG_MASK##t) == __IOREG_DEBUG_MASK##t) { \
		*__IOREG_ADDRESS_alias(register_name, t) = __IOREG_WRITE_AS_FIX(register_name, value, t);\
	} else { \
		*__IOREG_ADDRESS_alias(register_name, t) = (*__IOREG_ADDRESS_alias(register_name, t) & ~(__##register_name##_WRITE_MASK & __IOREG_DEBUG_MASK##t)) | ((__IOREG_DEBUG_TYPE##t)value & (__##register_name##_WRITE_MASK & __IOREG_DEBUG_MASK##t)); \
	} \
}
#define IOREG_WRITE(register_name, value) __IOREG_ACTION(__IOREG_WRITE, register_name, value)

// TODO: check write, write-as masks
// write value to register array
#define __IOREG_ARRAY_WRITE(t, array_name, index, value) { \
	*__IOREG_ADDRESS(array_name[index], t) = (__IOREG_DEBUG_TYPE##t)value; \
}
#define IOREG_ARRAY_WRITE(array_name, index, value) __IOREG_ACTION(__IOREG_ARRAY_WRITE, array_name, index, value)



/*
 * IO REGISTERS RESET MACROS
 */

// reset register to its reset state (hw-set fields and read-undef are not changed)
#define __IOREG_RESET(t, register_name) { \
	if (__##register_name##_RESET_MASK == __IOREG_DEBUG_MASK##t) { \
		*__IOREG_ADDRESS_alias(register_name, t) = (__IOREG_DEBUG_TYPE##t)(__##register_name##_RESET_VALUE); \
	} else { \
		*__IOREG_ADDRESS_alias(register_name, t) = (__IOREG_DEBUG_TYPE##t)((*__IOREG_ADDRESS_alias(register_name, t) & ((~__##register_name##_RESET_MASK) & __IOREG_DEBUG_MASK##t)) | (__##register_name##_RESET_VALUE & __##register_name##_RESET_MASK & __IOREG_DEBUG_MASK##t)); \
	} \
}
#define IOREG_RESET(register_name) __IOREG_ACTION_SIZE(register_name, __IOREG_RESET, register_name)

// FIXME: implementation required
// reset register array (hw-set fields and read-undef are not changed)
#define IOREG_ARRAY_RESET(array_name)



/*
 * SOC MACROS
 */

// initialize SOC device
#define INIT_SOC_DEVICE(device_name) \
	device_name = new CCAT(CCAT(__, ___SOC_NAME), _##device_name##_class)(this); \
	AddIODevice(device_name, #device_name, __##device_name##_BASE, __##device_name##_SIZE)

// initialize SOC CPU core
// FIXME: not fully implementer implemented
#define INIT_SOC_CORE(core_type) \
	Core = new ARMCPU(core_type);
