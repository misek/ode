/*
 * socmacros.cpp
 *
 *  Created on: 18-05-2012
 *      Author: Łukasz Misek
 */

#include "socmacros.h"

#ifdef DEBUG_IO_FAST
const char* __current_io_device;
const char* __current_io_register;
const char* __current_io_device_access = __null;
#else
char __current_io_device[128];
char __current_io_register[128];
const char* __current_io_device_access = __null;
#endif
