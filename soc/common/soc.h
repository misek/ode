/*
 * soc.h
 *
 *  Created on: 12-05-2012
 *      Author: Łukasz Misek
 */

#ifndef SOC_H_
#define SOC_H_

#define SOC_METHOD __attribute__((regparm(3)))

#include <component.h>
#include "socmacros.h"

union RESERVED_REGISTER {
	uint32_t Word;
	uint32_t Reserved_0_31:32;
};

class SOCIODevice;

struct SOC_ATTACHED_DEVICE {
	EmulatedComponent* device;
	EMULATED_COMPONENT_TYPE type;
	int index;
};

typedef bool(SOC_METHOD *EnumIODeviceCallbask)(SOCIODevice* device);

class SOCBase {
private:
	unsigned attached_devices_count;
	SOC_ATTACHED_DEVICE* attached_devices;

	double frequency;

	SOC_METHOD static bool DumpIODevice(SOCIODevice* dev);
	SOC_METHOD static bool PowerOnDevice_cb(SOCIODevice* dev);
	SOC_METHOD static bool PowerOffDevice_cb(SOCIODevice* dev);
	SOC_METHOD static bool ResetDevice_cb(SOCIODevice* dev);

protected:
	bool powered_on;

public:
	SOCBase();
	virtual ~SOCBase();
	SOC_METHOD virtual bool Initialize();

	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();

	SOC_METHOD virtual void AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index);

	SOC_METHOD void AddIODevice(SOCIODevice* device, const char* name, uint32_t start_address, int size);
	SOC_METHOD bool EnumIODevices(EnumIODeviceCallbask cb);
	SOC_METHOD void DumpIODevices();

	SOC_METHOD double GetMainFrequency() { return frequency; };
};

class SOCIODevice {
private:
	Mutex lock;

protected:
	SOCBase* owner;
	bool powered_on;

public:
	uint32_t IO_start_address;
	uint32_t IO_size;
	const char* IO_name;

	SOCIODevice(SOCBase* soc);
	virtual ~SOCIODevice();

	SOC_METHOD virtual bool PowerOn();
	SOC_METHOD virtual bool PowerOff();
	SOC_METHOD virtual bool Reset();
	SOC_METHOD virtual void ResetRegisters();

	FORCE_INLINE void Lock() { lock.Lock(); };
	FORCE_INLINE void Unlock() { lock.Unlock(); };

	SOC_METHOD virtual uint8_t ReadByte(uint32_t IOAddress);
	SOC_METHOD virtual uint16_t ReadHalf(uint32_t IOAddress);
	SOC_METHOD virtual uint32_t ReadWord(uint32_t IOAddress);
	SOC_METHOD virtual void WriteByte(uint32_t IOAddress, uint8_t Value);
	SOC_METHOD virtual void WriteHalf(uint32_t IOAddress, uint16_t Value);
	SOC_METHOD virtual void WriteWord(uint32_t IOAddress, uint32_t Value);

	SOC_METHOD virtual bool StartDMATransfer(void* dma);

};

CODEGEN_CALL uint32_t IOReadWord(uint32_t address);
CODEGEN_CALL uint16_t IOReadHalf(uint32_t address);
CODEGEN_CALL uint8_t IOReadByte(uint32_t address);
CODEGEN_CALL void IOWriteWord(uint32_t address, uint32_t value);
CODEGEN_CALL void IOWriteHalf(uint32_t address, uint16_t value);
CODEGEN_CALL void IOWriteByte(uint32_t address, uint8_t value);

#endif /* SOC_H_ */
