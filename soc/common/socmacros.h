/*
 * macros.h
 *
 *  Created on: 12-05-2012
 *      Author: Łukasz Misek
 */

#ifndef SOCMACROS_H_
#define SOCMACROS_H_

#include "utils.h"


#ifdef DEBUG_IO
#define CPU_GetInstructionPointer() (0)
#ifdef DEBUG_IO_FAST
	extern char* __current_io_device;
	extern char* __current_io_register;
	extern char* __current_io_device_access = __null;

	#define IO_DEBUG_DEVICE(device, access_mode,...) { __current_io_device = device; __current_io_device_access = access_mode; __current_io_register = __null; }
	#define IO_DEBUG_REG(reg,...) __current_io_register = reg
	#define	IO_DEBUG(fmt,...) DEBUG_PRINT("%s: " ## fmt ## "\n", __current_io_device/*, __current_io_register*/, __VA_ARGS__)
	#define	IO_DEBUG_ERROR(fmt,...) { DEBUG_PRINT_COLOR(0x0C); IO_DEBUG(fmt, __VA_ARGS__); DEBUG_PRINT_COLOR(0x07); }
	#define	IO_DEBUG_ERROR_RO() IO_DEBUG_ERROR("write to read-only register!")
	#define	IO_DEBUG_ERROR_WO() IO_DEBUG_ERROR("read from write-only register!")
	#define	IO_DEBUGC(condition,fmt,...) if(condition) IO_DEBUG(fmt, __VA_ARGS__)
	#define	IO_NOTIMPLEMENTED(IOAddress) { DEBUG_PRINT("%08X: ", CPU_GetInstructionPointer()); DEBUG_PRINT_COLOR(0x0C); if (__current_io_register) DEBUG_PRINT("[%s] %s.%s: not implemented!\n", __current_io_device_access, __current_io_device, __current_io_register); else DEBUG_PRINT("[%s] %s.0x%04X: NOT IMPLEMENTED!\n", __current_io_device_access, __current_io_device, IOAddress); DEBUG_PRINT_COLOR(0x07); }
	#define	IO_NOTIMPLEMENTED_WRITE(IOAddress,Value) { DEBUG_PRINT("%08X: ", CPU_GetInstructionPointer()); DEBUG_PRINT_COLOR(0x0C); if (__current_io_register) DEBUG_PRINT("[%s] %s.%s <= %X: not implemented!\n", __current_io_device_access, __current_io_device, __current_io_register, Value); else DEBUG_PRINT("[%s] %s.0x%04X <= %X: not implemented!\n", __current_io_device_access, __current_io_device, IOAddress, Value); DEBUG_PRINT_COLOR(0x07); }
	#define IO_DEBUG_END() { __current_io_device = __null; __current_io_register = __null; }
#else
	extern char __current_io_device[128];
	extern char __current_io_register[128];
	extern const char* __current_io_device_access;

	#define IO_DEBUG_DEVICE(device,access_mode,...) { sprintf(__current_io_device, device, ##__VA_ARGS__); __current_io_device_access = access_mode; __current_io_register[0] = 0; }
	#define IO_DEBUG_REG(reg,...) sprintf(__current_io_register, reg, ##__VA_ARGS__)
	#define	IO_DEBUG(fmt,...) DEBUG_PRINT("%s: " fmt "\n", __current_io_device/*, __current_io_register*/, ##__VA_ARGS__)
	#define	IO_DEBUG_ERROR(fmt,...) { DEBUG_PRINT_COLOR(0x0C); IO_DEBUG(fmt, ##__VA_ARGS__); DEBUG_PRINT_COLOR(0x07); }
	#define	IO_DEBUG_ERROR_RO() IO_DEBUG_ERROR("write to read-only register!")
	#define	IO_DEBUG_ERROR_WO() IO_DEBUG_ERROR("read from write-only register!")
	#define	IO_DEBUGC(condition,fmt,...) if(condition) IO_DEBUG(fmt, ##__VA_ARGS__)
	#define	IO_NOTIMPLEMENTED(IOAddress) { DEBUG_PRINT("%08X: ", CPU_GetInstructionPointer()); DEBUG_PRINT_COLOR(0x0C); if (__current_io_register[0]) DEBUG_PRINT("[%s] %s.%s: not implemented!\n", __current_io_device_access, __current_io_device, __current_io_register); else DEBUG_PRINT("[%s] %s.0x%04X: NOT IMPLEMENTED!\n", __current_io_device_access, __current_io_device, IOAddress); DEBUG_PRINT_COLOR(0x07); }
	#define	IO_NOTIMPLEMENTED_WRITE(IOAddress,Value) { DEBUG_PRINT("%08X: ", CPU_GetInstructionPointer()); DEBUG_PRINT_COLOR(0x0C); if (__current_io_register[0]) DEBUG_PRINT("[%s] %s.%s <= %X: not implemented!\n", __current_io_device_access, __current_io_device, __current_io_register, Value); else DEBUG_PRINT("[%s] %s.0x%04X <= %X: not implemented!\n", __current_io_device_access, __current_io_device, IOAddress, Value); DEBUG_PRINT_COLOR(0x07); }
	#define IO_DEBUG_END() { __current_io_device[0] = 0; __current_io_register[0] = 0; }
#endif

#else
	#define IO_DEBUG_DEVICE(device, access_mode,...)
	#define IO_DEBUG_REG(reg,...)
	#define	IO_DEBUG(fmt,...)
	#define	IO_DEBUG_ERROR(fmt,...)
	#define	IO_DEBUG_ERROR_RO()
	#define	IO_DEBUG_ERROR_WO()
	#define	IO_DEBUGC(condition,fmt,...)
	#define	IO_NOTIMPLEMENTED(IOAddress)
	#define	IO_NOTIMPLEMENTED_WRITE(IOAddress,Value)
	#define IO_DEBUG_END()
#endif

#endif /* SOCMACROS_H_ */
