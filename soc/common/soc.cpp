/*
 * soc.cpp
 *
 *  Created on: 12-05-2012
 *      Author: Łukasz Misek
 */

#include "soc.h"
#include "FreqGenerator/FreqGenerator.h"

struct SOC_IO_1KB_INFO {
	SOCIODevice* device[1024 / 4];
};

struct SOC_IO_1MB_INFO {
	SOC_IO_1KB_INFO* items[1024];
};

struct SOC_IO_4GB_INFO {
	SOC_IO_1MB_INFO* items[4 * 1024];
};

SOC_IO_4GB_INFO dev_map;

SOCBase::SOCBase()
{
	memset(&dev_map, 0, sizeof(dev_map));
	attached_devices = __null;
	attached_devices_count = 0;
	powered_on = false;
	frequency = 0.0;
}

SOCBase::~SOCBase()
{

}

bool SOCBase::Initialize()
{
	return true;
}

bool SOCBase::PowerOn()
{
	if (!powered_on) {
		powered_on = EnumIODevices(PowerOnDevice_cb);
		powered_on = powered_on && Reset();
	}
	return powered_on;
}

bool SOCBase::PowerOff()
{
	if (powered_on) {
		powered_on = !EnumIODevices(PowerOffDevice_cb);
	}
	return !powered_on;
}

bool SOCBase::Reset()
{
	return EnumIODevices(ResetDevice_cb);
}

bool SOCBase::PowerOnDevice_cb(SOCIODevice* dev)
{
	bool result = dev->PowerOn();
	return result;
}

bool SOCBase::PowerOffDevice_cb(SOCIODevice* dev)
{
	return dev->PowerOff();
}

bool SOCBase::ResetDevice_cb(SOCIODevice* dev)
{
	return dev->Reset();
}

void SOCBase::AttachDevice(EmulatedComponent* device, EMULATED_COMPONENT_TYPE type, int index)
{
	SOC_ATTACHED_DEVICE* dev_info;
	if (attached_devices_count == 0) {
		attached_devices = new SOC_ATTACHED_DEVICE;
		dev_info = attached_devices;
	} else {
		unsigned int i = 0;
		dev_info = attached_devices;
		while (i < attached_devices_count)
		{
			if ((dev_info->index == index) && (dev_info->type == type)) {
				break;
			}
			dev_info++;
			i++;
		}
		if (i == attached_devices_count) {
			// add new item in array
			dev_info = new SOC_ATTACHED_DEVICE[attached_devices_count + 1];
			memcpy(dev_info, attached_devices, sizeof(SOC_ATTACHED_DEVICE) * attached_devices_count);
			delete attached_devices;
			attached_devices = dev_info;
			dev_info = &dev_info[attached_devices_count++];
		}
	}

	dev_info->device = device;
	dev_info->index = index;
	dev_info->type = type;

	switch (type) {
		case EMULATED_FREQUENCY_GENERATOR:
			frequency = dynamic_cast<FrequencyGenerator*>(device)->GetFrequency();
			ASSERT(frequency > 0);
			break;
		default:
			break;
	}
}

void SOCBase::AddIODevice(SOCIODevice* device, const char* name, uint32_t start_address, int size)
{
	device->IO_start_address = start_address;
	device->IO_size = size;
	device->IO_name = name;
	while (size > 0) {
		SOC_IO_1MB_INFO* p1 = dev_map.items[start_address >> 20];
		if (!p1) {
			p1 = new SOC_IO_1MB_INFO();
			memset(p1, 0, sizeof(SOC_IO_1MB_INFO));
			dev_map.items[start_address >> 20] = p1;
		}
		SOC_IO_1KB_INFO* p2 = p1->items[(start_address >> 10) & 0x3FF];
		if (!p2) {
			p2 = new SOC_IO_1KB_INFO();
			memset(p2, 0, sizeof(SOC_IO_1KB_INFO));
			p1->items[(start_address >> 10) & 0x3FF] = p2;
		}
		p2->device[(start_address >> 2) & 0xFF] = device;

		start_address += 4;
		size -= 4;
	}
}

bool SOCBase::EnumIODevices(EnumIODeviceCallbask cb)
{
	SOCIODevice* device = __null;
	for (uint i = 0; i < 4096; i++) {
		SOC_IO_1MB_INFO* p1 = dev_map.items[i];
		if (p1) {
			for (uint j = 0; j < 1024; j++) {
				SOC_IO_1KB_INFO* p2 = p1->items[j];
				if (p2) {
					for (uint k = 0; k < 255; k++) {
						if (p2->device[k] != device) {
							if (device) {
								if (!cb(device)) {
									return false;
								}
							}
							device = p2->device[k];
						}
					}
				}
			}
		}
	}
	if (device) {
		if (!cb(device)) {
			return false;
		}
	}
	return true;
}

bool SOCBase::DumpIODevice(SOCIODevice* dev)
{
	DEBUG_PRINT("0x%08X - 0x%08X, %s\n",
		dev->IO_start_address, dev->IO_start_address + dev->IO_size, dev->IO_name);

	return true;
}

void SOCBase::DumpIODevices()
{
	EnumIODevices(DumpIODevice);
}

SOCIODevice::SOCIODevice(SOCBase* soc):
	lock(true), powered_on(false), IO_start_address(0), IO_size(0), IO_name(__null)
{
	ASSERT(soc);
	owner = soc;
}

SOCIODevice::~SOCIODevice()
{

}

bool SOCIODevice::PowerOn()
{
	powered_on = true;
	return powered_on;
}

bool SOCIODevice::PowerOff()
{
	powered_on = false;
	return !powered_on;
}

bool SOCIODevice::Reset()
{
	ResetRegisters();
	return true;
}

void SOCIODevice::ResetRegisters()
{

}

uint8_t SOCIODevice::ReadByte(uint32_t IOAddress)
{
	return 0;
}

uint16_t SOCIODevice::ReadHalf(uint32_t IOAddress)
{
	return 0;
}

uint32_t SOCIODevice::ReadWord(uint32_t IOAddress)
{
	return 0;
}

void SOCIODevice::WriteByte(uint32_t IOAddress, uint8_t Value)
{

}

void SOCIODevice::WriteHalf(uint32_t IOAddress, uint16_t Value)
{

}

void SOCIODevice::WriteWord(uint32_t IOAddress, uint32_t Value)
{

}

bool SOCIODevice::StartDMATransfer(void* dma)
{
	return false;
}

uint32_t IOReadWord(uint32_t address)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				uint32_t result = device->ReadWord(address - device->IO_start_address);
				device->Unlock();
				return result;
			}
		}
	}
	DEBUG_PRINT("ReadWord(0x%08X): unknown device\n", address);
	return 0;
}

uint16_t IOReadHalf(uint32_t address)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				uint16_t result = device->ReadHalf(address - device->IO_start_address);
				device->Unlock();
				return result;
			}
		}
	}
	DEBUG_PRINT("ReadHalf(0x%08X): unknown device\n", address);
	return 0;
}

uint8_t IOReadByte(uint32_t address)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				uint8_t result = device->ReadByte(address - device->IO_start_address);
				device->Unlock();
				return result;
			}
		}
	}
	DEBUG_PRINT("ReadByte(0x%08X): unknown device\n", address);
	return 0;
}

void IOWriteWord(uint32_t address, uint32_t value)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				device->WriteWord(address - device->IO_start_address, value);
				device->Unlock();
				return;
			}
		}
	}
	DEBUG_PRINT("WiteWord(0x%08X, 0x%08X): unknown device\n", address, value);
}

void IOWriteHalf(uint32_t address, uint16_t value)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				device->WriteHalf(address - device->IO_start_address, value);
				device->Unlock();
				return;
			}
		}
	}
	DEBUG_PRINT("WiteHalf(0x%08X, 0x%04X): unknown device\n", address, value);
}

void IOWriteByte(uint32_t address, uint8_t value)
{
	SOC_IO_1MB_INFO* p1 = dev_map.items[address >> 20];
	if (p1) {
		SOC_IO_1KB_INFO* p2 = p1->items[(address >> 10) & 0x3FF];
		if (p2) {
			SOCIODevice* device = p2->device[(address >> 2) & 0xFF];
			if (device) {
				device->Lock();
				device->WriteByte(address - device->IO_start_address, value);
				device->Unlock();
				return;
			}
		}
	}
	DEBUG_PRINT("WiteByte(0x%08X, 0x%02X): unknown device\n", address, value);
}
