#!/usr/bin/env python
# coding=utf-8

import sys

CORE_CONFIG_DIR = "./.metadata/.plugins/org.eclipse.core.runtime/.settings/"
PREFS_FILE_NAME = CORE_CONFIG_DIR + "org.eclipse.cdt.core.prefs"

def main():
	try:
		os.makedirs(CORE_CONFIG_DIR)
	except:
		pass
	try:
		lines = [x.strip().split("=", 1) for x in open(PREFS_FILE_NAME, "rb").readlines()]
	except:
		lines = [('eclipse.preferences.version', '1')]
	lines_dict = {}
	for k, v in lines:
		lines_dict[k] = v
	lines_dict['environment/workspace/PKGCONFIG/operation'] = 'append'
	lines_dict['environment/workspace/PKGCONFIG/value'] = '`pkg-config --cflags --libs gtk+-3.0 libpulse-simple`'
	lines_dict['environment/workspace/PKGCONFIG/delimiter'] = '\\:'
	with open(PREFS_FILE_NAME, "wb") as f:
		for k in sorted(lines_dict.keys()):
			f.write("%s=%s\n" % (k, lines_dict[k]))
	print "OK"

if __name__ == '__main__':
    main()
