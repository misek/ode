/*
 * board.cpp
 *
 *  Created on: 25-01-2013
 *      Author: lms
 */

#include "board.h"

BoardBase::BoardBase()
{

}

BoardBase::~BoardBase()
{

}

void BoardBase::Initialize()
{
	//FIXME:
}

void BoardBase::PowerOn()
{
	std::list<BoardComponent*>::iterator it = components.begin();
	while (it != components.end()) {
		BoardComponent* cmp = *it;
		cmp->cmp->PowerOn();
		++it;
	}
}

void BoardBase::PowerOff()
{
	std::list<BoardComponent*>::iterator it = components.begin();
	while (it != components.end()) {
		BoardComponent* cmp = *it;
		cmp->cmp->PowerOff();
		++it;
	}
}

bool BoardBase::PressButton(const char* name)
{
	//FIXME:
	return false;
}

bool BoardBase::ReleaseButton(const char* name)
{
	//FIXME:
	return false;
}

void BoardBase::AddComponent(EmulatedComponent* component, const char* name)
{
	BoardComponent* cmp = new BoardComponent();
	cmp->name = name;
	cmp->cmp = component;
	cmp->widget = component->GetWidget();
	components.push_back(cmp);
}

BoardComponent* BoardBase::GetComponent(const char* name)
{
	std::list<BoardComponent*>::iterator it = components.begin();
	while (it != components.end()) {
		BoardComponent* cmp = *it;
		if (strcmp(cmp->name, name) == 0) {
			return cmp;
		}
		++it;
	}
	return __null;
}

GtkWidget* BoardBase::GetWidget(const char* name)
{
	BoardComponent* cmp = GetComponent(name);
	return cmp ? cmp->widget : __null;
}

bool BoardBase::SetConfig(const char* name, const char* param, const char* value)
{
	return false;
}
