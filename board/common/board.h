/*
 * board.h
 *
 *  Created on: 25-01-2013
 *      Author: lms
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <list>
#include <gtk/gtk.h>
#include "utils.h"
#include "config.h"
#include "component.h"

class HardwareButton {
	const char* name;
	int id;
};

class BoardComponent {
public:
	const char* name;
	EmulatedComponent* cmp;
	GtkWidget* widget;
};

class BoardBase {
protected:
	std::list<HardwareButton*> buttons;
	std::list<BoardComponent*> components;

	virtual void AddComponent(EmulatedComponent* component, const char* name);

public:
	BoardBase();
	virtual ~BoardBase();
	virtual void Initialize();

	virtual void PowerOn();
	virtual void PowerOff();
	virtual bool PressButton(const char* name);
	virtual bool ReleaseButton(const char* name);

	virtual bool SetConfig(const char* name, const char* param, const char* value);

	BoardComponent* GetComponent(const char* name);
	GtkWidget* GetWidget(const char* name);
};

#endif /* BOARD_H_ */
