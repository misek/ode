/*
 * 1000A_MSD_035TCboard.cpp
 *
 *  Created on: 25-01-2013
 *      Author: Łukasz Misek
 */

#include "1000A_MSD_035TCboard.h"

	//GPIO->SetEINTLevel(SDI_CARD_DETECT_EINTNO, 1); // SD card not inserted
//	GPIO->SetEINTLevel(0, 1); // keypad
//	GPIO->SetEINTLevel(1, 1); // keypad
//	GPIO->SetEINTLevel(2, 1); // keypad
//	GPIO->SetEINTLevel(8, 1); // power

SOC_METHOD void GPIO_read_GPB(GPIO_cb_param* param, Board1000A_MSD_035TC* board)
{
	if (param->mask.Pin6) {
		param->value.Pin6 = 1; // keyboard lock
	}
}

SOC_METHOD void GPIO_read_GPC(GPIO_cb_param* param, Board1000A_MSD_035TC* board)
{
	if (param->mask.Pin6) {
		param->value.Pin6 = IIC->Bus.GetSDA();
	}
}

SOC_METHOD void GPIO_read_GPF(GPIO_cb_param* param, Board1000A_MSD_035TC* board)
{
	//FIXME: keyboard?
	param->value.Pin0 = 1;
	param->value.Pin1 = 1;
	param->value.Pin2 = 1;
}

SOC_METHOD void GPIO_read_GPG(GPIO_cb_param* param, Board1000A_MSD_035TC* board)
{
	if (param->mask.Pin10) {
		param->value.Pin10 = 0; // SD not inserted
	}
	if (param->mask.Pin13) {
		param->value.Pin13 = 1; // large block NAND
	}
}

SOC_METHOD void GPIO_write_GPC(GPIO_cb_param* param, Board1000A_MSD_035TC* board)
{
	if (param->mask.Pin5) {
		// SCL
		IIC->Bus.SetSCL(param->value.Pin5);
	}
	if (param->mask.Pin6) {
		// SDA
		IIC->Bus.SetSDA(param->value.Pin6);
	}
}

Board1000A_MSD_035TC::Board1000A_MSD_035TC(): BoardBase(), m_sdmmc_present(false)
{

}

Board1000A_MSD_035TC::~Board1000A_MSD_035TC()
{
	//FIXME:BoardBase::~BoardBase();
}

void Board1000A_MSD_035TC::Initialize()
{
	// frequency generator (12MHz)
	m_Quartz.SetFrequency(12 * 1000 * 1000);
	AddComponent(&m_Quartz, EMULATED_FREQUENCY_GENERATOR, 0, "quartz");

	// RAM memory (2 * 32MB)
	m_RAM.SetSize(64 * 1024 * 1024);
	AddComponent(&m_RAM, EMULATED_RAM, 0, "ram");

	// NAND flash
	m_NAND.Initialize();
	AddComponent(&m_NAND, EMULATED_NANDFLASH, 0, "nand");

	// AT88SC
	m_AT88SC.Initialize();
	AddComponent(&m_AT88SC, EMULATED_I2C_DEVICE, 0, "at88sc");
	GPIO->SetReadCallback((GPIO_read_callback)&GPIO_read_GPB, GPB, this);
	GPIO->SetReadCallback((GPIO_read_callback)&GPIO_read_GPC, GPC, this);
	GPIO->SetReadCallback((GPIO_read_callback)&GPIO_read_GPF, GPF, this);
	GPIO->SetReadCallback((GPIO_read_callback)&GPIO_read_GPG, GPG, this);
	GPIO->SetWriteCallback((GPIO_write_callback)&GPIO_write_GPC, GPC, this);

	// LCD, 3.5" with TC
	m_SOC.AttachDevice(&m_LCD, EMULATED_LCD_DISPLAY, 0);
	AddComponent(&m_LCD, EMULATED_LCD_DISPLAY, 0, "lcd");

	// SDMMC card slot
	AddComponent(&m_SDMMC, EMULATED_SDMMC, 0, "sdmmc");

	BoardBase::Initialize();
}

void Board1000A_MSD_035TC::PowerOn()
{
	BoardBase::PowerOn();
	//TODO: move this somewhere else
<<<<<<< HEAD
	GPIO->SetEINTLevel(0, 1); // keypad
	GPIO->SetEINTLevel(1, 1); // keypad
	GPIO->SetEINTLevel(2, 1); // keypad
	GPIO->SetEINTLevel(8, 1); // power
=======
	//FIXME:m_SOC.GPIO->SetEINTLevel(0, 1); // keypad
	//FIXME:m_SOC.GPIO->SetEINTLevel(1, 1); // keypad
	//FIXME:m_SOC.GPIO->SetEINTLevel(2, 1); // keypad
	//FIXME:m_SOC.GPIO->SetEINTLevel(8, 1); // power

	//FIXME:m_SOC.GPIO->InputPin.GPB.GPB6 = 1; // keyboard lock
	//FIXME:m_SOC.GPIO->InputPin.GPG.GPG10 = 0; // SD not inserted
	//FIXME:m_SOC.GPIO->InputPin.GPG.GPG13 = 1; // large block NAND
>>>>>>> soc-editor

	m_SOC.PowerOn();
}

void Board1000A_MSD_035TC::PowerOff()
{
	m_SOC.PowerOff();
	BoardBase::PowerOff();
}

void Board1000A_MSD_035TC::AddComponent(EmulatedComponent* component, EMULATED_COMPONENT_TYPE type, int index, const char* name)
{
	m_SOC.AttachDevice(component, type, index);
	BoardBase::AddComponent(component, name);
}

bool Board1000A_MSD_035TC::SetConfig(const char* name, const char* param, const char* value)
{
	BoardComponent* cmp = GetComponent(name);
	if (cmp) {
		if (cmp->cmp == &m_NAND) {
			if (!strcmp(param, "file")) {
				return m_NAND.UseFile(value);
			}
		} else if (cmp->cmp == &m_AT88SC) {
			if (!strcmp(param, "file")) {
				m_AT88SC.Load(value);
			}
		} else if (cmp->cmp == &m_SDMMC) {
			if (!strcmp(param, "file")) {
				return m_SDMMC.AttachFile(value);
			}
		}
		return false;
	}
	return BoardBase::SetConfig(name, param, value);
}

