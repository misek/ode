/*
 * 1000A_MSD_035TCboard.h
 *
 *  Created on: 25-01-2013
 *      Author: Łukasz Misek
 */

#ifndef __1000A_MSD_035TCBOARD_H_
#define __1000A_MSD_035TCBOARD_H_

#include <board.h>
#include <utils.h>
#include <S3C2412X.h>
#include <FlashMemory/SamsungFlash.h>
#include <I2C/AT88SC.h>
#include <FreqGenerator/FreqGenerator.h>
#include <SDBus/sdbus.h>
#include <Memory/RAMDevice.h>
#include <LCD/lcd.h>

class Board1000A_MSD_035TC: public BoardBase {
private:
	S3C2412X::S3C2412X m_SOC;
	FrequencyGenerator m_Quartz;
	RAMMemory m_RAM;
	K9G8G08U0B m_NAND;
	AT88SC0104C m_AT88SC;
	LCD35_320x340 m_LCD;
	SDDevice m_SDMMC;

	bool m_sdmmc_present;

protected:
	virtual void AddComponent(EmulatedComponent* component, EMULATED_COMPONENT_TYPE type, int index, const char* name);

public:
	Board1000A_MSD_035TC();
	virtual ~Board1000A_MSD_035TC();
	virtual void Initialize();

	virtual void PowerOn();
	virtual void PowerOff();

	virtual bool SetConfig(const char* name, const char* param, const char* value);
};

#endif /* 1000A_MSD_035TC_H_ */
