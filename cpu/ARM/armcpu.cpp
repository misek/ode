/*
 * armcpu.cpp
 *
 *  Created on: 12-04-2012
 *      Author: Łukasz Misek
 */

#include "armcpu.h"
#include "decoder.h"
#include "codegen.h"
#include "SystemControlProcessor.h"
#include "VectorFloatingPoint.h"

#include "arm_info.h"

__thread ARMCPU* CPU = __null;
__thread ARMSystemControlCoprocessorContext* MMU = __null;

#ifdef DEBUG_DEASSEMBLED_CODE
char deassembled_instruction[1024];
#endif

const char* arm_mode_name(ARM_CPU_MODE mode) {
	switch (mode) {
		case ARM_CPU_MODE_FIQ:
			return "FIQ";
		case ARM_CPU_MODE_IRQ:
			return "IRQ";
		case ARM_CPU_MODE_SVC:
			return "SVC";
		case ARM_CPU_MODE_ABT:
			return "ABT";
		case ARM_CPU_MODE_UND:
			return "UND";
		case ARM_CPU_MODE_SYS:
			return "SYS";
		case ARM_CPU_MODE_USR:
			return "USR";
		default:
			return "unknown";
	}
}

ARMCONTEXT::ARMCONTEXT()
{
	memset(this, 0, sizeof(ARMCONTEXT));
}

PSR_REGISTER ARMCONTEXT::read_CPSR()
{
	PSR_REGISTER ret = uncached_CPSR;
	ret.Word &= 0x0FFFFFFF;
	ret.Word |= (flags.N << 31) | (flags.Z << 30) | (flags.C << 29) | (flags.V << 28);
	return ret;
}

ARMCPU::ARMCPU(int model): powered_on(false)
{
	memset(compiled, 0, sizeof(compiled));

	CPUInfo = CPU_INFO_TAB;
	while (CPUInfo->processor != model) {
		CPUInfo++;
	}
	ARCHInfo = ARCH_INFO_TAB;
	while (ARCHInfo->architecture != CPUInfo->architecture) {
		ARCHInfo++;
	}

	phys_memory = new PHYS_4GB_INFO;
	memset(phys_memory, 0, sizeof(PHYS_4GB_INFO));
	memset(coprocessors, 0, sizeof(coprocessors));
	coprocessors[10] = new VectorFloatingPointCoprocessor10(this);
	coprocessors[15] = new ARMSystemControlCoprocessor(this);

	ctx.CPU = this;
	ctx.MMU = &dynamic_cast<ARMSystemControlCoprocessor*>(coprocessors[15])->context;

	CompileASMRoutines();

	// FIXME:
	void* ram = new char[4096];
	AddPhysicalMemory(ram, 0, 4096);
	FILE* f = fopen("/projects/storage/ROM/lark_voyage_355c/flash.bin", "rb");
	fread(ram, 4096, 1, f);
	fclose(f);
}

ARMCPU::~ARMCPU()
{
	free_code_memory(compiled_functions.data, 64 * 1024);
	for (uint i = 0; i < 4 * 1024; i++) {
		PHYS_1MB_INFO* p = (PHYS_1MB_INFO*)phys_memory->items[i];
		if ((uintptr_t)p & 1) {
			p = (PHYS_1MB_INFO*)((uintptr_t)p - 1);
			delete p;
		}
	}
	delete phys_memory;
}

void ARMCPU::ClearCompiledMap()
{
	for (uint i = 0; i < COMPILED_MAP_ITEMS_COUNT; ++i) {
		register COMPILED_BLOCK_INFO** bi = compiled[i];
		if (bi) {
			for (uint j = 0; j < COMPILED_SUBMAP_ITEMS_COUNT; j++) {
				if (bi[j]) {
					delete bi[j];
				}
			}
			delete bi;
			compiled[i] = __null;
		}
	}
}

bool ARMCPU::InvalidateCompiledMap(uint32_t MVA)
{
	// returns true if entry was cleared
	COMPILED_BLOCK_INFO** sm = compiled[MVA >> 16];
	if (sm) {
		FIXME();
		uint index = (MVA & 0x0000FFFF) >> COMPILED_PAGE_SIZE_BITS;
		if (sm[index]) {
			delete sm[index];
			sm[index] = __null;
			return true;
		}
	}
	return false;
}

void ARMCPU::AddCompiledMapItem(uint32_t address, COMPILED_BLOCK_INFO* bi)
{
	COMPILED_BLOCK_INFO** sm = compiled[address >> 16];
	if (!sm) {
		sm = new COMPILED_BLOCK_INFO*[COMPILED_SUBMAP_ITEMS_COUNT];
		memset(sm, 0, COMPILED_SUBMAP_ITEMS_COUNT * sizeof(COMPILED_BLOCK_INFO*));
		compiled[address >> 16] = sm;
	}
	sm[(address & 0x0000FFFF) >> COMPILED_PAGE_SIZE_BITS] = bi;
}

COMPILED_BLOCK_INFO* ARMCPU::GetCompiledData(uint32_t address)
{
	COMPILED_BLOCK_INFO** sm = compiled[address >> 16];
	if (sm) {
		return sm[(address & 0x0000FFFF) >> COMPILED_PAGE_SIZE_BITS];
	}
	return __null;
}

void ARMCPU::AddPhysicalMemory(void* data, uint32_t start, uint32_t size)
{
	// page size granularity
	ASSERT((size % ARCHInfo->MinPageSize) == 0);
	ASSERT((start % ARCHInfo->MinPageSize) == 0);

	while (size) {
		if ((start % (1024 * 1024)) | (size < 1024 * 1024)) {
			// 1KB space
			PHYS_1MB_INFO* p = (PHYS_1MB_INFO*)phys_memory->items[start >> 20];
			if (!p) {
				p = new PHYS_1MB_INFO;
				memset(p, 0, sizeof(PHYS_1MB_INFO));
				phys_memory->items[start >> 20] = (void*)((uintptr_t)p | 1);
			} else {
				if (((uintptr_t)p) & 1) {
					// already 1MB mapping
					p = (PHYS_1MB_INFO*)(((uintptr_t)p) & ~1);
				} else {
					// mapped 1MB, split it
					uintptr_t old_p = (uintptr_t)p;
					p = new PHYS_1MB_INFO;
					phys_memory->items[start >> 20] = (void*)((uintptr_t)p | 1);
					for (uint i = 0; i < 1024; i++) {
						p->items[i] = (void*)old_p;
						old_p += 1024;
					}
				}
			}
			p->items[(start >> 10) & 0x3FF] = data;

			size -= 1024;
			start += 1024;
			data = (void*)(((uintptr_t)data) + 1024);
		} else {
			// 1MB space
			PHYS_1MB_INFO* p = (PHYS_1MB_INFO*)phys_memory->items[start >> 20];
			if (((uintptr_t)p) & 1) {
				// 1MB is already mapped, delete it
				p = (PHYS_1MB_INFO*)(((uintptr_t)p) & ~1);
				delete p;
			}
			phys_memory->items[start >> 20] = data;

			size -= 1024 * 1024;
			start += 1024 * 1024;
			data = (void*)(((uintptr_t)data) + 1024 * 1024);
		}
	}
}

#define PLACE_INTERRUPT_CHECK() { \
	void* data = ctx.output; \
	place_call_argument(0, imm, ctx.PC); \
	place_call_ptr_imm(&ctx.CPU->interrupt_check_function, 1); \
	ctx.output = data; \
}

CODEGEN_CALL void SwitchCPUModeInvalid()
{
	FIXME();
	asm("int 3");
}

void* ARMCPU::CompileSwitchCPUMode(void* data)
{
	for (int to = 0; to < 32; to++) {
		SwitchCPUMode[to] = &SwitchCPUModeInvalid;
		for (int from = 0; from < 32; from++) {
			compiled_functions.switch_cpu_mode[to][from] = &SwitchCPUModeInvalid;
		}
	}

#define has_SPSR(mode) ( \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_FIQ) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_IRQ) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_SVC) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_ABT) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_UND))

#define is_priv(mode) ( \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_SYS) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_FIQ) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_IRQ) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_SVC) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_ABT) || \
		(ARM_CPU_MODE_ ## mode == ARM_CPU_MODE_UND))

	// first step, check for current mode
#define check_current_mode(mode_to) \
	place_align(16); \
	SwitchCPUMode[ARM_CPU_MODE_ ## mode_to] = (p_void__void)data; \
	place_mov_reg32__ptr32_imm(regEAX, &context.uncached_CPSR); \
	place_and_reg32__imm(regEAX, 0x0000001F); \
	place_mov_reg32__ptr32_imm_reg_mul(regEAX, compiled_functions.switch_cpu_mode[ARM_CPU_MODE_ ## mode_to], regEAX, mul_ptr); \
	place_jmp_reg(regEAX)

#define save_banked_registers(mode, bank) \
	place_mov_reg32__ptr32_imm(regEAX, &context.regs[R13]); \
	place_mov_reg32__ptr32_imm(regECX, &context.regs[R14]); \
	place_mov_ptr32_imm__reg32(&context.banked.R13_ ## bank, regEAX); \
	place_mov_ptr32_imm__reg32(&context.banked.R14_ ## bank, regECX); \
	if (has_SPSR(mode)) { \
		place_mov_reg32__ptr32_imm(regEAX, &context.SPSR); \
		place_mov_ptr32_imm__reg32(&context.banked.SPSR_ ## bank, regEAX); \
	}

#define restore_banked_registers(mode, bank) \
	place_mov_reg32__ptr32_imm(regEAX, &context.banked.R13_ ## bank); \
	place_mov_reg32__ptr32_imm(regECX, &context.banked.R14_ ## bank); \
	place_mov_ptr32_imm__reg32(&context.regs[R13], regEAX); \
	place_mov_ptr32_imm__reg32(&context.regs[R14], regECX); \
	if (has_SPSR(mode)) { \
		place_mov_reg32__ptr32_imm(regEAX, &context.banked.SPSR_ ## bank); \
		place_mov_ptr32_imm__reg32(&context.SPSR, regEAX); \
	}

#define switch_mode(mode_to, mode_from, bank_restore, bank_save) \
	place_align(16); \
	compiled_functions.switch_cpu_mode[ARM_CPU_MODE_ ## mode_to][ARM_CPU_MODE_ ## mode_from] = (p_void__void)data; \
	if (ARM_CPU_MODE_ ## mode_to != ARM_CPU_MODE_ ## mode_from) { \
		save_banked_registers(mode_from, bank_save); \
		restore_banked_registers(mode_to, bank_restore); \
		/* restore mode flags */ \
		if (has_SPSR(mode_to) != has_SPSR(mode_from)) { \
			place_mov_ptr8_imm__imm(&context.has_SPSR, has_SPSR(mode_to)); \
		} \
		if (is_priv(mode_to) != is_priv(mode_from)) { \
			place_mov_ptr8_imm__imm(&context.is_priv, is_priv(mode_to)); \
		} \
		place_and_ptr32_imm__imm(&context.uncached_CPSR, ~0x1F); \
		place_or_ptr32_imm__imm(&context.uncached_CPSR, ARM_CPU_MODE_ ## mode_to); \
	} \
	place_ret()

#define add_switch(mode_to, bank_restore) \
	check_current_mode(mode_to); \
	switch_mode(mode_to, USR, bank_restore, usr); \
	switch_mode(mode_to, FIQ, bank_restore, fiq); \
	switch_mode(mode_to, IRQ, bank_restore, irq); \
	switch_mode(mode_to, SVC, bank_restore, svc); \
	switch_mode(mode_to, ABT, bank_restore, abt); \
	switch_mode(mode_to, UND, bank_restore, und); \
	switch_mode(mode_to, SYS, bank_restore, usr);

	add_switch(USR, usr);
	add_switch(FIQ, fiq);
	add_switch(IRQ, irq);
	add_switch(SVC, svc);
	add_switch(ABT, abt);
	add_switch(UND, und);
	add_switch(SYS, usr);

	return data;
}

void ARMCPU::CompileASMRoutines()
{
	void* data = allocate_code_memory(64 * 1024);
	compiled_functions.data = data;

	{
		// do not check for interrupt/no interrupts pending
		place_align(16);
		compiled_functions.no_interrupt_pending = data;
		interrupt_check_function = data;
		place_ret();
	}

	{
		// IRQ is pending
		place_align(16);
		compiled_functions.irq_pending = data;
		place_pop_reg(regECX); // return address
		place_call_obj_argument(0, reg32, regEAX);
		place_jmp_obj(this, ARMCPU, RaiseIRQException);
	}

	{
		// FIQ is pending
		place_align(16);
		compiled_functions.fiq_pending = data;
		place_pop_reg(regECX); // return address
		place_call_obj_argument(0, reg32, regEAX);
		place_jmp_obj(this, ARMCPU, RaiseFIQException);
	}

	{
		// Map PA to host address (CF set if IO space)
		place_align(16);
		PA_to_Host = (p_pvoid__uint32_t)data;
		place_mov_reg__reg(regECX, regEAX);											// MOV	ECX, address
		place_shr_reg32__imm(regECX, 20);											// SHR  ECX, 20		// 1MB block
		place_mov_reg__ptr_imm_reg_mul(regEDX, phys_memory, regECX, mul_ptr);		// MOV  EDX, [phys_memory + ECX * sizeof(void*)]
		place_test_reg32__reg32(regEDX, regEDX);									// TEST	EDX, EDX
		place_condjump(Z, IO_space);												// JZ	@IO_space
		place_test_reg32__imm(regEDX, 1);											// TEST	EDX, 1
		place_condjump(Z, MEM_1MB);													// JZ	@MEM_1MB
		place_and_reg__imm(regEDX, ~1);												// AND	EDX, ~1
		place_mov_reg__reg(regECX, regEAX);											// MOV	ECX, address
		place_shr_reg32__imm(regECX, 10);											// SHR  ECX, 10		// 1KB block
		place_and_reg32__imm(regECX, (1024 - 1));									// AND	EAX, (1024 - 1)
		place_mov_reg32__ptr32_reg_reg_mul(regEDX, regEDX, regECX, mul_ptr);		// MOV	EDX, [EDX + ECX * sizeof(void*)]
		place_and_reg32__imm(regEAX, (1024 - 1));									// AND	EAX, (1024 - 1)
		place_add_reg__reg(regEAX, regEDX);											// ADD	EAX, EDX
		place_clc();																// CLC
		place_ret();																// RET
		place_fix_label(MEM_1MB);													// @MEM_1MB:
		place_and_reg32__imm(regEAX, (1024 * 1024 - 1));							// AND	EAX, (1024 * 1024 - 1)
		place_add_reg__reg(regEAX, regEDX);											// ADD	EAX, EDX
		place_clc();																// CLC
		place_ret();																// RET
		place_fix_label(IO_space);													// @IO_space:
		place_stc();																// STC
		place_ret();																// RET
	}

	{
		// Map PA to host memory (NULL if IO space)
		place_align(16);
		PA_to_HostMem = (p_pvoid__uint32_t)data;
		place_call_imm(PA_to_Host, 1);												// CALL	PA_to_Host
		place_condjump(NC, ok);														// JNC	@ok
		place_mov_reg__imm(regEAX, __null);											// MOV	EAX, __null
		place_fix_label(ok);														// @ok:
		place_ret();																// RET
	}

	{
		// TODO: create both version:
		//  - MMU disabled
		//  - MMU enabled
		place_align(16);
		BranchHelper = (p_void__void)data;
		// instructions to be when calling this function:
		// CALL CPU->BranchHelper
		// DD	branch_destination

		// function will not return
		// if compiled code is found:
		// 1. functions call will be changed to direct jump
		// 2. execute code
		// if not, return to C/C++ code
		place_pop_reg(regESI);														// POP	ESI 		// return address (branch destination)
		place_mov_reg32__ptr32_reg(regEBX, regESI);									// MOV	EBX, [ESI]	// destination address
		// check alignment
		place_test_reg32__imm(regEBX, 0b11);										// TEST EBX, 0b11
		place_condjump(Z, properly_aligned);										// JZ	@properly_aligned
		place_breakpoint();															// FIXME: raise unaligned exception
		place_fix_label(properly_aligned);											// @properly_aligned:
		// check if MMU enabled, add process ID
		place_mov_reg32__ptr32_imm(regECX, &ctx.MMU->ControlRegister);				// MOV	ECX, [MMU.ControlRegister]
		place_test_reg32__imm(regECX, 1);											// TEST ECX, ECX	// check if MMU is enabled
		place_condjump(Z, MMU_disabled);											// JZ	@MMU_disabled
		place_test_reg32__imm(regEBX, 0xFE000000);									// TEST	EBX, 0xFE000000
		place_condjump(NZ, skip_process_id);										// JNZ  @skip_process_id
		place_or_reg32__ptr32_imm(regEBX, &ctx.MMU->ProcessID);						// OR	EBX, [MMU.ProcessID]
		place_fix_label(skip_process_id);											// @skip_process_id:
		place_fix_label(MMU_disabled);												// @MMU_disabled:
		// get compiled block
		place_call_obj_argument(0, reg32, regEBX);									// MOV	EDX, EBX	// currentPC
		place_call_obj(this, ARMCPU, GetCompiledData, 1);							// CALL	ARMCPU::GetCompiledData
		place_test_reg__reg(regEAX, regEAX);										// TEST	EAX, EAX	// check if compiled block found
		place_condjump(Z, return_to_C);												// JZ	@return_to_C
		// get compiled code address
		place_mov_reg32__reg32(regECX, regEBX);										// MOV	ECX, EBX	// destination address
		place_and_reg32__imm(regECX, COMPILED_PAGE_SIZE - 1);						// AND	ECX, (page_size-1)
		place_mov_reg__ptr_reg_reg_mul_imm(regEAX, regEAX, regECX, mul_ptr - COMPILED_GRANULARITY, offsetof(COMPILED_BLOCK_INFO, compiled_code)); // MOV	EAX, [EAX + ECX + offsetof(COMPILED_BLOCK_INFO::compiledcode))
		place_test_reg__reg(regEAX, regEAX);										// TEST	EAX, EAX
		place_condjump(Z, no_compiled_code_found);									// JZ	@no_compiled_code_found
		// replace call with direct jump to code
		place_mov_ptr8_reg_imm__imm(regESI, -5, 0xE9);								// MOV	byte ptr [ESI - 5], 0xE9
		place_mov_reg__reg(regECX, regEAX);											// MOV	ECX, EAX
		place_sub_reg__reg(regECX, regESI);											// SUB	ECX, ESI
		place_mov_ptr32_reg_imm__reg32(regESI, -4, regECX);							// MOV	[ESI - 4], ECX
		place_jmp_reg(regEAX);														// JMP	EAX
		// need to compile code
		place_fix_label(no_compiled_code_found);									// @no_compiled_code_found
		place_fix_label(return_to_C);												// @return_to_C:
		place_mov_ptr32_imm__reg32(&context.regs[PC], regEBX);						// MOV	[CPU.context[PC]], EBX
		place_ret();																// RET
	}

	{
		place_align(16);
		PCModifiedHelper = (p_void__void)data;
		// function will not return (always jump to this function!)
		// if compiled code is found, then execute code
		// else return to C/C++ code
		place_mov_reg32__ptr32_imm(regEBX, &context.regs[PC]);						// MOV	EBX, [CPU.context[PC]]	// destination address
		// check alignment
		place_test_reg32__imm(regEBX, 0b11);										// TEST EBX, 0b11
		place_condjump(Z, properly_aligned);										// JZ	@properly_aligned
		place_breakpoint();															// FIXME: raise unaligned exception
		place_fix_label(properly_aligned);											// @properly_aligned:
		// check if MMU enabled, add process ID
		place_mov_reg32__ptr32_imm(regECX, &ctx.MMU->ControlRegister);				// MOV	ECX, [MMU.ControlRegister]
		place_test_reg32__imm(regECX, 1);											// TEST ECX, ECX	// check if MMU is enabled
		place_condjump(Z, MMU_disabled);											// JZ	@MMU_disabled
		place_test_reg32__imm(regEBX, 0xFE000000);									// TEST	EBX, 0xFE000000
		place_condjump(NZ, skip_process_id);										// JNZ  @skip_process_id
		place_or_reg32__ptr32_imm(regEBX, &ctx.MMU->ProcessID);						// OR	EBX, [MMU.ProcessID]
		place_fix_label(skip_process_id);											// @skip_process_id:
		place_fix_label(MMU_disabled);												// @MMU_disabled:
		// get compiled block
		place_call_obj_argument(0, reg32, regEBX);									// MOV	EDX, ESI	// currentPC
		place_call_obj(this, ARMCPU, GetCompiledData, 1);							// CALL	ARMCPU::GetCompiledData
		place_test_reg__reg(regEAX, regEAX);										// TEST	EAX, EAX	// check if compiled block found
		place_condjump(Z, return_to_C);												// JZ	@return_to_C
		// get compiled code address
		place_mov_reg32__reg32(regECX, regEBX);										// MOV	ECX, EBX	// destination address
		place_and_reg32__imm(regECX, COMPILED_PAGE_SIZE - 1);						// AND	ECX, (page_size-1)
		place_mov_reg__ptr_reg_reg_mul_imm(regEAX, regEAX, regECX, mul_ptr - COMPILED_GRANULARITY, offsetof(COMPILED_BLOCK_INFO, compiled_code)); // MOV	EAX, [EAX + ECX + offsetof(COMPILED_BLOCK_INFO::compiledcode))
		place_test_reg__reg(regEAX, regEAX);										// TEST	EAX, EAX
		place_condjump(Z, no_compiled_code_found);									// JZ	@no_compiled_code_found
		place_jmp_reg(regEAX);														// JMP	EAX
		// need to compile code
		place_fix_label(no_compiled_code_found);									// @no_compiled_code_found
		place_fix_label(return_to_C);												// @return_to_C:
		place_mov_ptr32_imm__reg32(&context.regs[PC], regEBX);						// MOV	[CPU.context[PC]], EBX
		place_ret();																// RET
	}

	data = CompileSwitchCPUMode(data);
}

void* ARMCPU::MainLoopThread(void* data)
{
	pthread_setname_np(pthread_self(), "ARMCPU");
	((ARMCPU*)data)->MainLoop();
	return data;
}

void ARMCPU::MainLoop()
{
	CPU = this;
	MMU = ctx.MMU;
	Reset();
	while (powered_on) {
		uint32_t currentPC = context.regs[PC];
		uint32_t currentPC_mod = currentPC;
		if (ctx.MMU->ControlRegister.M) {
			if ((currentPC_mod & 0xFE000000) == 0) {
				currentPC_mod |= ctx.MMU->ProcessID.Word;
			}
		}

		//DEBUG_PRINT("--> Executing at: 0x%08X ==> 0x%08X\n", currentPC, currentPC_mod);

		if (context.uncached_CPSR.T) {
			// Thumb instruction set
			FIXME();
		} else {
			// ARM instruction set
			if (currentPC_mod & 0b11) {
				// unaligned instruction address
				DEBUG_PRINT("Unaligned instruction execution: 0x%08X\n", context.regs[PC]);
				FIXME();
			} else {
				COMPILED_BLOCK_INFO* cbi = GetCompiledData(currentPC_mod);
				if (!cbi) {
					// we need to create new block
					cbi = new COMPILED_BLOCK_INFO;
					cbi->data_size = 32 * 1024;
					cbi->data = allocate_code_memory(cbi->data_size);
					cbi->data_free = cbi->data;
					AddCompiledMapItem(currentPC_mod, cbi);
				}

				// address within page
				uint32_t PC_index = currentPC_mod & (COMPILED_PAGE_SIZE - 1);
				if (!has_compiled_code(cbi, PC_index)) {
					// translate MVA to PA
					uint32_t phys_PC;;
					if (ctx.MMU->ControlRegister.M) {
						// convert from VA to PA
#ifdef MMU_TRANSLATE_ASM
						uint64_t PA = ctx.MMU->VA_to_PA64[SizeWord][AccessExecute](currentPC_mod);
#else
						uint64_t PA = ctx.MMU->VA_to_PA[SizeWord][AccessExecute](ctx.MMU, currentPC_mod);
#endif
						if (PA > 0xFFFFFFFF) {
							RaisePrefetchException(currentPC_mod);
							continue;
						}
						phys_PC = PA & 0xFFFFFFFF;
					} else {
						phys_PC = currentPC;
					}

					ARM_INSTRUCTION* data = (ARM_INSTRUCTION*)PA_to_HostMem(phys_PC);
					if (!data) {
						ASSERT(false, "attempt to execute from invalid address (0x%08X)", CPU.context.regs[PC]);
						// FIXME: do exception
						FIXME();
						continue;
					}
					ctx.output = cbi->data_free;
					ctx.PC = currentPC;
					ctx.PC_PA = phys_PC;
					// need to compile code at this address
					uint32_t page_limit = PC_index;
					// interrupt should be checked at instruction boundary.
					// because of performance reason we do this only on block start
					*compiled_code_addr(cbi, page_limit) = ctx.output;
					PLACE_INTERRUPT_CHECK();
					while (decode_arm_instruction(&ctx, *data++)) {
						ctx.PC += sizeof(ARM_INSTRUCTION);
						ctx.PC_PA += sizeof(ARM_INSTRUCTION);
						page_limit += sizeof(ARM_INSTRUCTION);
						if (page_limit == COMPILED_PAGE_SIZE) {
							// branch helper needed, for jump to next page
							void* data = ctx.output;
							place_call_imm(BranchHelper, 0);
							emit32(ctx.PC);
							ctx.output = data;
							break;
						}
						compiled_function p = compiled_code_value(cbi, page_limit);
						if (p) {
							// instruction at this address is already compiled in this page
							void* data = ctx.output;
							place_jmp_imm(p);
							ctx.output = data;
							break;
						}
						*compiled_code_addr(cbi, page_limit) = ctx.output;
					}
					cbi->data_free = ctx.output;
				}
				// execute block
				cbi->execute(PC_index);
			}
		}
	}
}

void ARMCPU::PowerOn()
{
	if (!powered_on) {
		powered_on = true;
		pthread_create(&thread, __null, &ARMCPU::MainLoopThread, this);
	}
}

void ARMCPU::PowerOff()
{
	if (powered_on) {
		powered_on = false;
		pthread_join(thread, __null);
	}
}

void ARMCPU::Reset()
{
	// start CPU mode
	context.uncached_CPSR.M = ARM_CPU_MODE_SVC;
	context.is_priv = true;
	context.has_SPSR = true;

	context.uncached_CPSR.T = 0;	// Execute in ARM state
	context.uncached_CPSR.F = 1;	// Disable fast interrupts
	context.uncached_CPSR.I = 1;	// Disable normal interrupts
	context.uncached_CPSR.A = 1;	// Disable Imprecise Aborts (v6 only)
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_RESET;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}
	// reset all coprocessors
	for (int i = 0; i < 16; i++) {
		if (coprocessors[i]) {
			coprocessors[i]->Reset();
		}
	}

	// update interrupt check function address
	interrupt_check_function = compiled_functions.no_interrupt_pending;
}

uint32_t ARMCPU::ReadCPSR()
{
	PSR_REGISTER ret = context.read_CPSR();
	return ret.Word;
}

void ARMCPU::UpdateInterruptCheckFunction()
{
	switch ((context.uncached_CPSR.Word >> 6) & 3) {
		case 0b00:
			// FIQ and IRQ are enabled
			if (context.pending_interrupt & INT_PENDING_FIQ) {
				interrupt_check_function = compiled_functions.fiq_pending;
			} else {
				if (context.pending_interrupt & INT_PENDING_IRQ) {
					interrupt_check_function = compiled_functions.irq_pending;
				} else {
					interrupt_check_function = compiled_functions.no_interrupt_pending;
				}
			}
			break;
		case 0b01:
			// FIQ is disabled
			if (context.pending_interrupt & INT_PENDING_IRQ) {
				interrupt_check_function = compiled_functions.irq_pending;
			} else {
				interrupt_check_function = compiled_functions.no_interrupt_pending;
			}
			break;
		case 0b10:
			// IRQ is disabled
			if (context.pending_interrupt & INT_PENDING_FIQ) {
				interrupt_check_function = compiled_functions.fiq_pending;
			} else {
				interrupt_check_function = compiled_functions.no_interrupt_pending;
			}
			break;
		case 0b11:
			// FIQ and IRQ are disabled
			interrupt_check_function = compiled_functions.no_interrupt_pending;
			break;
	}
}

bool ARMCPU::WriteCPSR(uint32_t value, uint32_t mask)
{
	//DEBUG_PRINT("WriteCPSR: new mode: %s, PC: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)(value & 0x1f)), context.regs[PC]);
	uint32_t m;
	m = mask & ARCHInfo->PSRBits.PrivMask;
	if (m) {
		if (context.is_priv) {
			// Privileged bits: A, I, F, M[4:0]
			m &= ~0x1F;
			ARM_CPU_MODE new_mode = (ARM_CPU_MODE)((0x10 | value) & 0x1F);
			if (context.uncached_CPSR.M != new_mode) {
				// change processor mode
				SwitchCPUMode[new_mode]();
			}
			// update A, I, F flags
			m = mask & ARCHInfo->PSRBits.PrivMask & 0x1C0;
			InterruptFunctionLock();
			context.uncached_CPSR.Word &= ~m;
			context.uncached_CPSR.Word |= value & m;
			UpdateInterruptCheckFunction();
			InterruptFunctionUnlock();
		}
	}
	m = mask & ARCHInfo->PSRBits.UserMask;
	if (m) {
		// User-writeable bits: N, Z, C, V, Q, GE[3:0], E
		context.uncached_CPSR.Word &= ~m;
		context.uncached_CPSR.Word |= value & m;
		context.flags.N = context.uncached_CPSR.N;
		context.flags.Z = context.uncached_CPSR.Z;
		context.flags.C = context.uncached_CPSR.C;
		context.flags.V = context.uncached_CPSR.V;
	}
	m = mask & ARCHInfo->PSRBits.StateMask;
	if (m) {
		// Execution state bits: T, J
		// need to return to C/C++ code to switch to another execution state: ARM/Thumb/Jazelle
		context.uncached_CPSR.Word &= ~m;
		context.uncached_CPSR.Word |= value & m;
	}
	// return zero if need to return to C/C++ code
	return m == 0;
}

void ARMCPU::RaiseAbortException(uint32_t currentPC)
{
	//DEBUG_PRINT("RaiseAbortException (mode: %s), PC: 0x%08X, VA: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC, ctx.MMU->FaultAddress.Word);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_ABT) {
		// already in abort mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 8;
	} else {
		context.banked.SPSR_abt = context.read_CPSR();
		context.banked.R14_abt = currentPC + 8;
		SwitchCPUMode[ARM_CPU_MODE_ABT](); // Enter Abort mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.A = 1; // Disable Imprecise Data Aborts (v6 only)
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_DATA_ABORT;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	if (!context.uncached_CPSR.F && (context.pending_interrupt & INT_PENDING_FIQ)) {
		interrupt_check_function = compiled_functions.fiq_pending;
	} else {
		interrupt_check_function = compiled_functions.no_interrupt_pending;
	}
	InterruptFunctionUnlock();
}

void ARMCPU::RaisePrefetchException(uint32_t currentPC)
{
	//DEBUG_PRINT("RaisePrefetchException (mode: %s), PC: 0x%08X, VA: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC, ctx.MMU->FaultAddress.Word);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_ABT) {
		// already in abort mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 4;
	} else {
		context.banked.SPSR_abt = context.read_CPSR();
		context.banked.R14_abt = currentPC + 4;
		SwitchCPUMode[ARM_CPU_MODE_ABT](); // Enter Abort mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.A = 1; // Disable Imprecise Data Aborts (v6 only)
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_PREFETCH_ABORT;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	if (!context.uncached_CPSR.F && (context.pending_interrupt & INT_PENDING_FIQ)) {
		interrupt_check_function = compiled_functions.fiq_pending;
	} else {
		interrupt_check_function = compiled_functions.no_interrupt_pending;
	}
	InterruptFunctionUnlock();
}

void ARMCPU::RaiseUndefinedException(uint32_t currentPC)
{
	//DEBUG_PRINT("RaiseUndefinedException (mode: %s), PC: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_UND) {
		// already in undefined mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 4;
	} else {
		context.banked.SPSR_und = context.read_CPSR();
		context.banked.R14_und = currentPC + 4;
		SwitchCPUMode[ARM_CPU_MODE_UND](); // Enter Undefined mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_UNDEFINED;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	if (!context.uncached_CPSR.F && (context.pending_interrupt & INT_PENDING_FIQ)) {
		interrupt_check_function = compiled_functions.fiq_pending;
	} else {
		interrupt_check_function = compiled_functions.no_interrupt_pending;
	}
	InterruptFunctionUnlock();
}

void ARMCPU::RaiseSWIException(uint32_t currentPC)
{
	//DEBUG_PRINT("RaiseSWIException (mode: %s), PC: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_SVC) {
		// already in SWI mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 4;
	} else {
		context.banked.SPSR_svc = context.read_CPSR();
		context.banked.R14_svc = currentPC + 4;
		SwitchCPUMode[ARM_CPU_MODE_SVC](); // Enter SWI mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_SWI;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	if (!context.uncached_CPSR.F && (context.pending_interrupt & INT_PENDING_FIQ)) {
		interrupt_check_function = compiled_functions.fiq_pending;
	} else {
		interrupt_check_function = compiled_functions.no_interrupt_pending;
	}
	InterruptFunctionUnlock();
}

void ARMCPU::RaiseIRQException(uint32_t currentPC)
{
	//DEBUG_PRINT("RaiseIRQException (mode: %s), PC: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_IRQ) {
		// already in IRQ mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 4; // next instruction is at currentPC (check for interrupt before isntruction execution)
	} else {
		context.banked.SPSR_irq = context.read_CPSR();
		context.banked.R14_irq = currentPC + 4; // next instruction is at currentPC (check for interrupt before isntruction execution)
		SwitchCPUMode[ARM_CPU_MODE_IRQ](); // Enter IRQ mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.A = 1; // Disable Imprecise Data Aborts (v6 only)
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_IRQ;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	if (!context.uncached_CPSR.F && (context.pending_interrupt & INT_PENDING_FIQ)) {
		interrupt_check_function = compiled_functions.fiq_pending;
	} else {
		interrupt_check_function = compiled_functions.no_interrupt_pending;
	}
	InterruptFunctionUnlock();
}

void ARMCPU::RaiseFIQException(uint32_t currentPC)
{
	ASSERT(context.uncached_CPSR.F == 0);
	//DEBUG_PRINT("RaiseFIQException (mode: %s), PC: 0x%08X\n", arm_mode_name((ARM_CPU_MODE)context.uncached_CPSR.M), currentPC);
	if (context.uncached_CPSR.M == ARM_CPU_MODE_FIQ) {
		// already in FIQ mode
		context.SPSR = context.read_CPSR();
		context.regs[R14] = currentPC + 4; // next instruction is at currentPC (check for interrupt before isntruction execution)
	} else {
		context.banked.SPSR_fiq = context.read_CPSR();
		context.banked.R14_fiq = currentPC + 4; // next instruction is at currentPC (check for interrupt before isntruction execution)
		SwitchCPUMode[ARM_CPU_MODE_FIQ](); // Enter FIQ mode
	}
	InterruptFunctionLock();
	context.uncached_CPSR.T = 0; // Execute in ARM state
	context.uncached_CPSR.F = 1; // Disable fast interrupts
	context.uncached_CPSR.I = 1; // Disable normal interrupts
	context.uncached_CPSR.A = 1; // Disable Imprecise Data Aborts (v6 only)
	context.uncached_CPSR.E = ctx.MMU->ControlRegister.EE; // Endianness on exception entry

	context.regs[PC] = EXCEPTION_VECTOR_FIQ;
	if (ctx.MMU->ControlRegister.M && ctx.MMU->ControlRegister.V) {
		context.regs[PC] |= EXCEPTION_VECTOR_HIGH;
	}

	// update interrupt check function address
	interrupt_check_function = compiled_functions.no_interrupt_pending;
	InterruptFunctionUnlock();
}

COMPILED_BLOCK_INFO::COMPILED_BLOCK_INFO()
{
	data = __null;
	data_free = __null;
	data_size = 0;
	memset(compiled_code, 0, sizeof(compiled_code));
}

COMPILED_BLOCK_INFO::~COMPILED_BLOCK_INFO()
{
	if (data) {
		free_code_memory(data, data_size);
	}

}

void COMPILED_BLOCK_INFO::execute(unsigned int offset)
{
	//FIXME: save registers
	asm("pusha");
	compiled_code_value(this, offset)();
	asm("popa");
}

void ARMCPU::SetIRQPending()
{
	InterruptFunctionLock();
	if ((context.pending_interrupt & INT_PENDING_IRQ) == 0) {
		context.pending_interrupt |= INT_PENDING_IRQ;
		switch ((context.uncached_CPSR.Word >> 6) & 3) {
			case 0b00:
				// IRQ and FIQ are enabled
				// FIQ has higher priority
				if (context.pending_interrupt & INT_PENDING_FIQ) {
					interrupt_check_function = compiled_functions.fiq_pending;
				} else {
					interrupt_check_function = compiled_functions.irq_pending;
				}
				break;
			case 0b01:
				// FIQ is disabled
				interrupt_check_function = compiled_functions.irq_pending;
				break;
			case 0b10:
				// IRQ is disabled
				if (context.pending_interrupt & INT_PENDING_FIQ) {
					interrupt_check_function = compiled_functions.fiq_pending;
				} else {
					interrupt_check_function = compiled_functions.no_interrupt_pending;
				}
				break;
			case 0b11:
				// FIQ and IRQ are disabled
				interrupt_check_function = compiled_functions.no_interrupt_pending;
				break;
		}
	}
	InterruptFunctionUnlock();
	idleEvent.Signal();
}

void ARMCPU::ClearIRQPending()
{
	InterruptFunctionLock();
	if (context.pending_interrupt & INT_PENDING_IRQ) {
		context.pending_interrupt &= ~INT_PENDING_IRQ;
		switch ((context.uncached_CPSR.Word >> 6) & 3) {
			case 0b00:
			case 0b10:
				// FIQ is enabled
				if (context.pending_interrupt & INT_PENDING_FIQ) {
					interrupt_check_function = compiled_functions.fiq_pending;
				} else {
					interrupt_check_function = compiled_functions.no_interrupt_pending;
				}
				break;
			case 0b01:
			case 0b11:
				// FIQ is disabled
				interrupt_check_function = compiled_functions.no_interrupt_pending;
				break;
		}
	}
	InterruptFunctionUnlock();
}

void ARMCPU::SetFIQPending()
{
	InterruptFunctionLock();
	if ((context.pending_interrupt & INT_PENDING_FIQ) == 0) {
		context.pending_interrupt |= INT_PENDING_FIQ;
		switch ((context.uncached_CPSR.Word >> 6) & 3) {
			case 0b00:
			case 0b10:
				// IRQ and FIQ are enabled
				// IRQ is disabled
				// FIQ has higher priority
				interrupt_check_function = compiled_functions.fiq_pending;
				break;
			case 0b01:
				// FIQ is disabled
				if (context.pending_interrupt & INT_PENDING_IRQ) {
					interrupt_check_function = compiled_functions.irq_pending;
				} else {
					interrupt_check_function = compiled_functions.no_interrupt_pending;
				}
				break;
			case 0b11:
				// IRQ and FIQ are disabled
				interrupt_check_function = compiled_functions.no_interrupt_pending;
				break;
		}
	}
	InterruptFunctionUnlock();
}

void ARMCPU::ClearFIQPending()
{
	InterruptFunctionLock();
	if (context.pending_interrupt & INT_PENDING_FIQ) {
		context.pending_interrupt &= ~INT_PENDING_FIQ;
		switch ((context.uncached_CPSR.Word >> 6) & 3) {
			case 0b00:
			case 0b01:
				// IRQ is enabled
				if (context.pending_interrupt & INT_PENDING_IRQ) {
					interrupt_check_function = compiled_functions.irq_pending;
				} else {
					interrupt_check_function = compiled_functions.no_interrupt_pending;
				}
				break;
			case 0b10:
			case 0b11:
				// IRQ is disabled
				interrupt_check_function = compiled_functions.no_interrupt_pending;
				break;
		}
	}
	InterruptFunctionUnlock();
}

void ARMCPU::EnterIdle()
{
	idleEvent.WaitFor();
}
