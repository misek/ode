/*
 * decoder.h
 *
 *  Created on: 12-04-2012
 *      Author: Łukasz Misek
 */

#ifndef DECODER_H_
#define DECODER_H_

class ARMCPU;
struct ARMSystemControlCoprocessorContext;

#include "utils.h"
#include "armcpu.h"
#include "codegen.h"
#include "deasm.h"

#define n_GPR(no) ctx->CPU->context.regs[no]
#define n_Rd (instr.DataProcessing.Rd)
#define n_Rn (instr.DataProcessing.Rn)
#define n_Rm (instr.DataProcessing.Rm)
#define n_Rs (instr.DataProcessing.Rs)
#define p_GPR(no) &n_GPR(no)
#define p_Rd p_GPR(n_Rd)
#define p_Rn p_GPR(n_Rn)
#define p_Rm p_GPR(n_Rm)
#define p_Rs p_GPR(n_Rs)

union ARM_INSTRUCTION {
	uint32_t Word;

	ARM_INSTRUCTION(uint32_t v);

	struct {
		uint32_t data:25;
		uint32_t type:3;
		uint32_t cond:4;
	};
	union {
		struct {
			uint32_t Rm:4;
			uint32_t R:1;
			uint32_t shift:2;
			uint32_t :1; // must be 0
			uint32_t Rs:4;
			uint32_t Rd:4;
			uint32_t Rn:4;
			uint32_t S:1;
			uint32_t opcode:4;
			uint32_t I:1;
		};
		struct {
			uint32_t :7;
			uint32_t shift_amount:5;
		};
		struct {
			uint32_t immediate:8;
			uint32_t rotate:4;
		};
	} DataProcessing;
	struct {
		uint32_t immed:24;
		uint32_t L:1;
	} Branch;
	union {
		struct {
			uint32_t Rm:4;
			uint32_t :4; // must be 0x1001
			uint32_t Rs:4;
			uint32_t Rn:4;
			uint32_t Rd:4;
			uint32_t S:1;
			uint32_t A:1;
			uint32_t Un:1;
			uint32_t L:1;
		};
		struct {
			uint32_t :20;
			uint32_t op1:4;
		};
		struct {
			uint32_t :12;
			uint32_t RdLo:4;
			uint32_t RdHi:4;
		};
	} Multiply;
	union {
		struct {
			uint32_t offset_12:12;
			uint32_t Rd:4;
			uint32_t Rn:4;
			uint32_t L:1;
			uint32_t W:1;
			uint32_t B:1;
			uint32_t U:1;
			uint32_t P:1;
			uint32_t R:1;
		};
		struct {
			uint32_t Rm:4;
			uint32_t M:1; // must be 0
			uint32_t shift:2;
			uint32_t shift_imm:5;
		};
	} LoadStore;
	struct {
		uint32_t register_list:16;
		uint32_t Rn:4;
		uint32_t L:1;
		uint32_t W:1;
		uint32_t S:1;
		uint32_t U:1;
		uint32_t P:1;
	} LoadStoreMultiple;
	struct {
		uint32_t :12; // SBZ
		uint32_t Rd:4;
		uint32_t :4; // SBO
		uint32_t :1; // 0
		uint32_t :1; // 0
		uint32_t R:1;
		uint32_t :1; // 0
		uint32_t :1; // 1
		uint32_t :1; // 0
		uint32_t :1; // 0
		uint32_t :1; // 0
	} MovePSRtoRegister;
	union {
		struct {
			uint32_t Rm:4;
			uint32_t op2:4;
			uint32_t Rs:4;
			uint32_t Rd:4;
			uint32_t Rn:4;
			uint32_t :1; // must be 0
			uint32_t op1:2;
			uint32_t :2; // must be 0b10
			uint32_t I:1;
			uint32_t :2; // must be 0b00
		};
		struct {
			uint32_t immed_8:8;
			uint32_t rotate_imm:4;
		};
	} ControlDSPExtension;
	union {
		struct {
			uint32_t Rm:4;
			uint32_t :1;
			uint32_t op1:2;
			uint32_t :1;
			uint32_t Rs:4;
			uint32_t Rd:4;
			uint32_t Rn:4;
			uint32_t L:1;
			uint32_t W:1;
			uint32_t I:1;
			uint32_t U:1;
			uint32_t P:1;
		};
		struct {
			uint32_t LoOffset:4;
			uint32_t :1;
			uint32_t H:1;
			uint32_t S:1;
			uint32_t :1;
			uint32_t HiOffset:4;
		};
	} LoadStoreExtension;
	struct {
		uint32_t Immediate:8;
		uint32_t rotate_imm:4;
		uint32_t :4; // SBO
		uint32_t field_c:1;
		uint32_t field_x:1;
		uint32_t field_s:1;
		uint32_t field_f:1;
		uint32_t :2; // 0b10
		uint32_t R:1;
		uint32_t :2; // 0b10
		uint32_t I:1;
		uint32_t :1; // 0
		uint32_t :1; // 0
	} MoveRegisterToPSR;
	struct {
		uint32_t CRm:4;
		uint32_t Bit_4:1;	// must be 1
		uint32_t opcode_2:3;
		uint32_t cp_num:4;
		uint32_t Rd:4;
		uint32_t CRn:4;
		uint32_t L:1;
		uint32_t opcode_1:3;
		uint32_t SWI:1;
	} CoprocessorRegisterTransfer;
	struct {
		uint32_t :4;
		uint32_t opcode2:4;
		uint32_t :12;
		uint32_t opcode1:8;
	} UnconditionalInstruction;
};

struct DecoderContext {
	void* output;
	uint32_t PC;
	uint32_t PC_PA;
	ARMCPU* CPU;
	ARMSystemControlCoprocessorContext* MMU;
	bool last_jump;
};

void* init_code_memory(void* data);
#define compiled_code_end()
// returns false, if current instruction is jump, or PC change (unconditional only)
bool decode_arm_instruction(DecoderContext* ctx, const ARM_INSTRUCTION instr);

void* place_UNPREDICTABLE(void* data);
void* place_UNDEFINED(void* data);

#ifdef DEBUG_DEASSEMBLED_CODE

extern char deassembled_instruction[1024];

#define DEBUG_INSTRUCTION(PC, PC_PA, instr) { \
	if (deasm_instruction(PC, instr, deassembled_instruction)) { \
		/*DEBUG_PRINT("[%08X => %08X] 0x%08X\t%s\n", PC, PC_PA, instr, deassembled_instruction);*/ \
	} else { \
		DEBUG_PRINT("unknown instruction at: 0x%08X = 0x%08X\n", PC, instr); \
		FIXME(); \
	} \
}
#else
#define DEBUG_INSTRUCTION(PC, PC_PA, instr)
#endif

#endif /* DECODER_H_ */
