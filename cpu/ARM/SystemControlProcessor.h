/*
 * SystemControlProcessor.h
 *
 *  Created on: 15-06-2012
 *      Author: Łukasz Misek
 */

#ifndef SYSTEMCONTROLPROCESSOR_H_
#define SYSTEMCONTROLPROCESSOR_H_

#include "coprocessor.h"

// translate VA to PA function to ASM
#define MMU_TRANSLATE_ASM
// use TLBB in translation routine (only C++ routine)
//#define MMU_USE_TLB

union PTELevel1Descriptor5 {
	uint32_t Word;
	uint32_t type:2;
	struct {

	} Fault;
	struct {
		uint32_t :2;
		uint32_t B:1;
		uint32_t C:1;
		uint32_t IMP:1;
		uint32_t Domain:4;
		uint32_t :1; // SBZ
		uint32_t AP:2;
		uint32_t :8; // SBZ
		uint32_t BaseAddress:12;
	} Section;
	struct {
		uint32_t :2;
		uint32_t IMP:3;
		uint32_t Domain:4;
		uint32_t :1; // SBZ
		uint32_t TableBaseAddress:22;
	} Coarse;
	struct {
		uint32_t :2;
		uint32_t IMP:3;
		uint32_t Domain:4;
		uint32_t :3; // SBZ
		uint32_t FinePageTableBase:20;
	} Fine;
};

union PTELevel1Descriptor6 {
	uint32_t Word;
};

union PTELevel2Descriptor5 {
	uint32_t Word;
	uint32_t type:2;
	struct {

	} Fault;
	struct {
		uint32_t :2;
		uint32_t B:1;
		uint32_t C:1;
		uint32_t AP0:2;
		uint32_t AP1:2;
		uint32_t AP2:2;
		uint32_t AP3:2;
		uint32_t :4;
		uint32_t BaseAddress:16;
	} Large;
	struct {
		uint32_t :2;
		uint32_t B:1;
		uint32_t C:1;
		uint32_t AP0:2;
		uint32_t AP1:2;
		uint32_t AP2:2;
		uint32_t AP3:2;
		uint32_t BaseAddress:20;
	} Small;
	struct {
		uint32_t :2;
		uint32_t B:1;
		uint32_t C:1;
		uint32_t AP:2;
		uint32_t :4;
		uint32_t BaseAddress:22;
	} Tiny;
};

union PTELevel2Descriptor6 {
	uint32_t Word;
};

#ifdef MMU_USE_TLB
#define TLB_ENTRY_COUNT	64

enum TLBEntry_Type {
	tlbInvalid = 0,
	tlbFake = 1,
	tlb1MB = 2,
	tlb64KB = 3,
	tlb16KB = 4,
	tlb4KB = 5,
	tlb1KB = 6
};

struct TLBEntry {
	TLBEntry_Type type;
	uint32_t MVA;
	uint32_t PA;
	uint32_t Domain;
	uint32_t AP;
};

struct TLBUnit {
	int current;
	TLBEntry entry[TLB_ENTRY_COUNT];
};
#endif

enum MMUAccessType {
	AccessRead = 0,
	AccessWrite = 1,
	AccessReadWrite = 2,
	AccessExecute = 3	// it is used to select ITLBUnit, no actual permission in VMSAv5
};

enum MMUAccessSize {
	SizeByte = 0,
	SizeHalfWord = 1,
	SizeWord = 2
};

enum MMUFaultStatus {
	MMUFault_Alignment						= 0b0001,	// may be also 0b0011
	MMUFault_ExternalAbortLevel1			= 0b1100,
	MMUFault_ExternalAbortLevel2			= 0b1110,
	MMUFault_TranslationSection				= 0b0101,
	MMUFault_TranslationPage				= 0b0111,
	MMUFault_DomainSection					= 0b1001,
	MMUFault_DomainPage						= 0b1011,
	MMUFault_PermissionSection				= 0b1101,
	MMUFault_PermissionPage					= 0b1111,
	MMUFault_ExternalAbortSection			= 0b1000,
	MMUFault_ExternalAbortPage				= 0b1010
};

class ARMSystemControlCoprocessor;

#ifdef MMU_TRANSLATE_ASM
typedef uint32_t (CODEGEN_CALL *fun_VA_to_PA)(uint32_t VA);
typedef uint64_t (CODEGEN_CALL *fun_VA_to_PA64)(uint32_t VA);
#else
typedef uint64_t (CODEGEN_CALL *fun_VA_to_PA)(ARMSystemControlCoprocessorContext* MMU, uint32_t VA);
#endif

struct ARMSystemControlCoprocessorContext {
	ARMSystemControlCoprocessor* owner;

	uint32_t* TTBR0;

#ifdef MMU_USE_TLB
	TLBUnit instructionTLB;
	TLBUnit dataTLB;
#endif

	uint32_t last_VA;
	uint32_t last_VA_PA;

	fun_VA_to_PA VA_to_PA[3][4];
#ifdef MMU_TRANSLATE_ASM
	fun_VA_to_PA64 VA_to_PA64[3][4]; // returns PA in 64-bit value where v[32:63] == 0 when no errors
#endif

	// Register 1: Control registers
	union {
		uint32_t Word;
		struct {
			uint32_t M:1;
			uint32_t A:1;
			uint32_t C:1;
			uint32_t W:1;
			uint32_t P:1;
			uint32_t D:1;
			uint32_t L:1;
			uint32_t B:1;
			uint32_t S:1;
			uint32_t R:1;
			uint32_t F:1;
			uint32_t Z:1;
			uint32_t I:1;
			uint32_t V:1;
			uint32_t RR:1;
			uint32_t L4:1;
			uint32_t :5;
			uint32_t FI:1;
			uint32_t U:1;
			uint32_t XP:1;
			uint32_t VE:1;
			uint32_t EE:1;
			uint32_t L2:1;
			uint32_t :5; // SBZ/UNP
		};
	} ControlRegister;
	union {
		uint32_t Word;
		// IMPLEMENTATION DEFINED
	} AuxiliaryControlRegister;
	union {
		uint32_t Word;
		struct {
			uint32_t cp0:2;
			uint32_t cp1:2;
			uint32_t cp2:2;
			uint32_t cp3:2;
			uint32_t cp4:2;
			uint32_t cp5:2;
			uint32_t cp6:2;
			uint32_t cp7:2;
			uint32_t cp8:2;
			uint32_t cp9:2;
			uint32_t cp10:2;
			uint32_t cp11:2;
			uint32_t cp12:2;
			uint32_t cp13:2;
			uint32_t :4; // UNP/SBZP
		};
	} CoprocessorAccessControlRegister;

	// Register 2: Translation table base
	union {
		uint32_t Word;
		struct {
			uint32_t C:1;
			uint32_t S:1;
			uint32_t IMP:1;
			uint32_t RGN:2;
			uint32_t TranslationTableBase0:27;
		};
	} TranslationTableBase0;
	union {
		uint32_t Word;
		struct {
			uint32_t C:1;
			uint32_t S:1;
			uint32_t IMP:1;
			uint32_t RGN:2;
			uint32_t :9;
			uint32_t TranslationTableBase1:18;
		};
	} TranslationTableBase1;
	union {
		uint32_t Word;
		struct {
			uint32_t N:3;
		};
	} TranslationTableBaseControl;

	// Register 3: Domain access control
	union {
		uint32_t Word;
		struct {
			uint32_t D0:2;
			uint32_t D1:2;
			uint32_t D2:2;
			uint32_t D3:2;
			uint32_t D4:2;
			uint32_t D5:2;
			uint32_t D6:2;
			uint32_t D7:2;
			uint32_t D8:2;
			uint32_t D9:2;
			uint32_t D10:2;
			uint32_t D11:2;
			uint32_t D12:2;
			uint32_t D13:2;
			uint32_t D14:2;
			uint32_t D15:2;
		};
	} DomainAccessControl;

	// Register 5: Fault Status Register
	union {
		uint32_t Word;
		struct {
			uint32_t Status:4;
			uint32_t Domain:4;
			uint32_t :1; // always 0
			uint32_t :23; // UNP/SBZ
		};
	} DataFaultStatus, InstructionFaultStatus;
	union {
		uint32_t Word;
		struct {

		};
	} FaultAddress;

	// Register 7: Cache Management
	union {
		uint32_t Word;
		struct {
			uint32_t C:1;
		};
	} CacheDirtyStatus;
	union {
		uint32_t Word;
		struct {
			uint32_t R:1;
		};
	} BlockTransferStatus;

	// Register 13: FCSE PID/Context ID
	union {
		uint32_t Word;
		struct {
			uint32_t :25;
			uint32_t PID:7;
		};
	} ProcessID;
	union {
		uint32_t Word;
		struct {
			uint32_t ASIC:8;
			uint32_t PROCID:24;
		};
	} ContextID;

#ifndef MMU_TRANSLATE_ASM
	CODEGEN_CALL void RaiseMMUFault(MMUFaultStatus code, uint32_t address);
#endif

	ARMSystemControlCoprocessorContext() { memset(this, 0, sizeof(ARMSystemControlCoprocessorContext)); };
};

class ARMSystemControlCoprocessor: public ARMCoprocessor {
private:
	void* codeData;

	CODEGEN_CALL void DumpVAMapping();
	CODEGEN_CALL bool SetControlRegisterHelper(uint32_t value, uint32_t currentPC);
#ifdef MMU_USE_TLB
	CODEGEN_CALL void InvalidateTLB(TLBUnit* tlb);
	CODEGEN_CALL void InvalidateTLBEntry(TLBUnit* tlb, uint32_t MVA);
#endif
#ifdef MMU_TRANSLATE_ASM
	void* CompileVA_to_PA(MMUAccessSize size, MMUAccessType access, void* data);
#endif
	CODEGEN_CALL void CompileFunctions();

protected:
	virtual bool DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr);
public:
	ARMSystemControlCoprocessor(ARMCPU* owner);
	~ARMSystemControlCoprocessor();
	ARMSystemControlCoprocessorContext context;

	virtual void Reset();

};

#endif /* SYSTEMCONTROLPROCESSOR_H_ */
