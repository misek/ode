/*
 * SystemControlProcessor.cpp
 *
 *  Created on: 15-06-2012
 *      Author: Łukasz Misek
 */

#include "SystemControlProcessor.h"

#define COMPILED_CODE_SIZE (4 * 1024)

#ifndef MMU_TRANSLATE_ASM

//#define DEBUG_TRANSLATION DEBUG_PRINT
#define DEBUG_TRANSLATION(...)

template <MMUAccessSize access_size, MMUAccessType access_type> CODEGEN_CALL uint64_t VA_to_PA_v5(ARMSystemControlCoprocessorContext* MMU, uint32_t VA)
{
	uint32_t page_size_mask = ~(MMU->owner->cpu->ARCHInfo->MinPageSize - 1);

#define CHECK_AP(ap, descriptor_type) \
	switch (ap) { \
		case 0b00: \
			if ((access_type == AccessRead) || (access_type == AccessExecute)) { \
				/* ROM memory, read-only */ \
				switch ((MMU->ControlRegister.Word >> 8) & 3) { \
					case 0b00: \
						/* Priv: No access		User: No access */ \
						MMUFault(MMUFault_Permission##descriptor_type); \
						break; \
					case 0b01: \
						/* Priv: Read			User: No access */ \
						if (!MMU->owner->cpu->context.is_priv) { \
							MMUFault(MMUFault_Permission##descriptor_type); \
						} \
						break; \
					case 0b10: \
						/* Priv: Read			User: Read */ \
						break; \
					case 0b11: \
						/* reserved */ \
						FIXME(); \
						break; \
				} \
			} else { \
				/* ROM memory, any write is prohibited */ \
				MMUFault(MMUFault_Permission##descriptor_type); \
			} \
			break; \
		case 0b01: \
			/* Priv: Read/Write		User: No access */ \
			if (!MMU->owner->cpu->context.is_priv) { \
				MMUFault(MMUFault_Permission##descriptor_type); \
			} \
			break; \
		case 0b10: \
			/* Priv: Read/Write		User: Read */ \
			if ((access_type == AccessReadWrite) || (access_type == AccessWrite)) { \
				if (!MMU->owner->cpu->context.is_priv) { \
					MMUFault(MMUFault_Permission##descriptor_type); \
				} \
			} \
			break; \
		case 0b11: \
			/* Priv: Read/Write		User: Read/Write */ \
			break; \
	}

#define CHECK_DOMAIN(domain_no, ap, descriptor_type) \
	switch ((MMU->DomainAccessControl.Word >> (domain_no * 2)) & 3) { \
		case 0b00: \
			/* No access */ \
			MMUFault(MMUFault_Domain##descriptor_type); \
			break; \
		case 0b01: \
			/* Client */ \
			CHECK_AP(ap, descriptor_type); \
			break; \
		case 0b10: \
			/* Reserved */ \
			MMUFault(MMUFault_Domain##descriptor_type); \
			break; \
		case 0b11: \
			/* Manager */ \
			break; \
	}

#define MMUFault(type) \
	MMU->RaiseMMUFault(type, VA); \
	return uint64_t(-1);

#ifdef MMU_USE_TLB
	TLBUnit* pTLBUnit;
	// select proper TLB unit
	if (access_type == AccessExecute) {
		pTLBUnit = &MMU->instructionTLB;
	} else {
		pTLBUnit = &MMU->dataTLB;
	}
#endif

	if ((VA & page_size_mask) == MMU->last_VA) {
		return MMU->last_VA_PA | (VA & ~page_size_mask);
	}

	register uint32_t MVA = VA;

	// convert VA to MVA
	if ((MVA & 0xFE000000) == 0) {
		MVA |= MMU->ProcessID.Word;
	}

	if (access_type != AccessExecute) {
		if (access_size == SizeWord) {
			if ((MVA & 0b11) && MMU->ControlRegister.A) {
				//FIXME();
			}
		}
		if (access_size == SizeHalfWord) {
			if ((MVA & 0b1) && MMU->ControlRegister.A) {
				//FIXME();
			}
		}
	}

	uint32_t PA = 0;

#ifdef MMU_USE_TLB
	int tlbIndex = pTLBUnit->current;
	register TLBEntry* entry = &pTLBUnit->entry[tlbIndex];
	for (uint i = 0; i < TLB_ENTRY_COUNT; ++i) {
		entry = &pTLBUnit->entry[tlbIndex];
		switch (entry->type) {
			case tlbInvalid:
				// invalid, empty
				break;
			case tlbFake:
				// fake entry, immediately remove it
				entry->type = tlbInvalid;
				return MVA;
				goto TLB_found;
				break;
			case tlb1MB:
				// 1MB entry
				if (entry->MVA == (MVA & 0xFFF00000)) {
					CHECK_DOMAIN(entry->Domain, entry->AP, Section);
					PA = entry->PA | (MVA & 0x000FFFFF);
					goto TLB_found;
				}
				break;
			case tlb64KB:
				// 64KB entry
				if (entry->MVA == (MVA & 0xFFFF0000)) {
					CHECK_DOMAIN(entry->Domain, entry->AP, Page);
					PA = entry->PA | (MVA & 0x0000FFFF);
					goto TLB_found;
				}
				break;
			case tlb16KB:
				// 16KB entry
				if (entry->MVA == (MVA & 0xFFFFC000)) {
					CHECK_DOMAIN(entry->Domain, entry->AP, Page);
					PA = entry->PA | (MVA & 0x00003FFF);
					goto TLB_found;
				}
				break;
			case tlb4KB:
				// 4KB entry
				if (entry->MVA == (MVA & 0xFFFFF000)) {
					CHECK_DOMAIN(entry->Domain, entry->AP, Page);
					PA = entry->PA | (MVA & 0x00000FFF);
					goto TLB_found;
				}
				break;
			case tlb1KB:
				// 1KB entry
				if (entry->MVA == (MVA & 0xFFFFFC00)) {
					CHECK_DOMAIN(entry->Domain, entry->AP, Page);
					PA = entry->PA | (MVA & 0x000003FF);
					goto TLB_found;
				}
				break;
			default:
				ASSERT(FALSE);
				break;
		}
		tlbIndex = (tlbIndex + 1) & (TLB_ENTRY_COUNT - 1);
	}

	// valid TLB not found
	tlbIndex = (tlbIndex + 1) & (TLB_ENTRY_COUNT - 1);
	entry = &pTLBUnit->entry[tlbIndex];
#endif
	// do table walk
	PTELevel1Descriptor5 descL1;
	register PTELevel2Descriptor5* descL2;
	uint32_t pL2;

	if (MMU->TTBR0 == __null) {
		if (access_type != AccessExecute) {
			MMUFault(MMUFault_ExternalAbortSection);
		}
	} else {
		descL1.Word = MMU->TTBR0[MVA >> 20];

		switch (descL1.type) {
			case 0b00:
				// fault
				// Section translation fault
				MMUFault(MMUFault_TranslationSection);
				break;
			case 0b01:
				// coarse
				pL2 = (descL1.Coarse.TableBaseAddress << 10) + ((MVA >> 10) & 0x3FC);
				descL2 = (PTELevel2Descriptor5*)MMU->owner->cpu->PA_to_HostMem(pL2);
				if (descL2) {
					switch (descL2->type) {
						case 0b00:
						case 0b11:
							// fault
							// tiny - invalid for coarse
							MMUFault(MMUFault_TranslationPage);
							break;
						case 0b01:
							// large
							CHECK_DOMAIN(descL1.Coarse.Domain, (descL2->Word >> (4 + ((MVA >> 13) & 0b110))) & 3, Page);
							PA = (descL2->Large.BaseAddress << 16) | (MVA & 0x0000FFFF);
#ifdef MMU_USE_TLB
							switch ((descL2->Word >> 4) & 0b11111111) {
								case 0b00000000:
								case 0b01010101:
								case 0b10101010:
								case 0b11111111:
									// all four APs are equal
									entry->type = tlb64KB;
									entry->MVA = MVA & 0xFFFF0000;
									entry->PA = descL2->Large.BaseAddress << 16;
									entry->Domain = descL1.Coarse.Domain;
									entry->AP = descL2->Large.AP0;
									DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 64KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, entry->MVA, entry->PA, entry->Domain, entry->AP);
									break;
								default:
									// APs are different
									entry->type = tlb16KB;
									entry->MVA = MVA & 0xFFFFC000;
									entry->PA = (descL2->Large.BaseAddress << 16) | (MVA & 0x0000C000);
									entry->Domain = descL1.Coarse.Domain;
									entry->AP = (descL2->Word >> (4 + ((MVA >> 13) & 0b110))) & 3;
									DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 16KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, entry->MVA, entry->PA, entry->Domain, entry->AP);
									break;
							}
#else
							DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 16KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, MVA, PA, descL1.Coarse.Domain, (descL2->Word >> (4 + ((MVA >> 13) & 0b110))) & 3);
#endif
							break;
						case 0b10:
							// small
							CHECK_DOMAIN(descL1.Coarse.Domain, (descL2->Word >> (4 + ((MVA >> 9) & 0b110))) & 3, Page);
							PA = (descL2->Small.BaseAddress << 12) | (VA & 0x00000FFF);
#ifdef MMU_USE_TLB
							switch ((descL2->Word >> 4) & 0b11111111) {
								case 0b00000000:
								case 0b01010101:
								case 0b10101010:
								case 0b11111111:
									// all four APs are equal
									entry->type = tlb4KB;
									entry->MVA = MVA & 0xFFFFF000;
									entry->PA = descL2->Small.BaseAddress << 12;
									entry->Domain = descL1.Coarse.Domain;
									entry->AP = descL2->Small.AP0;
									DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 4KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, entry->MVA, entry->PA, entry->Domain, entry->AP);
									break;
								default:
									// APs are different
									entry->type = tlb1KB;
									entry->MVA = MVA & 0xFFFFFC00;
									entry->PA = (descL2->Small.BaseAddress << 12) | (MVA & 0x00000C00);
									entry->Domain = descL1.Coarse.Domain;
									entry->AP = (descL2->Word >> (4 + ((MVA >> 9) & 0b110))) & 3;
									DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 1KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, entry->MVA, entry->PA, entry->Domain, entry->AP);
									break;
							}
#else
							DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 1KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, MVA, PA, descL1.Coarse.Domain, (descL2->Word >> (4 + ((MVA >> 9) & 0b110))) & 3);
#endif
							break;
					}
				} else {
					FIXME();
					// Page translation fault
					MMUFault(MMUFault_ExternalAbortPage);
				}
				break;
			case 0b10:
				// section
				CHECK_DOMAIN(descL1.Section.Domain, descL1.Section.AP, Section);
				PA = (descL1.Section.BaseAddress << 20) | (MVA & 0x000FFFFF);
#ifdef MMU_USE_TLB
				entry->type = tlb1MB;
				entry->MVA = MVA & 0xFFF00000;
				entry->PA = descL1.Section.BaseAddress << 20;
				entry->Domain = descL1.Section.Domain;
				entry->AP = descL1.Section.AP;
#endif
				DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 1024KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, MVA, PA, descL1.Section.Domain, descL1.Section.AP);
				break;
			case 0b11:
				// fine
				pL2 = (descL1.Fine.FinePageTableBase << 12) + ((MVA >> 8) & 0xFFC);
				descL2 = (PTELevel2Descriptor5*)MMU->owner->cpu->PA_to_HostMem(pL2);
				if (descL2) {
					switch (descL2->type) {
						case 0b00:
						case 0b01:
						case 0b10:
							// fault
							// large - invalid
							// small - invalid
							MMUFault(MMUFault_TranslationPage);
							break;
						case 0b11:
							// tiny
							CHECK_DOMAIN(descL1.Coarse.Domain, descL2->Tiny.AP, Page);
							PA = (descL2->Tiny.BaseAddress << 10) | (MVA & 0x000003FF);
#ifdef MMU_USE_TLB
							entry->type = tlb1KB;
							entry->MVA = MVA & 0xFFFFFC00;
							entry->PA = descL2->Tiny.BaseAddress << 10;
							entry->Domain = descL1.Coarse.Domain;
							entry->AP = descL2->Tiny.AP;
#endif
							DEBUG_TRANSLATION("[%s] Translate: 0x%08X ==> 0x%08X ==> 0x%08X, 1KB, domain: %u, AP: %u\n", access_type == AccessExecute ? "EXEC" : "DATA", VA, MVA, PA, descL1.Coarse.Domain, descL2->Tiny.AP);
							break;
					}
				} else {
					FIXME();
					// Page translation fault
					MMUFault(MMUFault_ExternalAbortPage);
				}
				break;
		}
	}

	// save translation result to use with cache
	MMU->last_VA = VA & page_size_mask;
	MMU->last_VA_PA = PA & page_size_mask;
#ifdef MMU_USE_TLB
	pTLBUnit->current = tlbIndex;
TLB_found:
#endif
	return PA;

#undef MMUFault
}
#endif

ARMSystemControlCoprocessor::ARMSystemControlCoprocessor(ARMCPU* owner) : ARMCoprocessor(owner)
{
	codeData = __null;
	context.last_VA = (uint32_t)-1;
	context.owner = this;
	CompileFunctions();
}

ARMSystemControlCoprocessor::~ARMSystemControlCoprocessor()
{
	if (codeData) {
		free_code_memory(codeData, COMPILED_CODE_SIZE);
	}
}

void ARMSystemControlCoprocessor::Reset()
{
	ARMCoprocessor::Reset();
}

bool ARMSystemControlCoprocessor::DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr)
{
	// FIXME:
	/*
	 * Prior to ARMv6, MCR and MRC instructions can only be used when the processor is
	 * in a privileged mode. If they are executed when the processor is in User mode,
	 * an Undefined Instruction exception occurs.	 *
	 */

	register void* data = ctx->output;
	bool ret_value = true;

	// FIXME:MCRR instruction

	if (instr.CoprocessorRegisterTransfer.Rd == 15) {
		// R15 is not allowed for Rd
		//FIXME: goto unpredictable;
	}
	switch (instr.CoprocessorRegisterTransfer.CRn) {
		case 0:
			// ID codes (read-only)
			// Processor ID, Cache, Tightly-coupled Memory and TLB type
			if (instr.CoprocessorRegisterTransfer.L) {
				switch (instr.CoprocessorRegisterTransfer.CRm) {
					case 0:
						{
							// returns Main ID register if requested is not available
							uint idx = instr.CoprocessorRegisterTransfer.opcode_2;
							if (idx && ((ctx->CPU->CPUInfo->MMUIdentification.valid & (1 << idx)) == 0)) {
								idx = 0;
							}
							place_mov_ptr32_imm__imm(p_Rd, ((uint32_t*)&ctx->CPU->CPUInfo->MMUIdentification.MainIDRegister)[idx]);
						}
						break;
					default:
						goto unpredictable;
				}
			} else {
				goto unpredictable;
			}
			break;
		case 1:
			// Control bits (read/write)
			// System Configuration Bits
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					// FIXME: check access permission
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0:
							// Control register
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.ControlRegister);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								place_call_obj_argument(0, ptr32_imm, p_Rd);
								place_call_obj_argument(1, imm, ctx->PC);
								place_call_obj(this, ARMSystemControlCoprocessor, SetControlRegisterHelper, 2);
								place_test_reg32__reg32(regEAX, regEAX)
								place_condjump(Z, done);
								place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC + 4);
								place_ret();
								place_fix_label(done);
							}
							break;
						case 1:
							// Auxiliary control register (format IMPLEMENTATION DEFINED)
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.AuxiliaryControlRegister);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								// FIXME: write control bits
								FIXME();
							}
							break;
						case 2:
							// Coprocessor access control register
							FIXME();
							break;
						default:
							goto undef;
					}
					break;
				default:
					goto undef;
			}
			break;
		case 2:
			// Memory protection and control
			// Page Table Control
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Translation Table Base 0 (TTBR0)
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.TranslationTableBase0);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								place_mov_reg32__ptr32_imm(regEAX, p_Rd);
								place_mov_ptr32_imm__reg32(&context.TranslationTableBase0, regEAX);
								if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_v6_UP)) {
									FIXME();
								} else {
									place_and_reg32__imm(regEAX, 0xFFFFC000);
								}
								place_call_argument(0, reg32, regEAX);
								place_call_imm(ctx->CPU->PA_to_HostMem, 1);
								place_mov_ptr_imm__reg(&context.TTBR0, regEAX);
								// recompile functions
								place_call_obj(this, ARMSystemControlCoprocessor, CompileFunctions, 0);
							}
							break;
						case 0b001:
							// Translation Table Base 1(TTBR1)
							if (instr.CoprocessorRegisterTransfer.L) {
								FIXME();
							} else {
								FIXME();
							}
							break;
						case 0b010:
							// Translation Table Base Control
							if (instr.CoprocessorRegisterTransfer.L) {
								FIXME();
							} else {
								FIXME();
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 3:
			// Memory protection and control
			// Domain Access Control
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Domain access control
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.DomainAccessControl);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								place_mov_reg32__ptr32_imm(regEAX, p_Rd);
								place_mov_ptr32_imm__reg32(&context.DomainAccessControl, regEAX);
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 4:
			// Memory protection and control
			// Reserved
			goto unpredictable;
			break;
		case 5:
			// Memory protection and control
			// Fault status
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Data Fault Status Register
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.DataFaultStatus);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								FIXME();
							}
							break;
						case 0b001:
							// Instruction Fault Status Register
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.InstructionFaultStatus);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								FIXME();
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 6:
			// Memory protection and control
			// Fault address
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Fault Address
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.FaultAddress);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								FIXME();
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 7:
			// Cache and write buffer
			// Cache/write buffer control
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b100:
							// Wait for interrupt
							if (instr.CoprocessorRegisterTransfer.L) {
								FIXME();
							} else {
								// do nothing
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 4:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// PA value access - read and write (write for debug)
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 5:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Invalidate entire instruction cache (flush branch target cache, if applicable)
							place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC + 4);
							place_jmp_obj(ctx->CPU, ARMCPU, ClearCompiledMap);
							break;
						case 0b001:
							// Invalidate instruction cache line
							{
								place_call_obj_argument(0, ptr32_imm, p_Rd);
								place_call_obj(ctx->CPU, ARMCPU, InvalidateCompiledMap, 1);
								place_test_reg32__reg32(regEAX, regEAX);
								place_condjump(Z, ok);
								place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC + 4);
								place_ret();
								place_fix_label(ok);
							}
							break;
						case 0b010:
							// Invalidate instruction cache line
							FIXME();
							break;
						case 0b100:
							// Flush prefetch buffer (PrefetchFlush)
							FIXME();
							break;
						case 0b110:
							// Flush entire branch target cache (if applicable)
							FIXME();
							break;
						case 0b111:
							// Flush branch target cache entry (if applicable)
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 6:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Invalidate entire data cache
							FIXME();
							break;
						case 0b001:
							// Invalidate data cache line
							FIXME();
							break;
						case 0b010:
							// Invalidate data cache line
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 7:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Invalidate both instruction and data caches or unified cache (flush branch target cache, if applicable)
							FIXME();
							break;
						case 0b001:
							// Invalidate unified cache line
							FIXME();
							break;
						case 0b010:
							// Invalidate unified cache line
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 8:
					// PA lookup operations (execution, MCR only)
					FIXME();
					break;
				case 10:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Clean entire data cache
							break;
						case 0b001:
							// Clean data cache line
							break;
						case 0b010:
							// Clean data cache line
							break;
						case 0b011:
							// Test and clean (optional)
							if (instr.CoprocessorRegisterTransfer.L) {
								if (n_Rd == PC) {
									// set Z-flag
									place_mov_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 1);
								} else {
									FIXME();
								}
							} else {
								FIXME();
							}
							break;
						case 0b100:
							// Data Synchronization Barrier (formerly Drain Write Buffer)
							//FIXME();
							break;
						case 0b101:
							// Data Memory Barrier (Introduced with ARMv6. May be applied to earlier architecture variants.)
							FIXME();
							break;
						case 0b110:
							// Cache Dirty Status Register
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 11:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Clean entire unified cache
							FIXME();
							break;
						case 0b001:
							// Clean unified cache line
							FIXME();
							break;
						case 0b010:
							// Clean unified cache line
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 12:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Clean Data Cache Range
							FIXME();
							break;
						case 0b001:
							// Prefetch Instruction Range
							FIXME();
							break;
						case 0b010:
							// Prefetch Data Range
							FIXME();
							break;
						case 0b100:
							// Stop Prefetch Range
							FIXME();
							break;
						case 0b101:
							// Prefetch Status
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 13:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b001:
							// Prefetch instruction cache line (optional)
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 14:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Clean and invalidate entire data cache
							// nothing to do
							break;
						case 0b001:
							// Clean and invalidate data cache line
							// nothing to do
							break;
						case 0b010:
							// Clean and invalidate data cache line
							// nothing to do
							break;
						case 0b011:
							// Test, clean and invalidate (optional)
							if (instr.CoprocessorRegisterTransfer.L) {
								if (n_Rd == PC) {
									// set Z-flag
									place_mov_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 1);
								} else {
									FIXME();
								}
							} else {
								FIXME();
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				case 15:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// Clean and invalidate entire unified cache
							FIXME();
							break;
						case 0b001:
							// Clean and invalidate unified cache line
							FIXME();
							break;
						case 0b010:
							// Clean and invalidate unified cache line
							FIXME();
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 8:
			// Memory protection and control
			// TLB control
			if (instr.CoprocessorRegisterTransfer.L) {
				// c8 is write-only register
				goto unpredictable;
			} else {
				switch (instr.CoprocessorRegisterTransfer.CRm) {
					case 5:
						switch (instr.CoprocessorRegisterTransfer.opcode_2) {
							case 0:
								// Invalidate entire instruction TLB
#ifdef MMU_USE_TLB
								place_call_argument(0, imm, &MMU.instructionTLB);
								place_call_imm(InvalidateTLB, 1);
#endif
								break;
							case 1:
								// Invalidate instruction single entry
#ifdef MMU_USE_TLB
								place_call_argument(1, ptr32_imm, p_Rd);
								place_call_argument(0, imm, &MMU.instructionTLB);
								place_call_imm(InvalidateTLBEntry, 2);
#endif
								break;
							case 2:
								// Invalidate on ASID match instruction TLB
								FIXME();
								break;
							default:
								goto unpredictable;
						}
						break;
					case 6:
						switch (instr.CoprocessorRegisterTransfer.opcode_2) {
							case 0:
								// Invalidate entire data TLB
#ifdef MMU_USE_TLB
								place_call_argument(0, imm, &MMU.dataTLB);
								place_call_imm(InvalidateTLB, 1);
#endif
								break;
							case 1:
								// Invalidate data single entry
#ifdef MMU_USE_TLB
								place_call_argument(1, ptr32_imm, p_Rd);
								place_call_argument(0, imm, &MMU.dataTLB);
								place_call_imm(InvalidateTLBEntry, 2);
#endif
								break;
							case 2:
								// Invalidate on ASID match data TLB
								FIXME();
								break;
							default:
								goto unpredictable;
						}
						break;
					case 7:
						switch (instr.CoprocessorRegisterTransfer.opcode_2) {
							case 0:
								// Invalidate entire unified TLB or both
								// instruction and data TLBs
#ifdef MMU_USE_TLB
								place_call_argument(0, imm, &MMU.instructionTLB);
								place_call_imm(InvalidateTLB, 1);
								place_call_argument(0, imm, &MMU.dataTLB);
								place_call_imm(InvalidateTLB, 1);
#endif
								break;
							case 1:
								// Invalidate unified single entry
#ifdef MMU_USE_TLB
								place_call_argument(1, ptr32_imm, p_Rd);
								place_call_argument(0, imm, &MMU.instructionTLB);
								place_call_imm(InvalidateTLBEntry, 2);
								place_call_argument(1, ptr32_imm, p_Rd);
								place_call_argument(0, imm, &MMU.dataTLB);
								place_call_imm(InvalidateTLBEntry, 2);
#endif
								break;
							case 2:
								// Invalidate on ASID match unified TLB
								FIXME();
								break;
							default:
								goto unpredictable;
						}
						break;
					default:
						goto unpredictable;
				}
			}
			break;
		case 9:
			// Cache and write buffer
			// Cache lockdown
			FIXME();
			break;
		case 10:
			// Memory protection and control
			// TLB lockdown
			FIXME();
			break;
		case 11:
			// Tightly-coupled Memory Control
			// DMA Control
			FIXME();
			break;
		case 12:
			// Reserved
			// Reserved
			FIXME();
			break;
		case 13:
			// Process ID
			// Process ID
			switch (instr.CoprocessorRegisterTransfer.CRm) {
				case 0:
					switch (instr.CoprocessorRegisterTransfer.opcode_2) {
						case 0b000:
							// FCSE PID
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.ProcessID);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								place_mov_reg32__ptr32_imm(regEAX, p_Rd);
								place_and_reg32__imm(regEAX, 0xFE000000);
								place_mov_ptr32_imm__imm(&context.last_VA, -1);
								place_mov_ptr32_imm__reg32(&context.ProcessID, regEAX);
							}
							break;
						case 0b001:
							// Context ID
							if (instr.CoprocessorRegisterTransfer.L) {
								place_mov_reg32__ptr32_imm(regEAX, &context.ContextID);
								place_mov_ptr32_imm__reg32(p_Rd, regEAX);
							} else {
								FIXME();
								place_mov_reg32__ptr32_imm(regEAX, p_Rd);
								place_mov_ptr32_imm__reg32(&context.ContextID, regEAX);
							}
							break;
						default:
							goto unpredictable;
							break;
					}
					break;
				default:
					goto unpredictable;
			}
			break;
		case 14:
			// Reserved
			// -
			FIXME();
			break;
		case 15:
			// IMPLEMENTATION DEFINED
			// IMPLEMENTATION DEFINED
			FIXME();
			break;
	}
decode_end:
	ctx->output = data;
	return ret_value;
undef:
	ret_value = false;
	FIXME();
	data = place_UNDEFINED(data);
	goto decode_end;
unpredictable:
	ret_value = false;
	FIXME();
	data = place_UNPREDICTABLE(data);
	goto decode_end;
	ASSERT(false);
	return false;
}

#ifdef MMU_USE_TLB
void ARMSystemControlCoprocessor::InvalidateTLB(TLBUnit* tlb)
{
	for (uint i = 0; i < TLB_ENTRY_COUNT; i++) {
		tlb->entry[i].type = tlbInvalid;
	}
}

void ARMSystemControlCoprocessor::InvalidateTLBEntry(TLBUnit* tlb, uint32_t MVA)
{
	for (uint i = 0; i < TLB_ENTRY_COUNT; ++i) {
		register TLBEntry* entry = &tlb->entry[0];
		switch (entry->type) {
			case tlbInvalid:
				// invalid, empty
				break;
			case tlbFake:
				// fake entry
				break;
			case tlb1MB:
				// 1MB entry
				if (entry->MVA == (MVA & 0xFFF00000)) {
					entry->type = tlbInvalid;
				}
				break;
			case tlb64KB:
				// 64KB entry
				if (entry->MVA == (MVA & 0xFFFF0000)) {
					entry->type = tlbInvalid;
				}
				break;
			case tlb16KB:
				// 16KB entry
				if (entry->MVA == (MVA & 0xFFFFC000)) {
					entry->type = tlbInvalid;
				}
				break;
			case tlb4KB:
				// 4KB entry
				if (entry->MVA == (MVA & 0xFFFFF000)) {
					entry->type = tlbInvalid;
				}
				break;
			case tlb1KB:
				// 1KB entry
				if (entry->MVA == (MVA & 0xFFFFFC00)) {
					entry->type = tlbInvalid;
				}
				break;
			default:
				ASSERT(FALSE);
				break;
		}
	}
}
#endif

void ARMSystemControlCoprocessor::DumpVAMapping()
{
	if (cpu->CheckArch(CHECK_ARM_ARCH_v6_UP)) {
		FIXME();
	} else {
		// ARMv5 and below
		PTELevel1Descriptor5* tlb = (PTELevel1Descriptor5*)context.TTBR0;
		for (uint i = 0; i < 4 * 1024; i++) {
			switch (tlb->type) {
				case 0b00:
					// fault
					break;
				case 0b01:
					// coarse page table
					{
						PTELevel2Descriptor5* tlb2 = (PTELevel2Descriptor5*)cpu->PA_to_HostMem(tlb->Coarse.TableBaseAddress << 10);
						for (uint j = 0; j < 1024; j++) {
							switch (tlb2->type) {
								case 0b00:
									// fault
									break;
									FIXME();
								case 0b01:
									// large
									DEBUG_PRINT("  0x%08X => 0x%08X, 64kB\n", (i << 20) | (j << 12), tlb2->Large.BaseAddress << 16);
									break;
								case 0b10:
									// small
									DEBUG_PRINT("  0x%08X => 0x%08X, 4kB\n", (i << 20) | (j << 12), tlb2->Small.BaseAddress << 12);
									break;
								case 0b11:
									// tiny
									FIXME();
									break;
							}
							tlb2++;
						}
					}
					break;
				case 0b10:
					// section
					DEBUG_PRINT("0x%08X => 0x%08X, 1MB\n", i << 20, tlb->Section.BaseAddress << 20);
					break;
				case 0b11:
					// fine page table
					FIXME();
					break;
			}
			tlb++;
		}
	}
}

bool ARMSystemControlCoprocessor::SetControlRegisterHelper(uint32_t value, uint32_t currentPC)
{
	// return true if compiled map was cleared
	if ((context.ControlRegister.M == 0) && (value & 1)) {
		// enable MMU

		// WinCE does not set valid PTE entry mapping current PA => VA,
		// assuming that next instruction (MOV PC, R0) is already prefetched,
		// we add current PC to MMU translation cache

		context.last_VA = (currentPC + 4) & ~(cpu->ARCHInfo->MinPageSize - 1);
		context.last_VA_PA = context.last_VA;

		context.ControlRegister.Word = value;

		cpu->ClearCompiledMap();
		CompileFunctions();
		// set PC to next instruction
		cpu->context.regs[PC] = currentPC + 4;

		// compiled code is not available. we need return to C/C++ code
		asm("leave");
		asm("pop	eax");
		asm("ret");
	} else if (context.ControlRegister.M && !(value & 1)) {
		// disable MMU
		FIXME();
		CompileFunctions();
	}

	return false;
}

#ifdef MMU_TRANSLATE_ASM
void* ARMSystemControlCoprocessor::CompileVA_to_PA(MMUAccessSize size, MMUAccessType access, void* data)
{
	if (cpu->CheckArch(CHECK_ARM_ARCH_v5 | CHECK_ARM_ARCH_v4)) {
		place_align(16);
		context.VA_to_PA[size][access] = (fun_VA_to_PA)data;
		place_push_reg(regEBP);	// L2 descriptor (if used)
		place_push_reg(regESI);	// L1 descriptor
		place_push_reg(regEDI);	// original VA
		place_mov_reg32__reg32(regEDI, regEAX);	// EDI - original VA address
		if (context.ControlRegister.M) {
			// VA to PA mapping
			// argument:
			//		EAX -> VA
			// returns:
			//		EAX <- PA
			//		C flag set if mapping error, MMU Fault should be called
			uint32_t page_size_mask = ~(cpu->ARCHInfo->MinPageSize - 1);

#define place_result_OK() \
	place_pop_reg(regEDI); \
	place_pop_reg(regESI); \
	place_pop_reg(regEBP); \
	place_clc(); \
	place_ret()

#define place_result_OK_save() \
	place_mov_reg32__reg32(regECX, regEAX); \
	place_and_reg32__imm(regEDI, page_size_mask); \
	place_and_reg32__imm(regECX, page_size_mask); \
	place_mov_ptr32_imm__reg32(&MMU->last_VA, regEDI); \
	place_mov_ptr32_imm__reg32(&MMU->last_VA_PA, regECX); \
	place_result_OK()

#define place_result_ERROR() \
	place_pop_reg(regEDI); \
	place_pop_reg(regESI); \
	place_pop_reg(regEBP); \
	place_stc(); \
	place_ret()

#define place_MMUFault(code) \
	place_and_ptr32_imm__imm(&context.DataFaultStatus, 0xFFFFFF00); \
	place_or_ptr32_imm__imm(&context.DataFaultStatus, MMUFault_##code); \
	place_mov_ptr32_imm__reg32(&context.FaultAddress, regEDI); \
	place_result_ERROR()

#define place_CHECK_ROM_ACCESS(type) { \
	place_mov_reg32__ptr32_imm(regEDX, &context.ControlRegister); \
	place_test_reg8__imm(regDH, 1); \
	place_condjump(NZ, ROM_type_x1); \
	place_test_reg8__imm(regDH, 2); \
	place_condjump(NZ, ROM_type_10); \
	{ \
		/* Priv: No access		User: No access (0b00) */ \
		place_MMUFault(Permission##type); \
	} \
	place_fix_label(ROM_type_x1); \
	place_test_reg8__imm(regDH, 2); \
	place_condjump(NZ, ROM_type_11); \
	{ \
		/* Priv: Read			User: No access (0b01) */ \
		place_cmp_ptr8_imm__imm(&cpu->context.is_priv, 0); \
		place_condjump(NZ, access_OK); \
		place_MMUFault(Permission##type); \
		place_fix_label(access_OK); \
	} \
	place_fix_label(ROM_type_11); \
	{ \
		/* reserved (0b11) */ \
		place_breakpoint(); \
		place_MMUFault(Permission##type); \
	} \
	place_fix_label(ROM_type_10); \
	{ \
		/* Priv: Read			User: Read (0b10) */ \
	} \
}

// AP is placed on lower two bits of AL
#define place_CHECK_AP(type) { \
	place_test_reg8__imm(regAL, 1); \
	place_condjump(NZ, AP_type_x1); \
	place_test_reg8__imm(regAL, 2); \
	place_condjump(NZ, AP_type_10); \
	{ \
		/* ROM memory, read-only (0b00) */ \
		if ((access == AccessRead) || (access == AccessExecute)) { \
			place_CHECK_ROM_ACCESS(type); \
		} else { \
			/* ROM memory, any write is prohibited */ \
			place_MMUFault(Permission##type); \
		} \
	} \
	place_jump(done_00); \
	place_fix_label(AP_type_10); \
	{ \
		/* Priv: Read/Write		User: Read (0b10) */ \
		if ((access == AccessReadWrite) || (access == AccessWrite)) { \
			place_cmp_ptr8_imm__imm(&cpu->context.is_priv, 0); \
			place_condjump(NZ, access_OK); \
			place_MMUFault(Permission##type); \
			place_fix_label(access_OK); \
		} \
	} \
	place_jump(done_10); \
	place_fix_label(AP_type_x1); \
	place_test_reg8__imm(regAL, 2); \
	place_condjump(NZ, AP_type_11); \
	{ \
		/* Priv: Read/Write		User: No access (0b01) */ \
		place_cmp_ptr8_imm__imm(&cpu->context.is_priv, 0); \
		place_condjump(NZ, access_OK); \
		place_MMUFault(Permission##type); \
		place_fix_label(access_OK); \
	} \
	place_fix_label(AP_type_11); \
	{ \
		/* Priv: Read/Write		User: Read/Write (0b11) */ \
	} \
	place_fix_label(done_00); \
	place_fix_label(done_10); \
}

// AP from L1.Section
#define place_get_AP_Section() \
	place_mov_reg32__reg32(regEAX, regESI); \
	place_shr_reg32__imm(regEAX, 10)

// AP from L2.Tiny, L2 already on EAX
#define place_get_AP_Tiny() \
	place_shr_reg32__imm(regEAX, 4)

// AP from L2.Small
// EAX = L2
// EBP = VA
#define place_get_AP_Small() \
	place_mov_reg32__reg32(regECX, regEBP); \
	place_shr_reg32__imm(regEAX, 4); \
	place_shr_reg32__imm(regECX, 9); \
	place_and_reg8__imm(regCL, 0b110); \
	place_shr_reg32__CL(regEAX);

// AP from L2.Large
// EAX = L2
// EBP = VA
#define place_get_AP_Large() \
	place_mov_reg32__reg32(regECX, regEBP); \
	place_shr_reg32__imm(regEAX, 4); \
	place_shr_reg32__imm(regECX, 13); \
	place_and_reg8__imm(regCL, 0b110); \
	place_shr_reg32__CL(regEAX);

// ESI == L1 descriptor
#define place_CHECK_DOMAIN(type, AP_fun) { \
	/* get domain value */ \
	place_mov_reg32__ptr32_imm(regEAX, &context.DomainAccessControl); \
	place_mov_reg32__reg32(regECX, regESI); \
	place_shr_reg32__imm(regECX, 4); \
	place_and_reg8__imm(regCL, 0x1E); \
	place_shr_reg32__CL(regEAX); \
	place_test_reg32__imm(regEAX, 1); \
	place_condjump(NZ, domain_type_x1); \
	place_test_reg32__imm(regEAX, 2); \
	place_condjump(NZ, domain_type_10); \
	{ \
		/* No access (0b00) */ \
		place_MMUFault(Domain##type); \
	} \
	place_fix_label(domain_type_10); \
	{ \
		/* Reserved (0b10) */ \
		place_MMUFault(Domain##type); \
	} \
	place_fix_label(domain_type_x1); \
	place_test_reg32__imm(regEAX, 2); \
	place_condjump(NZ, domain_type_11); \
	{ \
		/* Client (0b01) */ \
		place_get_AP_##AP_fun(); \
		place_CHECK_AP(type); \
	} \
	place_fix_label(domain_type_11); \
	{ \
		/* Manager (0b11) */ \
	} \
}

			if (!context.TTBR0) {
				if (access != AccessExecute) {
					place_MMUFault(ExternalAbortSection);
				}
			}

			// convert VA to MVA
			place_test_reg32__imm(regEAX, 0xFE000000);
			place_condjump(NZ, already_MVA);
			place_or_reg32__ptr32_imm(regEAX, &context.ProcessID)
			place_fix_label(already_MVA);

			// check if last result is in the same page, if so, do not perform translation
			place_mov_reg32__reg32(regECX, regEAX);
			place_and_reg32__imm(regECX, page_size_mask);
			place_cmp_ptr32_imm__reg32(&context.last_VA, regECX);
			place_condjump(NZ, no_same_page);
			// same page, combine result
			place_and_reg32__imm(regEAX, ~page_size_mask);
			place_or_reg32__ptr32_imm(regEAX, &context.last_VA_PA);
			place_result_OK();
			place_fix_label(no_same_page);

			if (access != AccessExecute) {
				if (size == SizeWord) {
					//FIXME:
					//if ((MVA & 0b11) && MMU->ControlRegister.A) {
					//	//FIXME();
					//}
				}
				if (size == SizeHalfWord) {
					//FIXME:
					//if ((MVA & 0b1) && MMU->ControlRegister.A) {
					//	//FIXME();
					//}
				}
			}

			// get L1 descriptor
			// ESI <- TTBR0[MVA >> 20]
			place_shr_reg32__imm(regEAX, 20);
			place_mov_reg__ptr_imm_reg_mul(regESI, context.TTBR0, regEAX, mul4);

			// get descriptor type (lower 2 bits)
			place_test_reg32__imm(regESI, 1);
			place_condjump(NZ, L1_type_x1);
			place_test_reg32__imm(regESI, 2);
			place_condjump(NZ, L1_type_10);

			{
				// -----------------------------------------------------------
				// fault (0b00)
				place_MMUFault(TranslationSection);
			}

			place_fix_label(L1_type_10);
			{
				// -----------------------------------------------------------
				// section (0b10)
				// PA = (Section.BaseAddress << 20) | (MVA & 0x000FFFFF)
				// check domain
				place_CHECK_DOMAIN(Section, Section)
				// calculate PA
				place_mov_reg32__reg32(regEAX, regEDI);
				place_and_reg32__imm(regESI, 0xFFF00000);
				place_and_reg32__imm(regEAX, 0x000FFFFF);
				place_or_reg32__reg32(regEAX, regESI);
				place_result_OK_save();
			}

			place_fix_label(L1_type_x1);
			place_test_reg32__imm(regESI, 2);
			place_condjump(NZ, L1_type_11);

			{
				// -----------------------------------------------------------
				// coarse (0b01)
				// get L2 descriptor
				// pL2 = (Coarse.TableBaseAddress << 10) + ((MVA >> 10) & 0x3FC);
				place_mov_reg32__reg32(regEAX, regEDI);
				place_mov_reg32__reg32(regEDX, regESI);
				place_shr_reg32__imm(regEAX, 10);
				place_and_reg32__imm(regEDX, 0xFFFFFC00);
				place_and_reg32__imm(regEAX, 0x000003FC);
				place_or_reg32__reg32(regEAX, regEDX);
				place_call_argument(0, reg32, regEAX);
				place_call_imm(cpu->PA_to_Host, 1); // CF is set if PA points to IO space (invalid L2 descriptor address)
				place_condjump(NC, valid_L2_pointer);
				// invalid L2 pointer
				place_MMUFault(ExternalAbortPage);

				place_fix_label(valid_L2_pointer);
				place_mov_reg32__ptr32_reg(regEAX, regEAX);	// EAX <- L2 descriptor
				place_mov_reg32__reg32(regEBP, regEAX);
				// check L2 descriptor type
				place_test_reg8__imm(regAL, 1);
				place_condjump(NZ, L2_type_x1);
				place_test_reg8__imm(regAL, 2);
				place_condjump(NZ, L2_type_10);
				{
					// fault (0b00)
					place_MMUFault(TranslationPage);
				}
				place_fix_label(L2_type_10);
				{
					// small (0b10)
					// PA = (Small.BaseAddress << 12) | (VA & 0x00000FFF);
					// check domain
					place_CHECK_DOMAIN(Page, Small);
					// calculate PA
					place_mov_reg32__reg32(regEAX, regEBP);
					place_and_reg32__imm(regEDI, 0x00000FFF);
					place_and_reg32__imm(regEAX, 0xFFFFF000);
					place_or_reg32__reg32(regEAX, regEDI);
					place_result_OK_save();
				}
				place_fix_label(L2_type_x1);
				place_test_reg8__imm(regAL, 2);
				place_condjump(NZ, L2_type_11);
				{
					// large (0b01)
					// PA = (Large.BaseAddress << 16) | (MVA & 0x0000FFFF);
					// check domain
					place_CHECK_DOMAIN(Page, Large);
					// calculate PA
					place_mov_reg32__reg32(regEAX, regEBP);
					place_and_reg32__imm(regEDI, 0x0000FFFF);
					place_and_reg32__imm(regEAX, 0xFFFF0000);
					place_or_reg32__reg32(regEAX, regEDI);
					place_result_OK_save();
				}
				place_fix_label(L2_type_11);
				{
					// tiny (0b11) - invalid for coarse
					place_MMUFault(TranslationPage);
				}
			}
			place_fix_label(L1_type_11);
			{
				// -----------------------------------------------------------
				// fine page (0b11)
				// get L2 descriptor
				// pL2 = (Fine.FinePageTableBase << 12) + ((MVA >> 8) & 0xFFC);
				place_mov_reg32__reg32(regEAX, regEDI);
				place_mov_reg32__reg32(regEDX, regESI);
				place_shr_reg32__imm(regEAX, 8);
				place_and_reg32__imm(regEDX, 0xFFFFF000);
				place_and_reg32__imm(regEAX, 0x00000FFC);
				place_or_reg32__reg32(regEAX, regEDX);
				place_call_argument(0, reg32, regEAX);
				place_call_imm(cpu->PA_to_Host, 1); // CF is set if PA points to IO space (invalid L2 descriptor address)
				place_condjump(NC, valid_L2_pointer);
				// invalid L2 pointer
				place_MMUFault(ExternalAbortPage);

				place_fix_label(valid_L2_pointer);
				place_mov_reg32__ptr32_reg(regEAX, regEAX);	// EAX <- L2 descriptor
				place_mov_reg32__reg32(regEBP, regEAX);
				// check L2 descriptor type
				place_test_reg8__imm(regAL, 1);
				place_condjump(NZ, L2_type_x1);
				{
					// fault (0b00)
					// small (0b10) - invalid for tiny
					place_MMUFault(TranslationPage);
				}
				place_fix_label(L2_type_x1);
				place_test_reg8__imm(regAL, 2);
				place_condjump(NZ, L2_type_11);
				{
					// large (0b01) - invalid for tiny
					place_MMUFault(TranslationPage);
				}
				place_fix_label(L2_type_11);
				{
					// tiny (0b11)
					// PA = (Tiny.BaseAddress << 10) | (MVA & 0x000003FF);
					// check domain
					place_CHECK_DOMAIN(Page, Tiny);
					// calculate PA
					place_mov_reg32__reg32(regEAX, regEBP);
					place_and_reg32__imm(regEDI, 0x000003FF);
					place_and_reg32__imm(regEAX, 0xFFFFFC00);
					place_or_reg32__reg32(regEAX, regEDI);
					place_result_OK_save();
				}
			}

			// should never get here
			place_breakpoint();
		} else {
			// MMU disabled, function should never be called when MMU is disabled
			place_result_ERROR();
		}

		// compile 64-bit function, returns error code in upper half of result
		place_align(16);
		context.VA_to_PA64[size][access] = (fun_VA_to_PA64)data;
		place_call_imm(context.VA_to_PA[size][access], 0);
		place_mov_reg32__imm(regEDX, 0);
		place_set_flag__reg8(C, regDL);
		place_ret();
	} else {
		FIXME();
	}

	return data;
}
#endif

void ARMSystemControlCoprocessor::CompileFunctions()
{
	if (!codeData) {
		codeData = allocate_code_memory(COMPILED_CODE_SIZE);
	}

#ifdef MMU_TRANSLATE_ASM
	void* data = codeData;

#define __SetPtr_size(size) \
	CompileVA_to_PA(size, AccessRead, data); \
	CompileVA_to_PA(size, AccessWrite, data); \
	CompileVA_to_PA(size, AccessReadWrite, data); \
	CompileVA_to_PA(size, AccessExecute, data);
#else
#define __SetPtr_size(size) \
	if (cpu->CheckArch(CHECK_ARM_ARCH_v5 | CHECK_ARM_ARCH_v4)) { \
		context.VA_to_PA[size][AccessRead] = VA_to_PA_v5<size, AccessRead>; \
		context.VA_to_PA[size][AccessWrite] = VA_to_PA_v5<size, AccessWrite>; \
		context.VA_to_PA[size][AccessReadWrite] = VA_to_PA_v5<size, AccessReadWrite>; \
		context.VA_to_PA[size][AccessExecute] = VA_to_PA_v5<size, AccessExecute>; \
	} else { \
		FIXME(); \
	}
#endif
	__SetPtr_size(SizeByte);
	__SetPtr_size(SizeHalfWord);
	__SetPtr_size(SizeWord)
}


#ifndef MMU_TRANSLATE_ASM
void ARMSystemControlCoprocessorContext::RaiseMMUFault(MMUFaultStatus code, uint32_t address)
{
	DataFaultStatus.Status = code;
	//FIXME: DataFaultStatus.Domain =
	FaultAddress.Word = address;
}
#endif
