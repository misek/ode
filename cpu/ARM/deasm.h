/*
 * deasm.h
 *
 *  Created on: 06-05-2012
 *      Author: Łukasz Misek
 */

#ifndef DEASM_H_
#define DEASM_H_

#include <utils.h>

bool deasm_instruction(uint32_t PC, uint32_t instr, char* dest);

#endif /* DEASM_H_ */
