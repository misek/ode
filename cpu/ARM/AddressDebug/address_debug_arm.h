
struct ADDR_ITEM {
	const char* name;
	uint32_t address;
	unsigned int hitcount;
};

struct CONST_VALUE_ITEM {
	unsigned int value;
	const char* name;
};

#define CONST_VALUE_ITEMS(name) static const CONST_VALUE_ITEM name[1024] =
#define CONST_VALUE(name, values_list) name, "c", values_list

static uint32_t __last_ptr[1024];
static uint32_t *last_ptr = &__last_ptr[0];
static uint32_t __last_ptr_len[1024];
static uint32_t *last_ptr_len = &__last_ptr_len[0];

//FIXME:static LARGE_INTEGER __last_pc;
//FIXME:static LARGE_INTEGER __last_pc_mean_start[1024];
//FIXME:static double __last_pc_mean[1024];
//FIXME:static unsigned int __last_pc_no[1024];

//FIXME:
/*void CODEGEN_CALL startTimer()
{
	QueryPerformanceCounter(&__last_pc);
}

void CODEGEN_CALL resetTimer(int mean_index)
{
	__last_pc_no[mean_index] = 0;
	__last_pc_mean[mean_index] = 0.0;
}

void CODEGEN_CALL startTimer(int mean_index)
{
	QueryPerformanceCounter(&__last_pc_mean_start[mean_index]);
}

// time in ms
double CODEGEN_CALL stopTimer()
{
	LARGE_INTEGER curr, freq;
	QueryPerformanceCounter(&curr);
	curr.QuadPart -= __last_pc.QuadPart;
	QueryPerformanceFrequency(&freq);
	double t = (double)curr.QuadPart / (double)freq.QuadPart;
	return t * 1000;
}

double CODEGEN_CALL stopTimer(int mean_index)
{
	LARGE_INTEGER curr, freq;
	QueryPerformanceCounter(&curr);
	curr.QuadPart -= __last_pc_mean_start[mean_index].QuadPart;
	QueryPerformanceFrequency(&freq);
	double t = ((double)curr.QuadPart / (double)freq.QuadPart) * 1000;
	__last_pc_mean[mean_index] = __last_pc_no[mean_index] * __last_pc_mean[mean_index] + t;
	__last_pc_mean[mean_index] /= ++__last_pc_no[mean_index];
	return __last_pc_mean[mean_index];
}*/

void* CODEGEN_CALL getMemory(uint32_t ptr)
{
	//TODO: do not has to be continuous memory space
	//FIXME: get access to MMU
	if (MMU->ControlRegister.M) {
		/*uint64_t p = MMU.VA_to_PA[SizeByte][AccessRead](ptr);
		if (p > 0xFFFFFFFF) {
			return __null;
		}
		ptr = p & 0xFFFFFFFF;*/
	}
	return CPU->PA_to_HostMem(ptr);
}

void CODEGEN_CALL dumpMemory(uint32_t mem, int len, const char* spc)
{
	DEBUG_PRINT_COLOR(0x0B);
	DEBUG_PRINT_CHAR('{');
	while (len--)
	{
		unsigned char* p = (unsigned char*)getMemory(mem);
		if (!p)
			break;
		DEBUG_PRINT("%02X", *p);
		mem++;
		if (len)
			DEBUG_PRINT(spc);
	}
	DEBUG_PRINT_CHAR('}');
	DEBUG_PRINT_COLOR(0x07);
}

void CODEGEN_CALL dumpString(uint32_t mem)
{
	DEBUG_PRINT_COLOR(0x0B);
	DEBUG_PRINT_CHAR('"');
	while (true)
	{
		unsigned char* p = (unsigned char*)getMemory(mem);
		if (*p == 0)
			break;
		switch (*p) {
			case '\\':
				DEBUG_PRINT("\\");
				break;
			case '\n':
				DEBUG_PRINT("\n");
				break;
			case '\r':
				DEBUG_PRINT("\r");
				break;
			default:
				DEBUG_PRINT_CHAR(*p);
				break;
		}

		mem++;
	}
	DEBUG_PRINT_CHAR('"');
	DEBUG_PRINT_COLOR(0x07);
}

void CODEGEN_CALL dumpWideString(uint32_t mem, bool showQuotes = true)
{
	DEBUG_PRINT_COLOR(0x0B);
	if (mem) {
		if (showQuotes)
			DEBUG_PRINT_CHAR('"');
		while (true)
		{
			uint16_t* p = (uint16_t*)getMemory(mem);
			if (!p) {
				DEBUG_PRINT("???");
				break;
			}
			if (*p == 0)
				break;
			switch (*p) {
				case '\\':
					DEBUG_PRINT("\\");
					break;
				case '\n':
					DEBUG_PRINT("\n");
					break;
				case '\r':
					DEBUG_PRINT("\r");
					break;
				default:
					DEBUG_PRINT_CHAR(*p);
					break;
			}
			mem += 2;
		}
		if (showQuotes)
			DEBUG_PRINT_CHAR('"');
	} else {
		DEBUG_PRINT("NULL");
	}
	DEBUG_PRINT_COLOR(0x07);
}

void CODEGEN_CALL addLastPtr(uint32_t ptr, uint32_t len)
{
	*last_ptr++ = ptr;
	*last_ptr_len++ = len;
}

void CODEGEN_CALL dumpLastPtr()
{
	dumpMemory(*--last_ptr, *--last_ptr_len, "");
}

uint32_t CODEGEN_CALL getLastPtr()
{
	return *(last_ptr - 1);
}

uint32_t CODEGEN_CALL popLastPtr()
{
	--last_ptr_len;
	uint32_t result = getLastPtr();
	--last_ptr;
	return result;
}

void CODEGEN_CALL fun_auth_over(ADDR_ITEM* itm)
{
	//FIXME:Sleep(60 * 60 * 1000);
}

static int __fun_indent = 0;

#define PF_VALUE	0
#define PF_NAME		1
#define PF_HIDE		2
#define	PF_NL		4
#define	PF_ADDPTR	8
#define	PF_UNKNOWN	16
#define PF_CUSTOM	32
#define PF_DONOTINC 64
#define PF_GROUP_S  128
#define PF_GROUP_E  256

#define FUN_DEBUG(fmt,...) DEBUG_PRINT(fmt, ##__VA_ARGS__)
#define FUN_DEBUGI(fmt,...) {for (int _______i=0;_______i<__fun_indent;_______i++)DEBUG_PRINT("  "); FUN_DEBUG(fmt, ##__VA_ARGS__);}

#define FUN_DEBUG_PARAMS_PARENTH(s) {DEBUG_PRINT_COLOR(0x07); DEBUG_PRINT(s); DEBUG_PRINT_COLOR(0x07);}
#define FUN_DEBUG_PARAMS_LEFT_PARENTH() FUN_DEBUG_PARAMS_PARENTH("(")
#define FUN_DEBUG_PARAMS_RIGHT_PARENTH() FUN_DEBUG_PARAMS_PARENTH(")")
#define FUN_DEBUG_PARAMS_NAME(s) {DEBUG_PRINT_COLOR(0x0A); DEBUG_PRINT(s); DEBUG_PRINT_COLOR(0x07);}
#define FUN_DEBUG_PARAMS_EQ() {DEBUG_PRINT_COLOR(0x07); DEBUG_PRINT(" = "); DEBUG_PRINT_COLOR(0x07);}
#define FUN_DEBUG_PARAMS_VALUE_COLOR(fmt,val,col) {DEBUG_PRINT_COLOR(col); DEBUG_PRINT(fmt,val); DEBUG_PRINT_COLOR(0x07);}
#define FUN_DEBUG_PARAMS_VALUE(fmt,val) FUN_DEBUG_PARAMS_VALUE_COLOR(fmt, val, 0x0B)
#define FUN_DEBUG_PARAMS_NAME_VALUE(name,fmt,value) {FUN_DEBUG_PARAMS_NAME(name);FUN_DEBUG_PARAMS_EQ();FUN_DEBUG_PARAMS_VALUE(fmt,value);}
#define FUN_DEBUG_PARAMS_NAME_MEM(name) {FUN_DEBUG_PARAMS_NAME(name);FUN_DEBUG_PARAMS_EQ();dumpLastPtr();}
#define FUN_DEBUG_PARAMS_NAME_MEM_LN(txt,name) {FUN_DEBUGI("   << "txt);FUN_DEBUG_PARAMS_NAME(name);FUN_DEBUG_PARAMS_EQ();dumpLastPtr();FUN_DEBUG("\n");}
#define FUN_DEBUG_PARAMS_NAME_RESULT(name,fmt,value) {FUN_DEBUGI("   << ");FUN_DEBUG_PARAMS_NAME_VALUE(name, fmt, value);}
#define FUN_DEBUG_PARAMS_NAME_RESULT_LN(name,fmt,value) {FUN_DEBUG_PARAMS_NAME_RESULT(name, fmt, value);FUN_DEBUG("\n");}
#define FUN_DEBUG_PARAMS_COLON(nl) {DEBUG_PRINT_COLOR(0x07); if (p_no) {if (nl) {DEBUG_PRINT(",\n"); FUN_DEBUGI("    ");}else DEBUG_PRINT(", ");} else if(nl) {DEBUG_PRINT_CHAR('\n'); FUN_DEBUGI("    ");} DEBUG_PRINT_COLOR(0x07);}

#define FUN_DEBUG_PARAMS(cnt,...) {FUN_DEBUG_PARAMS_NONL(CPU, cnt, ##__VA_ARGS__); FUN_DEBUG("\n");}

uint32_t STACK_ARGUMENT(ARMCPU* cpu, int no)
{
	uint32_t stack_addr = cpu->context.regs[13] + no * 4;
	uint32_t* p = (uint32_t*)getMemory(stack_addr);
	if (p) {
		return *p;
	} else {
		return 0x0BADC0DE;
	}
}

void FUN_DEBUG_PARAMS_NONL(ARMCPU* cpu, int cnt, ...)
{
	va_list l;
	va_start(l, cnt);
	int p_no = 0;
	bool last_hidden = false;
	FUN_DEBUG_PARAMS_LEFT_PARENTH();
	while (p_no < cnt) {
		uint32_t val;
		if (p_no < 4) {
			val = cpu->context.regs[p_no];
		} else {
			val = STACK_ARGUMENT(cpu, p_no - 4);
		}
		char* name = va_arg(l, char*);
		char* format = va_arg(l, char*);
		int data_len = 0;
		CONST_VALUE_ITEM* cItems = __null;
		// get flags
		switch(format[0]) {
			case 'm':
				data_len = va_arg(l, int);
				break;
			case 'c':
				cItems = va_arg(l, CONST_VALUE_ITEM*);
				break;
		}
		int flags = va_arg(l, int);
		if (flags & PF_ADDPTR) {
			addLastPtr(val, va_arg(l, unsigned int));
		}
		if (flags & PF_CUSTOM) {
			val = va_arg(l, unsigned int); 
		}
		if (flags & PF_HIDE) {
			if (!last_hidden) {
				FUN_DEBUG_PARAMS_COLON(flags & PF_NL);
				last_hidden = true;
				if (flags & PF_GROUP_S) {
					DEBUG_PRINT_COLOR(0x0E);
					FUN_DEBUG("<<");
					DEBUG_PRINT_COLOR(0x07);
				}
				FUN_DEBUG_PARAMS_NAME("...");
				if (flags & PF_GROUP_E) {
					DEBUG_PRINT_COLOR(0x0E);
					FUN_DEBUG(">>");
					DEBUG_PRINT_COLOR(0x07);
				}
			}
		} else {
			FUN_DEBUG_PARAMS_COLON(flags & PF_NL);
			if (flags & PF_GROUP_S) {
				DEBUG_PRINT_COLOR(0x0E);
				FUN_DEBUG("<<");
				DEBUG_PRINT_COLOR(0x07);
			}
			if (flags & PF_NAME) {
				FUN_DEBUG_PARAMS_NAME(name);
				FUN_DEBUG_PARAMS_EQ();
			}
			if (flags & PF_UNKNOWN) {
				FUN_DEBUG_PARAMS_VALUE_COLOR("0x%08X", val, 0x0D);
			} else {
				switch (format[0]) {
					case 'c':
						ASSERT(cItems);
						while (cItems->name) {
							if (cItems->value == val) {
								break;
							} else {
								cItems++;
							}
						}
						if (cItems->name) {
							FUN_DEBUG_PARAMS_VALUE("%s", cItems->name);
						} else {
							FUN_DEBUG_PARAMS_VALUE_COLOR("/unknown 0x%X/", val, 0x0C);
						}
						break;
					case 'm':
						dumpMemory(val, data_len, "");
						break;
					case 's':
						dumpString(val);
						break;
					case 'p':
						// pointer
						FUN_DEBUG_PARAMS_VALUE("0x%08X", val);
						break;
					case 'w':
						dumpWideString(val);
						break;
					default:
						FUN_DEBUG_PARAMS_VALUE(format, val);
						break;
				}
			}
			if (flags & PF_GROUP_E) {
				DEBUG_PRINT_COLOR(0x0E);
				FUN_DEBUG(">>");
				DEBUG_PRINT_COLOR(0x07);
			}
		}
		if (flags & PF_DONOTINC) {
			cnt--;
		} else {
			p_no++;
		}
	}
	FUN_DEBUG_PARAMS_RIGHT_PARENTH();

	va_end(l);
}

static bool addressItemsInitialized = false;
static int addressItemsCount = 0;
ADDR_ITEM addressItems[1024];

ADDR_ITEM* CODEGEN_CALL FindAdddressItem(uint32_t address)
{
	if (!address)
		return __null;
	for (int i = 0; i < addressItemsCount; i++) {
		if (addressItems[i].address == address)
			return &addressItems[i];
	}
	return __null;
}

void CODEGEN_CALL OutputLabel(uint32_t address)
{
	ADDR_ITEM* itm = FindAdddressItem(address);
	if (itm) {
		itm->hitcount++;
		if (itm->name) {
			DEBUG_PRINT_COLOR(0x0E);
			if (itm->name[0] == '-') {
				__fun_indent--;
			}
			if (itm->name[1]) {
//				if (__fun_indent == 0)
//					DEBUG_PRINT("[%08X] ", Cpu.GPRs[15]);
				FUN_DEBUGI("%s", itm->name);
			}
			if (itm->name[0] == '+') {
				__fun_indent++;
			}
			DEBUG_PRINT_COLOR(0x07);
	//		DEBUG_PRINT(", hit: %d", itm->hitcount);
		}
//		if (itm->fun) {
//			itm->fun(itm);
//		} else {
//			if ((itm->name) && (itm->name[1])) {
//				DEBUG_PRINT("\n");
//			}
//		}
	}
}

bool CODEGEN_CALL CheckAddress(uint32_t addr, uint32_t addr2)
{
	if (addr == addr2)
		OutputLabel(addr);
	return addr == addr2;
}

void CODEGEN_CALL InitDebugAddresses(ARMCPU* CPU)
{
	if (!addressItemsInitialized) {
		ADDR_ITEM* __addr_item = addressItems;

		#define DEBUG_ADDRESS(__addr, __name) \
			static int ___address_aleady_defined_##__addr = 0; \
			addressItemsCount++; \
			__addr_item->name = __name; \
			__addr_item->address = __addr; \
			__addr_item->hitcount = 0; \
			__addr_item++; \
			if (0 && ___address_aleady_defined_##__addr)
		#include "address_debug_data.h"
		#undef DEBUG_ADDRESS

		addressItemsInitialized = true;
	}
}

void CODEGEN_CALL ExecuteDebugAddress(ARMCPU* CPU, uint32_t __addr_in)
{
	OutputLabel(__addr_in);
#define DEBUG_ADDRESS(__addr, __name) \
	if (__addr_in == __addr)
#include "address_debug_data.h"
#undef DEBUG_ADDRESS
}

