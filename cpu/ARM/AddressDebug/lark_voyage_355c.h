//#define __DEBUG_INIT_BOOT
//#define __DEBUG_AT88SC_BOOT
//#define __DEBUG_AT88SC_KERNEL
//#define __DEBUG_USBD_BOOT
//#define __DEBUG_USBD
//#define __DEBUG_NAND
//#define __DEBUG_SDI
//#define __DEBUG_NAND_PART0_FAL
//#define __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED
//#define __DEBUG_NAND_PART0_FMD
//#define __DEBUG_NAND_PART0
//#define __DEBUG_NAND_PART1_FAL
//#define __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED
//#define __DEBUG_NAND_PART1_FMD
//#define __DEBUG_NAND_PART1
//#define __DEBUG_FILE_API

#ifndef DEBUG_ADDRESS
#define DEBUG_ADDRESS(a, n) void invalid_function##a()
#endif

DEBUG_ADDRESS(0x800028D4, __null)
{
//	CPU->context.regs[R3] = 0xFF;
}

DEBUG_ADDRESS(0x800028E8, __null)
{
//	CPU->context.regs[R3] = 0xFF;
}

DEBUG_ADDRESS(0x03D62700, __null)
{
	//CPU->context.regs[R2] = 0;
}

DEBUG_ADDRESS(0x80263AE0, __null)
{
	// set maximum output debug string
	//CPU->context.regs[R0] = 0xFFFFFFFF;
}

DEBUG_ADDRESS(0x03F93B68, "CeLogData")
{
	FUN_DEBUG_PARAMS(8,
		"fTimeStamp", "%d", PF_VALUE,
		"wID", "0x%04X", PF_VALUE,
		"pData", "w", PF_VALUE,
		"wLen", "%u", PF_VALUE,
		"dwZoneUser", "0x%08X", PF_VALUE,
		"dwZoneCE", "0x%08X", PF_VALUE,
		"wFlag", "0x%04X", PF_VALUE,
		"fFlagged", "%d", PF_VALUE);
}

DEBUG_ADDRESS(0x80265088, __null)
{
	DEBUG_PRINT_COLOR(10);
	DEBUG_PRINT_CHAR(CPU->context.regs[0]);
	DEBUG_PRINT_COLOR(7);
}

DEBUG_ADDRESS(0x300080F4, __null)
{
	// start debug menu
	// BOOT menu password: 123
	//		SETUP password: 102030
	//CPU->context.regs[R5] = 0x0;
}

/*DEBUG_ADDRESS(0x03F81A8C, "ASF_CreateFileW")
{
	FUN_DEBUG_PARAMS(3,
		"", "", PF_HIDE,
		"", "", PF_HIDE,
		"lpFileName", "w", PF_VALUE);
}*/

#ifdef __DEBUG_INIT_BOOT

DEBUG_ADDRESS(0x000003FC, "LoadBootloaderFromNAND")
{
	FUN_DEBUG_PARAMS(1,
		"lpDest", "p", PF_NAME | PF_VALUE);
}

#endif

#ifdef __DEBUG_FILE_API

DEBUG_ADDRESS(0x03F91394, "CreateFileW")
{
	FUN_DEBUG_PARAMS(7,
		"lpFileName", "w", PF_VALUE,
		"dwDesiredAccess", "0x%08X", PF_VALUE,
		"dwShareMode", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE,
		"lpSecurityAttributes", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE,
		"dwCreationDisposition", "0x%08X", PF_VALUE | PF_NAME,
		"dwFlagsAndAttributes", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE,
		"hTemplateFile", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE);
}

/*static bool fw = false;

DEBUG_ADDRESS(0x03F9157C, "WriteFile")
{
	FUN_DEBUG_PARAMS(5,
		"hFile", "0x%08X", PF_VALUE,
		"lpBuffer", "m", CPU->context.regs[R2], PF_VALUE,
		"nNumberOfBytesToWrite", "0x%08X(%u)", PF_VALUE,
		"lpNumberOfBytesWritten", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE,
		"lpOverlapped", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE);
	if (fw) {
		fw = false;
		FILE* fout;
		fopen_s(&fout, "C:\\file_write.bin", "wb");
		size_t adj;
		char cache;
		unsigned __int32 val = (unsigned __int32)Mmu.MmuMapExecute.MapGuestVirtualToHost(CPU->context.regs[R1], &cache);
		if (val == 0) val = (unsigned __int32)BoardMapGuestPhysicalToHost(CPU->context.regs[R1], &adj);
		fwrite((void*)val, CPU->context.regs[R2], 1, fout);
		fclose(fout);
	}
}

DEBUG_ADDRESS(0x03F914E4, "ReadFile")
{
	FUN_DEBUG_PARAMS(5,
		"hFile", "0x%08X", PF_VALUE,
		"lpBuffer", "0x%08X", PF_NAME | PF_VALUE,
		"nNumberOfBytesToRead", "0x%08X(%u)", PF_VALUE,
		"lpNumberOfBytesRead", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE,
		"lpOverlapped", "0x%08X", PF_VALUE | PF_NAME | PF_HIDE);
}

DEBUG_ADDRESS(0x03F91568, __null)
{
	if (fw) {
		fw = false;
		FILE* fout;
		fopen_s(&fout, "C:\\file_read.bin", "wb");
		size_t adj;
		char cache;
		unsigned __int32 ptr = popLastPtr();
		unsigned __int32 val = (unsigned __int32)Mmu.MmuMapExecute.MapGuestVirtualToHost(ptr, &cache);
		if (val == 0) val = (unsigned __int32)BoardMapGuestPhysicalToHost(ptr, &adj);
		fwrite((void*)val, CPU->context.regs[R6], 1, fout);
		fclose(fout);
	} else
		popLastPtr();
}*/

#endif

#ifdef __DEBUG_NAND_PART0_FMD

DEBUG_ADDRESS(0x05E961E8, "+DSK_Init")
{
	FUN_DEBUG_PARAMS(4,
		"lpActiveReg", "w", PF_NAME | PF_VALUE,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x05E9676C, "-")
{

}

DEBUG_ADDRESS(0x05E95938, "+CalculateLogicalRange")
{
	FUN_DEBUG_PARAMS(1,
		"pRegion", "0x%08X", PF_VALUE | PF_NAME | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x05E959BC, "-")
{
	unsigned int* pRegion = (unsigned int*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwNumLogicalBlocks", "%u", pRegion[3]);
}

DEBUG_ADDRESS(0x05E9E0A4, "+FMD_GetBlockStatus")
{
	FUN_DEBUG_PARAMS(1,
		"blockId", "%u", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x05E9E0F8, "-")
{

}

DEBUG_ADDRESS(0x05E9E038, "+FMD_GetInfo")
{
	FUN_DEBUG_PARAMS(1,
		"pFlashInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR)
}

DEBUG_ADDRESS(0x05E9E09C, "-")
{
	typedef struct _FlashInfo {
		DWORD flashType;
		DWORD dwNumBlocks;
		DWORD dwBytesPerBlock;
		WORD wSectorsPerBlock;
		WORD wDataBytesPerSector;
	} FlashInfo, *PFlashInfo;
	FlashInfo* fi = (FlashInfo*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("flashType", "%u", fi->flashType);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwNumBlocks", "%u", fi->dwNumBlocks);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwBytesPerBlock", "%u", fi->dwBytesPerBlock);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("wSectorsPerBlock", "%u", fi->wSectorsPerBlock);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("wDataBytesPerSector", "%u", fi->wDataBytesPerSector);
}

DEBUG_ADDRESS(0x05E9DE50, "+FMD_ReadSector")
{
	FUN_DEBUG_PARAMS(5,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 64,
		"pSectorBuff", "0x%08X", PF_NAME | PF_VALUE,
		"pSectorInfoBuff", "0x%08X", PF_NAME | PF_VALUE,
		"dwNumSectors", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E9DEC8, "-")
{
}

#endif

#ifdef __DEBUG_NAND_PART0_FAL

DEBUG_ADDRESS(0x05E96C04, "+FAL::ReadFromMedia")
{
	//while (1) Sleep(100);
	unsigned int* preq = (unsigned int*)getMemory(CPU->context.regs[R1]);
	FUN_DEBUG_PARAMS(4,
		"this", "", PF_HIDE,
		"start", "%u", PF_NAME | PF_VALUE | PF_CUSTOM | PF_DONOTINC, preq[0],
		"count", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, preq[1],
		"fDoMap", "%u", PF_VALUE | PF_NAME);
}

DEBUG_ADDRESS(0x05E96DA0, "-")
{
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x05E98DA0, __null)
{
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x05E98DB4, __null)
{
//	DEBUG_PRINT("R9: %u\n", CPU->context.regs[R9]);
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x05E98DC0, __null)
{
//	DEBUG_PRINT("R7: %u\n", CPU->context.regs[R7]);
//	DEBUG_PRINT("");
}

DEBUG_ADDRESS(0x05E98C30, "+GetPhysicalSectorAddr")
{
	FUN_DEBUG_PARAMS(3,
		"", "", PF_HIDE,
		"sector_no", "%u", PF_NAME | PF_VALUE,
		"pResult", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x05E98C4C, "-")
{
	// exit 0
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x05E98C90, "-")
{
	// exit 1
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x05E98CA0, "-")
{
	// exit 2
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x05E9779C, "+FileSysFAL::StartupFAL")
{
	FUN_DEBUG_PARAMS(2,
		"this", "0x%08X", PF_NAME | PF_VALUE,
		"pRegion", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x05E97800, "-")
{
}

DEBUG_ADDRESS(0x05E96F68, "+FAL::StartupFAL")
{
	FUN_DEBUG_PARAMS(2,
		"this", "0x%08X", PF_NAME | PF_VALUE,
		"pRegion", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x05E96FD4, "-")
{
}

DEBUG_ADDRESS(0x05E97A8C, "+SMFlash::BuildUpMappingInfo")
{
	FUN_DEBUG_PARAMS(1,
		"this", "0x%08X", PF_HIDE)
}

DEBUG_ADDRESS(0x05E97E6C, "-")
{
//	while(1) Sleep(200000);
}

#ifdef __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x05E99388, "+SectorMgr::MarkBlockAsFree")
{
	FUN_DEBUG_PARAMS(2,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E993A4, "-")
{

}

#endif

#ifdef __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x05E99344, "+SectorMgr::MarkBlockAsDirty")
{
	FUN_DEBUG_PARAMS(2,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E99360, "-")
{

}

#endif

DEBUG_ADDRESS(0x05E9884C, "+GetPhysicalSectorAddr")
{
	FUN_DEBUG_PARAMS(4,
		"this", "0x%08X", PF_HIDE,
		"log_sector_no", "%u", PF_NAME | PF_VALUE,
		"pOut", "0x%08X", PF_VALUE | PF_ADDPTR, 8,
		"???", "0x%08X", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x05E98AFC, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "physical_sector_no");
}

DEBUG_ADDRESS(0x05E9783C, "+BuildSectorMapInfo")
{
	FUN_DEBUG_PARAMS(3,
		"???", "0x%08X", PF_UNKNOWN | PF_HIDE,
		"block_no", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, CPU->context.regs[R1] / 64,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E97958, "-")
{

}

#ifdef __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x05E993E0, "+MarkSectorAsDirty")
{
	FUN_DEBUG_PARAMS(4,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x05E99414, "-")
{
	// ERROR
}

DEBUG_ADDRESS(0x05E9942C, "-")
{

}

#endif

#ifdef __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x05E969A8, "+decode_logical_sector_number")
{
	FUN_DEBUG_PARAMS_NONL(1,
		"sectorInfo", "m", 8, PF_NAME | PF_VALUE | PF_ADDPTR);
}

DEBUG_ADDRESS(0x05E96A58, "-")
{
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_EQ();
	FUN_DEBUG_PARAMS_VALUE("%u\n", *pResult);
}

#endif

DEBUG_ADDRESS(0x05E98D8C, "+MarkBlockAsFull")
{
	FUN_DEBUG_PARAMS(4,
		"pMap", "0x%08X", PF_HIDE,
		"starting_sector_no", "%u", PF_NAME | PF_VALUE,
		"block_no", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] / 64,
		"", "", PF_UNKNOWN | PF_HIDE);
}

DEBUG_ADDRESS(0x05E98E50, "-")
{

}

DEBUG_ADDRESS(0x05E98D18, "+MapLogicalSector")
{
	FUN_DEBUG_PARAMS(5,
		"?", "?", PF_UNKNOWN | PF_HIDE,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R2] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] % 64,
		"", "", PF_UNKNOWN | PF_ADDPTR | PF_HIDE, 4);
}

DEBUG_ADDRESS(0x05E98D5C, "-")
{
	popLastPtr();
	// exit 1
	//	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "???");
}

DEBUG_ADDRESS(0x05E98D88, "-")
{
	popLastPtr();
	// exit 2
//	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "???");
}

#ifdef __DEBUG_NAND_PART0_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x05E99AAC, "+MarkSectorAsFree")
{
	FUN_DEBUG_PARAMS(4,
		"pSectorMgr", "0x%08X", PF_HIDE,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R2] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] % 64);
}

DEBUG_ADDRESS(0x05E99BB8, "-")
{

}

#endif

/*DEBUG_ADDRESS(0x05E98EC4, "+MarkSectorAsDirty_chunk")
{
	FUN_DEBUG_PARAMS(4,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE,
		"???", "0x%08X", PF_VALUE,
		"???", "0x%08X", PF_VALUE);
}

DEBUG_ADDRESS(0x05E98F74, "-")
{

}*/

DEBUG_ADDRESS(0x05E97AEC, __null)
{
	if (CPU->context.regs[R8] < 210)
		return;
//	DEBUG_PRINT("Processing block: %u\n", CPU->context.regs[R8]);
}

DEBUG_ADDRESS(0x05E97DD8, __null)
{
//	DEBUG_PRINT("MarkBlockUnusable: %u\n", CPU->context.regs[R8]);
}

#endif

#ifdef __DEBUG_NAND_PART0

DEBUG_ADDRESS(0x05E9C434, "+SMFlash::ReadBlockInfo")
{
	FUN_DEBUG_PARAMS(2,
		"blockNo", "%u", PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_NAME | PF_UNKNOWN | PF_HIDE);
}

DEBUG_ADDRESS(0x05E9C4B0, "-")
{
	FUN_DEBUGI(" << ");
	FUN_DEBUG_PARAMS_NAME_VALUE("status", "0x%02X\n", CPU->context.regs[R0]);
}

DEBUG_ADDRESS(0x05E9BB64, "+SMFlash::ReadSector")
{
	FUN_DEBUG_PARAMS(5,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 64,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 64,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"?", "%u", PF_HIDE | PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_HIDE | PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E9BC5C, "-")
{
	if (getLastPtr()) {
		FUN_DEBUG_PARAMS_NAME_MEM_LN("", "SectorInfo");
	} else
		popLastPtr();
}

DEBUG_ADDRESS(0x05E9AE70, "+NAND::ReadPage")
{
	FUN_DEBUG_PARAMS(6,
		"blockNo", "%u", PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"?", "%u", PF_HIDE | PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_HIDE | PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E9B620, "-")
{
}

DEBUG_ADDRESS(0x05E9AAA4, "+TranslateSectorNo")
{
	FUN_DEBUG_PARAMS(4,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pSector1", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4,
		"pSector2", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x05E9AB24, "-")
{
	unsigned int* p2 = (unsigned int*)getMemory(popLastPtr());
	unsigned int* p1 = (unsigned int*)getMemory(popLastPtr());

	char tmp[100];
	sprintf(tmp, "%u/%u + %u/%u (%u + %u)", *p1 / 128, *p1 % 128, *p2 / 128, *p2 % 128, *p1, *p2);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("pages", "%s", tmp);
}

DEBUG_ADDRESS(0x05E9AB38, "-")
{
	unsigned int* p2 = (unsigned int*)getMemory(popLastPtr());
	unsigned int* p1 = (unsigned int*)getMemory(popLastPtr());

	char tmp[100];
	sprintf(tmp, "%u/%u + %u/%u (%u + %u)", *p1 / 128, *p1 % 128, *p2 / 128, *p2 % 128, *p1, *p2);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("pages", "%s", tmp);
}

DEBUG_ADDRESS(0x05E9AB3C, "+SMFlash::ReadPageInfo")
{
	FUN_DEBUG_PARAMS(4,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"begin", "%u", PF_VALUE);
}

DEBUG_ADDRESS(0x05E9ACB0, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "PageInfo");
}

DEBUG_ADDRESS(0x05E9C4B4, "+SMFlash::EraseBlock")
{
	FUN_DEBUG_PARAMS(1,
		"blockNo", "0x%08X(%u)", PF_VALUE);
}

DEBUG_ADDRESS(0x05E9C5FC, "-")
{
}

DEBUG_ADDRESS(0x05E9BC60, "+SMFlash::WriteSector")
{
	FUN_DEBUG_PARAMS(4,
		"sectorNo", "%u", PF_NAME | PF_VALUE,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "m", 8, PF_VALUE,
		"?", "0x%08X", PF_HIDE, PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E9C16C, "-")
{
}

DEBUG_ADDRESS(0x05E9ACBC, "+SMFlash::WritePageInfo")
{
	FUN_DEBUG_PARAMS(3,
		"pageNo", "0x%08X(%u)", PF_VALUE,
		"pInfo", "m", 8, PF_VALUE,
		"?", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x05E9AE64, "-")
{
}

#endif

#ifdef __DEBUG_NAND_PART1_FMD

DEBUG_ADDRESS(0x07E961E8, "+DSK_Init")
{
	FUN_DEBUG_PARAMS(4,
		"lpActiveReg", "w", PF_NAME | PF_VALUE,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x07E9676C, "-")
{
	//DebugBreak();
}

DEBUG_ADDRESS(0x07E95938, "+CalculateLogicalRange")
{
	FUN_DEBUG_PARAMS(1,
		"pRegion", "0x%08X", PF_VALUE | PF_NAME | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x07E959BC, "-")
{
	unsigned int* pRegion = (unsigned int*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwNumLogicalBlocks", "%u", pRegion[3]);
}

DEBUG_ADDRESS(0x07E9E0A4, "+FMD_GetBlockStatus")
{
	FUN_DEBUG_PARAMS(1,
		"blockId", "%u", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x07E9E0F8, "-")
{

}

DEBUG_ADDRESS(0x07E9E038, "+FMD_GetInfo")
{
	FUN_DEBUG_PARAMS(1,
		"pFlashInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR)
}

DEBUG_ADDRESS(0x07E9E09C, "-")
{
	typedef struct _FlashInfo {
		DWORD flashType;
		DWORD dwNumBlocks;
		DWORD dwBytesPerBlock;
		WORD wSectorsPerBlock;
		WORD wDataBytesPerSector;
	} FlashInfo, *PFlashInfo;
	FlashInfo* fi = (FlashInfo*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("flashType", "%u", fi->flashType);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwNumBlocks", "%u", fi->dwNumBlocks);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("dwBytesPerBlock", "%u", fi->dwBytesPerBlock);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("wSectorsPerBlock", "%u", fi->wSectorsPerBlock);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("wDataBytesPerSector", "%u", fi->wDataBytesPerSector);
}

DEBUG_ADDRESS(0x07E9DE50, "+FMD_ReadSector")
{
	FUN_DEBUG_PARAMS(5,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 64,
		"pSectorBuff", "0x%08X", PF_NAME | PF_VALUE,
		"pSectorInfoBuff", "0x%08X", PF_NAME | PF_VALUE,
		"dwNumSectors", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E9DEC8, "-")
{
}

#endif

#ifdef __DEBUG_NAND_PART1_FAL

DEBUG_ADDRESS(0x07E96C04, "+FAL::ReadFromMedia")
{
	//while (1) Sleep(100);
	unsigned int* preq = (unsigned int*)getMemory(CPU->context.regs[R1]);
	FUN_DEBUG_PARAMS(4,
		"this", "", PF_HIDE,
		"start", "%u", PF_NAME | PF_VALUE | PF_CUSTOM | PF_DONOTINC, preq[0],
		"count", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, preq[1],
		"fDoMap", "%u", PF_VALUE | PF_NAME);
}

DEBUG_ADDRESS(0x07E96DA0, "-")
{
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x07E98DA0, __null)
{
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x07E98DB4, __null)
{
//	DEBUG_PRINT("R9: %u\n", CPU->context.regs[R9]);
//	DEBUG_PRINT("LR: %u\n", CPU->context.regs[R14]);
}

DEBUG_ADDRESS(0x07E98DC0, __null)
{
//	DEBUG_PRINT("R7: %u\n", CPU->context.regs[R7]);
//	DEBUG_PRINT("");
}

DEBUG_ADDRESS(0x07E98C30, "+GetPhysicalSectorAddr")
{
	FUN_DEBUG_PARAMS(3,
		"", "", PF_HIDE,
		"sector_no", "%u", PF_NAME | PF_VALUE,
		"pResult", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x07E98C4C, "-")
{
	// exit 0
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x07E98C90, "-")
{
	// exit 1
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x07E98CA0, "-")
{
	// exit 2
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	char tmp[100];
	if (pResult) {
		sprintf(tmp, "%u/%u = %u", *pResult / 64, *pResult % 64, *pResult);
	} else {
		sprintf(tmp, "INVALID PTR");
	}
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("physical_address", "%s", tmp);
}

DEBUG_ADDRESS(0x07E9779C, "+FileSysFAL::StartupFAL")
{
	FUN_DEBUG_PARAMS(2,
		"this", "0x%08X", PF_NAME | PF_VALUE,
		"pRegion", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x07E97800, "-")
{
}

DEBUG_ADDRESS(0x07E96F68, "+FAL::StartupFAL")
{
	FUN_DEBUG_PARAMS(2,
		"this", "0x%08X", PF_NAME | PF_VALUE,
		"pRegion", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x07E96FD4, "-")
{
}

DEBUG_ADDRESS(0x07E97A8C, "+SMFlash::BuildUpMappingInfo")
{
	FUN_DEBUG_PARAMS(1,
		"this", "0x%08X", PF_HIDE)
}

DEBUG_ADDRESS(0x07E97E6C, "-")
{
	FIXME();
}

#ifdef __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x07E99388, "+SectorMgr::MarkBlockAsFree")
{
	FUN_DEBUG_PARAMS(2,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E993A4, "-")
{

}

#endif

#ifdef __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x07E99344, "+SectorMgr::MarkBlockAsDirty")
{
	FUN_DEBUG_PARAMS(2,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E99360, "-")
{

}

#endif

DEBUG_ADDRESS(0x07E9884C, "+GetPhysicalSectorAddr")
{
	FUN_DEBUG_PARAMS(4,
		"this", "0x%08X", PF_HIDE,
		"log_sector_no", "%u", PF_NAME | PF_VALUE,
		"pOut", "0x%08X", PF_VALUE | PF_ADDPTR, 8,
		"???", "0x%08X", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x07E98AFC, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "physical_sector_no");
}

DEBUG_ADDRESS(0x07E9783C, "+BuildSectorMapInfo")
{
	FUN_DEBUG_PARAMS(3,
		"???", "0x%08X", PF_UNKNOWN | PF_HIDE,
		"block_no", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, CPU->context.regs[R1] / 64,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E97958, "-")
{

}

#ifdef __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x07E993E0, "+MarkSectorAsDirty")
{
	FUN_DEBUG_PARAMS(4,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN,
		"???", "0x%08X", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x07E9942C, "-")
{

}

#endif

#ifdef __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x07E969A8, "+decode_logical_sector_number")
{
	FUN_DEBUG_PARAMS_NONL(1,
		"sectorInfo", "m", 8, PF_NAME | PF_VALUE | PF_ADDPTR);
}

DEBUG_ADDRESS(0x07E96A58, "-")
{
	unsigned int* pResult = (unsigned int*)getMemory(popLastPtr());
	FUN_DEBUG_PARAMS_EQ();
	FUN_DEBUG_PARAMS_VALUE("%u\n", *pResult);
}

#endif

DEBUG_ADDRESS(0x07E98D8C, "+MarkBlockAsFull")
{
	FUN_DEBUG_PARAMS(4,
		"pMap", "0x%08X", PF_HIDE,
		"starting_sector_no", "%u", PF_NAME | PF_VALUE,
		"block_no", "%u", PF_NAME | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] / 64,
		"", "", PF_UNKNOWN | PF_HIDE);
}

DEBUG_ADDRESS(0x07E98E50, "-")
{

}

DEBUG_ADDRESS(0x07E98D18, "+MapLogicalSector")
{
	FUN_DEBUG_PARAMS(5,
		"?", "?", PF_UNKNOWN | PF_HIDE,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R2] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] % 64,
		"", "", PF_UNKNOWN | PF_ADDPTR | PF_HIDE, 4);
}

DEBUG_ADDRESS(0x07E98D5C, "-")
{
	popLastPtr();
	// exit 1
	//	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "???");
}

DEBUG_ADDRESS(0x07E98D88, "-")
{
	popLastPtr();
	// exit 2
//	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "???");
}

#ifdef __DEBUG_NAND_PART1_FAL_INCLUDE_NOT_USED

DEBUG_ADDRESS(0x07E99AAC, "+MarkSectorAsFree")
{
	FUN_DEBUG_PARAMS(4,
		"pSectorMgr", "0x%08X", PF_HIDE,
		"logical_sector_no", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"block_no", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R2] / 64,
		"sector_no", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM, CPU->context.regs[R2] % 64);
}

DEBUG_ADDRESS(0x07E99BB8, "-")
{

}

#endif

/*DEBUG_ADDRESS(0x07E98EC4, "+MarkSectorAsDirty_chunk")
{
	FUN_DEBUG_PARAMS(4,
		"???", "0x%08X", PF_VALUE,
		"blockNo", "%u", PF_NAME | PF_VALUE,
		"???", "0x%08X", PF_VALUE,
		"???", "0x%08X", PF_VALUE);
}

DEBUG_ADDRESS(0x07E98F74, "-")
{

}*/

DEBUG_ADDRESS(0x07E97AEC, __null)
{
	if (CPU->context.regs[R8] < 210)
		return;
//	DEBUG_PRINT("Processing block: %u\n", CPU->context.regs[R8]);
}

DEBUG_ADDRESS(0x07E97DD8, __null)
{
//	DEBUG_PRINT("MarkBlockUnusable: %u\n", CPU->context.regs[R8]);
}

#endif

#ifdef __DEBUG_NAND_PART1

DEBUG_ADDRESS(0x07E9C434, "+SMFlash::ReadBlockInfo")
{
	FUN_DEBUG_PARAMS(2,
		"blockNo", "%u", PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_NAME | PF_UNKNOWN | PF_HIDE);
}

DEBUG_ADDRESS(0x07E9C4B0, "-")
{
	FUN_DEBUGI(" << ");
	FUN_DEBUG_PARAMS_NAME_VALUE("status", "0x%02X\n", CPU->context.regs[R0]);
}

DEBUG_ADDRESS(0x07E9BB64, "+SMFlash::ReadSector")
{
	FUN_DEBUG_PARAMS(5,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 64,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 64,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"?", "%u", PF_HIDE | PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_HIDE | PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E9BC5C, "-")
{
	if (getLastPtr()) {
		FUN_DEBUG_PARAMS_NAME_MEM_LN("", "SectorInfo");
	} else
		popLastPtr();
}

DEBUG_ADDRESS(0x07E9AE70, "+NAND::ReadPage")
{
	FUN_DEBUG_PARAMS(6,
		"blockNo", "%u", PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"?", "%u", PF_HIDE | PF_NAME | PF_VALUE,
		"?", "0x%08X", PF_HIDE | PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E9B620, "-")
{
}

DEBUG_ADDRESS(0x07E9AAA4, "+TranslateSectorNo")
{
	FUN_DEBUG_PARAMS(4,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pSector1", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4,
		"pSector2", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 4);
}

DEBUG_ADDRESS(0x07E9AB24, "-")
{
	unsigned int* p2 = (unsigned int*)getMemory(popLastPtr());
	unsigned int* p1 = (unsigned int*)getMemory(popLastPtr());

	char tmp[100];
	sprintf_s(tmp, 100, "%u/%u + %u/%u (%u + %u)", *p1 / 128, *p1 % 128, *p2 / 128, *p2 % 128, *p1, *p2);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("pages", "%s", tmp);
}

DEBUG_ADDRESS(0x07E9AB38, "-")
{
	unsigned int* p2 = (unsigned int*)getMemory(popLastPtr());
	unsigned int* p1 = (unsigned int*)getMemory(popLastPtr());

	char tmp[100];
	sprintf_s(tmp, 100, "%u/%u + %u/%u (%u + %u)", *p1 / 128, *p1 % 128, *p2 / 128, *p2 % 128, *p1, *p2);
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("pages", "%s", tmp);
}

DEBUG_ADDRESS(0x07E9AB3C, "+SMFlash::ReadPageInfo")
{
	FUN_DEBUG_PARAMS(4,
		"blockNo", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, CPU->context.regs[R0] / 128,
		"sectorNo", "%u", PF_GROUP_E |PF_VALUE | PF_CUSTOM | PF_DONOTINC, CPU->context.regs[R0] % 128,
		"pInfo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"begin", "%u", PF_VALUE);
}

DEBUG_ADDRESS(0x07E9ACB0, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "PageInfo");
}

DEBUG_ADDRESS(0x07E9C4B4, "+SMFlash::EraseBlock")
{
	FUN_DEBUG_PARAMS(1,
		"blockNo", "0x%08X(%u)", PF_VALUE);
}

DEBUG_ADDRESS(0x07E9C5FC, "-")
{
}

DEBUG_ADDRESS(0x07E9BC60, "+SMFlash::WriteSector")
{
	FUN_DEBUG_PARAMS(4,
		"sectorNo", "%u", PF_NAME | PF_VALUE,
		"pData", "0x%08X", PF_NAME | PF_VALUE,
		"pInfo", "m", 8, PF_VALUE,
		"?", "0x%08X", PF_HIDE, PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E9C16C, "-")
{
}

DEBUG_ADDRESS(0x07E9ACBC, "+SMFlash::WritePageInfo")
{
	FUN_DEBUG_PARAMS(3,
		"pageNo", "0x%08X(%u)", PF_VALUE,
		"pInfo", "m", 8, PF_VALUE,
		"?", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x07E9AE64, "-")
{
}

#endif

#ifdef __DEBUG_SDI

DEBUG_ADDRESS(0x02F24BDC, __null)
{
	// set DMA channel
	//CPU->context.regs[R0] = uint(-1);
}

static const char* respType = __null;
static int respLen = 0;

DEBUG_ADDRESS(0x02F224AC, "CSDIOControllerBase::SendCommand")
{
	FUN_DEBUG_PARAMS(5,
		"this", "", PF_HIDE,
		"Cmd", "%u", PF_NAME | PF_VALUE,
		"Arg", "0x%08X", PF_VALUE,
		"respType", "%u", PF_NAME | PF_VALUE,
		"bDataTransfer", "%u", PF_NAME | PF_VALUE);

	switch (CPU->context.regs[R3]) {
		case 0:
			respType = "NoResponse";
			respLen = 0;
			break;
		case 1:
			respType = "ResponseR1";
			respLen = 6;
			break;
		case 2:
			respType = "ResponseR1b";
			respLen = 6;
			break;
		case 3:
			respType = "ResponseR2";
			respLen = 16;
			break;
		case 4:
			respType = "ResponseR3";
			respLen = 6;
			break;
		case 5:
			respType = "ResponseR4";
			respLen = 6;
			break;
		case 6:
			respType = "ResponseR5";
			respLen = 6;
			break;
		case 7:
			respType = "ResponseR6";
			respLen = 6;
			break;
		case 8:
			respType = "ResponseR7";
			respLen = 32;
			break;
	}
}

DEBUG_ADDRESS(0x02F228D0, __null)
{
	addLastPtr(CPU->context.regs[R5] + 0x20, respLen);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", respType);
}

#endif

#ifdef __DEBUG_AT88SC_BOOT

CONST_VALUE_ITEMS(AT88SC_commands) {
	{0xB2, "READ_USER_ZONE"}, 
	{0xB4, "SYSTEM_WRITE"},
	{0xB6, "SYSTEM_READ"},
	{0xB8, "VERIFY_CRYPTO"},
	{0xBA, "VERIFY_PASSWORD"},
};

DEBUG_ADDRESS(0x30008EA4, "AT88SC_Write")
{
	FUN_DEBUG_PARAMS(5,
		CONST_VALUE("cmd", AT88SC_commands), PF_VALUE,
		"param1", "0x%02X", PF_VALUE,
		"param2", "0x%02X", PF_VALUE,
		"write_len", "%u", PF_NAME | PF_VALUE,
		"data", "m", CPU->context.regs[R3], PF_NAME | PF_VALUE | PF_NL);
}

DEBUG_ADDRESS(0x30008FC0, "AT88SC_WriteCommand")
{
	FUN_DEBUG_PARAMS(5,
		CONST_VALUE("cmd", AT88SC_commands), PF_VALUE,
		"param1", "0x%02X", PF_VALUE,
		"param2", "0x%02X", PF_VALUE,
		"write_len", "%u", PF_NAME | PF_VALUE,
		"data", "m", CPU->context.regs[R3], PF_NAME | PF_VALUE | PF_NL);
}

DEBUG_ADDRESS(0x30008C80, "AT88SC_Read")
{
	FUN_DEBUG_PARAMS(5,
		CONST_VALUE("cmd", AT88SC_commands), PF_VALUE,
		"param1", "0x%02X", PF_VALUE,
		"param2", "0x%02X", PF_VALUE,
		"read_len", "%u", PF_NAME | PF_VALUE,
		"out", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R3]);
}

DEBUG_ADDRESS(0x30008E7C, __null)
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "out");
}

DEBUG_ADDRESS(0x30009090, "AT88SC_ReadUserZone")
{
	FUN_DEBUG_PARAMS(4,
		"zone", "%u", PF_VALUE | PF_NAME,
		"read_len", "%u", PF_NAME | PF_VALUE,
		"address", "0x%02X", PF_NAME | PF_VALUE,
		"out", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x300097D8, "AT88SC_ReadChecksum")
{
	FUN_DEBUG_PARAMS(1,
		"out", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009664, "AT88SC_ReadIssuerCode")
{
	FUN_DEBUG_PARAMS(1,
		"out", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009230, "AT88SC_Authenticate")
{
	FUN_DEBUG_PARAMS(3,
		"G_Sk", "m", 8, PF_NAME | PF_VALUE,
		"key_index", "0x%02X", PF_NAME | PF_VALUE,
		"encrypt", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009270, "  --> read cryptogram 0\n")
{
}

DEBUG_ADDRESS(0x30009344, "  --> verify crypto\n")
{
}

DEBUG_ADDRESS(0x30009368, "  --> read cryptogram 0\n")
{
}

DEBUG_ADDRESS(0x3000948C, "  --> FAILED!\n")
{
}

DEBUG_ADDRESS(0x3000CE40, "AT88SC_CompareData")
{
	FUN_DEBUG_PARAMS(3,
		"src1", "m", CPU->context.regs[R2], PF_VALUE,
		"src2", "m", CPU->context.regs[R2], PF_VALUE,
		"len", "%u", PF_NAME, PF_VALUE);
}

DEBUG_ADDRESS(0x30008508, "sub_30008508")
{
	FUN_DEBUG_PARAMS(4,
		"cryptogram", "m", 8, PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"G_Sk", "m", 8, PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"random", "m", 8, PF_NAME | PF_VALUE | PF_NL | PF_ADDPTR, 8,
		"?", "0x%08X", PF_NAME, PF_VALUE);
}

DEBUG_ADDRESS(0x30008588, __null)
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "cryptogram");
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "cryptogram");
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "cryptogram");
}

DEBUG_ADDRESS(0x3000858C, "sub_3000858C")
{
	FUN_DEBUG_PARAMS(3,
		"?", "0x%08X", PF_NAME | PF_VALUE | PF_NL | PF_ADDPTR, 8,
		"cryptogram", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8,
		"challenge", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, 8);
}

DEBUG_ADDRESS(0x3000875C, __null)
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "?");
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "valid_cryptogram");
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "challenge");
}

#endif

#ifdef __DEBUG_AT88SC_KERNEL

DEBUG_ADDRESS(0x80266414, __null)
{
	// fake OS auth
	//CPU->context.regs[R0] = 1;
}

DEBUG_ADDRESS(0x80265910, "AT88SC_Read")
{
	FUN_DEBUG_PARAMS(5,
		CONST_VALUE("cmd", AT88SC_commands), PF_VALUE,
		"param1", "0x%02X", PF_VALUE,
		"param2", "0x%02X", PF_VALUE,
		"read_len", "%u", PF_NAME | PF_VALUE,
		"out", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R3]);
}

DEBUG_ADDRESS(0x80265AE8, __null)
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "out");
}

DEBUG_ADDRESS(0x80265DFC, "AT88SC_Authenticate")
{
	FUN_DEBUG_PARAMS(3,
		"G_Sk", "m", 8, PF_NAME | PF_VALUE,
		"key_index", "0x%02X", PF_NAME | PF_VALUE,
		"encrypt", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80266728, "cm_AuthenEncyptCal")
{
	addLastPtr(CPU->context.regs[R7] + 0x17, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "Ci");
	addLastPtr(CPU->context.regs[R7] + 0x1F, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "G_Sk");
	addLastPtr(CPU->context.regs[R7] + 0x27, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "Q");
}

DEBUG_ADDRESS(0x80265B00, "AT88SC_WriteCommand")
{
	FUN_DEBUG_PARAMS(5,
		CONST_VALUE("cmd", AT88SC_commands), PF_VALUE,
		"param1", "0x%02X", PF_VALUE,
		"param2", "0x%02X", PF_VALUE,
		"write_len", "%u", PF_NAME | PF_VALUE,
		"data", "m", CPU->context.regs[R3], PF_NAME | PF_VALUE | PF_NL);
}

DEBUG_ADDRESS(0x05E9F9CC, "cm_AuthenEncyptCal")
{
	addLastPtr(CPU->context.regs[R7] + 0x17, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "Ci");
	addLastPtr(CPU->context.regs[R7] + 0x1F, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "G_Sk");
	addLastPtr(CPU->context.regs[R7] + 0x27, 8);
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "Q");
}

#endif

#ifdef __DEBUG_NAND

DEBUG_ADDRESS(0x30004BDC, "NAND_ReadID")
{
	FUN_DEBUG_PARAMS(1,
		"nand_no", "%d", PF_NAME | PF_VALUE);
}

#endif

#ifdef __DEBUG_USBD_BOOT

DEBUG_ADDRESS(0x30009B8C, __null)
{
	DEBUG_PRINT_COLOR(11);
	//DEBUG_PRINT_CHAR(CPU->context.regs[R0], stdout);
	DEBUG_PRINT_COLOR(7);
}

DEBUG_ADDRESS(0x300097F8, "+PrepareEp1Fifo")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x30009844, "-")
{

}

DEBUG_ADDRESS(0x30009848, "+Ep1Handler")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x30009880, "-")
{

}

DEBUG_ADDRESS(0x300098FC, "+ReconfigUsbd")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x300099E4, "-")
{

}

DEBUG_ADDRESS(0x3000A31C, "+sub3000A31C")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x3000A520, "-")
{

}

DEBUG_ADDRESS(0x300099E8, "+sub300099E8")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x30009A00, "-")
{

}

DEBUG_ADDRESS(0x30009A04, "+RdPktEp0")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R1],
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009A20, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "buf");
}

DEBUG_ADDRESS(0x30009A24, "+WrPktEp0")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "m", CPU->context.regs[R1], PF_VALUE,
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009A40, "-")
{

}

DEBUG_ADDRESS(0x30009A44, "+WrPktEp1")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "m", CPU->context.regs[R1], PF_VALUE,
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009A60, "-")
{

}

DEBUG_ADDRESS(0x30009A64, "+WrPktEp2")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "m", CPU->context.regs[R1], PF_NAME | PF_VALUE,
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009A80, "-")
{

}

DEBUG_ADDRESS(0x30009A84, "+RdPktEp3")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R1],
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009AA0, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "buf");
}

DEBUG_ADDRESS(0x3000A540, "+RdPktEp3_crc")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R1],
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x3000A570, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "buf");
}

DEBUG_ADDRESS(0x30009AA4, "+RdPktEp4")
{
	FUN_DEBUG_PARAMS(2,
		"buf", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, CPU->context.regs[R1],
		"num", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009AC0, "-")
{
	FUN_DEBUG_PARAMS_NAME_MEM_LN("", "buf");
}

DEBUG_ADDRESS(0x30009C18, "+IsrUsbd")
{
	FUN_DEBUG_PARAMS(1,
		"val", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009CD8, "-")
{

}

DEBUG_ADDRESS(0x30009D10, "+Isr_Init")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x30009DF8, "-")
{

}

DEBUG_ADDRESS(0x3000A148, "+EP3Handler")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x3000A250, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x3000A25C, "-")
{
	// exit 2
}

DEBUG_ADDRESS(0x3000A280, "-")
{
	// exit 3
}

DEBUG_ADDRESS(0x3000A288, "-")
{
	// exit 4
}

DEBUG_ADDRESS(0x3000A29C, "-")
{
	// exit 4
}

DEBUG_ADDRESS(0x3000A5C4, "+EP0Handler")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x3000A64C, "-")
{

}

DEBUG_ADDRESS(0x30009AC4, "+ConfigEp3DmaMode")
{
	FUN_DEBUG_PARAMS(2,
		"bufAddr", "0x%08X", PF_NAME | PF_VALUE,
		"count", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x30009B50, "-")
{

}

DEBUG_ADDRESS(0x30009B54, "+ConfigEp3IntMode")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x30009B84, "-")
{

}

DEBUG_ADDRESS(0x3000AF3C, "+InitDescriptorTable")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x3000B094, "-")
{

}

#endif

#ifdef __DEBUG_USBD

DEBUG_ADDRESS(0x02F032F4, "+UfnPdd_InitEndpoint")
{
	FUN_DEBUG_PARAMS(7,
		"pvPddContext", "0x%08X", PF_HIDE,
		"dwEndpoint", "%u", PF_VALUE,
		"Speed", "%u", PF_NAME | PF_VALUE,
		"pvReserved", "0x%08X", PF_HIDE,
		"bConfigurationValue", "%u", PF_NAME | PF_VALUE,
		"bInterfaceNumber", "%u", PF_NAME | PF_VALUE,
		"bAlternateSetting", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x02F03438, "-")
{

}

DEBUG_ADDRESS(0x02F019F0, "+ResetEndpoint")
{
	FUN_DEBUG_PARAMS(2,
		"pvPddContext", "0x%08X", PF_HIDE,
		"peps", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x02F01B30, "-")
{

}

#endif
