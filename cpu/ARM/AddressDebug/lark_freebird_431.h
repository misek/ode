//#define __DEBUG_ROM0_NAND
#define __DEBUG_ROM1
//#define __DEBUG_NAND
//#define __DEBUG_VFL
//#define __DEBUG_FTL
//#define __DEBUG_POWERBUTTON
#define __DEBUG_I2C

DEBUG_ADDRESS(0x03F8B4D8, "CreateProcessW")
{
	wchar_t* p = (wchar_t*)getMemory(Cpu.GPRs[R0]);
	if (p) {
		if (wcscmp(L"gps2.exe", p) == 0) {
			//wcscpy_s(p, 64, L"explorer.exe");
		}
	}

	FUN_DEBUG_PARAMS(10,
		"pszImageName", "w", PF_VALUE,
		"lpCommandLine", "w", PF_VALUE,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"lpCurrentDirectory", "w", PF_VALUE,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN);
}

DEBUG_ADDRESS(0x80284230, __null)
{
	// OALSetLogZones
	//Cpu.GPRs[R0] = 0xFFFFFFFF;
	Cpu.GPRs[R0] &= ~0x8000; // IRQ
	Cpu.GPRs[R0] &= ~0x200000; // OEMGetRealTime
	Cpu.GPRs[R0] &= ~0x4000000; // OEMIoControl
}

DEBUG_ADDRESS(0x800030E8, __null)
{
	// debug from ROM0
	fontcolor(0x0A);
	putc(Cpu.GPRs[R0], stdout);
	fontcolor(0x07);
}

DEBUG_ADDRESS(0x8004F9B8, __null)
{
	// debug from ROM1
	fontcolor(0x0B);
	//while (1) Sleep(1);
	putc(Cpu.GPRs[R0], stdout);
	fontcolor(0x07);
}

DEBUG_ADDRESS(0x80286988, __null)
{
	// debug from ROM2
	fontcolor(0x0A);
	putc(Cpu.GPRs[R0], stdout);
	fontcolor(0x07);
}

#ifdef __DEBUG_ROM0_NAND

DEBUG_ADDRESS(0x80005B64, "+NAND_Init")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x80005E20, "-")
{

}

DEBUG_ADDRESS(0x800053B4, "+NAND_Sync")
{
	FUN_DEBUG_PARAMS(2,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nPlaneBitmap", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80005598, "-")
{

}

DEBUG_ADDRESS(0x800065F4, "+Read_Spare")
{
	FUN_DEBUG_PARAMS(4,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R1] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R1] % 128,
		"pSpareCxt", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80006B28, "-")
{

}

DEBUG_ADDRESS(0x80006B4C, "+Read_Sector")
{
	FUN_DEBUG_PARAMS(7,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R1] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R1] % 128,
		"nSctOffset", "%u", PF_NAME | PF_VALUE,
		"pBuf", "0x%08X", PF_NAME | PF_VALUE,
		"pSpareCxt", "0x%08X", PF_NAME | PF_VALUE,
		"bCheckAllFF", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80006D2C, "-")
{

}

DEBUG_ADDRESS(0x80007300, "+_Read_512Byte")
{
	FUN_DEBUG_PARAMS(1,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x8000733C, "-")
{

}

DEBUG_ADDRESS(0x80007340, "+_Read_512Byte_Unaligned")
{
	FUN_DEBUG_PARAMS(1,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x800073D8, "-")
{

}

DEBUG_ADDRESS(0x800058F8, "+Decoding_MainECC")
{
	FUN_DEBUG_PARAMS(1,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80005938, "-")
{
	// exit 1 - failed
}

DEBUG_ADDRESS(0x80005A24, "-")
{
	// exit 2
}

DEBUG_ADDRESS(0x80006D3C, "+NAND_Read")
{
	FUN_DEBUG_PARAMS(8,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R1] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R1] % 128,
		"nSctBitmap", "0x%08X", PF_NAME | PF_VALUE,
		"nPlaneBitmap", "%0x%08X", PF_NAME | PF_VALUE,
		"pDBuf", "0x%08X", PF_NAME | PF_VALUE,
		"pSBuf", "0x%08X", PF_NAME | PF_VALUE,
		"bECCIn", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"bCleanCheck", "%u", PF_NAME | PF_VALUE | PF_HIDE);
}

DEBUG_ADDRESS(0x800072D8, "-")
{

}

#endif

#ifdef __DEBUG_NAND

DEBUG_ADDRESS(0x80291A68, "NAND_Init")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x802914C0, "NAND_Reset")
{
	FUN_DEBUG_PARAMS(1,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE);
}

DEBUG_ADDRESS(0x802912B8, "NAND_Sync")
{
	FUN_DEBUG_PARAMS(2,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nPlaneBitmap", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80292C40, "+NAND_Read")
{
	FUN_DEBUG_PARAMS(8,
		"nBank", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R1] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R1] % 128,
		"nSctBitmap", "0x%08X", PF_NAME | PF_VALUE,
		"nPlaneBitmap", "%0x%08X", PF_NAME | PF_VALUE,
		"pDBuf", "0x%08X", PF_NAME | PF_VALUE,
		"pSBuf", "0x%08X", PF_NAME | PF_VALUE,
		"bECCIn", "%u", PF_NAME | PF_VALUE | PF_HIDE,
		"bCleanCheck", "%u", PF_NAME | PF_VALUE | PF_HIDE);
}

DEBUG_ADDRESS(0x802931DC, "-")
{
}

DEBUG_ADDRESS(0x80291D3C, "NAND_Write")
{
	FUN_DEBUG_PARAMS(4,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN)
}

DEBUG_ADDRESS(0x802921B4, "NAND_Erase")
{
	FUN_DEBUG_PARAMS(4,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN)
}

DEBUG_ADDRESS(0x80290FB4, "NAND_Steploader_Write")
{
	FUN_DEBUG_PARAMS(4,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN,
		"", "", PF_UNKNOWN)
}

#endif

/*DEBUG_ADDRESS(0x8028964C, "+_ConvertL2V")
{
	FUN_DEBUG_PARAMS(2,
		"lNo", "0x%08X (%u)", PF_NAME | PF_VALUE,
		"vNo", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR);
}

DEBUG_ADDRESS(0x80289708, "-")
{
	unsigned int* v = (unsigned int*)getMemory(popLastPtr());
	if (Cpu.GPRs[R0]) {
		FUN_DEBUG_PARAMS_NAME_RESULT_LN("virtual_address", "0x%08X (%u)", *v);
	} else {
		FUN_DEBUG_PARAMS_NAME_RESULT_LN("result", "failed", __null);
	}
}*/

#ifdef __DEBUG_VFL

DEBUG_ADDRESS(0x8028E044, "+VFL_Init")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x8028E10C, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x8028E118, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x8028E964, "+VFL_Read")
{
	FUN_DEBUG_PARAMS(4,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R0] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R0] % 128,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE,
		"bCleanCheck", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x8028EC3C, "-")
{

}

DEBUG_ADDRESS(0x8028F6E0, "+VFL_CopyBack")
{
	FUN_DEBUG_PARAMS(5,
		"srcBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R0] / 128,
		"srcPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R0] % 128,
		"destBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R1] / 128,
		"destPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R1] % 128,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x80290064, "-")
{

}

DEBUG_ADDRESS(0x8028EC54, "+VFL_Write")
{
	FUN_DEBUG_PARAMS(3,
		"nBlock", "%u", PF_GROUP_S | PF_VALUE | PF_CUSTOM, Cpu.GPRs[R0] / 128,
		"nPage", "%u", PF_GROUP_E | PF_VALUE | PF_CUSTOM | PF_DONOTINC, Cpu.GPRs[R0] % 128,
		"pBuffer", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x8028EEA8, "-")
{

}

DEBUG_ADDRESS(0x8028EF28, "+VFL_Erase")
{
	FUN_DEBUG_PARAMS(1,
		"nBlock", "%u", PF_VALUE | PF_NAME);
}

DEBUG_ADDRESS(0x8028F6A0, "-")
{

}

DEBUG_ADDRESS(0x8028EEC0, "+VFL_Sync")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x8028EF1C, "-")
{

}

DEBUG_ADDRESS(0x802901CC, "+VFL_Format")
{
	FUN_DEBUG_PARAMS(0);
}

DEBUG_ADDRESS(0x802905EC, "-")
{

}

#endif

#ifdef __DEBUG_FTL

DEBUG_ADDRESS(0x8028A79C, "+FTL_Init")
{
	FUN_DEBUG_PARAMS(0)
}

DEBUG_ADDRESS(0x8028A8E0, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x8028A910, "-")
{
	// exit 2
}

DEBUG_ADDRESS(0x8028A52C, "+FTL_Format")
{
	FUN_DEBUG_PARAMS(0)
}

DEBUG_ADDRESS(0x8028A77C, "-")
{

}

DEBUG_ADDRESS(0x8028C850, "+FTL_Open")
{
	FUN_DEBUG_PARAMS(1,
		"pTotalScts", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR);
}

DEBUG_ADDRESS(0x8028C888, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x8028C8A0, "-")
{
	// exit 2
	unsigned int* v = (unsigned int*)getMemory(popLastPtr());
	if (v) {
		FUN_DEBUG_PARAMS_NAME_RESULT_LN("Total sectors", "%u", *v);
		FUN_DEBUG_PARAMS_NAME_RESULT_LN("Capacity", "%0.2f MB", (double)(*v * 512) / 1024 / 1024);
	}
}

DEBUG_ADDRESS(0x8028A948, "+FTL_Close")
{
	FUN_DEBUG_PARAMS(0)
}

DEBUG_ADDRESS(0x8028AA24, "-")
{

}

DEBUG_ADDRESS(0x8028BE94, "+FTL_ReadReclaim")
{
	FUN_DEBUG_PARAMS(0)
}

DEBUG_ADDRESS(0x8028BFD0, "-")
{

}

DEBUG_ADDRESS(0x8028C01C, "+FTL_GarbageCollect")
{
	FUN_DEBUG_PARAMS(0)
}

DEBUG_ADDRESS(0x8028C058, "-")
{
	// exit 1
}

DEBUG_ADDRESS(0x8028C068, "-")
{
	// exit 2
}

DEBUG_ADDRESS(0x8028A12C, "+FTL_Read")
{
	FUN_DEBUG_PARAMS(3,
		"nLsn", "%u", PF_NAME | PF_VALUE,
		"nNumOfScts", "%u", PF_NAME | PF_VALUE,
		"pBuf", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x8028A504, "-")
{

}

DEBUG_ADDRESS(0x8028C8B0, "+FTL_Write")
{
	FUN_DEBUG_PARAMS(3,
		"nLsn", "%u", PF_NAME | PF_VALUE,
		"nNumOfScts", "%u", PF_NAME | PF_VALUE,
		"pBuf", "0x%08X", PF_NAME | PF_VALUE)
}

DEBUG_ADDRESS(0x8028CF04, "-")
{

}

#endif

#ifdef __DEBUG_POWERBUTTON

DEBUG_ADDRESS(0x02C32038, __null)
{
	Cpu.GPRs[R10] = 0xFFFFFFFF;
}

DEBUG_ADDRESS(0x8024DCE0, __null)
{
	//Cpu.GPRs[R0] = 0;
}

DEBUG_ADDRESS(0x80287BB8, __null)
{
	//Cpu.GPRs[R1] = 8;
}

DEBUG_ADDRESS(0x80287BEC, __null)
{
	//CpuDumpContext();
	//Cpu.GPRs[R5] = 0;
}

#endif

#ifdef __DEBUG_I2C

DEBUG_ADDRESS(0x02C41940, "+HW_Read")
{
	FUN_DEBUG_PARAMS(5,
		"pI2C", "0x%08X", PF_HIDE,
		"SlaveAddr", "0x%02X", PF_NAME | PF_VALUE,
		"WordAddr", "0x%02X", PF_NAME | PF_VALUE,
		"pData", "0x%08X", PF_NAME | PF_VALUE | PF_ADDPTR, STACK_ARGUMENT(0),
		"Count", "%u", PF_VALUE);
}

DEBUG_ADDRESS(0x02C41ACC, "-")
{
	popLastPtr();
}

DEBUG_ADDRESS(0x02C41AD4, "+HW_Write")
{
	FUN_DEBUG_PARAMS(5,
		"pI2C", "0x%08X", PF_HIDE,
		"SlaveAddr", "0x%02X", PF_NAME | PF_VALUE,
		"WordAddr", "0x%02X", PF_NAME | PF_VALUE,
		"pData", "m", STACK_ARGUMENT(0), PF_NAME | PF_VALUE,
		"Count", "%u", PF_VALUE);
}

DEBUG_ADDRESS(0x02C41C00, "-")
{

}

#endif

#ifdef __DEBUG_ROM1

void __fastcall reverse_bytes(void* src, int len);
void __fastcall reverse_bits(void* src, int len);

DEBUG_ADDRESS(0x8005E2C0, __null)
{
	DEBUG_PRINT("RESP[1] = 0x%08X, should be: 0x%08X\n", Cpu.GPRs[R2], 0xf6db5fff);
	DEBUG_PRINT("RESP[2] = 0x%08X, should be: 0x%08X\n", Cpu.GPRs[R3], 0x5f5983d6);
	unsigned int a = Cpu.GPRs[R2];
	unsigned int x;
	x = a; DEBUG_PRINT("0x%08X %u %u\n", x, (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bytes(&x, 4); DEBUG_PRINT("0: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bits(&x, 4); DEBUG_PRINT("1: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bits(&x, 4); reverse_bytes(&x, 4); DEBUG_PRINT("2: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bytes(&x, 4); reverse_bits(&x, 4); DEBUG_PRINT("3: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	a = Cpu.GPRs[R3];
	x = a; DEBUG_PRINT("0x%08X %u %u\n", x, (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bytes(&x, 4); DEBUG_PRINT("0: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bits(&x, 4); DEBUG_PRINT("1: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bits(&x, 4); reverse_bytes(&x, 4); DEBUG_PRINT("2: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
	x = a; reverse_bytes(&x, 4); reverse_bits(&x, 4); DEBUG_PRINT("3: %u %u\n", (x << 22) >> 29, (x << 20) >> 28);
}
/*
.text:8005E2AC                 LDR     R3, [R6,#0x18]		>>> RESP[2]
.text:8005E2B0                 LDR     R5, [R6,#0x18]		>>> RESP[2]
.text:8005E2B4                 LDR     R1, [R6,#0x18]		>>> RESP[2]
.text:8005E2B8                 LDR     R4, [R6,#0x14]		>>> RESP[1]
.text:8005E2BC                 LDR     R2, [R6,#0x14]		>>> RESP[1]
.text:8005E2C0                 MOV     R3, R3,LSL#20		>>>
.text:8005E2C4                 MOV     LR, R3,LSR#28		>>> READ_BL_LEN
.text:8005E2C8                 MOV     R3, R2,LSL#22		>>>
.text:8005E2CC                 MOV     R3, R3,LSR#29		>>> C_SIZE_MULT
.text:8005E2D0                 ADD     R3, R3, #2			C_SIZE_MULT+2
.text:8005E2D4                 AND     R2, R1, #3
.text:8005E2D8                 MOV     R1, R7,LSL R3		(1<<(C_SIZE_MULT+2))
.text:8005E2DC                 MOV     R3, R8,LSL LR		(1<<READ_BL_LEN)
.text:8005E2E0                 MOV     R2, R2,LSL#10
.text:8005E2E4                 MUL     LR, R1, R3			(1<<READ_BL_LEN)*(1<<(C_SIZE_MULT+2))
.text:8005E2E8                 ORR     R3, R2, R4,LSR#22	>>> C_SIZE
.text:8005E2EC                 ADD     R3, R3, #1			(C_SIZE+1)
.text:8005E2F0                 MUL     R4, LR, R3			(1<<READ_BL_LEN)*(1<<(C_SIZE_MULT+2))*(C_SIZE+1)
.text:8005E2F4                 LDR     R0, =aCardsizeD ; "\n CardSize: %d\n"
.text:8005E2F8                 LDR     R3, [R6,#0x14]
.text:8005E2FC                 LDR     R2, [R6,#0x14]
.text:8005E300                 MOV     R1, R4*/


// c_size: F5B (3931)
// mult: 6
DEBUG_ADDRESS(0x8005E2C8, __null)
{
	DEBUG_PRINT("READ_BL_LEN = %u\n", Cpu.GPRs[R14]);
}

DEBUG_ADDRESS(0x8005E2D0, __null)
{
	DEBUG_PRINT("C_SIZE_MULT = %u\n", Cpu.GPRs[R3]);
}

DEBUG_ADDRESS(0x8005E2EC, __null)
{
	DEBUG_PRINT("C_SIZE = %u (%X)\n", Cpu.GPRs[R3], Cpu.GPRs[R3]);
}

DEBUG_ADDRESS(0x02CD434C, __null)
{
	DEBUG_PRINT("compare: 0x%08X == 0x%08X\n", Cpu.GPRs[R2], Cpu.GPRs[R3]);
}

DEBUG_ADDRESS(0x8005E318, __null)
{
//	while(1) Sleep(1);
}

DEBUG_ADDRESS(0x80059694, "+Parsing")
{
	FUN_DEBUG_PARAMS(2,
		"sFileName", "s", PF_NAME | PF_VALUE,
		"dwImageType", "%u", PF_NAME | PF_VALUE,
		"Buffer", "%u", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x8005A018, "-")
{

}

DEBUG_ADDRESS(0x8005A99C, "-")
{
	// skip test for UPDATE98.NB0 file
	Cpu.GPRs[R0] = 1;
}

DEBUG_ADDRESS(0x8005AA54, "-")
{
	// skip test for RESTOR98.NB0 file
	Cpu.GPRs[R0] = 1;
}

DEBUG_ADDRESS(0x8005A8F8, "+SDMMC_UpdateImage")
{
/*#define SD_UPDATE_BLOCK0 (0x1<<0)
#define SD_UPDATE_EBOOT (0x1<<1)
#define SD_UPDATE_LOGO (0x1<<2)
#define SD_UPDATE_OS (0x1<<3)
#define SD_UPDATE_FORTMAT_A (0x1<<4)
#define SD_UPDATE_FORTMAT_B (0x1<<5)
#define SD_UPDATE_FORTMAT_C (0x1<<6)
#define SD_UPDATE_ALL 0xFFFF
*/

	FUN_DEBUG_PARAMS(2,
		"u32channel", "%u", PF_NAME | PF_VALUE,
		"u32UpdateBit", "0x%08X", PF_NAME | PF_VALUE);
}

DEBUG_ADDRESS(0x8005B010, "-")
{

}

DEBUG_ADDRESS(0x8005DC30, "+FATGetFileSize")
{
	FUN_DEBUG_PARAMS(2,
		"file_name", "s", PF_NAME | PF_VALUE,
		"void*", "0x%08X", PF_VALUE);
}

DEBUG_ADDRESS(0x8005DC44, "-")
{
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("file_size", "%s", "file does not exist!");
}

DEBUG_ADDRESS(0x8005DC64, "-")
{
	FUN_DEBUG_PARAMS_NAME_RESULT_LN("file_size", "%u", Cpu.GPRs[R0]);
}

#endif