/*
 * codegen.cpp
 *
 *  Created on: 13-04-2012
 *      Author: Łukasz Misek
 */

#include "codegen.h"

#include <stdio.h>
#include <string.h>

#include "armcpu.h"

void* allocate_code_memory(size_t size)
{
#ifdef _WIN32
	return VirtualAlloc(__null, size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
#else
	return mmap(__null, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);
#endif
}

void free_code_memory(void* data, size_t size)
{
#ifdef _WIN32
	//FIXME:
	VirtualFree();
#else
	munmap(data, size);
#endif
}

char tmp_buf[BUF_COUNT][100];
int tmp_index = 0;

const char* ARM_regNAME(int no)
{
	if (no < 13)
		return_strf("R%u", no);
	if (no == 14)
		return "SP";
	if (no == 14)
		return "LR";
	if (no == 15)
		return "PC";
	return __null;
}

const char* PTR32_NAME(ARMCPU* cpu, void* ptr)
{
	for (int i= 0; i < 13; i++) {
		if (ptr == &cpu->context.regs[i]) {
			return_strf("R%u", i);
		}
	}
	if (ptr == &cpu->context.regs[13]) {
		return "SP";
	}
	if (ptr == &cpu->context.regs[14]) {
		return "LR";
	}
	if (ptr == &cpu->context.regs[15]) {
		return "PC";
	}

	return_strf("%p", ptr);
	return __null;
}

const char* PTR8_NAME(ARMCPU* cpu, void* ptr)
{
	if (ptr == &cpu->context.flags.C) {
		return "CFlag";
	}
	if (ptr == &cpu->context.flags.Z) {
		return "ZFlag";
	}
	if (ptr == &cpu->context.flags.N) {
		return "NFlag";
	}
	if (ptr == &cpu->context.flags.V) {
		return "VFlag";
	}
	return_strf("%p", ptr);
	return __null;
}

const char* X86_REG8_NAME(X86_REGISTER reg)
{
	switch (reg) {
		case regAL:
			return "AL";
		case regAH:
			return "AH";
		case regBL:
			return "BL";
		case regBH:
			return "BH";
		case regCL:
			return "CL";
		case regCH:
			return "CH";
		case regDL:
			return "DL";
		case regDH:
			return "DH";
	}
	return "<x86_invalid_reg8>";
}

const char* X86_REG16_NAME(X86_REGISTER reg)
{
	switch (reg) {
		case regAX:
			return "AX";
		case regBX:
			return "BX";
		case regCX:
			return "CX";
		case regDX:
			return "DX";
		case regSP:
			return "SP";
		case regBP:
			return "BP";
		case regDI:
			return "DI";
		case regSI:
			return "SI";
	}
	return "<x86_invalid_reg16>";
}

const char* X86_REG32_NAME(X86_REGISTER reg)
{
	switch (reg) {
		case regEAX:
			return "EAX";
		case regEBX:
			return "EBX";
		case regECX:
			return "ECX";
		case regEDX:
			return "EDX";
		case regESP:
			return "ESP";
		case regEBP:
			return "EBP";
		case regEDI:
			return "EDI";
		case regESI:
			return "ESI";
	}
	return "<x86_invalid_reg132>";
}






