/*
 * decoder.cpp
 *
 *  Created on: 12-04-2012
 *      Author: Łukasz Misek
 */

#include <utils.h>
#include "armcpu.h"
#include "decoder.h"
#include "SystemControlProcessor.h"

#ifdef DEBUG_INSTRUCTIONS
#include "AddressDebug/address_debug_arm.h"
#endif

// FIXME: move this to some debugging routine
void CODEGEN_CALL single_step_execution(ARMCPU* cpu) {
	char di[256];
	uint32_t currentPC = cpu->context.regs[PC];
	//FIXME:
	uint32_t* instr = (uint32_t*)0;//CPU->PA_to_HostMem(CPU->VA_to_PA(currentPC));
	deasm_instruction(currentPC, *instr, di);
	DEBUG_PRINT("R0:  0x%08X  R1:  0x%08X  R2:  0x%08X  R3:  0x%08X  R4:  0x%08X  R5:  0x%08X  R6:  0x%08X  R7:  0x%08X\n",
		cpu->context.regs[R0], cpu->context.regs[R1], cpu->context.regs[R2], cpu->context.regs[R3], cpu->context.regs[R4],
		cpu->context.regs[R5], cpu->context.regs[R6], cpu->context.regs[R7]);
	DEBUG_PRINT("R8:  0x%08X  R9:  0x%08X  R10: 0x%08X  R11: 0x%08X  R12: 0x%08X  SP:  0x%08X  LR:  0x%08X  PC:  0x%08X\n",
		cpu->context.regs[R8], cpu->context.regs[R9], cpu->context.regs[R10], cpu->context.regs[R11], cpu->context.regs[R12],
		cpu->context.regs[SP], cpu->context.regs[LR], PC);
	DEBUG_PRINT("INSTR: %-37s                 SPSR:0x%08X  CPSR:0x%08X  [%c%c%c%c, %s]\n",
		di, cpu->context.has_SPSR ? cpu->context.SPSR.Word: -1, cpu->context.read_CPSR().Word,
		cpu->context.flags.N ? 'N': 'n', cpu->context.flags.Z ? 'Z': 'z', cpu->context.flags.C ? 'C': 'c', cpu->context.flags.V ? 'V': 'v',
		arm_mode_name((ARM_CPU_MODE)cpu->context.uncached_CPSR.M));

	//getchar();
}

//#define PLACE_SINGLE_STEP() place_call_imm(single_step_execution, 1)
#define PLACE_SINGLE_STEP()

void* init_code_memory(void* data)
{
	return data;
}

inline void* __place_VA_to_Host(void* data, fun_VA_to_PA fun, DecoderContext* ctx)
{
	// VA is placed in EAX
	if (ctx->MMU->ControlRegister.M) {
#ifdef MMU_TRANSLATE_ASM
		place_call_argument(0, reg32, regEAX);
		place_call_imm(fun, 1);
		place_condjump(NC, done);
#else
		place_call_argument(1, reg32, regEAX);
		place_call_argument(0, imm, ctx->MMU);
		place_call_imm(fun, 2);
		place_test_reg32__reg32(regEDX, regEDX);
		place_condjump(Z, done);
#endif
		place_push_imm(ctx->CPU->PCModifiedHelper);
		place_call_obj_argument(0, imm, ctx->PC);
		place_jmp_obj(ctx->CPU, ARMCPU, RaiseAbortException);
		place_fix_label(done);
	}
	place_call_argument(0, reg32, regEAX);
	place_call_imm(ctx->CPU->PA_to_Host, 1);
	return data;
}

#define place_VA_to_Host(data, size, access, ctx) __place_VA_to_Host(data, ctx->MMU->VA_to_PA[size][access], ctx)

#define place_noSPSR_UNPREDICTABLE() \
	place_cmp_ptr8_imm__imm(&ctx->CPU->context.has_SPSR, 0); \
	place_condjump(NZ, ___current_mode_has_spsr); \
	data = place_UNPREDICTABLE(data); \
	place_fix_label(___current_mode_has_spsr)

bool decode_arm_instruction(DecoderContext* ctx, const ARM_INSTRUCTION instr)
{
#define do_return(___) { \
	if (instr_condition) { \
		place_fix_label_var(end_instruction, instr_condition_end); \
	} \
	if (instr_condition2) { \
		place_fix_label_var(end_instruction2, instr_condition_end2); \
	} \
	if (___) { \
		goto return_true; \
	} else { \
		goto return_false; \
	} \
}

	register void* data = ctx->output;

	DEBUG_INSTRUCTION(ctx->PC, ctx->PC_PA, instr.Word);
#ifdef DEBUG_VALID_PC
	place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC);
#endif

	PLACE_SINGLE_STEP();

#ifdef DEBUG_INSTRUCTIONS
	InitDebugAddresses(ctx->CPU);
	for (int i = 0; i < addressItemsCount; i++) {
		if ((addressItems[i].address & 0x01FFFFFF) == (ctx->PC & 0x01FFFFFF)) {
			place_mov_reg32__ptr32_imm(regEAX, &ctx->MMU->ControlRegister);
			place_test_reg32__imm(regEAX, 1);
			place_mov_reg32__imm(regEAX, ctx->PC);
			place_condjump(Z, no_MMU);
			place_test_reg32__imm(regEAX, 0xFE000000);
			place_condjump(NZ, ok);
			place_mov_reg32__ptr32_imm(regECX, &ctx->MMU->ProcessID);
			place_or_reg32__reg32(regEAX, regECX);
			place_fix_label(ok);
			place_fix_label(no_MMU);
			place_call_argument(1, reg32, regEAX);
			//FIXME: imm not compatible with pointer
			place_call_argument(0, imm, ctx->CPU);
			place_call_imm(ExecuteDebugAddress, 1);
		}
	}
#endif

	bool is_immediate = false;
	bool instr_condition = true;
	bool instr_condition2 = false;
	void* instr_condition_end = __null;
	void* instr_condition_end2 = __null;
	uint32_t imm_value;

	// add condition check if required
	switch (instr.cond) {
		case 0b0000:
			// EQ - Equal
			// Z set
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			break;
		case 0b0001:
			// NE - Not equal
			// Z clear
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			break;
		case 0b0010:
			// CS/HS - Carry set/unsigned higher or same
			// C set
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.C, 0);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			break;
		case 0b0011:
			// CC/LO - Carry clear/unsigned lower
			// C clear
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.C, 0);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			break;
		case 0b0100:
			// MI - Minus/negative
			// N set
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.N, 0);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			break;
		case 0b0101:
			// PL - Plus/positive or zero
			// N clear
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.N, 0);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			break;
		case 0b0110:
			// VS - Overflow
			// V set
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.V, 0);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			break;
		case 0b0111:
			// VC - No overflow
			// V clear
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.V, 0);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			break;
		case 0b1000:
			// HI - Unsigned higher
			// C set and Z clear
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.C, 0);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			instr_condition2 = true;
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
			place_condjump_var(NZ, end_instruction2, instr_condition_end2);
			break;
		case 0b1001:
			// LS - Unsigned lower or same
			// C clear or Z set
			{
				place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.C, 0);
				place_condjump(Z, condition_passed);
				place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
				place_condjump_var(Z, end_instruction, instr_condition_end);
				place_fix_label(condition_passed);
			}
			break;
		case 0b1010:
			// GE - Signed greater than or equal
			// N set and V set, or
			// N clear and V clear (N == V)
			place_mov_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.N);
			place_cmp_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.V);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			break;
		case 0b1011:
			// LT - Signed less than
			// N set and V clear, or
			// N clear and V set (N != V)
			place_mov_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.N);
			place_cmp_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.V);
			place_condjump_var(Z, end_instruction, instr_condition_end);
			break;
		case 0b1100:
			// GT - Signed greater than
			// Z clear, and either N set and V set, or
			// N clear and V clear (Z == 0,N == V)
			place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
			place_condjump_var(NZ, end_instruction, instr_condition_end);
			instr_condition2 = true;
			place_mov_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.N);
			place_cmp_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.V);
			place_condjump_var(NZ, end_instruction2, instr_condition_end2);
			break;
		case 0b1101:
			// LE - Signed less than or equal
			// Z set, or N set and V clear, or
			// N clear and V set (Z == 1 or N != V)
			{
				place_cmp_ptr8_imm__imm(&ctx->CPU->context.flags.Z, 0);
				place_condjump(NZ, condition_passed);
				place_mov_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.N);
				place_cmp_reg8__ptr8_imm(regAL, &ctx->CPU->context.flags.V);
				place_condjump_var(Z, end_instruction, instr_condition_end);
				place_fix_label(condition_passed);
			}
			break;
		case 0b1110:
			// AL - Always (unconditional)
			instr_condition = false;
			break;
		case 0b1111:
			// unconditional instructions
			instr_condition = false;
			if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_v5_UP)) {
				switch (instr.UnconditionalInstruction.opcode1) {
					case 0b01010101: // 0b01x1x101
					case 0b01011101:
					case 0b01110101:
					case 0b01111101:
						// PLD - Preload data
						// do nothing
						do_return(true);
						break;
					default:
						FIXME();
						goto undef;
				}
			} else {
				goto unpredictable;
			}
			break;
	}

	// instruction type detect
	switch (instr.type) {
		case 0b000:
		case 0b001:
			{
				enum INSTR_00x_TYPE {
					instrArithmetic, instrMultiply, instrControlDSP, instrLoadStore
				};
				INSTR_00x_TYPE iType;

				if ((instr.Word & 0x0F0000F0) == 0x00000090) {
					// Multiply instruction extension space
					iType = instrMultiply;
				} else if (((instr.Word & 0x0D900000) == 0x01000000) && ((instr.Word & 0x02000090) != 0x00000090)) {
					iType = instrControlDSP;
				} else if (((instr.Word & 0x0E000090) == 0x00000090) && ((instr.Word & 0x01000060) != 0x00000000)) {
					iType = instrLoadStore;
				} else {
					iType = instrArithmetic;
				}

				switch (iType) {
					case instrArithmetic:
						{
							bool need_shifter_carryout = instr.DataProcessing.S;
							bool shifter_carryout_valid = false;
							uint8_t imm_shifter_carryout;

							if (need_shifter_carryout) {
								switch(instr.DataProcessing.opcode) {
									case 0b0100: // ADD
									case 0b1011: // CMN
									case 0b0010: // SUB
									case 0b1010: // CMP
									case 0b0011: // RSB
									case 0b0101: // ADC
									case 0b0110: // SBC
									case 0b0111: // RSC
										// skip calculating carryout_value, as it is not required by status flags
										need_shifter_carryout = false;
										break;
								}
							}

							// data processing instructions
							void* shifter_start = data;
							// calculate parameter to regEDX (shifter)
							if (instr.DataProcessing.I) {
								// 32-bit immediate
								is_immediate = true;
								uint32_t val = instr.DataProcessing.immediate;
								int rot = instr.DataProcessing.rotate * 2;
								if (rot) {
									val = (val >> rot) | (val << (32 - rot));
									if (need_shifter_carryout) {
										shifter_carryout_valid = true;
										imm_shifter_carryout = val >> 31;
									}
								}
								if ((instr.DataProcessing.opcode & 0b1110) == 0b1110) {
									// MVN, negate value
									// BIC, negate value
									val = ~val;
								}
								imm_value = val;
							} else {
								if (n_Rm == PC) {
									place_mov_reg32__imm(regEDX, ctx->PC + 8);
								} else {
									place_mov_reg32__ptr32_imm(regEDX, p_Rm);
								}
								if (instr.DataProcessing.R) {
									// register shift
									if ((n_Rd == PC) | (n_Rm == PC) | (n_Rs == PC) | (n_Rn == PC)) {
										goto unpredictable;
									}
									switch (instr.DataProcessing.shift) {
										case 0b00:
											{
												/* LSL
												 * Logical Shift Left
												 *
												 * if Rs[7:0] == 0 then
												 *     shifter_operand = Rm
												 *     shifter_carry_out = C Flag
												 * else if Rs[7:0] < 32 then
												 *     shifter_operand = Rm Logical_Shift_Left Rs[7:0]
												 *     shifter_carry_out = Rm[32 - Rs[7:0]]
												 * else if Rs[7:0] == 32 then
												 *     shifter_operand = 0
												 *     shifter_carry_out = Rm[0]
												 * else // Rs[7:0] > 32
												 *     shifter_operand = 0
												 *     shifter_carry_out = 0
												 */
												place_mov_reg32__ptr32_imm(regECX, p_Rs);
												place_cmp_reg8__imm(regCL, 32);
												place_condjump(B, shift_normal);
												place_condjump(Z, shift_32);
												// shift > 32
												place_mov_reg32__imm(regEDX, 0);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_mov_reg8__imm(regBL, 0);
												}
												place_jump(shift_over_end);
												place_fix_label(shift_32);
												// shift == 32
												if (need_shifter_carryout) {
													place_test_reg32__imm(regEDX, 1);
													place_set_flag__reg8(NZ, regBL);
												}
												place_mov_reg32__imm(regEDX, 0);
												place_jump(shift_32_end);
												place_fix_label(shift_normal);
												// 0 <= shift <= 31
												if (need_shifter_carryout) {
													place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
													place_sahf();
												}
												place_shl_reg32__CL(regEDX);
												if (need_shifter_carryout) {
													place_set_flag__reg8(C, regBL);
												}
												place_fix_label(shift_32_end);
												place_fix_label(shift_over_end);
											}
											break;
										case 0b01:
											{
												/* LSR
												 * Logical Shift Right
												 *
												 * if Rs[7:0] == 0 then
												 *     shifter_operand = Rm
												 *     shifter_carry_out = C Flag
												 * else if Rs[7:0] < 32 then
												 *     shifter_operand = Rm Logical_Shift_Right Rs[7:0]
												 *     shifter_carry_out = Rm[Rs[7:0] - 1]
												 * else if Rs[7:0] == 32 then
												 *     shifter_operand = 0
												 *     shifter_carry_out = Rm[31]
												 * else // Rs[7:0] > 32
												 *     shifter_operand = 0
												 *     shifter_carry_out = 0
												 */
												place_mov_reg32__ptr32_imm(regECX, p_Rs);
												place_cmp_reg8__imm(regCL, 32);
												place_condjump(B, shift_normal);
												place_condjump(Z, shift_32);
												// shift > 32
												place_mov_reg32__imm(regEDX, 0);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_mov_reg8__imm(regBL, 0);
												}
												place_jump(shift_over_end);
												place_fix_label(shift_32);
												// shift == 32
												if (need_shifter_carryout) {
													place_test_reg32__reg32(regEDX, regEDX);
													place_set_flag__reg8(S, regBL);
												}
												place_mov_reg32__imm(regEDX, 0);
												place_jump(shift_32_end);
												place_fix_label(shift_normal);
												// 0 <= shift <= 31
												if (need_shifter_carryout) {
													place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
													place_sahf();
												}
												place_shr_reg32__CL(regEDX);
												if (need_shifter_carryout) {
													place_set_flag__reg8(C, regBL);
												}
												place_fix_label(shift_32_end);
												place_fix_label(shift_over_end);
											}
											break;
										case 0b10:
											{
												/* ASR
												 * Arithmetic Shift Right
												 *
												 * if Rs[7:0] == 0 then
												 *     shifter_operand = Rm
												 *     shifter_carry_out = C Flag
												 * else if Rs[7:0] < 32 then
												 *     shifter_operand = Rm Arithmetic_Shift_Right Rs[7:0]
												 *     shifter_carry_out = Rm[Rs[7:0] - 1]
												 * else // Rs[7:0] >= 32
												 *     if Rm[31] == 0 then
												 *         shifter_operand = 0
												 *         shifter_carry_out = Rm[31]
												 *     else // Rm[31] == 1
												 *         shifter_operand = 0xFFFFFFFF
												 *         shifter_carry_out = Rm[31]
												 */
												place_mov_reg32__ptr32_imm(regECX, p_Rs);
												place_cmp_reg8__imm(regCL, 32);
												place_condjump(B, shift_normal);
												// shift >= 32
												place_sar_reg32__imm(regEDX, 31);
												if (need_shifter_carryout) {
													place_set_flag__reg8(S, regBL);
												}
												place_jump(shift_over_end);
												place_fix_label(shift_normal);
												// 0 <= shift <= 31
												if (need_shifter_carryout) {
													place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
													place_sahf();
												}
												place_sar_reg32__CL(regEDX);
												if (need_shifter_carryout) {
													place_set_flag__reg8(C, regBL);
												}
												place_fix_label(shift_over_end);
											}
											break;
										case 0b11:
											/* ROR
											 * Rotate Right
											 *
											 * if Rs[7:0] == 0 then
											 *     shifter_operand = Rm
											 *     shifter_carry_out = C Flag
											 * else if Rs[4:0] == 0 then
											 *     shifter_operand = Rm
											 *     shifter_carry_out = Rm[31]
											 * else // Rs[4:0] > 0
											 *     shifter_operand = Rm Rotate_Right Rs[4:0]
											 *     shifter_carry_out = Rm[Rs[4:0] - 1]
											 */
											place_mov_reg32__ptr32_imm(regECX, p_Rs);
											place_cmp_reg8__imm(regCL, 0);
											place_condjump(NZ, Rs70_not0);
											if (need_shifter_carryout) {
												shifter_carryout_valid = true;
												place_mov_reg8__ptr8_imm(regBL, &ctx->CPU->context.flags.C);
											}
											place_jump(Rs70_end);
											place_fix_label(Rs70_not0);
											place_test_reg8__imm(regCL, 0b11111);
											place_condjump(NZ, Rs40_not0);
											if (need_shifter_carryout) {
												place_test_reg32__reg32(regEDX, regEDX);
												place_set_flag__reg8(S, regBL);
											}
											place_jump(Rs40_end);
											place_fix_label(Rs40_not0);
											place_ror_reg32__CL(regEDX);
											if (need_shifter_carryout) {
												place_set_flag__reg8(C, regBL);
											}
											place_fix_label(Rs70_end);
											place_fix_label(Rs40_end);
											break;
									}
								} else {
									// immediate shift
									unsigned int shift_imm = instr.DataProcessing.shift_amount;
									switch (instr.DataProcessing.shift) {
										case 0b00:
											/* LSL
											 * Logical Shift Left
											 *
											 * if shift_imm == 0 then
											 *     shifter_operand = Rm
											 *     shifter_carry_out = C Flag
											 * else
											 *     shifter_operand = Rm Logical_Shift_Left shift_imm
											 *     shifter_carry_out = Rm[32 - shift_imm]
											 */
											if (shift_imm) {
												place_shl_reg32__imm(regEDX, shift_imm);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(C, regBL);
												}
											} else {
												// no shift
												// C Flag does not change
											}
											break;
										case 0b01:
											/* LSR
											 * Logical Shift Right
											 *
											 * if shift_imm == 0 then
											 *     shifter_operand = 0
											 *     shifter_carry_out = Rm[31]
											 * else
											 *     shifter_operand = Rm Logical_Shift_Right shift_imm
											 *     shifter_carry_out = Rm[shift_imm - 1]
											 */
											if (shift_imm == 0) {
												// shift by 32
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_test_reg32__reg32(regEDX, regEDX);
													place_set_flag__reg8(S, regBL);
												}
												data = shifter_start;
												place_mov_reg32__imm(regEDX, 0);
											} else {
												place_shr_reg32__imm(regEDX, shift_imm);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(C, regBL);
												}
											}
											break;
										case 0b10:
											/* ASR
											 * Arithmetic Shift Right
											 *
											 * if shift_imm == 0 then
											 *     if Rm[31] == 0 then
											 *         shifter_operand = 0
											 *         shifter_carry_out = Rm[31]
											 *     else // Rm[31] == 1
											 *         shifter_operand = 0xFFFFFFFF
											 *         shifter_carry_out = Rm[31]
											 * else // shift_imm > 0
											 *     shifter_operand = Rm Arithmetic_Shift_Right <shift_imm>
											 *     shifter_carry_out = Rm[shift_imm - 1]
											 */
											if (shift_imm == 0) {
												place_sar_reg32__imm(regEDX, 31);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(S, regBL);
												}
											} else {
												place_sar_reg32__imm(regEDX, shift_imm);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(C, regBL);
												}
											}
											break;
										case 0b11:
											/* ROR, RRX
											 * Rotate Right, Rotate right with extend
											 *
											 * if shift_imm == 0 then
											 *     shifter_operand = (C Flag Logical_Shift_Left 31) OR (Rm Logical_Shift_Right 1)
											 *     shifter_carry_out = Rm[0]
											 * else // shift_imm > 0
											 *     shifter_operand = Rm Rotate_Right shift_imm
											 *     shifter_carry_out = Rm[shift_imm - 1]
											 */
											if (shift_imm == 0) {
												// RRX
												place_movzx_reg32__ptr8_imm(regEAX, &ctx->CPU->context.flags.C);
												place_shl_reg32__imm(regEAX, 31);
												place_shr_reg32__imm(regEDX, 1);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(C, regBL);
												}
												place_or_reg32__reg32(regEDX, regEAX);
											} else {
												// ROR
												place_ror_reg32__imm(regEDX, shift_imm);
												if (need_shifter_carryout) {
													shifter_carryout_valid = true;
													place_set_flag__reg8(C, regBL);
												}
											}
											break;
									}
								}
								if ((instr.DataProcessing.opcode & 0b1110) == 0b1110) {
									// MVN, negate value
									// BIC, negate value
									place_not_reg32(regEDX);
								}
							}
							void* load_Rn_value = data;
							bool n_Rd_is_PC = n_Rd == PC;
							if ((instr.DataProcessing.opcode & 0b1101) != 0b1101) {
								// load <Rn> (source operand) to regEAX
								// do not load <Rn> for MOV and MVN operation
								if (n_Rn == PC) {
									place_mov_reg32__imm(regEAX, ctx->PC + 8);
								} else {
									place_mov_reg32__ptr32_imm(regEAX, p_Rn);
								}
							}

							// do data operation
							// source is regEAX
							// shift_value is regEDX or in imm_value if is_immediate is set
							// shifter_carryout is in regBL or imm_shifter_carryout (if immediate value)
							switch(instr.DataProcessing.opcode) {
								case 0b0000:
									/* AND - Logical AND
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn AND shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (!n_Rd_is_PC && (n_Rn == n_Rd)) {
										data = load_Rn_value;
										if (is_immediate) {
											place_and_ptr32_imm__imm(p_Rd, imm_value);
										} else {
											place_and_ptr32_imm__reg32(p_Rd, regEDX);
										}
									} else {
										if (is_immediate) {
											place_and_reg32__imm(regEAX, imm_value);
										} else {
											place_and_reg32__reg32(regEAX, regEDX);
										}
										place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									}
									break;
								case 0b0001:
									/* EOR - Logical Exclusive OR
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn EOR shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (!n_Rd_is_PC && (n_Rn == n_Rd)) {
										data = load_Rn_value;
										if (is_immediate) {
											place_xor_ptr32_imm__imm(p_Rd, imm_value);
										} else {
											place_xor_ptr32_imm__reg32(p_Rd, regEDX);
										}
									} else {
										if (is_immediate) {
											place_xor_reg32__imm(regEAX, imm_value);
										} else {
											place_xor_reg32__reg32(regEAX, regEDX);
										}
										place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									}
									break;
								case 0b0010:
									/* SUB - Subtract
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn - shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(Rn - shifter_operand)
									 *     V Flag = OverflowFrom(Rn - shifter_operand)
									 */
									if (!n_Rd_is_PC && (n_Rn == n_Rd)) {
										data = load_Rn_value;
										if (is_immediate) {
											place_sub_ptr32_imm__imm(p_Rd, imm_value);
										} else {
											place_sub_ptr32_imm__reg32(p_Rd, regEDX);
										}
									} else {
										if (is_immediate) {
											place_sub_reg32__imm(regEAX, imm_value);
										} else {
											place_sub_reg32__reg32(regEAX, regEDX);
										}
										place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									}
									break;
								case 0b0011:
									/* RSB - Reverse Subtract
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = shifter_operand - Rn
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(shifter_operand - Rn)
									 *     V Flag = OverflowFrom(shifter_operand - Rn)
									 */
									if (is_immediate) {
										place_mov_reg32__imm(regEDX, imm_value);
									}
									place_sub_reg32__reg32(regEDX, regEAX);
									place_mov_ptr32_imm__reg32(p_Rd, regEDX);
									break;
								case 0b0100:
									/* ADD - Add
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn + shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = CarryFrom(Rn + shifter_operand)
									 *     V Flag = OverflowFrom(Rn + shifter_operand)
									 */
									if (!n_Rd_is_PC && (n_Rn == n_Rd)) {
										data = load_Rn_value;
										if (is_immediate) {
											place_add_ptr32_imm__imm(p_Rd, imm_value);
										} else {
											place_add_ptr32_imm__reg32(p_Rd, regEDX);
										}
									} else {
										if (is_immediate) {
											place_add_reg32__imm(regEAX, imm_value);
										} else {
											place_add_reg32__reg32(regEAX, regEDX);
										}
										place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									}
									break;
								case 0b0101:
									/* ADC - Add with Carry
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn + shifter_operand + C Flag
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = CarryFrom(Rn + shifter_operand + C Flag)
									 *     V Flag = OverflowFrom(Rn + shifter_operand + C Flag)
									 */
									place_mov_reg32__reg32(regECX, regEAX);
									place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
									place_sahf();
									if (is_immediate) {
										place_adc_reg32__imm(regECX, imm_value);
									} else {
										place_adc_reg32__reg32(regECX, regEDX);
									}
									place_mov_ptr32_imm__reg32(p_Rd, regECX);
									break;
								case 0b0110:
									/* SBC - Subtract with Carry
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn - shifter_operand - NOT(Carry Flag)
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(Rn - shifter_operand - NOT(Carry Flag))
									 *     V Flag = OverflowFrom(Rn - shifter_operand - NOT(Carry Flag))
									 */
									place_mov_reg32__reg32(regECX, regEAX);
									place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
									place_sahf();
									place_cmc();
									if (is_immediate) {
										place_sbb_reg32__imm(regECX, imm_value);
									} else {
										place_sbb_reg32__reg32(regECX, regEDX);
									}
									place_mov_ptr32_imm__reg32(p_Rd, regECX);
									break;
								case 0b0111:
									/* RSC - Reverse Subtract with Carry
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = shifter_operand - Rn - NOT(C Flag)
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(shifter_operand - Rn -NOT(C Flag))
									 *     V Flag = OverflowFrom(shifter_operand - Rn -NOT(C Flag))
									 */
									place_mov_reg32__reg32(regECX, regEAX);
									place_mov_reg8__ptr8_imm(regAH, &ctx->CPU->context.flags.C);
									place_sahf();
									place_cmc();
									if (is_immediate) {
										place_mov_reg32__imm(regEDX, imm_value);
									}
									place_sbb_reg32__reg32(regEDX, regECX);
									place_mov_ptr32_imm__reg32(p_Rd, regEDX);
									break;
								case 0b1000:
									/* TST - Test
									 *
									 * if ConditionPassed(cond) then
									 *     alu_out = Rn AND shifter_operand
									 *     N Flag = alu_out[31]
									 *     Z Flag = if alu_out == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (is_immediate) {
										place_and_reg32__imm(regEAX, imm_value);
									} else {
										place_and_reg32__reg32(regEAX, regEDX);
									}
									break;
								case 0b1001:
									/* TEQ - Test Equivalence
									 *
									 * if ConditionPassed(cond) then
									 *     alu_out = Rn EOR shifter_operand
									 *     N Flag = alu_out[31]
									 *     Z Flag = if alu_out == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (is_immediate) {
										place_xor_reg32__imm(regEAX, imm_value);
									} else {
										place_xor_reg32__reg32(regEAX, regEDX);
									}
									break;
								case 0b1010:
									/* CMP - Compare
									 *
									 * if ConditionPassed(cond) then
									 *     alu_out = Rn - shifter_operand
									 *     N Flag = alu_out[31]
									 *     Z Flag = if alu_out == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(Rn - shifter_operand)
									 *     V Flag = OverflowFrom(Rn - shifter_operand)
									 */
									if (is_immediate) {
										place_sub_reg32__imm(regEAX, imm_value);
									} else {
										place_sub_reg32__reg32(regEAX, regEDX);
									}
									break;
								case 0b1011:
									/* CMN - Compare Negated
									 *
									 * if ConditionPassed(cond) then
									 *     alu_out = Rn + shifter_operand
									 *     N Flag = alu_out[31]
									 *     Z Flag = if alu_out == 0 then 1 else 0
									 *     C Flag = NOT BorrowFrom(Rn + shifter_operand)
									 *     V Flag = OverflowFrom(Rn + shifter_operand)
									 */
									if (is_immediate) {
										place_add_reg32__imm(regEAX, imm_value);
									} else {
										place_add_reg32__reg32(regEAX, regEDX);
									}
									break;
								case 0b1100:
									/* ORR - Logical (inclusive) OR
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn OR shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (is_immediate) {
										place_or_reg32__imm(regEAX, imm_value);
									} else {
										place_or_reg32__reg32(regEAX, regEDX);
									}
									place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									break;
								case 0b1101:
									/* MOV - Move
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									if (instr.DataProcessing.S) {
										if (is_immediate) {
											place_mov_reg32__imm(regEDX, imm_value);
										}
										place_test_reg32__reg32(regEDX, regEDX);
									} else {
										if (is_immediate) {
											place_mov_ptr32_imm__imm(p_Rd, imm_value);
											break;
										}
									}
									place_mov_ptr32_imm__reg32(p_Rd, regEDX);
									break;
								case 0b1110:
									/* BIC - Bit Clear
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = Rn AND NOT shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									// value was already NOT-ed in shifter
									if (is_immediate) {
										place_and_reg32__imm(regEAX, imm_value);
									} else {
										place_and_reg32__reg32(regEAX, regEDX);
									}
									place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									break;
								case 0b1111:
									/* MVN - Move Not
									 *
									 * if ConditionPassed(cond) then
									 *     Rd = NOT shifter_operand
									 * if S == 1 and Rd == R15 then
									 *     if CurrentModeHasSPSR() then
									 *         CPSR = SPSR
									 *     else UNPREDICTABLE
									 * else if S == 1 then
									 *     N Flag = Rd[31]
									 *     Z Flag = if Rd == 0 then 1 else 0
									 *     C Flag = shifter_carry_out
									 *     V Flag = unaffected
									 */
									// value is already NOT-ed in shifter
									if (instr.DataProcessing.S) {
										if (is_immediate) {
											place_mov_reg32__imm(regEDX, imm_value);
										}
										place_test_reg32__reg32(regEDX, regEDX);
									} else {
										if (is_immediate) {
											place_mov_ptr32_imm__imm(p_Rd, imm_value);
											break;
										}
									}
									place_mov_ptr32_imm__reg32(p_Rd, regEDX);
									break;
							}

							// update CPSR
							if (instr.DataProcessing.S) {
								if (n_Rd_is_PC && ((instr.DataProcessing.opcode & 0b1100) != 0b1000)) {
									// skip for TST, TEQ, CMP, CMN
									// copy SPSR to CPSR
									place_noSPSR_UNPREDICTABLE();
									place_call_obj_argument(1, imm, ~ctx->CPU->ARCHInfo->PSRBits.UnallocMask);
									place_call_obj_argument(0, ptr32_imm, &ctx->CPU->context.SPSR);
									place_call_obj(ctx->CPU, ARMCPU, WriteCPSR, 2);
								} else {
									// update CPSR
									// these flags are the same for all opcodes
									// N Flag = alu_out[31]
									// Z Flag = if alu_out == 0 then 1 else 0
									place_set_flag__ptr8_imm(S, &ctx->CPU->context.flags.N);
									place_set_flag__ptr8_imm(Z, &ctx->CPU->context.flags.Z);
									switch (instr.DataProcessing.opcode) {
										case 0b0000: // AND
										case 0b0001: // EOR
										case 0b1000: // TST
										case 0b1001: // TEQ
										case 0b1100: // ORR
										case 0b1101: // MOV
										case 0b1110: // BIC
										case 0b1111: // MVN
											// N Flag = alu_out[31]
											// Z Flag = if alu_out == 0 then 1 else 0
											// C Flag = shifter_carry_out
											// V Flag = unaffected
											if (is_immediate) {
												if (shifter_carryout_valid) {
													place_mov_ptr8_imm__imm(&ctx->CPU->context.flags.C, imm_shifter_carryout);
												} else {
													// C flag is preserved
												}
											} else {
												if (shifter_carryout_valid) {
													place_mov_ptr8_imm__reg8(&ctx->CPU->context.flags.C, regBL);
												} else {
													// C flag is preserved
												}
											}
											break;
										case 0b0100: // ADD
										case 0b1011: // CMN
											// N Flag = alu_out[31]
											// Z Flag = if alu_out == 0 then 1 else 0
											// C Flag = CarryFrom(Rn + shifter_operand)
											// V Flag = OverflowFrom(Rn + shifter_operand)
											place_set_flag__ptr8_imm(C, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
										case 0b0010: // SUB
										case 0b1010: // CMP
											// N Flag = alu_out[31]
											// Z Flag = if alu_out == 0 then 1 else 0
											// C Flag = NOT BorrowFrom(Rn - shifter_operand)
											// V Flag = OverflowFrom(Rn - shifter_operand)
											place_set_flag__ptr8_imm(NC, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
										case 0b0011: // RSB
											// N Flag = Rd[31]
											// Z Flag = if Rd == 0 then 1 else 0
											// C Flag = NOT BorrowFrom(shifter_operand - Rn)
											// V Flag = OverflowFrom(shifter_operand - Rn)
											place_set_flag__ptr8_imm(NC, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
										case 0b0101: // ADC
											// N Flag = Rd[31]
											// Z Flag = if Rd == 0 then 1 else 0
											// C Flag = CarryFrom(Rn + shifter_operand + C Flag)
											// V Flag = OverflowFrom(Rn + shifter_operand + C Flag)
											place_set_flag__ptr8_imm(C, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
										case 0b0110: // SBC
											// N Flag = Rd[31]
											// Z Flag = if Rd == 0 then 1 else 0
											// C Flag = NOT BorrowFrom(Rn - shifter_operand - NOT(C Flag))
											// V Flag = OverflowFrom(Rn - shifter_operand - NOT(C Flag))
											place_set_flag__ptr8_imm(NC, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
										case 0b0111: // RSC
											// N Flag = Rd[31]
											// Z Flag = if Rd == 0 then 1 else 0
											// C Flag = NOT BorrowFrom(shifter_operand - Rn - NOT(C Flag))
											// V Flag = OverflowFrom(shifter_operand - Rn - NOT(C Flag))
											place_set_flag__ptr8_imm(NC, &ctx->CPU->context.flags.C);
											place_set_flag__ptr8_imm(O, &ctx->CPU->context.flags.V);
											break;
									}
								}
							}
							if (n_Rd_is_PC) {
								place_jmp_imm(ctx->CPU->PCModifiedHelper);
								do_return(instr.cond != 0b1110);
							}
							do_return(true);
							break;
						}
					case instrMultiply:
						switch (instr.Multiply.op1) {
							case 0b0000:
							case 0b0001:
								/* MUL - Multiply
								 *
								 * if ConditionPassed(cond) then
								 *     Rd = (Rm * Rs)[31:0]
								 *     if S == 1 then
								 *         N Flag = Rd[31]
								 *         Z Flag = if Rd == 0 then 1 else 0
								 *         C Flag = unaffected in v5 and above, UNPREDICTABLE in v4 and earlier
								 *         V Flag = unaffected
								 */
								if ((n_Rd == PC) | (n_Rm == PC) | (n_Rs == PC)) {
									goto unpredictable;
								}
								place_mov_reg32__ptr32_imm(regEAX, p_GPR(instr.Multiply.Rm));
								place_mul_ptr32_imm(p_GPR(instr.Multiply.Rs));
								if (instr.Multiply.S) {
									place_set_flag__ptr8_imm(S, &ctx->CPU->context.flags.N);
									place_set_flag__ptr8_imm(Z, &ctx->CPU->context.flags.Z);
									// we do C Flag unaffected in all architectures
								}
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.Rd), regEAX);
								do_return(true);
								break;
							case 0b0010:
							case 0b0011:
								/* MLA - Multiply Accumulate
								 *
								 * if ConditionPassed(cond) then
								 *     Rd = (Rm * Rs + Rn)[31:0]
								 *     if S == 1 then
								 *         N Flag = Rd[31]
								 *         Z Flag = if Rd == 0 then 1 else 0
								 *         C Flag = unaffected in v5 and above, UNPREDICTABLE in v4 and earlier
								 *         V Flag = unaffected
								 */
								if ((n_Rd == PC) | (n_Rm == PC) | (n_Rs == PC) | (n_Rn == PC)) {
									goto unpredictable;
								}
								place_mov_reg32__ptr32_imm(regEAX, p_GPR(instr.Multiply.Rm));
								place_mul_ptr32_imm(p_GPR(instr.Multiply.Rs));
								place_add_reg32__ptr32_imm(regEAX, p_GPR(instr.Multiply.Rn));
								if (instr.Multiply.S) {
									place_set_flag__ptr8_imm(S, &ctx->CPU->context.flags.N);
									place_set_flag__ptr8_imm(Z, &ctx->CPU->context.flags.Z);
									// we do C Flag unaffected in all architectures
								}
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.Rd), regEAX);
								do_return(true);
								break;
							case 0b1000:
							case 0b1001:
								/* UMULL - Unsigned Multiply Long
								 *
								 * if ConditionPassed(cond) then
								 *     RdHi = (Rm * Rs)[63:32] // Unsigned multiplication
								 *     RdLo = (Rm * Rs)[31:0]
								 *     if S == 1 then
								 *         N Flag = RdHi[31]
								 *         Z Flag = if (RdHi == 0) and (RdLo == 0) then 1 else 0
								 *         C Flag = unaffected // See "C and V flags" note
								 *         V Flag = unaffected // See "C and V flags" note
								 */
								if ((n_Rd == PC) | (n_Rm == PC) | (n_Rs == PC) | (n_Rn == PC)) {
									goto unpredictable;
								}
								place_mov_reg32__ptr32_imm(regEAX, p_GPR(instr.Multiply.Rm));
								place_mul_ptr32_imm(p_GPR(instr.Multiply.Rs));
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.RdLo), regEAX);
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.RdHi), regEDX);
								if (instr.Multiply.S) {
									place_test_reg32__reg32(regEDX, regEDX);
									place_set_flag__ptr8_imm(S, &ctx->CPU->context.flags.N);
									place_or_reg32__reg32(regEAX, regEDX);
									place_set_flag__ptr8_imm(Z, &ctx->CPU->context.flags.Z);
									// we do C Flag and V Flag unaffected in all architectures
								}
								do_return(true);
								break;
							case 0b1100:
							case 0b1101:
								/* SMULL - Signed Multiply Long
								 *
								 * if ConditionPassed(cond) then
								 *     RdHi = (Rm * Rs)[63:32] // Signed multiplication
								 *     RdLo = (Rm * Rs)[31:0]
								 *     if S == 1 then
								 *         N Flag = RdHi[31]
								 *         Z Flag = if (RdHi == 0) and (RdLo == 0) then 1 else 0
								 *         C Flag = unaffected // See "C and V flags" note
								 *         V Flag = unaffected // See "C and V flags" note
								 */
								if ((n_Rd == PC) | (n_Rm == PC) | (n_Rs == PC) | (n_Rn == PC)) {
									goto unpredictable;
								}
								place_mov_reg32__ptr32_imm(regEAX, p_GPR(instr.Multiply.Rm));
								place_imul_ptr32_imm(p_GPR(instr.Multiply.Rs));
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.RdLo), regEAX);
								place_mov_ptr32_imm__reg32(p_GPR(instr.Multiply.RdHi), regEDX);
								if (instr.Multiply.S) {
									place_test_reg32__reg32(regEDX, regEDX);
									place_set_flag__ptr8_imm(S, &ctx->CPU->context.flags.N);
									place_or_reg32__reg32(regEAX, regEDX);
									place_set_flag__ptr8_imm(Z, &ctx->CPU->context.flags.Z);
									// we do C Flag and V Flag unaffected in all architectures
								}
								do_return(true);
								break;
							default:
								FIXME();
								break;
						}
						FIXME();
						break;
					case instrControlDSP:
						if (instr.ControlDSPExtension.I || (instr.ControlDSPExtension.op2 == 0b0000)) {
							switch (instr.ControlDSPExtension.op1) {
								case 0b00:
								case 0b10:
									/* MRS - Move PSR to general-purpose register
									 *
									 * if ConditionPassed(cond) then
									 *     if R == 1 then
									 *         Rd = SPSR
									 *     else
									 *         Rd = CPSR
									 */
									if (n_Rd == PC) {
										goto unpredictable;
									} else {
										if (instr.MovePSRtoRegister.R) {
											place_noSPSR_UNPREDICTABLE();
											place_mov_reg32__ptr32_imm(regEAX, &ctx->CPU->context.SPSR);
										} else {
											place_call_obj(ctx->CPU, ARMCPU, ReadCPSR, 0);
										}
										place_mov_ptr32_imm__reg32(p_Rd, regEAX);
									}
									do_return(true);
									break;
								case 0b01:
								case 0b11:
									/* MSR - Move to Status Register from ARM Register
									 *
									 * if ConditionPassed(cond) then
									 *     if opcode[25] == 1 then
									 *         operand = 8_bit_immediate Rotate_Right (rotate_imm * 2)
									 *     else
									 *         operand = Rm
									 *     if (operand AND UnallocMask) !=0 then
									 *         UNPREDICTABLE // Attempt to set reserved bits
									 *     byte_mask = (if field_mask[0] == 1 then 0x000000FF else 0x00000000) OR
									 *                 (if field_mask[1] == 1 then 0x0000FF00 else 0x00000000) OR
									 *                 (if field_mask[2] == 1 then 0x00FF0000 else 0x00000000) OR
									 *                 (if field_mask[3] == 1 then 0xFF000000 else 0x00000000)
									 *     if R == 0 then
									 *         if InAPrivilegedMode() then
									 *             if (operand AND StateMask) != 0 then
									 *                 UNPREDICTABLE // Attempt to set non-ARM execution state
									 *             else
									 *                 mask = byte_mask AND (UserMask OR PrivMask)
									 *         else
									 *             mask = byte_mask AND UserMask
									 *         CPSR = (CPSR AND NOT mask) OR (operand AND mask)
									 *     else // R == 1
									 *         if CurrentModeHasSPSR() then
									 *             mask = byte_mask AND (UserMask OR PrivMask OR StateMask)
									 *             SPSR = (SPSR AND NOT mask) OR (operand AND mask)
									 *         else
									 *             UNPREDICTABLE
									 */
									if (instr.MoveRegisterToPSR.I) {
										// immediate
										imm_value = instr.MoveRegisterToPSR.Immediate;
										int rot = instr.MoveRegisterToPSR.rotate_imm * 2;
										if (rot) {
											imm_value = (imm_value >> rot) | (imm_value << (32 - rot));
										}
										// ignore write to reserved bits
										imm_value &= ~ctx->CPU->ARCHInfo->PSRBits.UnallocMask;
										// ESI - new value
										place_mov_reg32__imm(regESI, imm_value);
									} else {
										// register value
										if (instr.Word & 0x000000F0) {
											// The immediate and register forms are specified in precisely the same way as the immediate
											// and unshifted register forms of Addressing Mode 1 (see Addressing Mode 1 -
											// Data-processing operands on page A5-2). All other forms of Addressing Mode 1 yield
											// UNPREDICTABLE results.
											goto unpredictable;
										}
										// ESI - new value
										place_mov_reg32__ptr32_imm(regESI, p_Rm);
										// ignore write to reserved bits
										place_and_reg32__imm(regESI, ~ctx->CPU->ARCHInfo->PSRBits.UnallocMask);
									}
									// in regESI, value to set
									uint32_t byte_mask = 0x00000000;
									if (instr.MoveRegisterToPSR.field_c)
										byte_mask |= 0x000000FF;
									if (instr.MoveRegisterToPSR.field_x)
										byte_mask |= 0x0000FF00;
									if (instr.MoveRegisterToPSR.field_s)
										byte_mask |= 0x00FF0000;
									if (instr.MoveRegisterToPSR.field_f)
										byte_mask |= 0xFF000000;
									if (instr.MoveRegisterToPSR.R) {
										// set SPSR
										// SPSR = (SPSR & ~mask) | (Rd & mask)
										place_noSPSR_UNPREDICTABLE();
										place_mov_reg32__ptr32_imm(regEAX, &ctx->CPU->context.SPSR);
										byte_mask &= ctx->CPU->ARCHInfo->PSRBits.UserMask | ctx->CPU->ARCHInfo->PSRBits.PrivMask | ctx->CPU->ARCHInfo->PSRBits.StateMask;
										place_and_reg32__imm(regESI, byte_mask);
										place_and_reg32__imm(regEAX, ~byte_mask);
										place_or_reg32__reg32(regEAX, regESI);
										place_mov_ptr32_imm__reg32(&ctx->CPU->context.SPSR, regEAX);
									} else {
										// set CPSR
										place_cmp_ptr8_imm__imm(&ctx->CPU->context.is_priv, 0);
										place_condjump(Z, unprivileged_mode);
										// privileged mode
										place_test_reg32__imm(regESI, ctx->CPU->ARCHInfo->PSRBits.StateMask);
										place_condjump(Z, valid_CPSR_priv);
										// Attempt to set non-ARM execution state
										data = place_UNPREDICTABLE(data);
										place_fix_label(valid_CPSR_priv);
										place_call_obj_argument(1, imm, (byte_mask & (ctx->CPU->ARCHInfo->PSRBits.UserMask | ctx->CPU->ARCHInfo->PSRBits.PrivMask)));
										place_jump(set_CPSR_end);
										place_fix_label(unprivileged_mode);
										// unprivileged mode
										place_call_obj_argument(1, imm, (byte_mask & ctx->CPU->ARCHInfo->PSRBits.UserMask));
										// write CPSR
										place_fix_label(set_CPSR_end);
										place_call_obj_argument(0, reg32, regESI);
										place_call_obj(ctx->CPU, ARMCPU, WriteCPSR, 2);
									}
									do_return(true);
									break;
							}
						} else {
							switch (instr.ControlDSPExtension.op2) {
								case 0b0001:
									switch (instr.ControlDSPExtension.op1) {
										case 0b01:
											/* BX - Branch and Exchange
											 *
											 * if ConditionPassed(cond) then
											 *     CPSR T bit = Rm[0]
											 *     PC = Rm AND 0xFFFFFFFE
											 */
											if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_THUMB)) {
												if (n_Rm == PC) {
													// simple branch to PC + 8
													place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC + 8);
												} else {
													place_mov_reg32__ptr32_imm(regEAX, p_Rm);
													place_test_reg32__imm(regEAX, 1);
													place_condjump(Z, arm_code);
													place_or_ptr32_imm__imm(&ctx->CPU->context.uncached_CPSR, 1 << 5);
													place_and_reg32__imm(regEAX, 0xFFFFFFFE);
													place_fix_label(arm_code);
													place_mov_ptr32_imm__reg32(p_GPR(PC), regEAX);
												}
												place_jmp_imm(ctx->CPU->PCModifiedHelper);
												do_return(instr.cond != 0b1110);
											} else {
												goto undef;
											}
											break;
										case 0b11:
											/* CLZ - Count Leading Zeros
											 *
											 * if ConditionPassed(cond) then
											 *     if Rm == 0
											 *         Rd = 32
											 *     else
											 *         Rd = 31 - (bit position of most significant'1' in Rm)
											 */
											if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_v5_UP)) {
												if ((n_Rm == PC) || (n_Rd == PC)) {
													goto unpredictable;
												}
												place_mov_reg32__imm(regEAX, 32);
												place_bsr_reg32__ptr32_imm(regECX, p_Rm);
												place_condjump(Z, zero);
												place_sub_reg32__reg32(regEAX, regECX)
												place_dec_reg32(regEAX);
												place_fix_label(zero);
												place_mov_ptr32_imm__reg32(p_Rd, regEAX);
											} else {
												goto undef;
											}
											do_return(true);
											break;
										default:
											FIXME();
											goto undef;
											break;
									}
									break;
								case 0b0010:
									FIXME();
									break;
								case 0b0011:
									switch (instr.ControlDSPExtension.op1) {
										case 0b01:
											/* BLX - Branch with Link and Exchange
											 *
											 * if ConditionPassed(cond) then
											 *     target = Rm
											 *     LR = address of instruction after the BLX instruction
											 *     CPSR T bit = target[0]
											 *     PC = target AND 0xFFFFFFFE
											 */
											if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_THUMB)) {
												if (n_Rm == PC) {
													goto unpredictable;
												} else {
													place_mov_ptr32_imm__imm(p_GPR(LR), ctx->PC + 4);
													place_mov_reg32__ptr32_imm(regEAX, p_Rm);
													place_test_reg32__imm(regEAX, 1);
													place_condjump(Z, arm_code);
													place_or_ptr32_imm__imm(&ctx->CPU->context.uncached_CPSR, 1 << 5);
													place_and_reg32__imm(regEAX, 0xFFFFFFFE);
													place_fix_label(arm_code);
													place_mov_ptr32_imm__reg32(p_GPR(PC), regEAX);
												}
												place_jmp_imm(ctx->CPU->PCModifiedHelper);
												do_return(true);
											} else {
												goto undef;
											}
											break;
										default:
											FIXME();
											goto undef;
											break;
									}
									break;
								case 0b0101:
									FIXME();
									break;
								case 0b0111:
									FIXME();
									break;
								case 0b1000:
								case 0b1010:
								case 0b1100:
								case 0b1110:
									FIXME();
									break;
								default:
									FIXME();
									goto undef;
									break;
							}
						}
						break;
					case instrLoadStore:
						switch (instr.LoadStoreExtension.op1) {
							case 0b00:
								FIXME();
								break;
							case 0b01:
							case 0b10:
							case 0b11:
								{
									bool ret_val = true;
									// LDR/STR extension
									if (!instr.LoadStoreExtension.P || instr.LoadStoreExtension.W) {
										// pre-index with write-back, post-index
										if (n_Rn == PC) {
											goto unpredictable;
										}
									}
									if (n_Rn == PC) {
										place_mov_reg32__imm(regEAX, ctx->PC + 8);
									} else {
										place_mov_reg32__ptr32_imm(regEAX, p_Rn);
									}
									if (!instr.LoadStoreExtension.P) {
										// save address for post-index
										place_mov_reg32__reg32(regEBX, regEAX);
									}
									if (instr.LoadStoreExtension.I) {
										// immediate
										uint32_t offset_8 = (instr.LoadStoreExtension.HiOffset << 4) | instr.LoadStoreExtension.LoOffset;
										if (offset_8) {
											if (instr.LoadStoreExtension.P) {
												// pre-index
												if (instr.LoadStoreExtension.U) {
													place_add_reg32__imm(regEAX, offset_8);
												} else {
													place_sub_reg32__imm(regEAX, offset_8);
												}
												if (instr.LoadStoreExtension.W) {
													place_mov_reg32__reg32(regEBX, regEAX);
												}
											} else {
												// post-index
												if (instr.LoadStoreExtension.U) {
													place_add_reg32__imm(regEBX, offset_8);
												} else {
													place_sub_reg32__imm(regEBX, offset_8);
												}
											}
										}
									} else {
										// register
										if (n_Rm == PC) {
											goto unpredictable;
										}
										if (instr.LoadStoreExtension.P) {
											// pre-index
											if (instr.LoadStoreExtension.U) {
												place_add_reg32__ptr32_imm(regEAX, p_Rm);
											} else {
												place_sub_reg32__ptr32_imm(regEAX, p_Rm);
											}
											if (instr.LoadStoreExtension.W) {
												place_mov_reg32__reg32(regEBX, regEAX);
											}
										} else {
											// post-index
											if (instr.LoadStoreExtension.U) {
												place_add_reg32__ptr32_imm(regEBX, p_Rm);
											} else {
												place_sub_reg32__ptr32_imm(regEBX, p_Rm);
											}
										}
									}
									// calculated address is placed in EAX
									// EBX contains address to write-back (if needed)
									data = place_VA_to_Host(data, SizeWord, instr.LoadStoreExtension.L || !instr.LoadStoreExtension.H ? AccessRead: AccessWrite, ctx);
									// TODO: support for unaligned memory access
									place_condjump(C, IOAddress);
									// physical memory
									if (instr.LoadStoreExtension.L) {
										if (instr.LoadStoreExtension.H) {
											if (instr.LoadStoreExtension.S) {
												/* LDRSH - Load Register Signed Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0 then
												 *             data = Memory[address,2]
												 *         else
												 *             data = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         data = Memory[address,2]
												 *     Rd = SignExtend(data[15:0])
												 */
												if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_v4_UP)) {
													ret_val = n_Rd != PC;
													place_movsx_reg32__ptr16_reg(regEAX, regEAX);
													place_mov_ptr32_imm__reg32(p_Rd, regEAX);
												} else {
													goto undef;
												}
											} else {
												/* LDRH - Load Register Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0 then
												 *             data = Memory[address,2]
												 *         else
												 *             data = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         data = Memory[address,2]
												 *     Rd = ZeroExtend(data[15:0])
												 */
												ret_val = n_Rd != PC;
												place_movzx_reg32__ptr16_reg(regEAX, regEAX);
												place_mov_ptr32_imm__reg32(p_Rd, regEAX);
											}
										} else {
											/* LDRSB - Load Register Signed Byte
											 *
											 * MemoryAccess(B-bit, E-bit)
											 * if ConditionPassed(cond) then
											 *     data = Memory[address,1]
											 *     Rd = SignExtend(data)
											 */
											ret_val = n_Rd != PC;
											place_movsx_reg32__ptr8_reg(regEAX, regEAX);
											place_mov_ptr32_imm__reg32(p_Rd, regEAX);
										}
									} else {
										if (instr.LoadStoreExtension.H) {
											if (instr.LoadStoreExtension.S) {
												/* STRD - Store Registers Doubleword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * processor_id = ExecutingProcessor()
												 * if ConditionPassed(cond) then
												 *     if (Rd is even-numbered) and (Rd is not R14) and
												 *             (address[1:0] == 0b00) and
												 *             ((CP15_reg1_Ubit == 1) or (address[2] == 0)) then
												 *         Memory[address,4] = Rd
												 *         Memory[address+4,4] = R(d+1)
												 *     else
												 *         UNPREDICTABLE
												 *     if Shared(address) then // from ARMv6
											     *         physical_address = TLB(address)
												 *         ClearExclusiveByAddress(physical_address,processor_id,4)
												 *     if Shared(address+4)
												 *         physical_address = TLB(address+4)
												 *         ClearExclusiveByAddress(physical_address,processor_id,4)
												 */
												if (ctx->CPU->CheckArch((ARM_ARCH)(ARM_ARCH_v5TE | ARM_ARCH_v5TEJ | ARM_ARCH_v6))) {
													FIXME();
												} else {
													goto undef;
												}
											} else {
												/* STRH - Store Register Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * processor_id = ExecutingProcessor()
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0b0 then
												 *             Memory[address,2] = Rd[15:0]
												 *         else
												 *             Memory[address,2] = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         Memory[address,2] = Rd[15:0]
												 *     if Shared(address) then // ARMv6
												 *         physical_address = TLB(address)
												 *         ClearExclusiveByAddress(physical_address,processor_id,2)
												 */
												place_mov_reg32__ptr32_imm(regECX, p_Rd);
												place_mov_ptr16_reg__reg16(regEAX, regCX);
											}
										} else {
											/* LDRD - Load Registers Doubleword
											 *
											 * if ConditionPassed(cond) then
											 *     if (Rd is even-numbered) and (Rd is not R14) and
											 *             (address[1:0] == 0b00) and
											 *             ((CP15_reg1_Ubit == 1) or (address[2] == 0)) then
											 *         Rd = Memory[address,4]
											 *         R(d+1) = memory[address+4,4]
											 *     else
											 *         UNPREDICTABLE
											 */
											if (ctx->CPU->CheckArch((ARM_ARCH)(ARM_ARCH_v5TE | ARM_ARCH_v5TEJ | ARM_ARCH_v6))) {
												FIXME();
											} else {
												goto undef;
											}
										}
									}
									place_jump(end);
									place_fix_label(IOAddress);
									// IO memory
									if (instr.LoadStoreExtension.L) {
										if (instr.LoadStoreExtension.H) {
											if (instr.LoadStoreExtension.S) {
												/* LDRSH - Load Register Signed Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0 then
												 *             data = Memory[address,2]
												 *         else
												 *             data = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         data = Memory[address,2]
												 *     Rd = ZeroExtend(data[15:0])
												 */
												if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_v4_UP)) {
													place_call_argument(0, reg32, regEAX);
													place_call_imm(IOReadHalf, 1);
													place_movsx_reg32__reg16(regEAX, regAX);
													place_mov_ptr32_imm__reg32(p_Rd, regEAX);
												} else {
													goto undef;
												}
											} else {
												/* LDRH - Load Register Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0 then
												 *             data = Memory[address,2]
												 *         else
												 *             data = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         data = Memory[address,2]
												 *     Rd = ZeroExtend(data[15:0])
												 */
												place_call_argument(0, reg32, regEAX);
												place_call_imm(IOReadHalf, 1);
												place_and_reg32__imm(regEAX, 0x0000FFFF);
												place_mov_ptr32_imm__reg32(p_Rd, regEAX);
											}
										} else {
											/* LDRSB - Load Register Signed Byte
											 *
											 * MemoryAccess(B-bit, E-bit)
											 * if ConditionPassed(cond) then
											 *     data = Memory[address,1]
											 *     Rd = SignExtend(data)
											 */
											place_call_argument(0, reg32, regEAX);
											place_call_imm(IOReadByte, 1);
											place_movsx_reg32__reg8(regEAX, regAL);
											place_mov_ptr32_imm__reg32(p_Rd, regEAX);
										}
									} else {
										if (instr.LoadStoreExtension.H) {
											if (instr.LoadStoreExtension.S) {
												/* STRD - Store Registers Doubleword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * processor_id = ExecutingProcessor()
												 * if ConditionPassed(cond) then
												 *     if (Rd is even-numbered) and (Rd is not R14) and
												 *             (address[1:0] == 0b00) and
												 *             ((CP15_reg1_Ubit == 1) or (address[2] == 0)) then
												 *         Memory[address,4] = Rd
												 *         Memory[address+4,4] = R(d+1)
												 *     else
												 *         UNPREDICTABLE
												 *     if Shared(address) then // from ARMv6
											     *         physical_address = TLB(address)
												 *         ClearExclusiveByAddress(physical_address,processor_id,4)
												 *     if Shared(address+4)
												 *         physical_address = TLB(address+4)
												 *         ClearExclusiveByAddress(physical_address,processor_id,4)
												 */
												if (ctx->CPU->CheckArch((ARM_ARCH)(ARM_ARCH_v5TE | ARM_ARCH_v5TEJ | ARM_ARCH_v6))) {
													FIXME();
												} else {
													goto undef;
												}
											} else {
												/* STRH - Store Register Halfword
												 *
												 * MemoryAccess(B-bit, E-bit)
												 * processor_id = ExecutingProcessor()
												 * if ConditionPassed(cond) then
												 *     if (CP15_reg1_Ubit == 0) then
												 *         if address[0] == 0b0 then
												 *             Memory[address,2] = Rd[15:0]
												 *         else
												 *             Memory[address,2] = UNPREDICTABLE
												 *     else // CP15_reg1_Ubit == 1
												 *         Memory[address,2] = Rd[15:0]
												 *     if Shared(address) then // ARMv6
												 *         physical_address = TLB(address)
												 *         ClearExclusiveByAddress(physical_address,processor_id,2)
												 */
												place_mov_reg32__ptr32_imm(regEDX, p_Rd);
												place_and_reg32__imm(regEDX, 0x0000FFFF);
												place_call_argument(1, reg32, regEDX);
												place_call_argument(0, reg32, regEAX);
												place_call_imm(IOWriteHalf, 2);
											}
										} else {
											/* LDRD - Load Registers Doubleword
											 *
											 * if ConditionPassed(cond) then
											 *     if (Rd is even-numbered) and (Rd is not R14) and
											 *             (address[1:0] == 0b00) and
											 *             ((CP15_reg1_Ubit == 1) or (address[2] == 0)) then
											 *         Rd = Memory[address,4]
											 *         R(d+1) = memory[address+4,4]
											 *     else
											 *         UNPREDICTABLE
											 */
											if (ctx->CPU->CheckArch((ARM_ARCH)(ARM_ARCH_v5TE | ARM_ARCH_v5TEJ | ARM_ARCH_v6))) {
												FIXME();
											} else {
												goto undef;
											}
										}
									}
									place_fix_label(end);

									// write back calculated value
									if (!instr.LoadStoreExtension.P || instr.LoadStoreExtension.W) {
										// pre-index with write-back, post-index
										place_mov_ptr32_imm__reg32(p_Rn, regEBX);
									}

									if (!ret_val) {
										place_jmp_imm(ctx->CPU->PCModifiedHelper);
										do_return(false);
									} else {
										do_return(true);
									}
								}
								break;
							}
						break;
					default:
						FIXME();
						break;
				}
			}
			break;
		case 0b010:
		case 0b011:
			{
				// Load And Store

				bool ret_value = true;

				// resolve address
				if (instr.LoadStore.R) {
					// register offset/index
					if (n_Rm == PC) {
						goto unpredictable;
					}
					if (n_Rn == PC) {
						place_mov_reg32__imm(regEAX, ctx->PC + 8);
					} else {
						place_mov_reg32__ptr32_imm(regEAX, p_Rn);
					}
					// calculate index
					place_mov_reg32__ptr32_imm(regEBX, p_Rm);
					uint32_t shift_imm = instr.LoadStore.shift_imm;
					switch (instr.LoadStore.shift) {
						case 0b00:
							// LSL
							if (shift_imm) {
								place_shl_reg32__imm(regEBX, shift_imm);
							}
							break;
						case 0b01:
							// LSR
							if (shift_imm) {
								place_shr_reg32__imm(regEBX, shift_imm);
							} else {
								// LSR #32
								place_mov_reg32__imm(regEBX, 0);
							}
							break;
						case 0b10:
							// ASR
							if (shift_imm) {
								place_sar_reg32__imm(regEBX, shift_imm);
							} else {
								// ASR #32
								place_sar_reg32__imm(regEBX, 31);
							}
							break;
						case 0b11:
							if (shift_imm) {
								// ROR
								FIXME();
							} else {
								// RRX
								FIXME();
							}
							break;
					}
					if (instr.LoadStore.P) {
						// pre-index
						if (instr.LoadStore.U) {
							place_add_reg32__reg32(regEAX, regEBX);
						} else {
							place_sub_reg32__reg32(regEAX, regEBX);
						}
						if (instr.LoadStore.W) {
							// store final Rm value in EBX
							place_mov_reg32__reg32(regEBX, regEAX);
						}
					} else {
						// post-index, index is saved in EBX
						if (instr.LoadStore.U) {
							place_add_reg32__reg32(regEBX, regEAX);
						} else {
							place_sub_reg32__reg32(regEBX, regEAX);
						}
					}
				} else {
					// immediate offset
					int32_t offset = instr.LoadStore.offset_12;
					if (!instr.LoadStore.U) {
						offset = -offset;
					}
					if (n_Rn == PC) {
						// use PC
						if (instr.LoadStore.P) {
							// pre-indexed
							place_mov_reg32__imm(regEAX, ctx->PC + 8 + offset);
							if (instr.LoadStore.W) {
								goto unpredictable;
							}
						} else {
							// post-indexed
							goto unpredictable;
						}
					} else {
						place_mov_reg32__ptr32_imm(regEAX, p_Rn);
						if (instr.LoadStore.P) {
							// pre-index
							if (offset) {
								place_add_reg32__imm(regEAX, offset);
							}
							if (instr.LoadStore.W) {
								place_mov_reg32__reg32(regEBX, regEAX);
							}
						} else {
							// post-index
							if (offset) {
								place_lea_reg32__reg32_imm(regEBX, regEAX, offset);
							} else {
								place_mov_reg32__reg32(regEBX, regEAX);
							}
						}
					}
				}
				// address is stored in EAX
				MMUAccessType acc = instr.LoadStore.L ? AccessRead : AccessWrite;
				if (instr.LoadStore.B) {
					data = place_VA_to_Host(data, SizeByte, acc, ctx);
				} else {
					data = place_VA_to_Host(data, SizeWord, acc, ctx);
				}

				// Host address/PA is placed in EAX
				if (instr.LoadStore.L) {
					/* LDR
					 *
					 * MemoryAccess(B-bit, E-bit)
					 * if ConditionPassed(cond) then
					 *     if (CP15_reg1_Ubit == 0) then
					 *         data = Memory[address,4] Rotate_Right (8 * address[1:0])
					 *     else // CP15_regUbit == 1
					 *         data = Memory[address,4]
					 *     if (Rd is R15) then
					 * 	       if (ARMv5 or above) then
					 *             PC = data AND 0xFFFFFFFE
					 *             T Bit = data[0]
					 *         else
					 *             PC = data AND 0xFFFFFFFC
					 *     else
					 *         Rd = data
					 */
					//FIXME: fix load from unaligned address
					place_condjump(C, IOAddress);
					// memory
					// FIXME:change to CMOV
					if (instr.LoadStore.B) {
						place_movzx_reg32__ptr8_reg(regEAX, regEAX);
					} else {
						place_mov_reg32__ptr32_reg(regEAX, regEAX);
					}
					place_jump(end);
					place_fix_label(IOAddress);
					place_call_argument(0, reg32, regEAX);
					if (instr.LoadStore.B) {
						place_call_imm(IOReadByte, 1);
						place_and_reg32__imm(regEAX, 0x000000FF);
					} else {
						place_call_imm(IOReadWord, 1);
					}
					place_fix_label(end);
					if (n_Rd == PC) {
						// branch, will be set-up at the end
					} else {
						place_mov_ptr32_imm__reg32(p_Rd, regEAX);
					}
					ret_value = (n_Rd != 15) || (instr.cond != 0b1110);
				} else {
					/* STR
					 *
					 * MemoryAccess(B-bit, E-bit)
					 * processor_id = ExecutingProcessor()
					 * if ConditionPassed(cond) then
					 *     Memory[address,4] = Rd
					 *     if Shared(address) then
					 *         // from ARMv6
					 *         physical_address = TLB(address)
					 *         ClearExclusiveByAddress(physical_address,processor_id,4)
					 */
					place_condjump(C, IOAddress);
					// memory
					if (n_Rd == PC) {
						place_mov_ptr32_imm__imm(regEAX, ctx->PC + 8);
					} else {
						place_mov_reg32__ptr32_imm(regEDX, p_Rd);
						if (instr.LoadStore.B) {
							place_mov_ptr8_reg__reg8(regEAX, regDL);
						} else {
							place_mov_ptr32_reg__reg32(regEAX, regEDX);
						}
					}
					place_jump(end);
					place_fix_label(IOAddress);
					// IO
					if (n_Rd == PC) {
						place_call_argument(1, imm, ctx->PC + 8);
					} else {
						place_call_argument(1, ptr32_imm, p_Rd);
					}
					place_call_argument(0, reg32, regEAX);
					if (instr.LoadStore.B) {
						place_call_imm(IOWriteByte, 2);
					} else {
						place_call_imm(IOWriteWord, 2);
					}
					place_fix_label(end);
				}

				// write Rn after operation
				if (!instr.LoadStore.P || instr.LoadStore.W) {
					place_mov_ptr32_imm__reg32(p_Rn, regEBX);
				}
				if (!ret_value) {
					if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_THUMB)) {
						place_and_reg32__imm(regEAX, ~1);
						//FIXME();
					} else {
						place_and_reg32__imm(regEAX, ~3);
					}
					place_mov_ptr32_imm__reg32(p_Rd, regEAX);
					place_jmp_imm(ctx->CPU->PCModifiedHelper);
				}
				do_return(ret_value);
			}
			break;
		case 0b100:
			{
				if (n_Rn == PC) {
					goto unpredictable;
				}
				if (!instr.LoadStoreMultiple.L && (instr.LoadStoreMultiple.register_list & (1 << 15))) {
					// when PC will be stored, we will have to store in registers list first
					place_mov_ptr32_imm__imm(p_GPR(PC), ctx->PC + 8);
				}
				// calculate number of bits
				unsigned int number_of_bits = 0;
				uint32_t bits = instr.LoadStoreMultiple.register_list;
				for (int i = 0; i <= 15; i++) {
					if (bits & (1 << i)) {
						number_of_bits++;
					}
				}
				if (!number_of_bits) {
					// no bits set, UNPREDICTABLE
					goto unpredictable;
				}
				// calculate start address
				place_mov_reg32__ptr32_imm(regEBP, p_Rn);
				if (instr.LoadStoreMultiple.U) {
					if (instr.LoadStoreMultiple.P) {
						/* Increment Before
						 *
						 * start_address = Rn + 4
						 * end_address = Rn + (Number_Of_Set_Bits_In(register_list) * 4)
						 * if ConditionPassed(cond) and W == 1 then
						 *     Rn = Rn + (Number_Of_Set_Bits_In(register_list) * 4)
						 */
						place_add_reg32__imm(regEBP, 4);
					} else {
						/* Increment After
						 *
						 * start_address = Rn
						 * end_address = Rn + (Number_Of_Set_Bits_In(register_list) * 4) - 4
						 * if ConditionPassed(cond) and W == 1 then
						 *     Rn = Rn + (Number_Of_Set_Bits_In(register_list) * 4)
						 */
					}
				} else {
					if (instr.LoadStoreMultiple.P) {
						/* Decrement Before
						 *
						 * start_address = Rn - (Number_Of_Set_Bits_In(register_list) * 4)
						 * end_address = Rn - 4
						 * if ConditionPassed(cond) and W == 1 then
						 *     Rn = Rn - (Number_Of_Set_Bits_In(register_list) * 4)
						 */
						place_sub_reg32__imm(regEBP, number_of_bits * 4);
					} else {
						/* Decrement After
						 *
						 * start_address = Rn - (Number_Of_Set_Bits_In(register_list) * 4) + 4
						 * end_address = Rn
						 * if ConditionPassed(cond) and W == 1 then
						 *     Rn = Rn - (Number_Of_Set_Bits_In(register_list) * 4)
						 */
						place_sub_reg32__imm(regEBP, number_of_bits * 4 + 4);
					}
				}
				place_jump(do_job);

				// store offsets to registers
				bool user_mode = false;
				if (instr.LoadStoreMultiple.S) {
					if (instr.LoadStoreMultiple.L) {
						user_mode = (instr.LoadStoreMultiple.register_list >> PC) == 0;
					} else {
						user_mode = true;
					}
				}
				void* registers_offset_table = data;
				for (int i = 0; i <= 15; i++) {
					if (bits & (1 << i)) {
						// FIXME:support for FIQ
						if (user_mode && ((i == R13) || (i == R14))) {
							emit8((offsetof(ARMCONTEXT, banked.R13_usr) >> 2) + (i - R13));
						} else {
							emit8((offsetof(ARMCONTEXT, regs) >> 2) + i);
						}
					}
				}
				emit8(0);

				// resolve PA routine, return destination HOST address at EDI, C Flag is set if IO memory
				// will not return if failed to translate access
				void* resolve_PA = data;
				place_pop_reg32(regEDI);	// return address, must remove from stack because
											// jump to PCModifiedHandler may occur
				place_call_argument(0, reg32, regEBP);
				data = place_VA_to_Host(data, SizeWord, instr.LoadStoreMultiple.L ? AccessRead : AccessWrite, ctx);
				place_xchg_reg32__reg32(regEAX, regEDI);
				place_jmp_reg(regEAX);

				place_fix_label(do_job);
				place_mov_reg__imm(regESI, registers_offset_table);
				place_mov_reg__imm(regEBX, &ctx->CPU->context);
				// EBP - guest address <Rn>
				// EDI - host memory address
				// ESI - registers table
				// EAX - current register number
				// EBX - CPU context
				place_call_imm(resolve_PA, 0);

				void* IOAddress_pos;
				place_condjump_var(C, IOAddress, IOAddress_pos);

				bool ret_val = true;

				if (instr.LoadStoreMultiple.L) {
# define place_LDM_general_registers() \
	/* physical memory */ \
	void* next_LDM = data; \
	place_label(next_LDM); \
	place_mov_reg32__imm(regEAX, 0); \
	place_lodsb(); \
	place_mov_reg32__ptr32_reg(regECX, regEDI); \
	place_mov_ptr32_reg_reg_mul__reg32(regEBX, regEAX, mul4, regECX) \
	place_lea_reg32__reg32_imm(regEDI, regEDI, 4); \
	place_cmp_ptr8_reg__imm(regESI, 0); \
	place_lea_reg32__reg32_imm(regEBP, regEBP, 4); \
	place_condjump(Z, end_LDM); \
	place_test_reg32__imm(regEBP, ctx->CPU->ARCHInfo->MinPageSize - 1); \
	place_fix_condjump(NZ, next_LDM); \
	place_push_imm(next_LDM); \
	place_jmp_imm(resolve_PA); \
	 \
	place_fix_label_var(IOAddress, IOAddress_pos); \
	/* IO memory space */ \
	void* next_LDM_IO = data; \
	place_label(next_LDM_IO); \
	place_call_argument(0, reg32, regEDI); \
	place_call_imm(IOReadWord, 1); \
	place_mov_reg32__reg32(regECX, regEAX); \
	place_mov_reg32__imm(regEAX, 0); \
	place_lodsb(); \
	place_mov_ptr32_reg_reg_mul__reg32(regEBX, regEAX, mul4, regECX); \
	place_lea_reg32__reg32_imm(regEDI, regEDI, 4); \
	place_cmp_ptr8_reg__imm(regESI, 0); \
	place_lea_reg32__reg32_imm(regEBP, regEBP, 4); \
	place_condjump(Z, end_LDM_IO); \
	place_test_reg32__imm(regEBP, ctx->CPU->ARCHInfo->MinPageSize - 1); \
	place_fix_condjump(NZ, next_LDM_IO); \
	place_push_imm(next_LDM_IO); \
	place_jmp_imm(resolve_PA); \
	 \
	place_fix_label(end_LDM); \
	place_fix_label(end_LDM_IO)

					if (instr.LoadStoreMultiple.S) {
						if (instr.LoadStoreMultiple.register_list & (1 << PC)) {
							/* Load general-purpose registers, with SPSR restore
							 *
							 * MemoryAccess(B-bit, E-bit)
							 * if ConditionPassed(cond) then
							 *     address = start_address
							 *
							 *     for i = 0 to 14
							 *         if register_list[i] == 1 then
							 *             Ri = Memory[address,4]
							 *             address = address + 4
							 *
							 *     if CurrentModeHasSPSR() then
							 *         CPSR = SPSR
							 *     else
							 *         UNPREDICTABLE
							 *
							 *     value = Memory[address,4]
							 *     PC = value
							 *     address = address + 4
							 *     assert end_address == address - 4
							 */

							// EBP - guest address <Rn>
							// EDI - host memory address
							// ESI - registers table
							// EBX - CPU context
							// => EAX - current register number
							// => ECX - value loaded from memory

							place_noSPSR_UNPREDICTABLE();

							place_LDM_general_registers();

							// write back must be done before setting CPSR, because, R13, R14 is banked
							if (instr.LoadStoreMultiple.W) {
								if (instr.LoadStoreMultiple.U) {
									place_add_ptr32_imm__imm(p_Rn, number_of_bits * 4);
								} else {
									place_sub_ptr32_imm__imm(p_Rn, number_of_bits * 4);
								}
							}

							place_call_obj_argument(1, imm, ~ctx->CPU->ARCHInfo->PSRBits.UnallocMask);
							place_call_obj_argument(0, ptr32_imm, &ctx->CPU->context.SPSR);
							place_call_obj(ctx->CPU, ARMCPU, WriteCPSR, 2);
							place_jmp_imm(ctx->CPU->PCModifiedHelper);
							do_return(instr.cond != 0b1110);
						} else {
							/* Load User mode registers
							 *
							 * MemoryAccess(B-bit, E-bit)
							 * if ConditionPassed(cond) then
							 *     address = start_address
							 *
							 *     for i = 0 to 14
							 *         if register_list[i] == 1 then
							 *             Ri_usr = Memory[address,4]
							 *             address = address + 4
							 *     assert end_address == address - 4
							 */

							// EBP - guest address <Rn>
							// EDI - host memory address
							// ESI - registers table
							// EBX - CPU context
							// => EAX - current register number
							// => ECX - value loaded from memory

							if (instr.LoadStoreMultiple.W) {
								goto unpredictable;
							}

							place_noSPSR_UNPREDICTABLE();

							place_LDM_general_registers();
						}
					} else {
						/* Load general-purpose registers
						 *
						 * MemoryAccess(B-bit, E-bit)
						 * if ConditionPassed(cond) then
						 *     address = start_address
						 *
						 *     for i = 0 to 14
						 *         if register_list[i] == 1 then
						 *             Ri = Memory[address,4]
						 *             address = address + 4
						 *
						 *
						 *     if register_list[PC] == 1 then
						 *         value = Memory[address,4]
						 *         if (architecture version 5 or above) then
						 *             pc = value AND 0xFFFFFFFE
						 *             T Bit = value[0]
						 *         else
						 *             pc = value AND 0xFFFFFFFC
						 *         address = address + 4
						 *     assert end_address == address - 4
						 */

						// EBP - guest address <Rn>
						// EDI - host memory address
						// ESI - registers table
						// EBX - CPU context
						// => EAX - current register number
						// => ECX - value loaded from memory

						place_LDM_general_registers();

						if (instr.LoadStoreMultiple.register_list & (1 << 15)) {
							// PC was modified
							if (ctx->CPU->CheckArch(CHECK_ARM_ARCH_THUMB)) {
								place_test_reg32__imm(regECX, 1);
								place_condjump(Z, ARM);
								place_or_ptr32_imm__imm(&ctx->CPU->context.uncached_CPSR, 1 << 5);
								place_fix_label(ARM);
								place_and_ptr32_imm__imm(p_GPR(PC), 0xFFFFFFFE);
							} else {
								place_and_ptr32_imm__imm(p_GPR(PC), 0xFFFFFFFC);
							}
							ret_val = false;
						}
					}
				} else {
					// store multiple
					if (instr.LoadStoreMultiple.S) {
						/* Store User-Mode registers
						 *
						 * MemoryAccess(B-bit, E-bit)
						 * processor_id = ExecutingProcessor()
						 * if ConditionPassed(cond) then
						 *     address = start_address
						 *     for i = 0 to 15
						 *         if register_list[i] == 1 then
						 *             Memory[address,4] = Ri_usr
						 *             address = address + 4
						 *             if Shared(address) then // from ARMv6
						 *                 physical_address = TLB(address)
						 *                 ClearExclusiveByAddress(physical_address,processor_id,4)
						 *     assert end_address == address - 4
						 */

						place_noSPSR_UNPREDICTABLE();
					}
					// for User-mode registers, offsets of registers were adjusted

					/* Store general-purpose registers
					 *
					 * MemoryAccess(B-bit, E-bit)
					 * processor_id = ExecutingProcessor()
					 * if ConditionPassed(cond) then
					 *     address = start_address
					 *     for i = 0 to 15
					 *         if register_list[i] == 1 then
					 *             Memory[address,4] = Ri
					 *             address = address + 4
					 *             if Shared(address) then // from ARMv6
					 *                 physical_address = TLB(address)
					 *                 ClearExclusiveByAddress(physical_address,processor_id,4)
					 *     assert end_address == address - 4
					 */

					// EBP - guest address <Rn>
					// EDI - host memory address/IO address
					// ESI - registers table
					// EBX - CPU context
					// => EAX - current register number

					// physical memory
					void* next_STM = data;
					place_label(next_STM);
					place_mov_reg32__imm(regEAX, 0);
					place_lodsb();
					place_mov_reg32__ptr32_reg_reg_mul(regEAX, regEBX, regEAX, mul4);
					place_stosd();
					place_cmp_ptr8_reg__imm(regESI, 0);
					place_lea_reg32__reg32_imm(regEBP, regEBP, 4);
					place_condjump(Z, end_STM);
					place_test_reg32__imm(regEBP, ctx->CPU->ARCHInfo->MinPageSize - 1);
					place_fix_condjump(NZ, next_STM);
					place_push_imm(next_STM);
					place_jmp_imm(resolve_PA);

					place_fix_label_var(IOAddress, IOAddress_pos);
					// IO memory space
					void* next_STM_IO = data;
					place_label(next_STM_IO);
					place_mov_reg32__imm(regEAX, 0);
					place_lodsb();
					place_call_argument(1, ptr32_reg_reg_mul, regEBX, regEAX, mul4);
					place_call_argument(0, reg32, regEDI);
					place_call_imm(IOWriteWord, 2);
					place_lea_reg32__reg32_imm(regEDI, regEDI, 4);
					place_cmp_ptr8_reg__imm(regESI, 0);
					place_lea_reg32__reg32_imm(regEBP, regEBP, 4);
					place_condjump(Z, end_STM_IO);
					place_test_reg32__imm(regEBP, ctx->CPU->ARCHInfo->MinPageSize - 1);
					place_fix_condjump(NZ, next_STM_IO);
					place_push_imm(next_STM_IO);
					place_jmp_imm(resolve_PA);

					place_fix_label(end_STM);
					place_fix_label(end_STM_IO);
				}

				// write-back
				if (instr.LoadStoreMultiple.W) {
					if (instr.LoadStoreMultiple.U) {
						place_add_ptr32_imm__imm(p_Rn, number_of_bits * 4);
					} else {
						place_sub_ptr32_imm__imm(p_Rn, number_of_bits * 4);
					}
				}

				if (!ret_val) {
					place_jmp_imm(ctx->CPU->PCModifiedHelper);
					do_return(instr.cond != 0b1110);
				} else {
					do_return(true);
				}
			}
			break;
		case 0b101:
		{
			/* B, BL - Branch, Branch with Link
			 *
			 * if ConditionPassed(cond) then
			 *     if L == 1 then
			 *         LR = address of the instruction after the branch instruction
			 *     PC = PC + (SignExtend_30(signed_immed_24) << 2)
			 */
			int32_t offset = (int32_t)instr.Branch.immed;
			if (offset & (1 << 23)) {
				offset |= 0x3F000000;
			}
			offset <<= 2;
			/* TODO: Memory bounds
			 * Branching backwards past location zero and forwards over the end of the 32-bit
			 * address space is UNPREDICTABLE.
			 *
			 * A4-11
			 */
			if (instr.Branch.L) {
				// BL
 				place_mov_ptr32_imm__imm(p_GPR(LR), ctx->PC + 4);
			}
			if (offset != -4) {
				place_call_imm(ctx->CPU->BranchHelper, 0);
				emit32(ctx->PC + offset + 8);
				do_return(instr.Branch.L || (instr.cond != 0b1110));
			} else {
				// branch to next instruction, just skip it
				do_return(true);
			}
			break;
		}
		case 0b110:
			// FIXME:
			goto notimplemented;
		case 0b111:
			if (instr.CoprocessorRegisterTransfer.SWI) {
				// software interrupt
				FIXME();
			} else {
				// coprocessor transfer
				if (ctx->CPU->coprocessors[instr.CoprocessorRegisterTransfer.cp_num]) {
					ctx->output = data;
					bool ret = ctx->CPU->coprocessors[instr.CoprocessorRegisterTransfer.cp_num]->DecodeInstruction(ctx, instr);
					data = ctx->output;
					do_return(ret);
				} else {
					// coprocessor not found
					goto undef;
				}
			}
			goto notimplemented;
	}

	ctx->output = data;

return_false:
	ctx->output = data;
	return false;
return_true:
	ctx->output = data;
	return true;

notimplemented:
	DEBUG_PRINT("not implemented: 0x%08X: 0x%08X\n", ctx->PC, instr.Word);
	place_breakpoint();
	return false;
unpredictable:
	ctx->output = place_UNPREDICTABLE(data);
	return false;
undef:
	ctx->output = place_UNDEFINED(data);
	return false;
}

ARM_INSTRUCTION::ARM_INSTRUCTION(uint32_t v)
{
	Word = v;
}

void* place_UNPREDICTABLE(void* data)
{
	//FIXME: to implement
	place_breakpoint();
	return data;
}

void* place_UNDEFINED(void* data)
{
	//FIXME: to implement
	place_breakpoint();
	return data;
}
