/*
 * VectorFloatingPoint.cpp
 *
 *  Created on: 15-06-2012
 *      Author: Łukasz Misek
 */

#include "VectorFloatingPoint.h"
#include "armcpu.h"

VectorFloatingPointCoprocessor10::VectorFloatingPointCoprocessor10(ARMCPU* owner): ARMCoprocessor(owner)
{

}

void VectorFloatingPointCoprocessor10::Reset()
{
	ARMCoprocessor::Reset();
}

bool VectorFloatingPointCoprocessor10::DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr)
{
	register void* data = ctx->output;
	bool ret_value = true;

	switch (instr.CoprocessorRegisterTransfer.opcode_1) {
		case 0b111:
			switch (instr.CoprocessorRegisterTransfer.opcode_2) {
				case 0b000:
				case 0b001:
				case 0b010:
				case 0b011:
					/* FMRX - Floating-point Move to Register from System Register
					 *
					 * if ConditionPassed(cond) then
					 *     Rd = reg
					 *
					 */
					/* FMXR - Floating-point Move to System Register from Register
					 *
					 * if ConditionPassed(cond) then
					 *     reg = Rd
					 *
					 */
					switch (instr.CoprocessorRegisterTransfer.CRn) {
						case 0b0000:
							// FPSID
							if (instr.CoprocessorRegisterTransfer.L) {
								FIXME();
							} else {
								FIXME();
							}
							break;
						case 0b0001:
							if (n_Rd == PC) {
								// FMSTAT
								FIXME();
							} else {
								// FPSCR
								if (instr.CoprocessorRegisterTransfer.L) {
									FIXME();
								} else {
									FIXME();
								}
							}
							break;
						case 0b1000:
							// FPEXC
							if (instr.CoprocessorRegisterTransfer.L) {
								//place_mov_ptr32_imm__imm(p_Rd, 0);
								//FIXME();
							} else {
								//FIXME();
							}
							break;
						default:
							FIXME();
							goto undef;
							break;
					}
					break;
				default:
					FIXME();
					break;
			}
			break;
		default:
			FIXME();
			goto undef;
	}

decode_end:
	ctx->output = data;
	return ret_value;
undef:
	ret_value = false;
	FIXME();
	data = place_UNDEFINED(data);
	goto decode_end;
unpredictable:
	ret_value = false;
	FIXME();
	data = place_UNPREDICTABLE(data);
	goto decode_end;
	ASSERT(false);
	return false;
}
