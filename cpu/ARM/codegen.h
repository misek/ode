/*
 * codegen.h
 *
 *  Created on: 13-04-2012
 *      Author: Łukasz Misek
 */

#ifndef CODEGEN_H_
#define CODEGEN_H_

#include <stdint.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/mman.h>
#endif

#include "config.h"
#include "armcpu.h"

/*
 * general macros form:
 * place_<instruction>_<param1>__<param2>__<param3>
 *
 * <instruction>:
 * 		instruction mnemonic
 *
 * <param1>, <param2>, <param3>:
 * 		optional instruction parameters
 *
 * 	parameter format:
 * 		<param> = <ptr> | <reg> | <imm>
 *
 * 		<ptr> = ptr<size>_<reg>_<imm>:
 * 			pointer to data
 * 		<reg> = reg<size>
 *
 * 		<size>:
 * 			operand size, 8, 16, 32 or 64 bit,
 * 			when not specified, machine word size is used (32 on x86, 64 on x86_64)
 *
 * 		<reg>:
 * 			register name
 *
 * 		<imm>: immediate value, no size is specified for operand,
 * 			optimal encoding will be used based on imm value
 *
 * 	some examples:
 * 		place_mov_ptr32_regimm__reg32(regEAX, 123, regECX):
 * 			mov dword ptr [EAX + 123], ECX	(32-bit)
 * 			mov dword ptr [RAX + 123], ECX	(64-bit)
 * 		place_mov_ptr_imm__reg(0x12345, REC_EDX):
 * 			mov dword ptr [0x12345], EDX	(32-bit)
 * 			mov qword ptr [0x12345], RDX	(64-bit)
 * 		place_xor_reg_reg(regEAX, REC_EDX):
 * 			xor EAX, EDX 					(32-bit)
 * 			xor RAX, RDX 					(64-bit)
 * 		place_cmp_reg32__reg32(regEAX, REC_EDX):
 * 			cmp EAX, EDX 					(32-bit, 64-bit)
 */

enum X86_REGISTER {
	regAL = 0, regAX = 0, regEAX = 0,
	regCL = 1, regCX = 1, regECX = 1,
	regDL = 2, regDX = 2, regEDX = 2,
	regBL = 3, regBX = 3, regEBX = 3,

	regAH = 4, regSP = 4, regESP = 4,
	regCH = 5, regBP = 5, regEBP = 5,
	regDH = 6, regSI = 6, regESI = 6,
	regBH = 7, regDI = 7, regEDI = 7
};

void* allocate_code_memory(size_t size);
void free_code_memory(void* data, size_t size);

#ifdef DEBUG_COMPILED_CODE
#define DEBUG_PLACE(fmt, ...) { DEBUG_PRINT("[ASM]: " fmt "\n", ##__VA_ARGS__); }
#else
#define DEBUG_PLACE(fmt, ...)
#endif

#define emit_data(p, v) *(p) = v; data = (p) + 1
#define emit8(v) emit_data((uint8_t*)data, (uint8_t)(v))
#define emit16(v) emit_data((uint16_t*)data, (uint16_t)(v))
#define emit32(v) emit_data((uint32_t*)data, (uint32_t)(v))
#define emit64(v) emit_data((uint64_t*)data, (uint64_t)(v))
#define emit_ptr(v) emit_data((uintptr_t*)data, (uintptr_t)(v))

#define BUF_COUNT 100
extern char tmp_buf[BUF_COUNT][100];
extern int tmp_index;

#define return_str(s) { if (tmp_index == BUF_COUNT) tmp_index++; strcpy(tmp_buf[tmp_index], s); return tmp_buf[tmp_index++]; }
#define return_strf(fmt, ...) { if (tmp_index == BUF_COUNT) tmp_index++; sprintf(tmp_buf[tmp_index], fmt, __VA_ARGS__); return tmp_buf[tmp_index++]; }

const char* ARM_regNAME(int no);
const char* PTR32_NAME(ARMCPU* cpu, void* ptr);
const char* PTR8_NAME(ARMCPU* cpu, void* ptr);

const char* X86_REG8_NAME(X86_REGISTER reg);
const char* X86_REG16_NAME(X86_REGISTER reg);
const char* X86_REG32_NAME(X86_REGISTER reg);
#define X86_REG_NAME X86_REG32_NAME

#include "codegen/x86.h"

#endif /* CODEGEN_H_ */
