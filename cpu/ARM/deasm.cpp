/*
 * deasm.cpp
 *
 *  Created on: 06-05-2012
 *      Author: Łukasz Misek
 */

#include "deasm.h"

struct ARM_INSTRUCTION_DEF {
	uint32_t mask;
	uint32_t value;
	const char* format;
};

// format parameters:
/*
 * %%						percent sign
 * %c 						condition code
 * %x<start>;<count>;		hexadecimal value from bit <start>, <count> bits, eg: %x13;5;
 * %u<start>;<count>;		unsigned value from bit <start>, <count> bits, eg: %u13;5;
 * %s<start>;<count>;		signed value from bit <start>, <count> bits, eg: %s13;5;
 * %g<bit>					GPR register name, starting from <bit>, eg: %g12;
 * %1						Addressing mode 1 (shifter_operand)
 * %2						Addressing mode 2 (LDR/STR)
 * %3						Addressing mode 3 (Miscellaneous Loads and Stores)
 * %f<set>;<not_set>;<bit>;	Flag depending on bit value, eg: %fS;;20;
 * %F<set>;<bit>;			shortcut for %f<set>;;<bit>;
 * %S						shortcut for %FS;20;
 * %t						parameter delimiter, eg tabulator
 * %b						branch destination
 * %m						<Rm> register (0:3)
 * %s						<Rs> register (8:11)
 * %d						<Rd> register (12:15)
 * %n						<Rn> register (16:19)
 * %r						registers list, used in LDM/STM
 */

const char* cond_codes[] = {
	"EQ", "NE", "CS", "CC", "MI", "PL", "VS", "VC",
	"HI", "LS", "GE", "LT", "GT", "LE", "", ""
};

ARM_INSTRUCTION_DEF instructions[] = {
	{0xFD70F000, 0xF550F000, "PLD\t%2"},
	{0x0FF000F0, 0x01200010, "BX%c\t%m"},
	{0x0FF000F0, 0x01200030, "BLX%c\t%m"},
	{0x0FF000F0, 0x01600010, "CLZ%c\t%d%t%m"},
	{0x0FE000F0, 0x00200090, "MLA%c%S\t%n%t%m%t%s%t%d"},
	{0x0FE000F0, 0x00000090, "MUL%c%S\t%n%t%m%t%s"},
	{0x0FE000F0, 0x00C00090, "SMULL%c%S\t%d%t%n%t%m%t%s"},
	{0x0FC000F0, 0x00800090, "UM%fLA;UL;21;L%c%S\t%d%t%n%t%m%t%s"},
	{0x0FB00000, 0x01000000, "MRS%c\t%d%t%fSPSR;CPSR;22;"},
	{0x0DB00000, 0x01200000, "MSR%c\t%fS;C;22;PSR_%Fc;16;%Fx;17;%Fs;18;%Ff;19;%t%1"},
	{0x0DE00000, 0x00A00000, "ADC%c%S\t%d%t%n%t%1"},
	{0x0E0000F0, 0x000000D0, "%fLDR;STR;20;%c%fSB;D;20;\t%d%t"},
	{0x0E0000F0, 0x000000B0, "%fLDR;STR;20;%cH\t%d%t%3"},
	{0x0E0000F0, 0x000000F0, "%fLDR;STR;20;%cSH\t%d%t%3"},
	{0x0E0000F0, 0x000000D0, "%fLDR;STR;20;%cD\t%d%t%3"},
	{0x0DE00000, 0x00800000, "ADD%c%S\t%d%t%n%t%1"},
	{0x0DE00000, 0x00000000, "AND%c%S\t%d%t%n%t%1"},
	{0x0E000000, 0x0A000000, "B%FL;24;%c\t%b"},
	{0x0DE00000, 0x01C00000, "BIC%c%S\t%d%t%n%t%1"},
	{0x0DF00000, 0x01700000, "CMN%c\t%n%t%1"},
	{0x0DF00000, 0x01500000, "CMP%c\t%n%t%1"},
	{0x0DE00000, 0x00200000, "EOR%c%S\t%d%t%n%t%1"},
	{0x0C000000, 0x04000000, "%fLDR;STR;20;%c%FB;22;\t%d%t%2"},
	// VFP instructions
	{0x0FFF0F90, 0x0EF00A10, "FM%fRX;XR;20;\t%d%tFPSID"},
	{0x0FFF0F90, 0x0EF10A10, "FM%fRX;XR;20;\t%d%tFPSCR"},
	{0x0FFF0F90, 0x0EF80A10, "FM%fRX;XR;20;\t%d%tFPEXC"},
	{0x0F100010, 0x0E000010, "MCR%c\tp%u8;4;%t%u21;3;%t%d%tc%u16;4;%tc%u0;4;%t%u5;3;"},
	{0x0DE00000, 0x01A00000, "MOV%c%S\t%d%t%1"},
	{0x0F100010, 0x0E100010, "MRC%c\tp%u8;4;%t%u21;3;%t%d%tc%u16;4;%tc%u0;4;%t%u5;3;"},
	{0x0DE00000, 0x01E00000, "MVN%c%S\t%d%t%1"},
	{0x0DE00000, 0x01800000, "ORR%c%S\t%d%t%n%t%1"},
	{0x0DE00000, 0x00600000, "RSB%c%S\t%d%t%n%t%1"},
	{0x0DE00000, 0x00E00000, "RSC%c%S\t%d%t%n%t%1"},
	{0x0DE00000, 0x00C00000, "SBC%c%S\t%d%t%n%t%1"},
	{0x0DE00000, 0x00400000, "SUB%c%S\t%d%t%n%t%1"},
	{0x0E000000, 0x08000000, "%fLD;ST;20;M%c%fI;D;23;%fB;A;24;\t%n%F!;21;%t%r%F^;22;"},
	{0x0DF00000, 0x01300000, "TEQ%c\t%n%t%1"},
	{0x0DF00000, 0x01100000, "TST%c\t%n%t%1"},
	{0, 0, __null}
};

int get_int(const char*& src)
{
	int ret = 0;
	while (*src != ';') {
		ret = ret * 10 + ((*src++) - '0');
	}
	return ret;
}

void get_str(const char*& src, char* dest)
{
	while (*src != ';') {
		*dest++ = *src++;
	}
	*dest = 0;
}

bool deasm_instruction(uint32_t PC, uint32_t instr, char* dest)
{
#define add_str(s) { \
	const char* p = s; \
	while (*p) { \
		*dest++ = *p++; \
	} \
}
#define add_reg(no) { \
	if ((no) == 13) { \
		*dest++ = 'S'; \
		*dest++ = 'P'; \
	} else { \
		if ((no) == 14) { \
			*dest++ = 'L'; \
			*dest++ = 'R'; \
		} else { \
			if ((no) == 15) { \
				*dest++ = 'P'; \
				*dest++ = 'C'; \
			} else { \
				*dest++ = 'R'; \
				if ((no) >= 10) { \
					*dest++ = '1'; \
				} \
				*dest++ = '0' + ((no) % 10); \
			} \
		} \
	} \
}

	ARM_INSTRUCTION_DEF* def = instructions;
	char f1[16];
	char f2[16];
	int i, j;

	while (def->mask) {
		if ((instr & def->mask) == def->value) {
			// valid instruction found
			const char* src = def->format;
			while (*src) {
				if (*src == '%') {
					src++;
					switch (*src) {
						case '%':
							*dest++ = '%';
							break;
						case 'c':
							// condition code
							add_str(cond_codes[instr >> 28]);
							break;
						case 'g':
							// GPR register
							i = get_int(++src);
							i = (instr >> i) & 0x0F;
							add_reg(i);
							break;
						case 'd':
							// <Rd> register
							add_reg((instr >> 12) & 0x0F);
							break;
						case 'n':
							// <Rn> register
							add_reg((instr >> 16) & 0x0F);
							break;
						case 'm':
							// <Rm> register
							add_reg(instr & 0x0F);
							break;
						case 's':
							// <Rs> register
							add_reg((instr >> 8) & 0x0F);
							break;
						case 'b':
							// branch destination
							i = instr & 0x00FFFFFF;
							if (i & 0x00800000) {
								i |= 0xFF000000;
							}
							i <<= 2;
							i += PC + 8;
							dest += sprintf(dest, "0x%08X", i);
							break;
						case 'u':
							// unsigned
							i = get_int(++src);
							j = get_int(++src);
							i = (instr >> i) & ((1 << j) - 1);
							dest += sprintf(dest, "%u", i);
							break;
						case 'x':
							// hexadecimal
							i = get_int(++src);
							j = get_int(++src);
							i = (instr >> i) & ((1 << j) - 1);
							dest += sprintf(dest, "0x%X", i);
							break;
						case 'f':
							// bit flag
							get_str(++src, f1);
							get_str(++src, f2);
							i = get_int(++src);
							if (instr & (1 << i)) {
								add_str(f1);
							} else {
								add_str(f2);
							}
							break;
						case 'F':
							// bit flag
							get_str(++src, f1);
							i = get_int(++src);
							if (instr & (1 << i)) {
								add_str(f1);
							}
							break;
						case 'S':
							// set CPSR
							if (instr & (1 << 20)) {
								*dest++ = 'S';
							}
							break;
						case 'r':
							// registers list (STM/LDM)
							*dest++ = '{';
							{
								uint32_t regs = instr & 0x0000FFFF;
								int start = 0;
								int end = 0;
								while (regs) {
									end = start;
									while (regs & 1) {
										end++;
										regs >>= 1;
									}
									if (end > start) {
										if (end - start > 2) {
											add_reg(start);
											*dest++ = '-';
											add_reg(end - 1);
										} else {
											add_reg(start);
											if (end - start > 1) {
												*dest++ = ',';
												add_reg(end - 1);
											}
										}
										if (regs) {
											*dest++ = ',';
										}
									}
									start = end + 1;
									regs >>= 1;
								}
							}
							*dest++ = '}';
							break;
						case '1':
							// Addressing mode 1
							if (instr & (1 << 25)) {
								// 32-bit immediate
								i = instr & 0xFF;
								j = ((instr >> 8) & 0xF) * 2;
								if (j)
									i = (i >> j) | (i << (32 - j));
								dest += sprintf(dest, "#0x%X", i);
							} else {
								add_reg(instr & 0x0F);
								if (instr & 0x10) {
									// register shift
									switch ((instr >> 5) & 3) {
										case 0b00:
											dest += sprintf(dest, ", LSL ");
											break;
										case 0b01:
											dest += sprintf(dest, ", LSR ");
											break;
										case 0b10:
											dest += sprintf(dest, ", ASR ");
											break;
										case 0b11:
											dest += sprintf(dest, ", ROR ");
											break;
									}
									add_reg((instr >> 8) & 0xF);
								} else {
									// immediate shift
									i = (instr >> 7) & 0x1F;
									switch ((instr >> 5) & 3) {
										case 0b00:
											if (i) {
												dest += sprintf(dest, ", LSL #%d", i);
											}
											break;
										case 0b01:
											dest += sprintf(dest, ", LSR #%d", i);
											break;
										case 0b10:
											dest += sprintf(dest, ", ASR #%d", i);
											break;
										case 0b11:
											if (i) {
												dest += sprintf(dest, ", ROR #%d", i);
											} else {
												add_str(", RRX");
											}
											break;
									}
								}
							}
							break;
						case '2':
							{
								if (instr & (1 << 25)) {
									// register scale/index
									*dest++ = '[';
									add_reg((instr >> 16) & 0xF);
									if (instr & (1 << 24)) {
										// pre-indexed
										*dest++ = ',';
										*dest++ = ' ';
										if (instr & (1 << 23)) {
											// add index
										} else {
											*dest++ = '-';
										}
										add_reg(instr & 0xF);
										switch ((instr >> 5) & 3) {
											case 0b00:
												// LSL
												if ((instr >> 7) & 31) {
													dest += sprintf(dest, ", LSL #%d", (instr >> 7) & 31);
												}
												break;
											case 0b01:
												// LSR
												dest += sprintf(dest, ", LSR #%d", ((instr >> 7) & 31) ? ((instr >> 7) & 31) : 32);
												break;
											case 0b10:
												// ASR
												dest += sprintf(dest, ", ASR #%d", ((instr >> 7) & 31) ? ((instr >> 7) & 31) : 32);
												break;
											case 0b11:
												// ROR/RRX
												if ((instr >> 7) & 31) {
													dest += sprintf(dest, ", ROR #%d", (instr >> 7) & 31);
												} else {
													add_str(", RRX");
												}
												break;
										}
										*dest++ = ']';
										if (instr & (1 << 21)) {
											*dest++ = '!';
										}
									} else {
										// post-index
										*dest++ = ']';
										*dest++ = ',';
										*dest++ = ' ';
										if (instr & (1 << 23)) {
											// add index
										} else {
											*dest++ = '-';
										}
										add_reg(instr & 0xF);
										switch ((instr >> 5) & 3) {
											case 0b00:
												// LSL
												if ((instr >> 7) & 31) {
													dest += sprintf(dest, ", LSL #%d", (instr >> 7) & 31);
												}
												break;
											case 0b01:
												// LSR
												dest += sprintf(dest, ", LSR #%d", ((instr >> 7) & 31) ? ((instr >> 7) & 31) : 32);
												break;
											case 0b10:
												// ASR
												dest += sprintf(dest, ", ASR #%d", ((instr >> 7) & 31) ? ((instr >> 7) & 31) : 32);
												break;
											case 0b11:
												// ROR/RRX
												if ((instr >> 7) & 31) {
													dest += sprintf(dest, ", ROR #%d", (instr >> 7) & 31);
												} else {
													add_str(", RRX");
												}
												break;
										}
									}
								} else {
									// immediate
									*dest++ = '[';
									add_reg((instr >> 16) & 0xF);
									int32_t offset = instr & 0x00000FFF;
									if ((instr & (1 << 23)) == 0) {
										offset = -offset;
									}
									if (((instr >> 16) & 0xF) == 15) {
										// PC use
										offset += 8;
									}
									if (instr & (1 << 24)) {
										// pre-indexed
										if (offset) {
											*dest++ = ' ';
											if ((instr & (1 << 23)) == 0) {
												*dest++ = '-';
											} else {
												*dest++ = '+';
											}
											*dest++ = ' ';
											dest += sprintf(dest, "#0x%X", offset < 0 ? -offset : offset);
										}
										*dest++ = ']';
										if (instr & (1 << 21)) {
											*dest++ = '!';
										}
									} else {
										// post-indexed
										if (instr & (1 << 21)) {
											dest += sprintf(dest, "post-index_W");
										} else {
										}
										dest += sprintf(dest, "], #0x%X", offset < 0 ? -offset : offset);
									}
								}
							}
							break;
						case '3':
							{
								*dest++ = '[';
								add_reg((instr >> 16) & 0xF);
								if (instr & (1 << 22)) {
									// immediate offset/index
									uint32_t off = ((instr >> 4) & 0xF0) | (instr & 0x0F);
									if (instr & (1 << 24)) {
										// pre-index
										if (off) {
											add_str(", #0x");
											if ((instr & (1 << 23)) == 0) {
												*dest++ = '-';
											}
											dest += sprintf(dest, "%X", off);
										}
										*dest++ = ']';
										if (instr & (1 << 21)) {
											*dest++ = '!';
										}
									} else {
										// post-index
										add_str("], #0x");
										if ((instr & (1 << 23)) == 0) {
											*dest++ = '-';
										}
										dest += sprintf(dest, "%X", off);
									}
								} else {
									// register offset/index
									if (instr & (1 << 24)) {
										// pre-index
										add_str(", ");
										add_reg(instr & 0xF);
										*dest++ = ']';
										if (instr & (1 << 21)) {
											*dest++ = '!';
										}
									} else {
										// post-index
										add_str("], ");
										add_reg(instr & 0xF);
									}
								}
							}
							break;
						case 't':
							// parameter delimiter
							*dest++ = ',';
							*dest++ = ' ';
							break;
						default:
							DEBUG_PRINT("Invalid instruction format: %s, code: %c\n", def->format, *src);
							return false;
					}
				} else {
					*dest++ = *src;
				}
				src++;
			}
			*dest = 0;
			return true;
		}
		def++;
	}
	return false;
}

