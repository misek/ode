/*
 * coprocessor.h
 *
 *  Created on: 16-04-2012
 *      Author: Łukasz Misek
 */

#ifndef COPROCESSOR_H_
#define COPROCESSOR_H_

class ARMCPU;
struct DecoderContext;

#include "armcpu.h"
#include "decoder.h"
#include "codegen.h"

class ARMCoprocessor {
protected:
	virtual bool DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr);
	virtual bool DecodeDataProcessing(DecoderContext* ctx, ARM_INSTRUCTION instr);

public:
	ARMCoprocessor(ARMCPU* owner);
	virtual ~ARMCoprocessor();
	virtual void Reset();
	CODEGEN_CALL uint32_t Read(ARM_INSTRUCTION instr);
	CODEGEN_CALL void Write(ARM_INSTRUCTION instr, uint32_t value);

	ARMCPU* cpu;

	bool DecodeInstruction(DecoderContext* ctx, ARM_INSTRUCTION instr);
};

#endif /* COPROCESSOR_H_ */
