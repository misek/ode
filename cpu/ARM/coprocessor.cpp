/*
 * coprocessor.cpp
 *
 *  Created on: 16-04-2012
 *      Author: Łukasz Misek
 */

#include "coprocessor.h"
#include "armcpu.h"
#include "decoder.h"

ARMCoprocessor::ARMCoprocessor(ARMCPU* owner): cpu(owner)
{

}

ARMCoprocessor::~ARMCoprocessor()
{

}

void ARMCoprocessor::Reset()
{

}

bool ARMCoprocessor::DecodeInstruction(DecoderContext* ctx, ARM_INSTRUCTION instr)
{
	if (instr.CoprocessorRegisterTransfer.Bit_4) {
		return DecodeDataTransfer(ctx, instr);
	} else {
		return DecodeDataProcessing(ctx, instr);
	}
}

bool ARMCoprocessor::DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr)
{
	ctx->output = place_UNDEFINED(ctx->output);
	return false;
}

bool ARMCoprocessor::DecodeDataProcessing(DecoderContext* ctx, ARM_INSTRUCTION instr)
{
	ctx->output = place_UNDEFINED(ctx->output);
	return false;
}
