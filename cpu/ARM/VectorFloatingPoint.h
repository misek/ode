/*
 * VectorFloatingPoint.h
 *
 *  Created on: 15-06-2012
 *      Author: Łukasz Misek
 */

#ifndef VECTORFLOATINGPOINT_H_
#define VECTORFLOATINGPOINT_H_

#include "coprocessor.h"

class VectorFloatingPointCoprocessor10: public ARMCoprocessor {
private:

protected:
	virtual bool DecodeDataTransfer(DecoderContext* ctx, ARM_INSTRUCTION instr);
public:
	VectorFloatingPointCoprocessor10(ARMCPU* owner);

	virtual void Reset();

};

#endif /* VECTORFLOATINGPOINT_H_ */
