/*
 * armcpu.h
 *
 *  Created on: 12-04-2012
 *      Author: Łukasz Misek
 */

#ifndef ARMCPU_H_
#define ARMCPU_H_

class ARMCoprocessor;

#include "decoder.h"
#include "coprocessor.h"

#define ARM_PROC_ID(family, model, revision, patch) ((family << 24) | (model << 16) | (revision << 8) | patch)

// ARM9 processors
#define ARM9_PROC_ID(model, revision, patch) ARM_PROC_ID(9, model, revision, patch)
#define ARM_CPU_ARM9ES_r1		ARM9_PROC_ID(0, 1, 0)
#define ARM_CPU_ARM9ES_r2p1		ARM9_PROC_ID(0, 2, 1)
#define ARM_CPU_ARM9EJS			ARM9_PROC_ID(1, 0, 0)
#define ARM_CPU_ARM9TDMI_r0		ARM9_PROC_ID(2, 0, 0)
#define ARM_CPU_ARM9TDMI_r1		ARM9_PROC_ID(2, 1, 0)
#define ARM_CPU_ARM9TDMI_r2		ARM9_PROC_ID(2, 2, 0)
#define ARM_CPU_ARM9TDMI_r3		ARM9_PROC_ID(2, 3, 0)
#define ARM_CPU_ARM920T			ARM9_PROC_ID(3, 0, 0)
#define ARM_CPU_ARM922T			ARM9_PROC_ID(4, 0, 0)
#define ARM_CPU_ARM926EJS		ARM9_PROC_ID(5, 0, 0)
#define ARM_CPU_ARM940_r0		ARM9_PROC_ID(6, 0, 0)
#define ARM_CPU_ARM940_r2		ARM9_PROC_ID(6, 2, 0)
#define ARM_CPU_ARM946_r0		ARM9_PROC_ID(7, 0, 0)
#define ARM_CPU_ARM946_r1p1		ARM9_PROC_ID(7, 1, 1)
#define ARM_CPU_ARM966_r0		ARM9_PROC_ID(8, 0, 0)
#define ARM_CPU_ARM966_r1		ARM9_PROC_ID(8, 1, 0)
#define ARM_CPU_ARM966_r2p1		ARM9_PROC_ID(8, 2, 1)
#define ARM_CPU_ARM968ES		ARM9_PROC_ID(9, 0, 0)

// ARM processor architectures
enum ARM_ARCH {
	ARM_ARCH_v4 	= 0x00000001,
	ARM_ARCH_v4T 	= 0x00000002,
	ARM_ARCH_v5T 	= 0x00000100,
	ARM_ARCH_v5TE 	= 0x00000200,
	ARM_ARCH_v5TExP = 0x00000400,
	ARM_ARCH_v5TEJ 	= 0x00000800,
	ARM_ARCH_v6 	= 0x00010000
};

#define CHECK_ARM_ARCH_v4 (ARM_ARCH_v4 | ARM_ARCH_v4T)
#define CHECK_ARM_ARCH_v5 (ARM_ARCH_v5T | ARM_ARCH_v5TE | ARM_ARCH_v5TExP | ARM_ARCH_v5TEJ)
#define CHECK_ARM_ARCH_v6 (ARM_ARCH_v6)
#define CHECK_ARM_ARCH_v6_UP (CHECK_ARM_ARCH_v6)
#define CHECK_ARM_ARCH_v5_UP (CHECK_ARM_ARCH_v5 | CHECK_ARM_ARCH_v6_UP)
#define CHECK_ARM_ARCH_v4_UP (CHECK_ARM_ARCH_v4 | CHECK_ARM_ARCH_v5_UP)
#define CHECK_ARM_ARCH_THUMB (ARM_ARCH_v4T | CHECK_ARM_ARCH_v5_UP)

struct ARM_ARCH_INFO {
	ARM_ARCH architecture;
	struct {
		uint32_t UnallocMask;
		uint32_t UserMask;
		uint32_t PrivMask;
		uint32_t StateMask;
	} PSRBits;
	unsigned int MinPageSize;
};

struct ARM_CPU_INFO {
	int processor;
	ARM_ARCH architecture;

	struct {
		uint32_t valid;
		union {
			int32_t Word;
			struct {
				uint32_t Revision:4;
				uint32_t PrimaryPartNumber:12;
				uint32_t Variant:7;
				uint32_t A:1;
				uint32_t Implementor:8;
			} ARM7;
			struct {
				uint32_t Revision:4;
				uint32_t PrimaryPartNumber:12;
				uint32_t Architecture:4;
				uint32_t Variant:4;
				uint32_t Implementor:8;
			} PostARM7;
		} MainIDRegister;
		union {
			int32_t Word;
			struct {
				uint32_t Isize:12;
				uint32_t Dsize:12;
				uint32_t S:1;
				uint32_t ctype:4;
				uint32_t :3; // always 0
			};
		} CacheTypeRegister;
		union {
			int32_t Word;
			struct {
				uint32_t ITCM:3;
				uint32_t :13; // SBZ/UNP
				uint32_t DTCM:3;
				uint32_t :10; // SBZ/UNP
				uint32_t :3; // always 0
			};
		} TCMTypeRegister;
		union {
			int32_t Word;
			struct {
				uint32_t S:1;
				uint32_t :7; // SBZ/UNP
				uint32_t DLsize:8;
				uint32_t ILsize:8;
				uint32_t :8; // SBZ/UNP
			};
		} TLBTypeRegister;
		union {
			int32_t Word;
			struct {
				uint32_t S:1;
				uint32_t :7; // SBZ/UNP
				uint32_t DRegion:8;
				uint32_t IRegion:8;
				uint32_t :8; // SBZ/UNP
			};
		} MPUTypeRegister;
	} MMUIdentification;
};

extern ARM_CPU_INFO CPU_INFO_TAB[];
extern ARM_ARCH_INFO ARCH_INFO_TAB[];

// processor modes
enum ARM_CPU_MODE {
	ARM_CPU_MODE_USR = 0b10000,
	ARM_CPU_MODE_FIQ = 0b10001,
	ARM_CPU_MODE_IRQ = 0b10010,
	ARM_CPU_MODE_SVC = 0b10011,
	ARM_CPU_MODE_ABT = 0b10111,
	ARM_CPU_MODE_UND = 0b11011,
	ARM_CPU_MODE_SYS = 0b11111
};

union PSR_REGISTER {
	uint32_t Word;
	struct {
		uint32_t M:5;
		uint32_t T:1;
		uint32_t F:1;
		uint32_t I:1;
		uint32_t A:1;
		uint32_t E:1;
		uint32_t :6;
		uint32_t GE:4;
		uint32_t :4;
		uint32_t J:1;
		uint32_t :2;
		uint32_t Q:1;
		uint32_t V:1;
		uint32_t C:1;
		uint32_t Z:1;
		uint32_t N:1;
	};
};

#define CPSR_CAHED_MASK 0x

enum ARM_REGISTER_NAME {
	R0 = 0,
	R1 = 1,
	R2 = 2,
	R3 = 3,
	R4 = 4,
	R5 = 5,
	R6 = 6,
	R7 = 7,
	R8 = 8,
	R9 = 9,
	R10 = 10,
	R11 = 11,
	R12 = 12,
	R13 = 13,
	R14 = 14,
	R15 = 15,
	SP = 13,
	LR = 14,
	PC = 15
};

enum ARM_PENDING_INTERRUPT {
	INT_PENDING_NONE = 0,
	INT_PENDING_IRQ = 1,
	INT_PENDING_FIQ = 2
};

struct ARMCONTEXT {
	// General-purpose registers
	uint32_t regs[16];
	struct {
		// banked registers
		uint32_t R8_usr, R9_usr, R10_usr, R11_usr, R12_usr, R13_usr, R14_usr;
		uint32_t R13_svc, R14_svc;
		uint32_t R13_abt, R14_abt;
		uint32_t R13_und, R14_und;
		uint32_t R13_irq, R14_irq;
		uint32_t R8_fiq, R9_fiq, R10_fiq, R11_fiq, R12_fiq, R13_fiq, R14_fiq;
		PSR_REGISTER SPSR_usr; // fake register (does not exists)
		PSR_REGISTER SPSR_svc;
		PSR_REGISTER SPSR_abt;
		PSR_REGISTER SPSR_und;
		PSR_REGISTER SPSR_irq;
		PSR_REGISTER SPSR_fiq;
	} banked;

	// Current Program Status Register
	// cached bits are stored individually to improve speed
	PSR_REGISTER uncached_CPSR;
	// cached flags, for faster access
	union {
		uint32_t Word;
		struct {
			uint8_t N;
			uint8_t Z;
			uint8_t C;
			uint8_t V;
		};
	} flags;
	bool has_SPSR;
	bool is_priv;

	int pending_interrupt;

	// Saved Program Status Register
	PSR_REGISTER SPSR;

	ARMCONTEXT();
	CODEGEN_CALL PSR_REGISTER read_CPSR();
};

enum ADDRESS_SPACE_TYPE {
	ADDRESS_SPACE_NONE = 0,
	ADDRESS_SPACE_IO = 1,
	ADDRESS_SPACE_MEM = 2
};

struct PHYS_1MB_INFO {
	void* items[1024];
};

struct PHYS_4GB_INFO {
	void* items[4 * 1024];	// 1MB each, if lower bit is set,
							// then it is pointer to table describing each 1KB
};

#define COMPILED_PAGE_SIZE_BITS	10
#define COMPILED_PAGE_SIZE		(1 << COMPILED_PAGE_SIZE_BITS)
#define COMPILED_GRANULARITY	2

typedef void (*compiled_function)();

struct COMPILED_BLOCK_INFO {
	void* data;
	void* data_free;
	void* compiled_code[COMPILED_PAGE_SIZE >> COMPILED_GRANULARITY];
	size_t data_size;

	COMPILED_BLOCK_INFO();
	~COMPILED_BLOCK_INFO();

	void execute(unsigned int offset);
};

#define compiled_code_value(bi, index) ((compiled_function)bi->compiled_code[index >> COMPILED_GRANULARITY])
#define compiled_code_addr(bi, index) (&bi->compiled_code[index >> COMPILED_GRANULARITY])
#define has_compiled_code(bi, index) (compiled_code_value(bi, index) != __null)

union RESOLVED_ADDRESS {
	void* data;
	uint32_t ptr;
};

#define EXCEPTION_VECTOR_RESET				0x00000000
#define EXCEPTION_VECTOR_UNDEFINED			0x00000004
#define EXCEPTION_VECTOR_SWI				0x00000008
#define EXCEPTION_VECTOR_PREFETCH_ABORT		0x0000000C
#define EXCEPTION_VECTOR_DATA_ABORT			0x00000010
#define EXCEPTION_VECTOR_IRQ				0x00000018
#define EXCEPTION_VECTOR_FIQ				0x0000001C
#define EXCEPTION_VECTOR_HIGH				0xFFFF0000

typedef COMPILED_BLOCK_INFO** COMPILED_MAP;
#define COMPILED_SUBMAP_ITEMS_COUNT (1 << (16 - COMPILED_PAGE_SIZE_BITS))
#define COMPILED_MAP_ITEMS_COUNT (64 * 1024)

typedef void (CODEGEN_CALL *p_void__void)();
typedef void* (CODEGEN_CALL *p_pvoid__uint32_t)(uint32_t);

extern __thread ARMCPU* CPU;
extern __thread ARMSystemControlCoprocessorContext* MMU;

class ARMCPU {
private:
	PHYS_4GB_INFO* phys_memory;
	struct {
		void* data;

		void* no_interrupt_pending;
		void* fiq_pending;
		void* irq_pending;

		p_void__void switch_cpu_mode[32][32];
	} compiled_functions;

	DecoderContext ctx;
	pthread_t thread;
	bool powered_on;

	void* interrupt_check_function;
	AutoResetEvent idleEvent;
	Mutex interruptFunctionLock;
	CODEGEN_CALL COMPILED_BLOCK_INFO* GetCompiledData(uint32_t address);

	CODEGEN_CALL void AddCompiledMapItem(uint32_t address, COMPILED_BLOCK_INFO* bi);

	static void* MainLoopThread(void* data);
	void MainLoop();

	FORCE_INLINE void InterruptFunctionLock() { interruptFunctionLock.Lock(); };
	FORCE_INLINE void InterruptFunctionUnlock() { interruptFunctionLock.Unlock(); };

	void* CompileSwitchCPUMode(void* data);
	void CompileASMRoutines();
public:
	COMPILED_MAP compiled[COMPILED_MAP_ITEMS_COUNT];

	ARMCONTEXT context;
	ARMCoprocessor* coprocessors[16];

	ARM_CPU_INFO* CPUInfo;
	ARM_ARCH_INFO* ARCHInfo;

	ARMCPU(int model);
	virtual ~ARMCPU();

	virtual void PowerOn();
	virtual void PowerOff();
	virtual void Reset();

	p_void__void PCModifiedHelper;
	p_void__void BranchHelper;
	p_pvoid__uint32_t PA_to_Host;
	p_pvoid__uint32_t PA_to_HostMem;
	CODEGEN_CALL void ClearCompiledMap();
	CODEGEN_CALL bool InvalidateCompiledMap(uint32_t MVA);

	CODEGEN_CALL void AddPhysicalMemory(void* data, uint32_t start, uint32_t size);

	CODEGEN_CALL uint32_t ReadCPSR();
	CODEGEN_CALL bool WriteCPSR(uint32_t value, uint32_t mask);
	CODEGEN_CALL void UpdateInterruptCheckFunction();
	p_void__void SwitchCPUMode[32];

	CODEGEN_CALL void RaiseAbortException(uint32_t currentPC);
	CODEGEN_CALL void RaisePrefetchException(uint32_t currentPC);
	CODEGEN_CALL void RaiseUndefinedException(uint32_t currentPC);
	CODEGEN_CALL void RaiseSWIException(uint32_t currentPC);
	CODEGEN_CALL void RaiseIRQException(uint32_t currentPC);
	CODEGEN_CALL void RaiseFIQException(uint32_t currentPC);

	CODEGEN_CALL void SetIRQPending();
	CODEGEN_CALL void ClearIRQPending();
	CODEGEN_CALL void SetFIQPending();
	CODEGEN_CALL void ClearFIQPending();

	CODEGEN_CALL void EnterIdle();

	FORCE_INLINE bool CheckArch(int arch_to_check) { return ((int)ARCHInfo->architecture & arch_to_check) != 0; };
};

const char* arm_mode_name(ARM_CPU_MODE mode);

extern CODEGEN_CALL uint32_t IOReadWord(uint32_t address);
extern CODEGEN_CALL uint16_t IOReadHalf(uint32_t address);
extern CODEGEN_CALL uint8_t IOReadByte(uint32_t address);
extern CODEGEN_CALL void IOWriteWord(uint32_t address, uint32_t value);
extern CODEGEN_CALL void IOWriteHalf(uint32_t address, uint16_t value);
extern CODEGEN_CALL void IOWriteByte(uint32_t address, uint8_t value);

#endif /* ARMCPU_H_ */
