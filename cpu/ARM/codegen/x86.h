/*
 * x86.h
 *
 *  Created on: 15-04-2012
 *      Author: Łukasz Misek
 */

#ifndef CODEGEN_X86_H_
#define CODEGEN_X86_H_

#include "x86_common.h"

// function CALL, argument passing

#define place_branch(destination) \
	DEBUG_PLACE("BRANCH    0x%08X", destination)

#define place_ret() \
	DEBUG_PLACE("RET"); \
	emit8(0xC3)

#define place_call_argument(no, type, ...) \
	if (no == 0) { \
		place_mov_reg32__##type(regEAX, ##__VA_ARGS__); \
	} else if (no == 1) { \
		place_mov_reg32__##type(regEDX, ##__VA_ARGS__); \
	} else if (no == 2) { \
		place_mov_reg32__##type(regECX, ##__VA_ARGS__); \
	} else { \
		place_push_##type(__VA_ARGS__); \
	}

#define place_call_obj_argument(no, type, ...) \
	if (no == 0) { \
		place_mov_reg32__##type(regEDX, ##__VA_ARGS__); \
	} else if (no == 1) { \
		place_mov_reg32__##type(regECX, ##__VA_ARGS__); \
	} else { \
		place_push_##type(__VA_ARGS__); \
	}

#define place_call_imm(destination, arguments) \
	DEBUG_PLACE("CALL\t%s", #destination); \
	emit8(0xE8); \
	emit_ptr((uintptr_t)destination-(uintptr_t)data - 4)

#define place_call_obj(object, _class, function, arguments) \
	DEBUG_PLACE("CALL\t(%s*)%s->%s", #_class, #object, #function); \
	place_mov_reg__imm(regEAX, object); \
	emit8(0xE8); \
	emit_ptr((uintptr_t)reinterpret_cast<void*>(&_class::function)-(uintptr_t)data - 4)

#define place_call_ptr_imm(ptr, arguments) \
	DEBUG_PLACE("CALL\tdword ptr [%s]", PTR_NAME(ptr)); \
	emit8(0xFF); \
	emit_modRM(0b00, 2, 5); \
	emit_ptr(ptr)

#define place_breakpoint() \
	DEBUG_PLACE("INT 3"); \
	emit8(0xCC)

// jumps, labels

#define place_label(label) \
	DEBUG_PLACE("%s:", #label); \
	void* __label__##label = data

#define place_condjump_var(type, label, var) \
	DEBUG_PLACE("J%s\t%s", #type, #label); \
	emit8(0x0F); \
	emit8(0x80 | JUMP_J##type); \
	var = data; \
	emit32(0);

#define place_condjump(type, label) \
	DEBUG_PLACE("J%s\t%s", #type, #label); \
	emit8(0x0F); \
	emit8(0x80 | JUMP_J##type); \
	uint32_t* __label__##label##diff_ptr = (uint32_t*)data; \
	emit32(0)

#define place_fix_condjump(type, label) \
	place_condjump(type, label); \
	fix_label(label)

#define place_jump(label) \
	DEBUG_PLACE("JMP\t%s", #label); \
	emit8(0xE9); \
	uint32_t* __label__##label##diff_ptr = (uint32_t*)data; \
	emit32(0)

#define place_fix_jump(label) \
	place_jump(label); \
	fix_label(label)

#define fix_label_var(label, var) \
	uintptr_t __label__##label##diff = (uintptr_t)__label__##label - (uintptr_t)var - sizeof(uintptr_t); \
	*((uintptr_t*)var) = __label__##label##diff

#define fix_label(label) \
	uintptr_t __label__##label##diff = (uintptr_t)__label__##label - (uintptr_t)__label__##label##diff_ptr - sizeof(uintptr_t); \
	*__label__##label##diff_ptr = __label__##label##diff

#define place_fix_label(label) \
	place_label(label); \
	fix_label(label)

#define place_fix_label_var(label, var) \
	place_label(label); \
	fix_label_var(label, var)

// JMP

#define place_jmp_imm(addr) \
	DEBUG_PLACE("JMP\t%p", addr); \
	emit8(0xE9); \
	emit_ptr((uintptr_t)addr - (uintptr_t)data - 4)

#define place_jmp_obj(object, _class, function) \
	DEBUG_PLACE("CALL\t(%s*)%s->%s", #_class, #object, #function); \
	place_mov_reg__imm(regEAX, object); \
	emit8(0xE9); \
	emit_ptr((uintptr_t)(void*)&_class::function-(uintptr_t)data - 4)

#define place_jmp_reg(reg) \
	DEBUG_PLACE("JMP\t%s", X86_REG_NAME(reg)); \
	emit8(0xFF); \
	emit_modRM(0b11, 4, reg)

#define place_jmp_imm_reg_mul(imm, reg, mul) \
	DEBUG_PLACE("JMP\t[0x%08X + %s * %d]", imm, X86_REG_NAME(reg), mul); \
	emit8(0xFF); \
	emit_modRM(0b10, 4, 0x100); \
	emit_SIB(mul, 0b100, reg); \
	emit_ptr(imm)

// SET<flag>

#define place_set_flag__ptr8_imm(flag, ptr8) \
	DEBUG_PLACE("SET%s\tbyte ptr [%s]", #flag, PTR8_NAME(ptr8)); \
	emit8(0x0F); \
	emit_op_ptr8_imm(0x90 + JUMP_J##flag, ptr8)

#define place_set_flag__reg8(flag, reg8) \
	DEBUG_PLACE("SET%s\%s", #flag, X86_REG8_NAME(reg8)); \
	emit8(0x0F); \
	emit8(0x90 + JUMP_J##flag); \
	emit_modRM(0b11, 0, reg8)

// strings instructions

#define place_lodsd() \
	DEBUG_PLACE("LODSD"); \
	emit8(0xAD)

#define place_lodsb() \
	DEBUG_PLACE("LODSB"); \
	emit8(0xAC)

#define place_stosb() \
	DEBUG_PLACE("STOSB"); \
	emit8(0xAA)

#define place_stosd() \
	DEBUG_PLACE("STOSD"); \
	emit8(0xAB)

#define place_movsb() \
	DEBUG_PLACE("MOVSB"); \
	emit8(0xA4)

#define place_movsd() \
	DEBUG_PLACE("MOVSD"); \
	emit8(0xA5)

// CMC, CLC, STC

#define place_cmc() \
	DEBUG_PLACE("CMC"); \
	emit8(0xF5)

#define place_stc() \
	DEBUG_PLACE("STC"); \
	emit8(0xF9)

#define place_clc() \
	DEBUG_PLACE("CLC"); \
	emit8(0xF8)

// SAHF

#define place_sahf() \
	DEBUG_PLACE("SAHF"); \
	emit8(0x9E)

// LAHF

#define place_lahf() \
	DEBUG_PLACE("LAHF"); \
	emit8(0x9F)

// MOVZX

#define place_movzx_reg32__ptr8_imm(reg32, ptr8) \
	DEBUG_PLACE("MOVZX\t%s, byte ptr [%s]", X86_REG32_NAME(reg32), PTR8_NAME(ptr8)); \
	emit8(0x0F); \
	emit8(0xB6); \
	emit_modRM(0b00, reg32, 5); \
	emit_ptr(ptr8)

#define place_movzx_reg32__ptr8_reg(reg32, reg) \
	DEBUG_PLACE("MOVZX\t%s, byte ptr [%s]", X86_REG32_NAME(reg32), X86_REG_NAME(reg)); \
	emit8(0x0F); \
	emit8(0xB6); \
	emit_modRM(0b00, reg32, reg)

#define place_movzx_reg32__ptr16_reg(reg32, reg) \
	DEBUG_PLACE("MOVZX\t%s, word ptr [%s]", X86_REG32_NAME(reg32), X86_REG_NAME(reg)); \
	emit8(0x0F); \
	emit8(0xB7); \
	emit_modRM(0b00, reg32, reg)

// MOVSX

#define place_movsx_reg32__ptr8_imm(reg32, ptr8) \
	DEBUG_PLACE("MOVSX\t%s, byte ptr [%s]", X86_REG32_NAME(reg32), PTR8_NAME(ptr8)); \
	emit8(0x0F); \
	emit8(0xBE); \
	emit_modRM(0b00, reg32, 5); \
	emit_ptr(ptr8)

#define place_movsx_reg32__ptr8_reg(reg32, reg) \
	DEBUG_PLACE("MOVSX\t%s, byte ptr [%s]", X86_REG32_NAME(reg32), X86_REG_NAME(reg)); \
	emit8(0x0F); \
	emit8(0xBE); \
	emit_modRM(0b00, reg32, reg)

#define place_movsx_reg32__ptr16_reg(reg32, reg) \
	DEBUG_PLACE("MOVSX\t%s, word ptr [%s]", X86_REG32_NAME(reg32), X86_REG_NAME(reg)); \
	emit8(0x0F); \
	emit8(0xBF); \
	emit_modRM(0b00, reg32, reg)

#define place_movsx_reg32__reg16(reg32, reg16) \
	DEBUG_PLACE("MOVSX\t%s, %s", X86_REG32_NAME(reg32), X86_REG16_NAME(reg16)); \
	emit8(0x0F); \
	emit8(0xBF); \
	emit_modRM(0b11, reg32, reg16)

#define place_movsx_reg32__reg8(reg32, reg8) \
	DEBUG_PLACE("MOVSX\t%s, %s", X86_REG32_NAME(reg32), X86_REG8_NAME(reg8)); \
	emit8(0x0F); \
	emit8(0xBE); \
	emit_modRM(0b11, reg32, reg8)

// BSR

#define place_bsr_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("MOVZX\t%s, %s", X86_REG32_NAME(reg1), X86_REG_NAME32(reg2)); \
	emit8(0x0F); \
	emit8(0xBD); \
	emit_modRM(0b11, reg1, reg2)

#define place_bsr_reg32__ptr32_imm(reg32, ptr32) \
	DEBUG_PLACE("MOVZX\t%s, dword ptr [%s]", X86_REG32_NAME(reg32), PTR32_NAME32(ptr32)); \
	emit8(0x0F); \
	emit8(0xBD); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

// LEA

#define place_lea_reg32__reg32_imm(reg32, reg1, imm) \
	DEBUG_PLACE("LEA\t%s, [%s + 0x%08X]", X86_REG32_NAME(reg32), X86_REG32_NAME(reg1), imm); \
	emit8(0x8D); \
	emit_modRM(0b10, reg32, reg1); \
	emit32(imm)

// MOV

#define place_mov_reg32__reg32(reg1, reg2) \
	if (reg1 != reg2) { \
		DEBUG_PLACE("MOV\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
		emit_op_reg32__reg32(0x89, reg1, reg2); \
	}

#define place_mov_reg__reg place_mov_reg32__reg32

#define place_mov_reg8__ptr8_imm(reg8, ptr8) \
	DEBUG_PLACE("MOV\t%s, byte ptr [%s]", X86_REG8_NAME(reg8), PTR8_NAME(ptr8)); \
	if (reg8 == regAL) { \
		emit8(0xA0); \
	} else { \
		emit8(0x8A); \
		emit_modRM(0b00, reg8, 0b101); \
	} \
	emit_ptr(ptr8)

#define place_mov_reg32__ptr32_imm(reg32, ptr32) \
	DEBUG_PLACE("MOV\t%s, dword ptr [%s]", X86_REG32_NAME(reg32), PTR32_NAME(ptr32)); \
	if (reg32 == regEAX) { \
		emit8(0xA1); \
	} else { \
		emit8(0x8B); \
		emit_modRM(0b00, reg32, 0b101); \
	} \
	emit_ptr(ptr32)

#define place_mov_reg32__ptr32_reg_imm8(reg32, ptr_reg, imm8) \
	DEBUG_PLACE("MOV\t%s, dword ptr [%s + 0x%02X]", X86_REG32_NAME(reg32), X86_REG_NAME(ptr_reg), imm8); \
	emit8(0x8B); \
	emit_modRM(0b01, reg32, ptr_reg); \
	emit8(imm8)

#define place_mov_reg32__ptr32_reg(reg32, ptr_reg) \
	if (ptr_reg == regEBP) { \
		place_mov_reg32__ptr32_reg_imm8(reg32, ptr_reg, 0); \
	} else { \
		DEBUG_PLACE("MOV\t%s, dword ptr [%s]", X86_REG32_NAME(reg32), X86_REG_NAME(ptr_reg)); \
		emit8(0x8B); \
		emit_modRM(0b00, reg32, ptr_reg); \
	}

#define place_mov_ptr32_reg__reg32(ptr_reg, reg32) \
	DEBUG_PLACE("MOV\tdword ptr [%s], %s", X86_REG_NAME(ptr_reg), X86_REG32_NAME(reg32)); \
	emit8(0x89); \
	emit_modRM(0b00, reg32, ptr_reg)

// TODO: 8-bit displacement
#define place_mov_ptr32_reg_imm__reg32(ptr_reg, disp, reg32) \
	DEBUG_PLACE("MOV\tdword ptr [%s + 0x%08X], %s", X86_REG_NAME(ptr_reg), disp, X86_REG32_NAME(reg32)); \
	emit8(0x89); \
	emit_modRM(0b10, reg32, ptr_reg); \
	emit32(disp)

#define place_mov_ptr8_reg__reg8(ptr_reg, reg8) \
	DEBUG_PLACE("MOV\tbyte ptr [%s], %s", X86_REG_NAME(ptr_reg), X86_REG8_NAME(reg8)); \
	emit8(0x88); \
	emit_modRM(0b00, reg8, ptr_reg)

// TODO: 8-bit displacement
#define place_mov_ptr8_reg_imm__imm(ptr_reg, disp, imm) \
	DEBUG_PLACE("MOV\tbyte ptr [%s + 0x%08X], 0x02X", X86_REG_NAME(ptr_reg), disp, imm); \
	emit8(0xC6); \
	emit_modRM(0b10, 0, ptr_reg); \
	emit32(disp); \
	emit8(imm)

#define place_mov_ptr16_reg__reg16(ptr_reg, reg16) \
	DEBUG_PLACE("MOV\tword ptr [%s], %s", X86_REG_NAME(ptr_reg), X86_REG16_NAME(reg16)); \
	emit8(0x66); \
	emit8(0x89); \
	emit_modRM(0b00, reg16, ptr_reg)

#define place_mov_reg32__ptr32_reg_reg_mul(reg32, base_reg, disp_reg, mul) \
	DEBUG_PLACE("MOV\t%s, dword ptr [%s + %s * %d]", X86_REG32_NAME(reg32), X86_REG32_NAME(base_reg), X86_REG32_NAME(disp_reg), 1 << mul); \
	emit8(0x8B); \
	emit_modRM(0b00, reg32, 0b100); \
	emit_SIB(mul, base_reg, disp_reg)

#define place_mov_reg__ptr_reg_reg_mul place_mov_reg32__ptr32_reg_reg_mul

#define place_mov_reg32__ptr32_reg_reg_mul_imm(reg32, base_reg, disp_reg, mul, imm) \
	DEBUG_PLACE("MOV\t%s, dword ptr [%s + %s * %d + %s]", X86_REG32_NAME(reg32), X86_REG32_NAME(base_reg), X86_REG32_NAME(disp_reg), 1 << mul, PTR32_NAME(imm)); \
	emit8(0x8B); \
	emit_modRM(0b10, reg32, 0b100); \
	emit_SIB(mul, base_reg, disp_reg); \
	emit_ptr(imm)

#define place_mov_reg__ptr_reg_reg_mul_imm place_mov_reg32__ptr32_reg_reg_mul_imm

#define place_mov_reg32__ptr32_imm_reg_mul(reg32, base_imm, disp_reg, mul) \
	DEBUG_PLACE("MOV\t%s, dword ptr [%s + %s * %d]", X86_REG32_NAME(reg32), X86_PTR32_NAME(base_imm), X86_REG32_NAME(disp_reg), 1 << mul); \
	emit8(0x8B); \
	emit_modRM(0b00, reg32, 0b100); \
	emit_SIB(mul, 5, disp_reg); \
	emit_ptr(base_imm)

#define place_mov_reg__ptr_imm_reg_mul place_mov_reg32__ptr32_imm_reg_mul

#define place_mov_ptr32_reg_reg_mul__reg32(base_reg, disp_reg, mul, reg32) \
	DEBUG_PLACE("MOV\tdword ptr [%s + %s * %d], %s", X86_REG32_NAME(base_reg), X86_REG32_NAME(disp_reg), 1 << mul, X86_REG32_NAME(reg32)); \
	emit8(0x89); \
	emit_modRM(0b00, reg32, 0b100); \
	emit_SIB(mul, base_reg, disp_reg)

#define place_mov_reg8__imm(reg8, imm) \
	DEBUG_PLACE("MOV\t%s, 0x%08X", X86_REG8_NAME(reg8), imm); \
	emit8(0xB0 + reg8); \
	emit8(imm)

#define place_mov_reg32__imm(reg32, imm) \
	DEBUG_PLACE("MOV\t%s, 0x%08X", X86_REG32_NAME(reg32), (uint32_t)imm); \
	emit8(0xB8 + reg32); \
	emit32(imm)

#define place_mov_reg__imm place_mov_reg32__imm

#define place_mov_reg__imm place_mov_reg32__imm

#define place_mov_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("MOV\tdword ptr [%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	if (reg32 == regEAX) { \
		emit8(0xA3); \
	} else { \
		emit8(0x89); \
		emit_modRM(0b00, reg32, 0b101); \
	} \
	emit_ptr(ptr32)

#define place_mov_ptr_imm__reg place_mov_ptr32_imm__reg32

#define place_mov_ptr32_imm__imm(ptr32, imm) \
	DEBUG_PLACE("MOV\tdword ptr [%s], 0x%08X", PTR32_NAME(ptr32), imm); \
	emit_op_ptr32_imm__imm32(0xC7, 0, ptr32, imm)

#define place_mov_ptr8_imm__imm(ptr8, imm) \
	DEBUG_PLACE("MOV\tbyte ptr [%s], 0x%02X", PTR8_NAME(ptr8), imm); \
	emit_op_ptr8_imm__imm8(0xC6, 0, ptr8, imm)

#define place_mov_ptr8_imm__reg8(ptr8, reg8) \
	DEBUG_PLACE("MOV\tbyte ptr [%s], %s", PTR8_NAME(ptr8), X86_REG8_NAME(reg8)); \
	if (reg8 == regAL) { \
		emit8(0xA0); \
	} else { \
		emit8(0x88); \
		emit_modRM(0b00, reg8, 0b101); \
	} \
	emit_ptr(ptr8)

// CMOV<flag>

#define place_cmov_flag_reg32__ptr32_reg(flag, reg32, ptr_reg) \
	DEBUG_PLACE("CMOV%s\t%s, dword ptr [%s]", #flag, X86_REG32_NAME(reg32), X86_REG_NAME(ptr_reg)); \
	emit8(0x0F); \
	emit8(0x40 + JUMP_J##flag); \
	emit_modRM(0b00, ptr_reg, reg32)

// CMP

#define place_cmp_reg8__imm(reg8, imm) \
	DEBUG_PLACE("CMP\%s, 0x%02X", REG8_NAME(reg8), imm); \
	if (reg8 == regAL) { \
		emit8(0x3C); \
	} else { \
		emit8(0x80); \
		emit_modRM(0b11, 7, reg8); \
	} \
	emit8(imm)

#define place_cmp_reg8__reg8(reg1, reg2) \
	DEBUG_PLACE("CMP\%s, %s", REG8_NAME(reg1), REG8_NAME(reg2)); \
	emit8(0x38); \
	FIXME(); emit_modRM(0b11, reg1, reg2)

#define place_cmp_reg8__ptr8_imm(reg8, ptr8) \
	DEBUG_PLACE("CMP\ %s, byte ptr [%s]", REG8_NAME(reg8), PTR8_NAME(ptr8)); \
	emit8(0x3A); \
	emit_modRM(0b00, reg8, 5); \
	emit_ptr(ptr8);

#define place_cmp_ptr8_imm__imm(ptr8, imm) \
	DEBUG_PLACE("CMP\tbyte ptr [%s], 0x%02X", PTR8_NAME(ptr8), imm); \
	emit_op_ptr8_imm__imm8(0x80, 7, ptr8, imm)

#define place_cmp_ptr32_imm__imm(ptr32, imm) \
	DEBUG_PLACE("CMP\tdword ptr [%s], 0x%08X", PTR32_NAME(ptr32), imm); \
	emit_op_ptr32_imm__imm32(0x81, 7, ptr32, imm)

#define place_cmp_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("CMP\tdword ptr [%s], %s", PTR32_NAME(ptr32), reg32); \
	emit8(0x39); \
	emit_modRM(0b00, reg32, 5); \
	emit_ptr(ptr32)

#define place_cmp_ptr32_reg__imm(reg, imm) \
	DEBUG_PLACE("CMP\tdword ptr [%s], 0x%08X", X86_REG_NAME(reg), imm); \
	emit_op_ptr32_reg__imm32(0x81, 7, reg, imm)

#define place_cmp_ptr8_reg__imm(reg, imm) \
	DEBUG_PLACE("CMP\tbyte ptr [%s], 0x%08X", X86_REG_NAME(reg), imm); \
	emit_op_ptr8_reg__imm8(0x80, 7, reg, imm)

// PUSH

#define place_push_imm(imm) \
	if ((uintptr_t)imm <= 0x7F) { \
		DEBUG_PLACE("PUSH\t0x%02X", (uintptr_t)imm); \
		emit8(0x6A); \
		emit8((uint8_t)(uintptr_t)imm); \
	} else { \
		DEBUG_PLACE("PUSH\t0x%08X", (uintptr_t)imm);\
		emit8(0x68); \
		emit_ptr((uintptr_t)imm); \
	}

#define place_push_reg32(reg32) \
	DEBUG_PLACE("PUSH\t%s", X86_REG32_NAME(reg32)); \
	emit8(0x50 + reg32)

#define place_push_reg place_push_reg32

#define place_push_ptr32_reg(reg) \
	DEBUG_PLACE("PUSH\t[%s]", X86_REG_NAME(reg)); \
	emit8(0xFF); \
	emit_modRM(0b00, 6, reg)

#define place_push_ptr32_imm(ptr32) \
	DEBUG_PLACE("PUSH\t[%s]", PTR32_NAME(ptr32)); \
	emit8(0xFF); \
	emit_modRM(0b00, 6, 5); \
	emit_ptr(ptr32)

#define place_push_ptr32_reg_reg_mul(reg_base, reg_index, mul) \
	DEBUG_PLACE("PUSH\t[%s + %s]", X86_REG_NAME(reg_base), X86_REG_NAME(reg_index)); \
	FIXME()

// POP

#define place_pop_reg32(reg32) \
	DEBUG_PLACE("POP\t%s", X86_REG32_NAME(reg32)); \
	emit8(0x58 + reg32)

#define place_pop_reg place_pop_reg32

// INC

#define place_inc_reg32(reg32) \
	DEBUG_PLACE("INC\t%s", X86_REG32_NAME(reg32)); \
	emit8(0x40 + reg32)

// DEC

#define place_dec_reg32(reg32) \
	DEBUG_PLACE("DEC\t%s", X86_REG32_NAME(reg32)); \
	emit8(0x48 + reg32)

// SUB

#define place_sub_ptr32_imm__imm(ptr32, imm) \
	if ((unsigned int)imm <= 0x7F) { \
		DEBUG_PLACE("SUB\t[%s], 0x%02X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm8(0x83, 5, ptr32, imm); \
	} else { \
		DEBUG_PLACE("SUB\t[%s], 0x%08X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm32(0x81, 5, ptr32, imm); \
	}

#define place_sub_reg32__ptr32_imm(reg32, ptr32) \
	DEBUG_PLACE("SUB\t%s, dword ptr [%s]", X86_REG32_NAME(reg32), PTR32_NAME(ptr32)); \
	emit8(0x2B); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_sub_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("SUB\tdword ptr [%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	emit8(0x29); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_sub_reg32__imm(reg32, imm) \
	DEBUG_PLACE("SUB\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x2D); \
	} else { \
		emit8(0x81); \
		emit_modRM(0b11, 0b101, reg32); \
	} \
	emit32(imm)

#define place_sub_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("SUB\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x29, reg1, reg2)

#define place_sub_reg__reg place_sub_reg32__reg32

// SBB

#define place_sbb_reg32__imm(reg32, imm) \
	DEBUG_PLACE("SBB\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x1D); \
	} else { \
		emit8(0x81); \
		emit_modRM(0b11, 0b011, reg32); \
	} \
	emit32(imm)

#define place_sbb_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("SBB\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x1B, reg2, reg1)

// ADC

#define place_adc_reg32__imm(reg32, imm) \
	DEBUG_PLACE("ADC\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x15); \
	} else { \
		emit_op_reg32__reg32(0x81, reg32, 2); \
	} \
	emit32(imm)

#define place_adc_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("ADC\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x11, reg1, reg2)

// ADD

#define place_add_ptr32_imm__imm(ptr32, imm) \
	if ((unsigned int)imm <= 0x7F) { \
		DEBUG_PLACE("ADD\t[%s], 0x%02X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm8(0x83, 0, ptr32, imm); \
	} else { \
		DEBUG_PLACE("ADD\t[%s], 0x%08X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm32(0x81, 0, ptr32, imm); \
	}

#define place_add_reg32__imm(reg32, imm) \
	DEBUG_PLACE("ADD\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x05); \
	} else { \
		emit_op_reg32__reg32(0x81, reg32, 0); \
	} \
	emit32(imm)

#define place_add_reg32__ptr32_imm(reg32, ptr32) \
	DEBUG_PLACE("ADD\t%s, dword ptr [%s]", X86_REG32_NAME(reg32), PTR32_NAME(ptr32)); \
	emit8(0x03); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_add_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("ADD\tdword ptr [%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	emit8(0x01); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_add_reg__imm place_add_reg32__imm

#define place_add_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("ADD\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x01, reg1, reg2)

#define place_add_reg__reg place_add_reg32__reg32

// IMUL

#define place_imul_reg32(reg32) \
	DEBUG_PLACE("IMUL\t%s", X86_REG32_NAME(reg32)); \
	emit8(0xF7); \
	emit_modRM(0b11, 5, reg32)

#define place_imul_ptr32_imm(ptr32) \
	DEBUG_PLACE("IMUL\tdword ptr [%s]", PTR32_NAME(ptr32)); \
	emit_op_ptr32_imm(0xF7, 5, ptr32)

// MUL

#define place_mul_reg32(reg32) \
	DEBUG_PLACE("MUL\t%s", X86_REG32_NAME(reg32)); \
	emit8(0xF7); \
	emit_modRM(0b11, 4, reg32)

#define place_mul_ptr32_imm(ptr32) \
	DEBUG_PLACE("MUL\tdword ptr [%s]", PTR32_NAME(ptr32)); \
	emit_op_ptr32_imm(0xF7, 4, ptr32)

// AND

#define place_and_ptr32_imm__imm(ptr32, imm) \
	if ((unsigned int)imm <= 0x7F) { \
		DEBUG_PLACE("AND\t[%s], 0x%02X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm8(0x83, 4, ptr32, imm); \
	} else { \
		DEBUG_PLACE("AND\t[%s], 0x%08X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm32(0x81, 4, ptr32, imm); \
	}

#define place_and_reg32__imm(reg32, imm) \
	DEBUG_PLACE("AND\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x25); \
	} else { \
		emit_op_reg32__reg32(0x81, reg32, 4); \
	} \
	emit32(imm)

#define place_and_reg__imm place_and_reg32__imm

#define place_and_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("AND\tdword ptr [%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	emit8(0x21); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_and_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("AND\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x21, reg1, reg2)

#define place_and_reg8__imm(reg8, imm) \
	DEBUG_PLACE("AND\t%s, 0x%08X", X86_REG8_NAME(reg8), imm); \
	if (reg8 == regAL) { \
		emit8(0x24); \
	} else { \
		emit_op_reg32__reg32(0x82, reg8, 4); \
	} \
	emit8(imm)

// OR

#define place_or_ptr32_imm__imm(ptr32, imm) \
	if ((unsigned int)imm <= 0x7F) { \
		DEBUG_PLACE("OR\t[%s], 0x%02X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm8(0x83, 1, ptr32, imm); \
	} else { \
		DEBUG_PLACE("OR\t[%s], 0x%08X", PTR32_NAME(ptr32), imm); \
		emit_op_ptr32_imm__imm32(0x81, 1, ptr32, imm); \
	}

#define place_or_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("OR\t[%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	emit_op_ptr32_imm__reg32(0x09, reg32, ptr32, 0)

#define place_or_reg32__ptr32_imm(reg32, ptr32) \
	DEBUG_PLACE("OR\t%s, [%s]", X86_REG32_NAME(reg32), PTR32_NAME(ptr32)); \
	emit8(0x0B); \
	emit_modRM(0, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_or_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("OR\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x09, reg1, reg2)

#define place_or_reg__reg place_or_reg32__reg32

#define place_or_reg32__imm(reg32, imm) \
	DEBUG_PLACE("OR\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x0D); \
	} else { \
		emit8(0x81); \
		emit_modRM(0b11, 1, reg32); \
	} \
	emit32(imm)

// XOR

#define place_xor_reg8__imm(reg8, imm) \
	DEBUG_PLACE("XOR\t%s, 0x%02X", X86_REG8_NAME(reg8), imm); \
	if (reg8 == regAL) { \
		emit8(0x34); \
	} else { \
		emit8(0x80); \
		emit_modRM(0b11, 6, reg8); \
	} \
	emit8(imm)

#define place_xor_reg32__imm(reg32, imm) \
	DEBUG_PLACE("XOR\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0x35); \
	} else { \
		emit_op_reg32__reg32(0x81, reg32, 6); \
	} \
	emit32(imm)

#define place_xor_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("XOR\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x33, reg2, reg1)

#define place_xor_ptr32_imm__imm(ptr32, imm) \
	DEBUG_PLACE("XOR\tdword ptr [%s], 0x%08X", PTR32_NAME(ptr32), imm); \
	emit8(0x81); \
	emit_modRM(0b00, 6, 0b101); \
	emit_ptr(ptr32); \
	emit32(imm)

#define place_xor_ptr32_imm__reg32(ptr32, reg32) \
	DEBUG_PLACE("XOR\tdword ptr [%s], %s", PTR32_NAME(ptr32), X86_REG32_NAME(reg32)); \
	emit8(0x31); \
	emit_modRM(0b00, reg32, 0b101); \
	emit_ptr(ptr32)

#define place_xor_reg__reg place_xor_reg32__reg32

// TEST

#define place_test_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("TEST\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	emit_op_reg32__reg32(0x85, reg1, reg2)

#define place_test_reg__reg place_test_reg32__reg32

#define place_test_reg32__imm(reg32, imm) \
	DEBUG_PLACE("TEST\t%s, 0x%08X", X86_REG32_NAME(reg32), imm); \
	if (reg32 == regEAX) { \
		emit8(0xA9); \
	} else { \
		emit8(0xF7); \
		emit_modRM(0b11, 0, reg32); \
	} \
	emit32(imm)

#define place_test_reg8__imm(reg8, imm) \
	DEBUG_PLACE("TEST\t%s, 0x%02X", X86_REG8_NAME(reg8), imm); \
	if (reg8 == regAL) { \
		emit8(0xA8); \
	} else { \
		emit8(0xF6); \
		emit_modRM(0b11, 0, reg8); \
	} \
	emit8(imm)

// ROR

#define place_ror_reg8__imm(reg8, imm) \
	DEBUG_PLACE("ROR\t%s, %d", X86_REG8_NAME(reg8), imm); \
	if (imm == 1) { \
		emit8(0xD0); \
		emit_modRM(0b11, 1, reg32); \
	} else { \
		emit8(0xC0); \
		emit_modRM(0b11, 1, reg32); \
		emit8(imm); \
	}

#define place_ror_reg32__imm(reg32, imm) \
	DEBUG_PLACE("ROR\t%s, %d", X86_REG32_NAME(reg32), imm); \
	if (imm == 1) { \
		emit8(0xD1); \
		emit_modRM(0b11, 1, reg32); \
	} else { \
		emit8(0xC1); \
		emit_modRM(0b11, 1, reg32); \
		emit8(imm); \
	}

#define place_ror_reg32__CL(reg32) \
	DEBUG_PLACE("ROR\t%s, CL", X86_REG32_NAME(reg32)); \
	emit8(0xD3); \
	emit_modRM(0b11, 1, reg32)

// ROL

#define place_rol_reg8__imm(reg8, imm) \
	DEBUG_PLACE("ROL\t%s, %d", X86_REG8_NAME(reg8), imm); \
	if (imm == 1) { \
		emit8(0xD0); \
		emit_modRM(0b11, 0, reg32); \
	} else { \
		emit8(0xC0); \
		emit_modRM(0b11, 0, reg32); \
		emit8(imm); \
	}

#define place_rol_reg32__imm(reg32, imm) \
	DEBUG_PLACE("ROL\t%s, %d", X86_REG32_NAME(reg32), imm); \
	if (imm == 1) { \
		emit8(0xD1); \
		emit_modRM(0b11, 0, reg32); \
	} else { \
		emit8(0xC1); \
		emit_modRM(0b11, 0, reg32); \
		emit8(imm); \
	}

#define place_rol_reg32__CL(reg32) \
	DEBUG_PLACE("ROL\t%s, CL", X86_REG32_NAME(reg32)); \
	emit8(0xD3); \
	emit_modRM(0b11, 0, reg32)

// SHR

#define place_shr_reg8__imm(reg8, imm) \
	DEBUG_PLACE("SHR\t%s, %d", X86_REG8_NAME(reg8), imm); \
	if (imm == 1) { \
		emit8(0xD0); \
		emit_modRM(0b11, 5, reg32); \
	} else { \
		emit8(0xC0); \
		emit_modRM(0b11, 5, reg32); \
		emit8(imm); \
	}

#define place_shr_reg32__imm(reg32, imm) \
	DEBUG_PLACE("SHR\t%s, %d", X86_REG32_NAME(reg32), imm); \
	if (imm == 1) { \
		emit8(0xD1); \
		emit_modRM(0b11, 5, reg32); \
	} else { \
		emit8(0xC1); \
		emit_modRM(0b11, 5, reg32); \
		emit8(imm); \
	}

#define place_shr_reg32__CL(reg32) \
	DEBUG_PLACE("SHR\t%s, CL", X86_REG32_NAME(reg32)); \
	emit8(0xD3); \
	emit_modRM(0b11, 5, reg32)

// SAR

#define place_sar_reg8__imm(reg8, imm) \
	DEBUG_PLACE("SAR\t%s, %d", X86_REG8_NAME(reg8), imm); \
	if (imm == 1) { \
		emit8(0xD0); \
		emit_modRM(0b11, 7, reg32); \
	} else { \
		emit8(0xC0); \
		emit_modRM(0b11, 7, reg32); \
		emit8(imm); \
	}

#define place_sar_reg32__imm(reg32, imm) \
	DEBUG_PLACE("SAR\t%s, %d", X86_REG32_NAME(reg32), imm); \
	if (imm == 1) { \
		emit8(0xD1); \
		emit_modRM(0b11, 7, reg32); \
	} else { \
		emit8(0xC1); \
		emit_modRM(0b11, 7, reg32); \
		emit8(imm); \
	}

#define place_sar_reg32__CL(reg32) \
	DEBUG_PLACE("SAR\t%s, CL", X86_REG32_NAME(reg32)); \
	emit8(0xD3); \
	emit_modRM(0b11, 7, reg32)

// SHL

#define place_shl_reg8__imm(reg8, imm) \
		FIXME(); \
	DEBUG_PLACE("SHL\t%s, %d", X86_REG8_NAME(reg8), imm); \
	if (imm == 1) { \
		emit8(0xD0); \
		emit_modRM(0b11, 4, reg32); \
	} else { \
		emit8(0xC0); \
		emit_modRM(0b11, 4, reg32); \
		emit8(imm); \
	}

#define place_shl_reg32__imm(reg32, imm) \
	DEBUG_PLACE("SHL\t%s, %d", X86_REG32_NAME(reg32), imm); \
	if (imm == 1) { \
		emit8(0xD1); \
		emit_modRM(0b11, 4, reg32); \
	} else { \
		emit8(0xC1); \
		emit_modRM(0b11, 4, reg32); \
		emit8(imm); \
	}

#define place_shl_reg32__CL(reg32) \
	DEBUG_PLACE("SHL\t%s, CL", X86_REG32_NAME(reg32)); \
	emit8(0xD3); \
	emit_modRM(0b11, 4, reg32)

// NOT

#define place_not_reg32(reg32) \
	DEBUG_PLACE("NOT\t%s", X86_REG32_NAME(reg32)); \
	emit_op_reg32__reg32(0xF7, reg32, 2)

// XCHG

#define place_xchg_reg32__reg32(reg1, reg2) \
	DEBUG_PLACE("NOT\t%s, %s", X86_REG32_NAME(reg1), X86_REG32_NAME(reg2)); \
	if (reg1 == regEAX) { \
		emit8(0x90 + reg2); \
	} else { \
		FIXME(); \
	}


// additional macros

#define place_align(al) \
	data = (void*)(((uintptr_t)data + al) & ~(al - 1))

#define place_ret_C() \
{ \
	place_condjump(NC, ___C_flag_is_not_set); \
	place_ret(); \
	place_fix_label(___C_flag_is_not_set); \
}

#define place_ret_CL_false() \
{ \
	place_cmp_reg8__imm(regCL, 0); \
	place_condjump(NZ, ___CL_is_not_zero); \
	place_ret(); \
	place_fix_label(___CL_is_not_zero); \
}

#define place_ret_AL_imm(imm) \
{ \
	place_cmp_reg8__imm(regAL, imm); \
	place_condjump(NZ, ___AL_not_equa); \
	place_ret(); \
	place_fix_label(___AL_not_equa); \
}

#define place_ret_AL_true() \
{ \
	place_cmp_reg8__imm(regAL, 0); \
	place_condjump(Z, ___AL_is_zero); \
	place_ret(); \
	place_fix_label(___AL_is_zero); \
}

#define place_ret_AL_false() place_ret_AL_imm(0)

#endif /* CODEGEN_ARM_X86_H_ */
