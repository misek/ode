/*
 * x86_common.h
 *
 *  Created on: 18-04-2012
 *      Author: Łukasz Misek
 */

#ifndef CODEGEN_X86_COMMON_H_
#define CODEGEN_X86_COMMON_H_

#define JUMP_JO		0
#define JUMP_JNO	1
#define JUMP_JB		2
#define JUMP_JNAE	2
#define JUMP_JC		2
#define JUMP_JNB	3
#define JUMP_JAE	3
#define JUMP_JNC	3
#define JUMP_JZ		4
#define JUMP_JE		4
#define JUMP_JNZ	5
#define JUMP_JNE	5
#define JUMP_JBE	6
#define JUMP_JNA	6
#define JUMP_JNBE	7
#define JUMP_JA		7
#define JUMP_JS		8
#define JUMP_JNS	9
#define JUMP_JP		10
#define JUMP_JPE	10
#define JUMP_JNP	11
#define JUMP_JP0	11
#define JUMP_JL		12
#define JUMP_JNGE	12
#define JUMP_JNL	13
#define JUMP_JGE	13
#define JUMP_JLE	14
#define JUMP_JNG	14
#define JUMP_JNLE	15
#define JUMP_JG		15

// helper macros

#define mul1		0
#define mul2		1
#define	mul4		2
#define mul8		3
#if __WORDSIZE == 64
#define mul_ptr		mul8
#else
#define mul_ptr		mul4
#endif

#define modRM(mod, reg, rm) (((mod) << 6) + ((reg) << 3) + rm)
#define emit_modRM(mod, reg, rm) emit8(modRM(mod, reg, rm))
#define SIB(SS, base, index) (((SS) << 6) + ((index) << 3) + (base))
#define emit_SIB(SS, base, index) emit8(SIB(SS, base, index))

#define emit_op_ptr32_imm__imm32(op, reg, ptr32, imm32) \
	emit8(op); \
	emit_modRM(0, reg, 0b101); \
	emit_ptr(ptr32); \
	emit32(imm32)

#define emit_op_ptr32_reg__imm32(op, reg, ptr_reg, imm32) \
	emit8(op); \
	emit_modRM(0, reg, ptr_reg); \
	emit32(imm32)

#define emit_op_ptr8_reg__imm8(op, reg, ptr_reg, imm8) \
	emit8(op); \
	emit_modRM(0, reg, ptr_reg); \
	emit8(imm8)

#define emit_op_ptr8_imm__imm8(op, reg, ptr8, imm8) \
	emit8(op); \
	emit_modRM(0, reg, 0b101); \
	emit_ptr(ptr8); \
	emit8(imm8)

#define emit_op_ptr32_imm__imm8 emit_op_ptr8_imm__imm8

#define emit_op_ptr32_imm(op, reg, ptr32) \
	emit8(op); \
	emit_modRM(0, reg, 0b101); \
	emit_ptr(ptr32)

#define emit_op_ptr32_imm__reg32(op, reg, ptr32, reg32) \
	FIXME(); \
	emit8(op); \
	emit_modRM(0, reg, 0b101); \
	emit_ptr(ptr32)

#define emit_op_ptr8_imm(op, ptr8) \
	emit8(op); \
	emit_modRM(0, 0, 0b101); \
	emit_ptr(ptr8)

#define emit_op_reg32__reg32(op, reg1, reg2) \
	emit8(op); \
	emit_modRM(0b11, reg2, reg1)

#define STACK(n) (4 * n)

#endif /* X86_COMMON_H_ */
