#include "AT88SC.h"

AT88SCDevice::AT88SCDevice(): I2CDevice()
{
	cmd_bytes = 0;
	user_zone_size = 0;
	user_zones_count = 0;
	page_size = 0;
	current_zone = 0;
	state = st_invalid;
	ucCM_Authenticate = false;
	ucCM_Encrypt = false;
	memset(user_zones, 0, sizeof(user_zones));
	memset(&config_zone, 0xFF, sizeof(config_zone));
}

AT88SCDevice::~AT88SCDevice()
{
	FreeMemory();
}

void AT88SCDevice::Initialize()
{
	memset(&config_zone, 0xFF, sizeof(config_zone));
	InitMemory();
}

void AT88SCDevice::InitMemory()
{
	FreeMemory();
	for (unsigned int i = 0; i < user_zones_count; i++) {
		user_zones[i] = new uint8_t[user_zone_size];
		memset(user_zones[i], 0xFF, user_zone_size);
	}
}

void AT88SCDevice::FreeMemory()
{
	for (int i = 0; i < 16; i++) {
		if (user_zones[i]) {
			delete user_zones[i];
			user_zones[i] = __null;
		}
	}
}

bool AT88SCDevice::Load(const char* filename)
{
	FILE* fIn = fopen(filename, "rb");
	bool result = false;
	if (fIn) {
		result = Load(fIn);
		fclose(fIn);
	}
	return result;
}

bool AT88SCDevice::Load(FILE* fIn)
{
	if (fread(&user_zones_count, sizeof(int), 1, fIn) != 1) return false;
	if (fread(&user_zone_size, sizeof(int), 1, fIn) != 1) return false;
	if (fread(&page_size, sizeof(int), 1, fIn) != 1) return false;
	if (fread(&config_zone, sizeof(config_zone), 1, fIn) != 1) return false;
	// read user zones
	InitMemory();
	for (unsigned int i = 0; i < user_zones_count; i++) {
		if (fread(user_zones[i], user_zone_size, 1, fIn) != 1)
			return false;
	}
	return true;
}

bool AT88SCDevice::LoadFromMemory(void* fMemory)
{
#define _memcpy(dest, len) memcpy(dest, fMemory, len); fMemory = (void*)((uintptr_t)fMemory + len)

	_memcpy(&user_zones_count, sizeof(int));
	_memcpy(&user_zone_size, sizeof(int));
	_memcpy(&page_size, sizeof(int));
	_memcpy(&config_zone, sizeof(config_zone));
	// read user zones
	InitMemory();
	for (unsigned int i = 0; i < user_zones_count; i++) {
		_memcpy(user_zones[i], user_zone_size);
	}
	return true;
#undef _memcpy
}

bool AT88SCDevice::Save(const char* filename)
{
	FILE* fIn = fopen(filename, "wb");
	bool result = false;
	if (fIn) {
		result = Save(fIn);
		fclose(fIn);
	}
	return result;
}

bool AT88SCDevice::Save(FILE* fOut)
{
	if (fwrite(&user_zones_count, sizeof(int), 1, fOut) != 1) return false;
	if (fwrite(&user_zone_size, sizeof(int), 1, fOut) != 1) return false;
	if (fwrite(&page_size, sizeof(int), 1, fOut) != 1) return false;
	if (fwrite(&config_zone, sizeof(config_zone), 1, fOut) != 1) return false;
	// write user zones
	for (unsigned int i = 0; i < user_zones_count; i++) {
		if (fwrite(user_zones[i], user_zone_size, 1, fOut) != 1)
			return false;
	}
	return true;
}

void AT88SCDevice::Start()
{
	I2CDevice::Start();
	if (cmd_bytes == 1) {
		if (AT88SC_Command_Allowed(cmd[0])) {
			switch (cmd[0] & 0x0F) {
				case 0x08:
					// reset crypto
					ucCM_Authenticate = false;
					ucCM_Encrypt = false;
					cm_ResetCrypto();
					break;
			}
		}
	}
	cmd_bytes = 0;
	state = st_command;
}

void AT88SCDevice::ACK(int ack)
{
	I2CDevice::ACK(ack);
	if (!ack) {
		switch (state) {
			case st_data_send:
				if ((bytes_remaining & 0xFF) == cmd[3]) {
					// ack after first byte, already transfered, so ignore it
					--bytes_remaining;
				} else {
					if (bytes_remaining--)
						AT88SC_DataRead(&data);
				}
				break;
			default:
				//ASSERT(false);
				break;
		}
	}
}

int AT88SCDevice::ACK()
{
	I2CDevice::ACK();
	int ret = 1;
	switch (state) {
		case st_invalid:
			return 1;
		case st_command:
			switch (cmd_bytes) {
				case 1:
					if (AT88SC_Command_Allowed(cmd[0]))
						return 0; else
						return 1;
				case 2:
				case 3:
					return 0;
				case 4:
					ret = AT88SC_Command(cmd[0]);
					if (state == st_data_send) {
						ret = AT88SC_DataRead(&data);
					}
					return ret;
			}
			break;
		case st_data_receive:
			return AT88SC_DataReceived(cmd[4 + cmd[3] - bytes_remaining--]);
		default:
			//ASSERT(false);
			break;
	}
	return 1;
}

void AT88SCDevice::Write(uint8_t b)
{
	I2CDevice::Write(b);

	switch (state) {
		case st_invalid:
			break;
		case st_command:
			if (cmd_bytes < 4)
				cmd[cmd_bytes++] = b;
			if (cmd_bytes == 4) {
				bytes_remaining = cmd[3];
			}
			break;
		case st_data_receive:
			cmd[4 + cmd[3] - bytes_remaining] = b;
			break;
		default:
			//ASSERT(false);
			break;
	}
}

void AT88SCDevice::ReturnBytes(uint8_t *data, int length)
{
}

bool AT88SCDevice::AT88SC_Command_Allowed(uint8_t c)
{
	if (((c & 0xF0) == 0xB0) || ((c >> 4) == config_zone.Data.AccessControl.DCR.CS)) {
		switch(c & 0x0F) {
			case 0x02:
			case 0x04:
			case 0x06:
			case 0x08:
			case 0x0A:
				return true;
		}
	}
	return false;
}

int AT88SCDevice::AT88SC_Command(uint8_t c)
{
	cm_GPAcmd2(cmd);
	DBG_I2C("AT88SC: command %02X %02X %02X %02X\n", cmd[0], cmd[1], cmd[2], cmd[3]);
	switch (c & 0x0F) {
		case 0x02:
			return AT88SC_ReadUserZone();
		case 0x04:
			return AT88SC_SystemWRITE();
		case 0x06:
			return AT88SC_SystemREAD();
		case 0x08:
			return AT88SC_VerifyCrypto();
		case 0x0A:
			return AT88SC_VerifyPassword();
	}
	return 1;
}

int AT88SCDevice::AT88SC_DataReceived(uint8_t d)
{
	switch (cmd[0] & 0x0F) {
		case 0x08:
			return AT88SC_VerifyCrypto_Data(d);
		case 0x0A:
			return AT88SC_VerifyPassword_Data(d);
	}
	return 1;
}

int AT88SCDevice::AT88SC_DataRead(uint8_t *d)
{
	switch (cmd[0] & 0x0F) {
		case 0x06:
			return AT88SC_SystemREAD_Data(d);
		case 0x02:
			return AT88SC_ReadUserZone_Data(d);
	}
	return 1;
}

int AT88SCDevice::AT88SC_SystemWRITE()
{
	switch(cmd[1]) {
		case 0x00:
			// write config zone
			break;
		case 0x01:
			// write fuses
			break;
		case 0x02:
			// send checksum
			break;
		case 0x03:
			// set user zone
			state = st_invalid;
			if (cmd[2] < user_zones_count) {
				current_zone = cmd[2];
				return 0;
			} else {
				return 1;
			}
	}
	state = st_invalid;
	return 1;
}

int AT88SCDevice::AT88SC_SystemREAD()
{
	switch(cmd[1]) {
		case 0x00:
			// read config zone
			if (bytes_remaining == 0)
				bytes_remaining = 256;
			read_ptr = cmd[2];
			state = st_data_send;
			return 0;
		case 0x01:
			// read fuse byte
			break;
		case 0x02:
			// read checksum
			bytes_remaining = 2;
			read_ptr = (unsigned int)(config_zone.Data.Forbidden.Data.Checksum - config_zone.Byte);
			state = st_data_send;
			return 0;
	}
	state = st_invalid;
	return 1;
}

int AT88SCDevice::AT88SC_SystemREAD_Data(uint8_t *d)
{
	*d = config_zone.Byte[read_ptr++];
	// Only password zone is ever encrypted
	cm_GPAencrypt(ucCM_Encrypt && (read_ptr >= 0xB0), d, 1);
	return 0;
}

int AT88SCDevice::AT88SC_ReadUserZone()
{
	read_ptr = *((uint16_t*)&cmd[1]);
	bytes_remaining = cmd[3];
	if (bytes_remaining == 0)
		bytes_remaining = 256;
	if (read_ptr + bytes_remaining <= user_zone_size) {
		state = st_data_send;
		return 0; 
	} else {
		state = st_invalid;
		return 1;
	}
}

int AT88SCDevice::AT88SC_ReadUserZone_Data(uint8_t *d)
{
	*d = user_zones[current_zone][read_ptr++];
	cm_GPAencrypt(ucCM_Encrypt, d, 1);
	return 0;
}

int AT88SCDevice::AT88SC_VerifyPassword()
{
	if ((cmd[1] & 0xE8) == 0) {
		state = st_data_receive; 
		return 0;
	} else {
		state = st_invalid;
		return 1;
	}
}

int AT88SCDevice::AT88SC_VerifyPassword_Data(uint8_t d)
{
	if (bytes_remaining == 0) {
		// all bytes received
		uint8_t* pwd = (uint8_t*)&config_zone.Data.Password[cmd[1] & 0xF];
		if (cmd[1] >> 4) 
			pwd += 4;
		if (memcmp(pwd + 1, &cmd[4], 3)) {
			DBG_I2C("AT88SC: verify password failed!\n");
			*pwd = 0x00;
		} else {
			*pwd = 0xFF;
		}
		return 0;
	} else {
		return 0;
	}	
}

int AT88SCDevice::AT88SC_VerifyCrypto()
{
	if (((cmd[1] & 0xEC) == 0) && (cmd[3] == 16)) {
		state = st_data_receive; 
		return 0;
	} else {
		state = st_invalid;
		return 1;
	}
}

int AT88SCDevice::AT88SC_VerifyCrypto_Data(uint8_t d)
{
	if (bytes_remaining == 0) {
		// all bytes received
		int index = cmd[1] & 0x0F;
		uint8_t G_Sk[8], ret[8];
		memcpy(G_Sk, (cmd[1] & 0xF0) ? config_zone.Data.Cryptography[index].SessionEncryptionKey : config_zone.Data.SecretSeed[index], 8);
		cm_AuthenEncryptCal(&config_zone.Data.Cryptography[index].AAC, G_Sk, &cmd[4], ret);
		if (memcmp(ret, &cmd[12], 8)) {
			// failed
			DBG_I2C("AT88SC: verify crypto failed!\n");
			config_zone.Data.Cryptography[index].AAC = 0x00;
			ucCM_Authenticate = false;
			ucCM_Encrypt = false;
		} else {
			// success
			config_zone.Data.Cryptography[index].AAC = 0xFF;
			memcpy(config_zone.Data.Cryptography[index].SessionEncryptionKey, G_Sk, 8);
			if (cmd[1] & 0xF0)
				ucCM_Encrypt = true; else
				ucCM_Authenticate = true;
		}
		return 0;
	} else {
		return 0;
	}	
}

// Macros for all of the registers
#define RA		 (ucGpaRegisters[0])
#define RB		 (ucGpaRegisters[1])
#define RC		 (ucGpaRegisters[2])
#define RD		 (ucGpaRegisters[3])
#define RE		 (ucGpaRegisters[4])
#define RF		 (ucGpaRegisters[5])
#define RG		 (ucGpaRegisters[6])
#define TA		 (ucGpaRegisters[7])
#define TB		 (ucGpaRegisters[8])
#define TC		 (ucGpaRegisters[9])
#define TD		 (ucGpaRegisters[10])
#define TE		 (ucGpaRegisters[11])
#define SA		 (ucGpaRegisters[12])
#define SB		 (ucGpaRegisters[13])
#define SC		 (ucGpaRegisters[14])
#define SD		 (ucGpaRegisters[15])
#define SE		 (ucGpaRegisters[16])
#define SF		 (ucGpaRegisters[17])
#define SG		 (ucGpaRegisters[18])
#define Gpa_byte (ucGpaRegisters[19])
#define Gpa_Regs (20)

// Defines for constants used
#define CM_MOD_R (0x1F)
#define CM_MOD_T (0x1F)
#define CM_MOD_S (0x7F)

// Macros for common operations
#define cm_Mod(x,y,m) ((x+y)>m?(x+y-m):(x+y))
#define cm_RotT(x)	  (((x<<1)&0x1e)|((x>>4)&0x01))
#define cm_RotR(x)	  (((x<<1)&0x1e)|((x>>4)&0x01))
#define cm_RotS(x)	  (((x<<1)&0x7e)|((x>>6)&0x01))

// Reset the cryptographic state
void AT88SCDevice::cm_ResetCrypto()
{
	uint8_t i;
	
	for (i = 0; i < Gpa_Regs; ++i) 
		ucGpaRegisters[i] = 0;
	ucCM_Encrypt = ucCM_Authenticate = false;
}

// Generate next value
uint8_t AT88SCDevice::cm_GPAGen(uint8_t Datain)
{
	uint8_t Din_gpa;
	uint8_t Ri, Si, Ti;
	uint8_t R_sum, S_sum, T_sum;
	
	// Input Character
	Din_gpa = Datain^Gpa_byte;
	Ri = Din_gpa&0x1f;								//Ri[4:0] = Din_gpa[4:0]
	Si = ((Din_gpa<<3)&0x78)|((Din_gpa>>5)&0x07);	//Si[6:0] = {Din_gpa[3:0], Din_gpa[7:5]}
	Ti = (Din_gpa>>3)&0x1f;							//Ti[4:0] = Din_gpa[7:3];
	   
	//R polynomial
	R_sum = cm_Mod(RD, cm_RotR(RG), CM_MOD_R);
	RG = RF;
	RF = RE;
	RE = RD;
	RD = RC^Ri;
	RC = RB;
	RB = RA;
	RA = R_sum;
	
	//S ploynomial
	S_sum = cm_Mod(SF, cm_RotS(SG), CM_MOD_S);
	SG = SF;
	SF = SE^Si;
	SE = SD;
	SD = SC;
	SC = SB;
	SB = SA;
	SA = S_sum;
	
	//T polynomial
	T_sum = cm_Mod(TE,TC,CM_MOD_T);
	TE = TD;
	TD = TC;
	TC = TB^Ti;
	TB = TA;
	TA = T_sum;

	// Output Stage
	Gpa_byte =(Gpa_byte<<4)&0xF0;									// shift gpa_byte left by 4
	Gpa_byte |= ((((RA^RE)&0x1F)&(~SA))|(((TA^TD)&0x1F)&SA))&0x0F;	// concat 4 prev bits and 4 new bits
	return Gpa_byte;
}

// Clock some zeros into the state machine
void AT88SCDevice::cm_GPAGenN(uint8_t Count)
{
	while (Count--)
		cm_GPAGen(0x00);
}

// Clock some zeros into the state machine, then clock in a byte of data
void AT88SCDevice::cm_GPAGenNF(uint8_t Count, uint8_t DataIn)
{
	cm_GPAGenN(Count);							// First ones are allways zeros
	cm_GPAGen(DataIn);							// Final one is sometimes different
}

// Include 2 bytes of a command into a polynominal
void AT88SCDevice::cm_GPAcmd2(uint8_t* pucInsBuff)
{
	cm_GPAGenNF(5, pucInsBuff[2]);
	cm_GPAGenNF(5, pucInsBuff[3]);
}

// Include 3 bytes of a command into a polynominal
void AT88SCDevice::cm_GPAcmd3(uint8_t* pucInsBuff)
{
	cm_GPAGenNF(5, pucInsBuff[1]);
	cm_GPAcmd2(pucInsBuff);
}

// Include the data in the polynominals and decrypt it required
void AT88SCDevice::cm_GPAdecrypt(bool ucEncrypt, uint8_t* pucBuffer, uint8_t ucCount)
{
	uint8_t i;
	   
	for (i = 0; i < ucCount; ++i) {
		if (ucEncrypt) 
			pucBuffer[i] = pucBuffer[i] ^ Gpa_byte;
		cm_GPAGen(pucBuffer[i]);
		cm_GPAGenN(5);		// 5 clocks with 0x00 data
	}
}

// Include the data in the polynominals and encrypt it required
void AT88SCDevice::cm_GPAencrypt(bool ucEncrypt, uint8_t* pucBuffer, uint8_t ucCount)
{
	uint8_t i, ucData;

	for (i = 0; i<ucCount; i++) {
		ucData = pucBuffer[i];
//			cm_GPAGen(ucData);
		if (ucEncrypt) 
			pucBuffer[i] = pucBuffer[i] ^ Gpa_byte;
		cm_GPAGen(ucData);
		cm_GPAGenN(5);							// 5 0x00s
//			cm_GPAGen(pucBuffer[i]);
	}
}

// Do authenticate/encrypt chalange encryption
void AT88SCDevice::cm_AuthenEncryptCal(uint8_t *Ci, uint8_t *G_Sk, uint8_t *Q, uint8_t *Ch)
{	
	uint8_t i, j;
	
	// Reset all registers
	cm_ResetCrypto();
	
	// Setup the cryptographic registers
	for(j = 0; j < 4; j++) {
		for(i = 0; i<3; i++) cm_GPAGen(Ci[2*j]);	
		for(i = 0; i<3; i++) cm_GPAGen(Ci[2*j+1]);
		cm_GPAGen(Q[j]);
	}
	
	for(j = 0; j<4; j++ ) {
		for(i = 0; i<3; i++) cm_GPAGen(G_Sk[2*j]);
		for(i = 0; i<3; i++) cm_GPAGen(G_Sk[2*j+1]);
		cm_GPAGen(Q[j+4]);
	}
	
	// begin to generate Ch
	cm_GPAGenN(6);					// 6 0x00s
	Ch[0] = Gpa_byte;

	for (j = 1; j<8; j++) {
		cm_GPAGenN(7);				// 7 0x00s
		Ch[j] = Gpa_byte;
	}
	
	// then calculate new Ci and Sk, to compare with the new Ci and Sk read from eeprom
	Ci[0] = 0xff;					// reset AAC 
	for(j = 1; j<8; j++) {
		cm_GPAGenN(2);				// 2 0x00s
		  Ci[j] = Gpa_byte;
	}

	for(j = 0; j<8; j++) {
		 cm_GPAGenN(2);				// 2 0x00s
		 G_Sk[j] = Gpa_byte;
	}
   
	cm_GPAGenN(3);					// 3 0x00s
}

AT88SC0104C::AT88SC0104C(): AT88SCDevice()
{

}

void AT88SC0104C::Initialize()
{
	user_zones_count = 4;
	user_zone_size = 32;
	page_size = 16;
	AT88SCDevice::Initialize();
	// ATR
	config_zone.Data.Identification.AnswerToReset[0] = 0x3B;
	config_zone.Data.Identification.AnswerToReset[1] = 0xB2;
	config_zone.Data.Identification.AnswerToReset[2] = 0x11;
	config_zone.Data.Identification.AnswerToReset[3] = 0x00;
	config_zone.Data.Identification.AnswerToReset[4] = 0x10;
	config_zone.Data.Identification.AnswerToReset[5] = 0x80;
	config_zone.Data.Identification.AnswerToReset[6] = 0x00;
	config_zone.Data.Identification.AnswerToReset[7] = 0x01;
	// Fab Code
	config_zone.Data.Identification.FabCode[0] = 0x10;
	config_zone.Data.Identification.FabCode[1] = 0x10;
	// write 7 password
	config_zone.Data.Password[7].Write[0] = 0xDD;
	config_zone.Data.Password[7].Write[1] = 0x42;
	config_zone.Data.Password[7].Write[2] = 0x97;
}
