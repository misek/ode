#include "I2CBus.h"

I2CDevice::I2CDevice(): EmulatedComponent()
{
	bus = __null;
	sda = 1;
	scl = 1;
	bit_no = 0;
	sda_out = 1;
	active = false;
}

void I2CDevice::ConnectToI2CBus(I2CBus* b)
{
	// device can be connected only to one I2C bus
	if (bus) {
		bus->RemoveDevice(this);
	}
	bus = b;
	if (bus) {
		bus->AddDevice(this);
	}
}

void I2CDevice::Start()
{
	DBG_I2C_BUS("I2CDevice::Start()\n");
	bit_no = 0;
	active = true;
}

void I2CDevice::Stop()
{
	DBG_I2C_BUS("I2CDevice::Stop()\n");
}

void I2CDevice::Write(uint8_t b)
{
	DBG_I2C_BUS("I2CDevice::Write( 0x%02X )\n", b);
}

void I2CDevice::ACK(int ack)
{
	DBG_I2C_BUS("I2CDevice::ACK( %d )\n", ack);
}

int I2CDevice::ACK()
{
	DBG_I2C_BUS("I2CDevice::ACK( )\n");
	return 1;
}

void I2CDevice::SetSDA0()
{
	sda = 0;
	if (scl)
		Start();
}

void I2CDevice::SetSDA1()
{
	sda = 1;
	if (scl)
		Stop();
}

void I2CDevice::SetSCL0()
{
	DBG_I2C_BUS("I2CDevice::SetSCL0()\n");
	scl = 0;
	if (active) {
		if (bit_no == 8)
		{
			// receive data and send ack
			Write(data);
			sda_out = ACK();
		} else {
			sda_out = (data >> (7 - bit_no)) & 1;
		}
	}
}

void I2CDevice::SetSCL1()
{
	DBG_I2C_BUS("I2CDevice::SetSCL1()\n");
	scl = 1;
	if (active) {
		if (bit_no == 8)
		{
			// ack after byte
			ACK(sda);
			bit_no = 0;
		} else {
			int mask = ~(1 << (7 - bit_no));
			data = (data & mask) | (sda << (7 - bit_no));
			bit_no++;
		}
	}
}

I2CBus::I2CBus(): byte_out(0), ackCallback(__null)
{
	last_sda = -1;
	last_scl = -1;
	device_count = 0;
}

void I2CBus::SetSDA(int val)
{
	val = val ? 1 : 0;
	if (last_sda != val) {
		last_sda = val;
		for (int i = 0; i < device_count; i++) {
			if (last_sda)
				devices[i]->SetSDA1(); else
				devices[i]->SetSDA0();
		}
	}
}

void I2CBus::SetACKCallback(I2CBusSetACKCallback cb)
{
	ackCallback = cb;
}

int I2CBus::GetSDA()
{
	DBG_I2C_BUS("<== SDA = %d\n", -1);
	for (int i = 0; i < device_count; i++) {
		if (devices[i]->active)
			return devices[i]->sda_out;
	}
	return 1;
}

void I2CBus::SetSCL(int val)
{
	val = val ? 1 : 0;
	if (last_scl != val) {
		last_scl = val;
		for (int i = 0; i < device_count; i++) {
			if (last_scl)
				devices[i]->SetSCL1(); else
				devices[i]->SetSCL0();
		}
	}
}

void I2CBus::AddDevice(I2CDevice* device)
{
	devices[device_count++] = device;
}

void I2CBus::RemoveDevice(I2CDevice* device)
{
	//FIXME: to implement
}

