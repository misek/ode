#ifndef AT88SC_H_INCLUDED
#define AT88SC_H_INCLUDED

#include <stdio.h>
#include <string.h>

#include "I2CBus.h"

union AT88SCConfigZone {
	uint8_t Byte[256];
	struct {
		struct {
			uint8_t AnswerToReset[8];
			uint8_t FabCode[2];
			uint8_t MTZ[2];
			uint8_t CardManufacturerCode[4];
		} Identification;
		struct {
			uint8_t LotHistoryCode[8];
		} ReadOnly;
		struct {
			struct {
				uint8_t CS:4;
				uint8_t ETA:1;
				uint8_t UAT:1;
				uint8_t UCR:1;
				uint8_t SME:1;
			} DCR;
			uint8_t IdentificationNumberNc[7];
			struct {
				struct {
					uint8_t PGO:1;
					uint8_t MDF:1;
					uint8_t WLM:1;
					uint8_t ER:1;
					uint8_t AM:2;
					uint8_t PM:2;
				} AR;
				struct {
					uint8_t PW:3;
					uint8_t Res:1;
					uint8_t POK:2;
					uint8_t AK:2;
				} PR;
			} Zones[16];
			uint8_t IssuerCode[16];
		} AccessControl;
		struct {
			uint8_t AAC;
			uint8_t Cryptogram[7];
			uint8_t SessionEncryptionKey[8];
		} Cryptography[4];
		uint8_t SecretSeed[4][8];
		struct {
			uint8_t PAC_Write;
			uint8_t Write[3];
			uint8_t PAC_Read;
			uint8_t Read[3];
		} Password[8];
		union {
			uint8_t Byte[16];
			struct {
				uint8_t Checksum[2];
				uint8_t Fuses;
			} Data;
		} Forbidden;
	} Data;
};

class AT88SCDevice: public I2CDevice {
private:
	uint8_t cmd[4 + 256];
	uint8_t ucGpaRegisters[20];
	bool ucCM_Encrypt;
	bool ucCM_Authenticate;
	int cmd_bytes;
	unsigned int bytes_remaining;
	enum STATE { st_invalid, st_command, st_data_receive, st_data_send } state;

	COMPONENT_METHOD void InitMemory();
	COMPONENT_METHOD void FreeMemory();

	COMPONENT_METHOD void cm_ResetCrypto();
	COMPONENT_METHOD uint8_t cm_GPAGen(uint8_t Datain);
	COMPONENT_METHOD void cm_GPAGenN(uint8_t Count);
	COMPONENT_METHOD void cm_GPAGenNF(uint8_t Count, uint8_t DataIn);
	COMPONENT_METHOD void cm_GPAcmd2(uint8_t* pucInsBuff);
	COMPONENT_METHOD void cm_GPAcmd3(uint8_t* pucInsBuff);
	COMPONENT_METHOD void cm_GPAdecrypt(bool ucEncrypt, uint8_t* pucBuffer, uint8_t ucCount);
	COMPONENT_METHOD void cm_GPAencrypt(bool ucEncrypt, uint8_t* pucBuffer, uint8_t ucCount);
	COMPONENT_METHOD void cm_AuthenEncryptCal(uint8_t *Ci, uint8_t *G_Sk, uint8_t *Q, uint8_t *Ch);

protected:
	AT88SCConfigZone config_zone;
	uint8_t* user_zones[16];
	unsigned int user_zones_count;
	unsigned int user_zone_size;
	unsigned int page_size;
	unsigned int current_zone;
	unsigned int read_ptr;

	COMPONENT_METHOD void ReturnBytes(uint8_t *data, int length);

	COMPONENT_METHOD virtual bool AT88SC_Command_Allowed(uint8_t c);
	COMPONENT_METHOD virtual int AT88SC_Command(uint8_t c);
	COMPONENT_METHOD virtual int AT88SC_DataReceived(uint8_t d);
	COMPONENT_METHOD virtual int AT88SC_DataRead(uint8_t* d);
	COMPONENT_METHOD virtual int AT88SC_SystemWRITE();
	COMPONENT_METHOD virtual int AT88SC_SystemREAD();
	COMPONENT_METHOD virtual int AT88SC_SystemREAD_Data(uint8_t* d);
	COMPONENT_METHOD virtual int AT88SC_ReadUserZone();
	COMPONENT_METHOD virtual int AT88SC_ReadUserZone_Data(uint8_t* d);
	COMPONENT_METHOD virtual int AT88SC_VerifyPassword();
	COMPONENT_METHOD virtual int AT88SC_VerifyPassword_Data(uint8_t d);
	COMPONENT_METHOD virtual int AT88SC_VerifyCrypto();
	COMPONENT_METHOD virtual int AT88SC_VerifyCrypto_Data(uint8_t d);

public:
	AT88SCDevice();
	virtual ~AT88SCDevice();

	COMPONENT_METHOD void Start();
	COMPONENT_METHOD void ACK(int ack);
	COMPONENT_METHOD int ACK();
	COMPONENT_METHOD void Write(uint8_t b);

	COMPONENT_METHOD virtual void Initialize();
	COMPONENT_METHOD virtual bool Load(const char* filename);
	COMPONENT_METHOD virtual bool Load(FILE* fIn);
	COMPONENT_METHOD virtual bool LoadFromMemory(void* fMemory);
	COMPONENT_METHOD virtual bool Save(const char* filename);
	COMPONENT_METHOD virtual bool Save(FILE* fOut);

};

class AT88SC0104C: public AT88SCDevice {
public:
	AT88SC0104C();

	COMPONENT_METHOD virtual void Initialize();

};

#endif
