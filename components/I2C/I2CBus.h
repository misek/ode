#ifndef I2CBUS_H_INCLUDED
#define I2CBUS_H_INCLUDED

class I2CBus;

#include "../component.h"

#ifdef DEBUG_I2C
#define DBG_I2C(fmt, ...) DEBUG_PRINT("I2C: "fmt, ##__VA_ARGS__)
#else
#define DBG_I2C(fmt, ...)
#endif

#if 0
#define DBG_I2C_BUS(fmt, ...) DEBUG_PRINT("I2CBUS: "fmt, ##__VA_ARGS__)
#else
#define DBG_I2C_BUS(...)
#endif

typedef void (*I2CBusSetACKCallback)(I2CBus* bus, int ack);

class I2CDevice : public EmulatedComponent {
	friend class I2CBus;
protected:
	I2CBus* bus;
	bool active;

	int scl;
	int sda;
	int sda_out;
	int bit_no;
	uint8_t data;

	COMPONENT_METHOD virtual void Start();
	COMPONENT_METHOD virtual void Stop();
	COMPONENT_METHOD virtual int ACK();
	COMPONENT_METHOD virtual void ACK(int ack);
	COMPONENT_METHOD virtual void Write(uint8_t b);

public:
	I2CDevice();

	COMPONENT_METHOD void ConnectToI2CBus(I2CBus* b);

	COMPONENT_METHOD virtual void SetSDA0();
	COMPONENT_METHOD virtual void SetSDA1();
	COMPONENT_METHOD virtual void SetSCL0();
	COMPONENT_METHOD virtual void SetSCL1();

};

class I2CBus {
private:
	I2CDevice* devices[256];
	int device_count;
	int last_sda;
	int last_scl;
	int byte_out;

	I2CBusSetACKCallback ackCallback;

	COMPONENT_METHOD void SetACK(int val);
	COMPONENT_METHOD uint8_t I2CRead();
	COMPONENT_METHOD void I2CWrite(uint8_t);

public:
	I2CBus();

	COMPONENT_METHOD void SetSDA(int val);
	COMPONENT_METHOD int GetSDA();
	COMPONENT_METHOD void SetSCL(int val);

	COMPONENT_METHOD void SetACKCallback(I2CBusSetACKCallback cb);
	COMPONENT_METHOD void AddDevice(I2CDevice* device);
	COMPONENT_METHOD void RemoveDevice(I2CDevice* device);

};

#endif
