/*
 * device.h
 *
 *  Created on: 12-05-2012
 *      Author: Łukasz Misek
 */

#ifndef COMPONENT_H_
#define COMPONENT_H_

#include <gtk/gtk.h>
#include <utils.h>

#define COMPONENT_METHOD __attribute__((regparm(3)))

enum EMULATED_COMPONENT_TYPE {
	EMULATED_NANDFLASH,
	EMULATED_RAM,
	EMULATED_FREQUENCY_GENERATOR,
	EMULATED_I2C_DEVICE,
	EMULATED_LCD_DISPLAY,
	EMULATED_SDMMC
};

class EmulatedComponent {
public:
	EmulatedComponent();
	virtual ~EmulatedComponent();
	virtual COMPONENT_METHOD void PowerOn();
	virtual COMPONENT_METHOD void PowerOff();
	virtual COMPONENT_METHOD GtkWidget* GetWidget();

};

#endif /* COMPONENT_H_ */
