/*
 * FreqGenerator.cpp
 *
 *  Created on: 18-05-2012
 *      Author: Łukasz Misek
 */

#include "FreqGenerator.h"

FrequencyGenerator::FrequencyGenerator(): EmulatedComponent()
{
	frequency = 0.0;
}

void FrequencyGenerator::SetFrequency(double new_frequency)
{
	frequency = new_frequency;
}

double FrequencyGenerator::GetFrequency()
{
	return frequency;
}
