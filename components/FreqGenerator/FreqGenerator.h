/*
 * FreqGenerator.h
 *
 *  Created on: 18-05-2012
 *      Author: Łukasz Misek
 */

#ifndef FREQGENERATOR_H_
#define FREQGENERATOR_H_

#include "../component.h"

class FrequencyGenerator: public EmulatedComponent {
	double frequency;
public:
	FrequencyGenerator();

	COMPONENT_METHOD void SetFrequency(double new_frequency);
	COMPONENT_METHOD double GetFrequency();
};

#endif /* FREQGENERATOR_H_ */
