#include "SamsungFlash.h"

void K9G8G08XXX::Initialize()
{
	NANDFlashDeviceLargePage::Initialize();
}

void K9G8G08U0B::Initialize()
{
	// set NAND device parameters
	config.ID_BYTES = 5;
	config.ID[0] = 0xEC;
	config.ID[1] = 0xD3;
	config.ID[2] = 0x14;
	config.ID[3] = 0xA5;
	config.ID[4] = 0x64;

	config.BYTES_PER_PAGE = 2048;
	config.SPARE_SIZE = 64;
	config.PAGES_PER_BLOCK = 128;
	config.BLOCKS_PER_DEVICE = 4096;
	config.COL_ADDRESS_BYTES = 2;
	config.ROW_ADDRESS_BYTES = 3;

	K9G8G08XXX::Initialize();
}

void K9F1208X0C::Initialize()
{
	// set NAND device parameters
	config.ID_BYTES = 4;
	config.ID[0] = 0xEC;
	config.ID[1] = 0x76;
	config.ID[2] = 0x5A;
	config.ID[3] = 0x3F;

	config.BYTES_PER_PAGE = 512;
	config.SPARE_SIZE = 16;
	config.PAGES_PER_BLOCK = 32;
	config.BLOCKS_PER_DEVICE = 4096;
	config.COL_ADDRESS_BYTES = 1;
	config.ROW_ADDRESS_BYTES = 3;

	NANDFlashDeviceSmallPage::Initialize();
}
