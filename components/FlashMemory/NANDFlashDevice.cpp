#include "NANDFlashDevice.h"

#ifdef DEBUG_NAND
#define DBG_NAND(fmt, ...) DEBUG_PRINT(fmt, ##__VA_ARGS__)
#else
#define DBG_NAND(fmt, ...)
#endif

#define DBG_NAND_ERROR_OBJ(obj, fmt, ...) DBG_NAND("NAND ERROR(p: %04X, c: %02X): " fmt "\n", obj->buffer_ptr, obj->last_command, ##__VA_ARGS__)
#define DBG_NAND_ERROR(fmt, ...) DBG_NAND_ERROR_OBJ(this, fmt, ##__VA_ARGS__)
#ifdef DEBUG_NAND
#ifdef DEBUG_NAND_COMMAND
#define DBG_NAND_COMMAND(cmd, addr) {\
	if (cmd.final) {\
		DBG_NAND("NAND: %s(0x%02X) ", cmd.name, cmd.code);\
		switch(addr) {\
			case abnNone:\
				break;\
			case abnOne:\
				DBG_NAND("0x%02X (%u)", column_address, column_address);\
				break;\
			case abnColumn:\
				DBG_NAND("col: 0x%04X (%u)", column_address, column_address);\
				break;\
			case abnRow:\
				DBG_NAND("row: 0x%06X (block: %u)", row_address, row_address / config.PAGES_PER_BLOCK);\
				break;\
			case abnColumnRow:\
				DBG_NAND("addr: 0x%06X/0x%04X (block: %u, page: %u / %u)", row_address, column_address, \
					row_address / config.PAGES_PER_BLOCK, row_address % config.PAGES_PER_BLOCK, column_address);\
				break;\
			default:\
				ASSERT(false);\
				break;\
		}\
		DBG_NAND("\n");\
	}\
}
#define ReadWriteDebugInfo() DoReadWriteDebugInfo()
#else
#define DBG_NAND_COMMAND(cmd, addr)
#define ReadWriteDebugInfo()
#endif
#else
#define DBG_NAND_COMMAND(cmd, addr)
#define ReadWriteDebugInfo()
#endif


#define __setfn(tab, no, _fun, _fin, _funname) config.cmd_##tab[no].code = no; config.cmd_##tab[no].ptr = &_fun; config.cmd_##tab[no].final = _fin; config.cmd_##tab[no].name = _funname;

#define set_fn(no, fun) __setfn(fn, no, fun, false, #no)
#define set_fn_final(no, fun) __setfn(fn, no, fun, true, #no)
#define set_fn_address(no, fun) __setfn(fn_address, no, fun, false, #no)
#define set_fn_address_final(no, fun) __setfn(fn_address, no, fun, true, #no)
#define set_fn_thread(no, fun) __setfn(fn_thread, no, fun, false, #no)
#define set_fn_thread_final(no, fun) __setfn(fn_thread, no, fun, true, #no)

NANDFlashDevice::NANDFlashDevice(): EmulatedComponent()
{
	hFile = __null;
	last_command = 0;
	buffer_size = 0;
	buffer_ptr = 0;
	need_address = abnNone;
	prev_commands = 0;
	memset(&config, 0, sizeof(config));
	thread.Start(NANDFlashDevice::pThreadProc, this, "dev/NAND");
}

NANDFlashDevice::~NANDFlashDevice()
{
	thread.Terminate();
}

void NANDFlashDevice::Initialize()
{
	config.PAGE_SIZE = config.BYTES_PER_PAGE + config.SPARE_SIZE;
	config.PAGES_PER_DEVICE = config.BLOCKS_PER_DEVICE * config.PAGES_PER_BLOCK;

	buffer_size = config.PAGE_SIZE;
	buffer = new uint8_t[buffer_size];
	buffer2 = new uint8_t[buffer_size];
	address_valid2 = false;
	buffer_ptr = 0;
}

void NANDFlashDevice::thread_execute()
{
	last_thread_command = last_command;
	hEvent.Signal();
}

bool NANDFlashDevice::pThreadProc(Thread* t, void* data)
{
	NANDFlashDevice* device = (NANDFlashDevice*)data;
	device->ThreadFunction();

	return true;
}

void NANDFlashDevice::ThreadFunction()
{
	hEvent.WaitFor();
	// call function in thread
	if (config.cmd_fn_thread[last_thread_command].ptr) {
		config.cmd_fn_thread[last_thread_command].ptr(this);
	} else {
		DBG_NAND_ERROR("ThreadFunction()");
	}
}

void NANDFlashDevice::DoReadWriteDebugInfo()
{
	if ((read_bytes > 0) || (written_bytes > 0)) {
		DBG_NAND("--> ");
		if (read_bytes > 0) {
			DBG_NAND(" read 0x%X (%u) bytes", read_bytes, read_bytes);
		}
		if (written_bytes > 0) {
			DBG_NAND(" written 0x%X (%u) bytes", written_bytes, written_bytes);
		}
		DBG_NAND("\n");
	}
	written_bytes = 0;
	read_bytes = 0;
}

void NANDFlashDevice::cmd_execute()
{
	if (config.cmd_fn[last_command].ptr) {
		ReadWriteDebugInfo();
		read_status = false;
		DBG_NAND_COMMAND(config.cmd_fn[last_command], abnNone);
		config.cmd_fn[last_command].ptr(this);
	} else {
		DBG_NAND_ERROR("cmd_execute()");
	}
}

void NANDFlashDevice::cmd_execute_address()
{
	if (config.cmd_fn_address[last_command].ptr) {
		ReadWriteDebugInfo();
		read_status = false;
		DBG_NAND_COMMAND(config.cmd_fn_address[last_command], read_address);
		config.cmd_fn_address[last_command].ptr(this);
	} else {
		DBG_NAND_ERROR("cmd_execute_address()");
	}
}

void NANDFlashDevice::CloseFile()
{
	if (hFile) {
		fclose(hFile);
		hFile = __null;
	}
}

bool NANDFlashDevice::UseFile(const char* fileName)
{
	CloseFile();
	hFile = fopen(fileName, "r+b");
	if (hFile) {
		// successfully opened NAND dump image
	} else {
		hFile = __null;
	}
	return hFile != __null;
}

void NANDFlashDevice::WriteCommand(uint8_t command)
{
	prev_commands <<= 8;
	prev_commands |= last_command;
	last_command = command;
	cmd_execute();
	current_address_byte = 0;
}

void NANDFlashDevice::WriteAddress(uint8_t address)
{
	switch (need_address) {
		case abnNone:
			DBG_NAND_ERROR("WriteAddress(0x%02X)", address);
			break;
		case abnOne:
			// address of one byte required
			column_address = address;
			current_address_byte++;
			read_address = need_address;
			need_address = abnNone;
			cmd_execute_address();
			break;
		case abnColumn:
			// column address needed only
			column_address |= address << (8 * current_address_byte);
			if (++current_address_byte == config.COL_ADDRESS_BYTES) {
				read_address = need_address;
				need_address = abnNone;
				cmd_execute_address();
			}
			break;
		case abnRow:
			// row address needed only
			row_address |= address << (8 * current_address_byte);
			if (++current_address_byte == config.ROW_ADDRESS_BYTES) {
				read_address = need_address;
				need_address = abnNone;
				cmd_execute_address();
			}
			break;
		case abnColumnRow:
			// column and row address needed
			if (current_address_byte < config.COL_ADDRESS_BYTES) {
				// column address byte
				column_address |= address << (8 * current_address_byte);
				current_address_byte++;
			} else {
				// row address byte
				row_address |= address << (8 * (current_address_byte - config.COL_ADDRESS_BYTES));
				if (++current_address_byte == (config.COL_ADDRESS_BYTES + config.ROW_ADDRESS_BYTES)) {
					read_address = need_address;
					need_address = abnNone;
					cmd_execute_address();
				}
			}
			break;
	}
}

void NANDFlashDevice::WriteByte(uint8_t Value)
{
	if (buffer_ptr < buffer_size) {
		buffer[buffer_ptr++] = Value;
		written_bytes += 1;
	} else {
		DBG_NAND_ERROR("WriteByte(0x%02X)", Value);
	}
}

void NANDFlashDevice::WriteHalfWord(uint16_t Value)
{
	if (buffer_ptr < buffer_size - 1) {
		uint16_t* buffer16 = (uint16_t*)(buffer + buffer_ptr);
		*buffer16 = Value;
		buffer_ptr += 2;
		written_bytes += 2;
	} else {
		DBG_NAND_ERROR("WriteHalfWord(0x%04X)", Value);
	}
}

void NANDFlashDevice::WriteWord(uint32_t Value)
{
	if (buffer_ptr < buffer_size - 3) {
		uint32_t* buffer32 = (uint32_t*)(buffer + buffer_ptr);
		*buffer32 = Value;
		buffer_ptr += 4;
		written_bytes += 4;
	} else {
		DBG_NAND_ERROR("WriteWord(0x%08X)", Value);
	}
}

uint8_t NANDFlashDevice::ReadByte()
{
	if (read_status) {
		return StatusByte.Byte;
	} else {
		if (buffer_ptr < buffer_size) {
			read_bytes += 1;
			return buffer[buffer_ptr++];
		} else {
			DBG_NAND_ERROR("ReadByte()");
			return 0;
		}
	}
}

uint16_t NANDFlashDevice::ReadHalfWord()
{
	if (buffer_ptr < buffer_size - 1) {
		uint16_t* buffer16 = (uint16_t*)(buffer + buffer_ptr);
		uint16_t ret = *buffer16;
		buffer_ptr += 2;
		read_bytes += 2;
		return ret;
	} else {
		DBG_NAND_ERROR("ReadHalfWord()");
		return 0;
	}
}

uint32_t NANDFlashDevice::ReadWord()
{
	if (buffer_ptr < buffer_size - 3) {
		uint32_t* buffer32 = (uint32_t*)(buffer + buffer_ptr);
		uint32_t ret = *buffer32;
		buffer_ptr += 4;
		read_bytes += 4;
		return ret;
	} else {
		DBG_NAND_ERROR("ReadWord()");
		return 0;
	}
}

bool NANDFlashDevice::SeekPage(unsigned int page_no)
{
	return fseek(hFile, page_no * config.PAGE_SIZE, SEEK_SET) == 0;
}

bool NANDFlashDevice::ReadPage(void* pBuffer)
{
	DBG_NAND("READ PAGE: 0x%06X (block: %u, page: %u)\n", (uint)(ftell(hFile) / config.PAGE_SIZE),
		(uint)(ftell(hFile) / config.PAGE_SIZE / config.PAGES_PER_BLOCK), (uint)((ftell(hFile) / config.PAGE_SIZE) % config.PAGES_PER_BLOCK));
	return fread(pBuffer, config.PAGE_SIZE, 1, hFile) == 1;
}

bool NANDFlashDevice::WritePage(void* pBuffer)
{
	DBG_NAND("WRITE PAGE: 0x%06X (block: %u, page: %u)\n", (uint)(ftell(hFile) / config.PAGE_SIZE),
		(uint)(ftell(hFile) / config.PAGE_SIZE / config.PAGES_PER_BLOCK), (uint)((ftell(hFile) / config.PAGE_SIZE) % config.PAGES_PER_BLOCK));
	return fwrite(pBuffer, config.PAGE_SIZE, 1, hFile) == 1;
}

void NANDFlashDevice::SetCallback(nand_callback cb, void* user)
{
	callback_fn = cb;
	callback_user = user;
}

void NANDFlashDevice::set_RnB(int value)
{
	if (callback_fn) {
		StatusByte.Ready = value;
		callback_fn(this, value ? nandReady : nandBusy, __null, callback_user);
	}
}

bool NANDFlashDevice::Read(void* pBuffer, unsigned int length)
{
	uint cpy = std::min(buffer_ptr - buffer_size, length);
	memcpy(pBuffer, &buffer[buffer_ptr], cpy);
	buffer_ptr+= cpy;
	if (cpy < length) {
		DBG_NAND_ERROR("Read(%u)", length);
	}

	return true;
}

bool NANDFlashDevice::ReadRAW(void* pBuffer, const unsigned long start, unsigned int length)
{
	uint rem = start % config.BYTES_PER_PAGE;
	uint page_no = start / config.BYTES_PER_PAGE;

	uint8_t* buf = new uint8_t[config.PAGE_SIZE];
	uint8_t* dest = (uint8_t*)pBuffer;

	while (length > 0) {
		if (!SeekPage(page_no++) || !ReadPage(buf)) {
			DBG_NAND_ERROR("ReadRAW(%u, %u)", (unsigned int)start, (unsigned int)length);
			delete buf;
			return false;
		}

		uint cpy = std::min(length, config.BYTES_PER_PAGE - rem);
		memcpy(dest, buf + rem, cpy);
		rem = 0;
		length -= cpy;
		dest += cpy;
	}
	delete buf;

	return true;
}

/*
common functions used in NAND flash devices implementation
*/

void NANDFlashDevice::cmd_NONE(NANDFlashDevice* device)
{
	// nothing to do
}

void NANDFlashDevice::cmd_RESET(NANDFlashDevice* device)
{
	// reset nand
	device->set_RnB(0);
	device->StatusByte.Byte = 0x00;
	device->buffer_ptr = 0;
	device->address_valid2 = false;
	device->set_RnB(1);
}

void NANDFlashDevice::cmd_READID(NANDFlashDevice* device)
{
	device->need_address = abnOne;
	device->column_address = 0;
}

void NANDFlashDevice::cmd_READID_address(NANDFlashDevice* device)
{
	ASSERT(device->column_address == 0);

	memcpy(device->buffer, device->config.ID, device->config.ID_BYTES);
	device->buffer_ptr = device->column_address;
}

void NANDFlashDevice::cmd_READ(NANDFlashDevice* device)
{
	device->need_address = abnColumnRow;
	device->column_address = 0;
	device->row_address = 0;
}

void NANDFlashDevice::cmd_READ_address(NANDFlashDevice* device)
{
	cmd_READ_address_large(device);
	device->prev_commands <<= 8;
	device->prev_commands |= device->last_command;
	cmd_READ1(device);
}

void NANDFlashDevice::cmd_READ_address1(NANDFlashDevice* device)
{
	device->column_address |= 0x100;
	// fallback to CMD_READ
	device->last_command = CMD_READ;
	cmd_READ_address(device);
}

void NANDFlashDevice::cmd_READ_address2(NANDFlashDevice* device)
{
	device->column_address |= device->config.BYTES_PER_PAGE;
	// fallback to CMD_READ
	device->last_command = CMD_READ;
	cmd_READ_address(device);
}

void NANDFlashDevice::cmd_READ_address_large(NANDFlashDevice* device)
{
	ASSERT(device->row_address < device->config.PAGES_PER_DEVICE);
	ASSERT(device->column_address < device->config.PAGE_SIZE);
}

void NANDFlashDevice::cmd_READ1(NANDFlashDevice* device)
{
	if ((device->prev_commands & 0xFF) == CMD_READ) {
		// switch execution to thread
		device->set_RnB(0);
		device->thread_execute();
	} else {
		DBG_NAND_ERROR_OBJ(device, "cmd_READ1() after: 0x%02X", device->prev_commands & 0xFF);
	}
}

void NANDFlashDevice::cmd_READ1_thread(NANDFlashDevice* device)
{
	if (device->SeekPage(device->row_address)) {
		device->ReadPage(device->buffer);
		device->buffer_ptr = device->column_address;
	}
	device->set_RnB(1);
}

void NANDFlashDevice::cmd_READ_RANDOM(NANDFlashDevice* device)
{
	device->need_address = abnColumn;
	device->column_address = 0;
}

void NANDFlashDevice::cmd_READ_RANDOM_address(NANDFlashDevice* device)
{
	ASSERT(device->column_address < device->config.PAGE_SIZE);
}

void NANDFlashDevice::cmd_READ_RANDOM2(NANDFlashDevice* device)
{
	if ((device->prev_commands & 0xFF) == CMD_READ_RANDOM) {
		device->buffer_ptr = device->column_address;
	} else {
		DBG_NAND_ERROR_OBJ(device, "cmd_READ_RANDOM2() after: 0x%02X", device->prev_commands & 0xFF);
	}
}

void NANDFlashDevice::cmd_WRITE(NANDFlashDevice* device)
{
	device->need_address = abnColumnRow;
	device->column_address = 0;
	device->row_address = 0;
}

void NANDFlashDevice::cmd_WRITE_address(NANDFlashDevice* device)
{
	ASSERT(device->row_address < device->config.PAGES_PER_DEVICE);
	ASSERT(device->column_address < device->config.PAGE_SIZE);

	device->buffer_ptr = device->column_address;
	memset(device->buffer, 0xFF, device->config.PAGE_SIZE);
}

void NANDFlashDevice::cmd_COPYBACK(NANDFlashDevice* device)
{
	device->need_address = abnColumn;
	device->column_address = 0;
}

void NANDFlashDevice::cmd_COPYBACK_address(NANDFlashDevice* device)
{
	ASSERT(device->column_address < device->config.PAGE_SIZE);

	device->buffer_ptr = device->column_address;
}

void NANDFlashDevice::cmd_WRITE2(NANDFlashDevice* device)
{
	device->StatusByte.Fail = 1;
	if (((device->prev_commands & 0xFF) == CMD_WRITE) || ((device->prev_commands & 0xFF) == CMD_COPYBACK)) {
		// switch execution to thread
		device->set_RnB(0);
		device->thread_execute();
	} else {
		DBG_NAND_ERROR_OBJ(device, "cmd_WRITE2() after: 0x%02X", device->prev_commands & 0xFF);
	}
}

void NANDFlashDevice::cmd_WRITE2_DUMMY(NANDFlashDevice* device)
{
	if (((device->prev_commands & 0xFF) == CMD_WRITE) || ((device->prev_commands & 0xFF) == CMD_COPYBACK)) {
		device->set_RnB(0);
		memcpy(device->buffer2, device->buffer, device->buffer_size);
		device->address_valid2 = true;
		device->row_address2 = device->row_address;
		device->set_RnB(1);
	} else {
		DBG_NAND_ERROR_OBJ(device, "cmd_WRITE2() after: 0x%02X", device->prev_commands & 0xFF);
	}
}

void NANDFlashDevice::cmd_WRITE2_thread(NANDFlashDevice* device)
{
	bool result = false;
	if (device->address_valid2) {
		device->address_valid2 = false;
		if (device->SeekPage(device->row_address2)) {
			uint8_t* page = new uint8_t[device->config.PAGE_SIZE];
			result = device->ReadPage(page);
			if (result) {
				uint32_t* src = (uint32_t*)device->buffer2;
				uint32_t* dest = (uint32_t*)page;
				for (unsigned int i = 0; i < device->config.PAGE_SIZE / 4; i++) {
					*dest++ &= *src++;
				}
				result = device->SeekPage(device->row_address2);
				if (result)
					result = device->WritePage(page);
			}
			delete page;
		}
	} else {
		result = true;
	}
	if (result && device->SeekPage(device->row_address)) {
		uint8_t* page = new uint8_t[device->config.PAGE_SIZE];
		result = device->ReadPage(page);
		if (result) {
			uint32_t* src = (uint32_t*)device->buffer;
			uint32_t* dest = (uint32_t*)page;
			for (unsigned int i = 0; i < device->config.PAGE_SIZE / 4; i++) {
				*dest++ &= *src++;
			}
			result = device->SeekPage(device->row_address);
			if (result)
				result = device->WritePage(page);
		}
		delete page;
	}
	if (!result) {
		device->StatusByte.Fail = 1;
		DBG_NAND_ERROR_OBJ(device, "cmd_WRITE2_thread(p: %u)", device->row_address);
	} else {
		device->StatusByte.Fail = 0;
	}
	device->set_RnB(1);
}

void NANDFlashDevice::cmd_ERASE(NANDFlashDevice* device)
{
	if ((device->prev_commands & 0xFF) == CMD_ERASE) {
		// repeated erase
		device->address_valid2 = true;
		device->row_address2 = device->row_address;
	}
	device->need_address = abnRow;
	device->row_address = 0;
}

void NANDFlashDevice::cmd_ERASE_address(NANDFlashDevice* device)
{
	ASSERT(device->row_address < device->config.PAGES_PER_DEVICE);
	ASSERT(device->row_address % device->config.PAGES_PER_BLOCK == 0);
}

void NANDFlashDevice::cmd_ERASE2(NANDFlashDevice* device)
{
	if ((device->prev_commands & 0xFF) == CMD_ERASE) {
		// switch execution to thread
		device->set_RnB(0);
		device->thread_execute();
	} else {
		DBG_NAND_ERROR_OBJ(device, "cmd_ERASE2() after: 0x%02X", device->prev_commands & 0xFF);
	}
}

void NANDFlashDevice::cmd_ERASE2_thread(NANDFlashDevice* device)
{
	bool result = false;

	unsigned int len = device->config.PAGE_SIZE * device->config.PAGES_PER_BLOCK;
	uint8_t* data = new uint8_t[len];
	memset(data, 0xFF, len);
	if (device->address_valid2) {
		device->address_valid2 = false;
		if (device->SeekPage(device->row_address2)) {
			DBG_NAND("ERASE BLOCK: 0x%04X (%u)\n",
				(unsigned int)(ftell(device->hFile) / device->config.PAGE_SIZE / device->config.PAGES_PER_BLOCK),
				(unsigned int)(ftell(device->hFile) / device->config.PAGE_SIZE / device->config.PAGES_PER_BLOCK));
			result = (fwrite(data, len, 1, device->hFile) == 1);
		}
	} else {
		result = true;
	}
	if (result && device->SeekPage(device->row_address)) {
		DBG_NAND("ERASE BLOCK: 0x%04X (%u)\n",
			(unsigned int)(ftell(device->hFile) / device->config.PAGE_SIZE / device->config.PAGES_PER_BLOCK),
			(unsigned int)(ftell(device->hFile) / device->config.PAGE_SIZE / device->config.PAGES_PER_BLOCK));
		result = (fwrite(data, len, 1, device->hFile) == 1);
	}
	delete data;

	if (!result) {
		device->StatusByte.Fail = 1;
		DBG_NAND_ERROR_OBJ(device, "cmd_ERASE2_thread(b: %u)", device->row_address / device->config.PAGES_PER_BLOCK);
	} else {
		device->StatusByte.Fail = 0;
	}
	device->set_RnB(1);
}

void NANDFlashDevice::cmd_STATUS(NANDFlashDevice* device)
{
	device->need_address = abnNone;
	device->read_status = true;
}

void NANDFlashDeviceBase::Initialize()
{
	NANDFlashDevice::Initialize();

	// reset
	// 0xFF
	set_fn_final(CMD_RESET, cmd_RESET);

	// read ID
	// 0x90 0x00
	set_fn(CMD_READID, cmd_READID);
	set_fn_address_final(CMD_READID, cmd_READID_address);

	// read status
	// 0x70
	set_fn_final(CMD_STATUS, cmd_STATUS);
}

void NANDFlashDeviceSmallPage::Initialize()
{
	NANDFlashDeviceBase::Initialize();

	// read page (executed in thread)
	// 0x00 <COLUMN> <ROW>
	set_fn(CMD_READ, cmd_READ);
	set_fn_address_final(CMD_READ, cmd_READ_address);
	set_fn_thread(CMD_READ, cmd_READ1_thread);
	// 0x01 <COLUMN> <ROW>
	set_fn(CMD_READ + 1, cmd_READ);
	set_fn_address_final(CMD_READ + 1, cmd_READ_address1);
	set_fn_thread(CMD_READ + 1, cmd_READ1_thread);
	// 0x50 <COLUMN> <ROW>
	set_fn(CMD_READ2, cmd_READ);
	set_fn_address_final(CMD_READ2, cmd_READ_address2);
	set_fn_thread(CMD_READ2, cmd_READ1_thread);
	// TODO: sequential read
	// TODO: read2
	// TODO: block protect
	// TODO: read block status

	// random data output
	// 0x05 <COLUMN> 0xE0
	set_fn(CMD_READ_RANDOM, cmd_READ_RANDOM);
	set_fn_address(CMD_READ_RANDOM, cmd_READ_RANDOM_address);
	set_fn_final(CMD_READ_RANDOM2, cmd_READ_RANDOM2);

	// write page (executed in thread)
	// 0x80 <COLUMN> <ROW> <data> 0x10
	set_fn(CMD_WRITE, cmd_WRITE);
	set_fn_address(CMD_WRITE, cmd_WRITE_address);
	set_fn_final(CMD_WRITE2, cmd_WRITE2);
	set_fn_thread(CMD_WRITE2, cmd_WRITE2_thread);

	// erase block (executed in thread)
	// 0x60 <BLOCK> 0xD0
	set_fn(CMD_ERASE, cmd_ERASE);
	set_fn_address(CMD_ERASE, cmd_ERASE_address);
	set_fn_final(CMD_ERASE2, cmd_ERASE2);
	set_fn_thread(CMD_ERASE2, cmd_ERASE2_thread);
}

void NANDFlashDeviceLargePage::Initialize()
{
	NANDFlashDeviceBase::Initialize();

	// read page (executed in thread)
	// 0x00 <COLUMN> <ROW> 0x30
	set_fn(CMD_READ, cmd_READ);
	set_fn_address_final(CMD_READ, cmd_READ_address_large);
	set_fn_final(CMD_READ1, cmd_READ1);
	set_fn_thread(CMD_READ1, cmd_READ1_thread);

	// random data output
	// 0x05 <COLUMN> 0xE0
	set_fn(CMD_READ_RANDOM, cmd_READ_RANDOM);
	set_fn_address_final(CMD_READ_RANDOM, cmd_READ_RANDOM_address);
	set_fn_final(CMD_READ_RANDOM2, cmd_READ_RANDOM2);

	// write page (executed in thread)
	// 0x80 <COLUMN> <ROW> <data> [<0x85> <COLUMN> <data>]* 0x10
	set_fn(CMD_WRITE, cmd_WRITE);
	set_fn_address_final(CMD_WRITE, cmd_WRITE_address);
	set_fn_final(CMD_WRITE2, cmd_WRITE2);
	set_fn(CMD_COPYBACK, cmd_COPYBACK);
	set_fn_address_final(CMD_COPYBACK, cmd_COPYBACK_address);
	set_fn_thread(CMD_WRITE2, cmd_WRITE2_thread);

	// Two-Plane page write
	// 0x80 <COLUMN> <ROW> <data> [<0x85> <COLUMN> <data>]* 0x11
	// 0x81 <COLUMN> <ROW> <data> [<0x85> <COLUMN> <data>]* 0x10
	set_fn(CMD_WRITE_PAGE2, cmd_WRITE);
	set_fn_address_final(CMD_WRITE_PAGE2, cmd_WRITE_address);
	set_fn_final(CMD_WRITE2_DUMMY, cmd_WRITE2_DUMMY);
	set_fn_thread(CMD_WRITE2_DUMMY, cmd_WRITE2_thread);

	// erase block (executed in thread)
	// 0x60 <BLOCK> 0xD0
	set_fn(CMD_ERASE, cmd_ERASE);
	set_fn_address_final(CMD_ERASE, cmd_ERASE_address);
	set_fn_final(CMD_ERASE2, cmd_ERASE2);
	set_fn_thread(CMD_ERASE2, cmd_ERASE2_thread);
}

