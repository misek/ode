#ifndef NANDFLASHDEVICE_H_INCLUDED
#define NANDFLASHDEVICE_H_INCLUDED

#include "../component.h"

#define CMD_READ					0x00		//	Read
#define CMD_READ_RANDOM				0x05		//	Random data output
#define CMD_READ_RANDOM2			0xE0		//	Random data output phase 2
#define CMD_READ1					0x30		//	Read1
#define CMD_READ2					0x50		//	Read2
#define CMD_READID					0x90		//	ReadID
#define CMD_WRITE					0x80		//	Write phase 1
#define CMD_WRITE_PAGE2				0x81		//	Two-Plane write page 2
#define CMD_COPYBACK				0x85		//  Copy-Back Program phase 1
#define CMD_WRITE2					0x10		//	Write phase 2
#define CMD_WRITE2_DUMMY			0x11		//	Two-Plane write dummy write confirm
#define CMD_ERASE					0x60		//	Erase phase 1
#define CMD_ERASE2					0xD0		//	Erase phase 2
#define CMD_STATUS					0x70		//	Status read
#define CMD_RESET					0xFF		//	Reset

class NANDFlashDevice;

enum NAND_CALLBACK_CODE {nandUnknown, nandReady, nandBusy};

typedef void (COMPONENT_METHOD *exec_fun_ptr)(NANDFlashDevice* dev);
typedef void (COMPONENT_METHOD *nand_callback)(NANDFlashDevice* dev, NAND_CALLBACK_CODE code, void* param, void* user);

struct NAND_FUN {
	exec_fun_ptr ptr;
	bool final;
	const char* name;
	int code;
};

struct NANDConfigParameters {
	unsigned int BYTES_PER_PAGE;
	unsigned int SPARE_SIZE;
	unsigned int PAGE_SIZE;
	unsigned int PAGES_PER_BLOCK;
	unsigned int BLOCKS_PER_DEVICE;
	unsigned int PAGES_PER_DEVICE;
	unsigned int ID_BYTES;
	unsigned int COL_ADDRESS_BYTES;
	unsigned int ROW_ADDRESS_BYTES;

	uint8_t ID[8];

	NAND_FUN cmd_fn[256];
	NAND_FUN cmd_fn_address[256];
	NAND_FUN cmd_fn_thread[256];
};

enum AddressBytesNeeded {abnNone, abnOne, abnColumn, abnRow, abnColumnRow};

class NANDFlashDevice: public EmulatedComponent {
private:
	nand_callback callback_fn;
	void* callback_user;

	Thread thread;
	AutoResetEvent hEvent;
	unsigned int written_bytes;
	unsigned int read_bytes;

	static CALLBACK bool pThreadProc(Thread* t, void* data);

	void DoReadWriteDebugInfo();

protected:
	NANDConfigParameters config;

	FILE* hFile;

	uint32_t prev_commands;
	uint8_t last_command;
	uint8_t last_thread_command;

	AddressBytesNeeded need_address;
	AddressBytesNeeded read_address;
	unsigned int current_address_byte;

	unsigned int column_address;
	unsigned int row_address;
	unsigned int column_address2;
	unsigned int row_address2;
	uint8_t* buffer2;
	bool address_valid2;

	uint8_t* buffer;
	unsigned int buffer_ptr;
	unsigned int buffer_size;

	union {
		uint8_t Byte;
		struct {
			uint8_t Fail:1;
			uint8_t Reserved_1:1;
			uint8_t Reserved_2:1;
			uint8_t Reserved_3:1;
			uint8_t Reserved_4:1;
			uint8_t Reserved_5:1;
			uint8_t Ready:1;
			uint8_t WriteProtect:1;
		};
	} StatusByte;

	bool read_status;

	COMPONENT_METHOD virtual void ThreadFunction();

	COMPONENT_METHOD virtual bool SeekPage(unsigned int page_no);
	COMPONENT_METHOD virtual bool ReadPage(void* pBuffer);
	COMPONENT_METHOD virtual bool WritePage(void* pBuffer);

	COMPONENT_METHOD virtual void CloseFile();

	COMPONENT_METHOD virtual void cmd_execute();
	COMPONENT_METHOD virtual void cmd_execute_address();

	COMPONENT_METHOD virtual void thread_execute();
	COMPONENT_METHOD virtual void set_RnB(int value);

	COMPONENT_METHOD static void cmd_NONE(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_RESET(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_READID(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READID_address(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_READ(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_address(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_address1(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_address2(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_address_large(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ1(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ1_thread(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_READ_RANDOM(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_RANDOM_address(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_READ_RANDOM2(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_WRITE(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_WRITE_address(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_COPYBACK(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_COPYBACK_address(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_WRITE2(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_WRITE2_DUMMY(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_WRITE2_thread(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_STATUS(NANDFlashDevice* device);

	COMPONENT_METHOD static void cmd_ERASE(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_ERASE_address(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_ERASE2(NANDFlashDevice* device);
	COMPONENT_METHOD static void cmd_ERASE2_thread(NANDFlashDevice* device);

public:
	NANDFlashDevice();
	virtual ~NANDFlashDevice();

	COMPONENT_METHOD virtual bool UseFile(const char* fileName);

	COMPONENT_METHOD virtual void Initialize();

	COMPONENT_METHOD virtual void WriteCommand(uint8_t command);
	COMPONENT_METHOD virtual void WriteAddress(uint8_t address);

	COMPONENT_METHOD virtual void WriteByte(uint8_t Value);
	COMPONENT_METHOD virtual void WriteHalfWord(uint16_t Value);
	COMPONENT_METHOD virtual void WriteWord(uint32_t Value);

	COMPONENT_METHOD virtual uint8_t ReadByte();
	COMPONENT_METHOD virtual uint16_t ReadHalfWord();
	COMPONENT_METHOD virtual uint32_t ReadWord();

	COMPONENT_METHOD virtual bool Read(void* pBuffer, unsigned int length);
	COMPONENT_METHOD virtual bool ReadRAW(void* pBuffer, const unsigned long start, unsigned int length);

	COMPONENT_METHOD virtual void SetCallback(nand_callback cb, void* user);

};

class NANDFlashDeviceBase: public NANDFlashDevice {
public:
	COMPONENT_METHOD virtual void Initialize();

};

class NANDFlashDeviceSmallPage: public NANDFlashDeviceBase {
public:
	COMPONENT_METHOD virtual void Initialize();

};

class NANDFlashDeviceLargePage: public NANDFlashDeviceBase {
public:
	COMPONENT_METHOD virtual void Initialize();

};

#endif
