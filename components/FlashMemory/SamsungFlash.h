#ifndef SAMSUNGFLASH_H_INCLUDED
#define SAMSUNGFLASH_H_INCLUDED

#include "NANDFlashDevice.h"

class K9G8G08XXX: public NANDFlashDeviceLargePage {
public:
	COMPONENT_METHOD void Initialize();

};

class K9G8G08U0B: public K9G8G08XXX {
public:
	COMPONENT_METHOD void Initialize();

};

class K9F1208X0C: public NANDFlashDeviceSmallPage {
public:
	COMPONENT_METHOD void Initialize();

};

#endif
