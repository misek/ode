#include "sdbus.h"

#ifdef DEBUG_SD
static const char* CARD_STATUS_STRING[16] = {
	"Idle", "Ready", "Identification", "Stabdby", "Transfer", "SendingData", "ReceiveData", "Programming", "Disconnect"};

#define DBG_SD_BUS(fmt, ...) DEBUG_PRINT("SDBUS: " fmt "\n", ##__VA_ARGS__)
#define DBG_SD(fmt, ...) DEBUG_PRINT("SDDEV[%04X, %s]: " fmt "\n", regRCA, CARD_STATUS_STRING[regCARD_STATUS.CURRENT_STATE], ##__VA_ARGS__)
#else
#define DBG_SD_BUS(fmt, ...)
#define DBG_SD(fmt, ...)
#endif

#if 0
#define DBG_SD_BUSv DBG_SD_BUS
#define DBG_SDv DBG_SD
#else
#define DBG_SD_BUSv(fmt, ...)
#define DBG_SDv(fmt, ...)
#endif

#define DBG_SD_BUS_CMD(cmd, arg) DBG_SD_BUSv("command %u:%u (0x%08X)", (cmd) >> 6, (cmd) & 0x3F, arg)
#define DBG_SD_CMD(cmd_index) DBG_SDv(#cmd_index "(%u), arg: 0x%08X", cmd_index, sdcmd.Argument.Word)
#define DBG_SD_CMD_InvalidState(cmd_index) DBG_SD("Invalid Card state: " #cmd_index "(0x%08X)", sdcmd.Argument.Word)

#define IsMMCCard() 0
#define LAST_CMD_APP 0x100

#define __R1(r) r.Short.Data.R1
#define __R1b(r) __R1(r)
#define __R2(r) r.Long.Data.R2
#define __R3(r) r.Short.Data.R3
#define __R6(r) r.Short.Data.R6

SDBus::SDBus() : currentCommandNo(0), lastCommandNo(0), devices_count(0)
{
	thread.Start(SDBus::SDBusThreadProc, this, "dev/SDBus");
}

SDBus::~SDBus()
{
	thread.Terminate();
}

void SDBus::AddDevice(SDDevice* dev)
{
	//TODO: do not allow multiple adding the same device
	dev->bus = this;
	devices[devices_count++] = dev;
}

void SDBus::SetCallback(SDBus_callback callback_fn)
{
	cb = callback_fn;
}

void SDBus::ExecuteCallback(SDDevice* dev, SDBUS_CALLBACKTYPE type, SDRESPONSE* resp)
{
	if (cb) {
		cb(this, dev, type, resp);
	}
}

bool SDBus::SDBusThreadProc(Thread* t, void* data)
{
	SDBus* bus = (SDBus*)data;

	bus->ThreadFunction();

	return true;
}

void SDBus::ThreadFunction()
{
	hEvent.WaitFor();
	SDBUS_REQUEST* req = NULL;
	commandSection.Lock();
	if (!requestList.empty()) {
		req = requestList.front();
		requestList.pop_front();
		// do again if another command pending
		if (requestList.size() > 0)
			hEvent.Signal();
	}
	commandSection.Unlock();
	if (!req) {
		return;
	}

	// send command to device
	bool command_was_sent = false;
	for (uint i = 0; i < devices_count; i++) {
		switch (req->type) {
			case reqCommand:
				currentCommandNo = req->cmdNo;
				SetWriteBufferSizeHint(0);
				command_was_sent |= devices[i]->SendCommand(&req->cmd, currentCommandNo);
				break;
			case reqReceiveData:
				devices[i]->ReceiveData(req->data_length);
				break;
			case reqSendData:
				devices[i]->WriteData(req->data, req->data_length);
				break;
			case reqBusyWait:
				ASSERT(responseDevice);
				break;
			default:
				ASSERT(false);
				break;
		}
	}

	// command was sent to device
	switch (req->type) {
		case reqCommand:
			if (command_was_sent) {
				// command was sent, notify
				ExecuteCallback(NULL, sd_CommandSent, NULL);
			}
			break;
		default:
			break;
	}

	// wait for current command to complete
	for (unsigned i = 0; i < devices_count; ++i) {
		devices[i]->WaitForIdle();
	}

	// post-command action
	switch (req->type) {
		case reqCommand:
			if (responseDevice == NULL) {
				// no device responds
				ExecuteCallback(NULL, sd_NoResponse, NULL);
			} else {
				ExecuteCallback(responseDevice, sd_Response, &sdresp);
			}
			break;
		case reqBusyWait:
			if (responseDevice) {
				responseDevice->WaitForIdle();
				ExecuteCallback(responseDevice, sd_BusyFinish, NULL);
			}
			break;
		default:
			break;
	}

	if (req->data) {
		delete (uint8_t*)req->data;
	}
	delete req;
}

bool SDBus::SendCommand(SDCOMMAND* cmd)
{
	SDBUS_REQUEST* req = new SDBUS_REQUEST;
	req->type = reqCommand;
	req->cmd = *cmd;
	req->data = NULL;
	commandSection.Lock();
	req->cmdNo = ++lastCommandNo;
	responseDevice = NULL;
	if (req->cmd.Command.Index == SD_CMD_GO_IDLE_STATE) {
		// clear queue if it is GO TO IDLE command
		requestList.clear();
	}
	requestList.push_back(req);
	hEvent.Signal();
	commandSection.Unlock();
	return true;
}

bool SDBus::BusyWait()
{
	SDBUS_REQUEST* req = new SDBUS_REQUEST;
	req->type = reqBusyWait;
	req->data = NULL;
	commandSection.Lock();
	requestList.push_back(req);
	hEvent.Signal();
	commandSection.Unlock();
	return true;
}

void SDBus::SetWriteBufferSizeHint(uint size)
{
	write_size_hint = size;
}

bool SDBus::SendData(void* data, uint size)
{
	SDBUS_REQUEST* req = NULL;
	commandSection.Lock();
	if (requestList.size() > 0) {
		req = requestList.front();
		if ((req->type == reqSendData) && (req->data_length + size <= SDBUS_REQUEST_DATA_CAPACITY)) {
			memcpy((uint8_t*)req->data + req->data_length, data, size);
			req->data_length += size;
		} else {
			req = NULL;
		}
	}
	if (req == NULL) {
		req = new SDBUS_REQUEST;
		req->type = reqSendData;
		req->data_length = size;
		req->data = new uint8_t[SDBUS_REQUEST_DATA_CAPACITY];
		memcpy(req->data, data, size);
		requestList.push_back(req);
	}
	if ((write_size_hint == 0) || (req->data_length >= write_size_hint)) {
		hEvent.Signal();
	}
	commandSection.Lock();
	return true;
}

bool SDBus::ReceiveData(unsigned int size)
{
	SDBUS_REQUEST* req = NULL;
	commandSection.Lock();
	if (requestList.size() > 0) {
		req = requestList.front();
		if (req->type == reqReceiveData) {
			req->data_length += size;
		} else {
			req = NULL;
		}
	}
	if (req == NULL) {
		req = new SDBUS_REQUEST;
		req->type = reqReceiveData;
		req->data_length = size;
		req->data = NULL;
		requestList.push_back(req);
		hEvent.Signal();
	}
	commandSection.Unlock();
	return true;
}

void SDBus::SendSDResponse(SDDevice* dev, SDBUS_CALLBACKTYPE type, SDRESPONSE* resp)
{
	if (cb) {
		switch (type) {
			case sd_Response:
				if (responseDevice) {
					// response already received
					if (responseDevice == dev) {
						// another response from the same device (write block)
						if (dev->sdcmdNo == currentCommandNo) {
							ExecuteCallback(dev, type, resp);
						} else {
							DBG_SD_BUS("command was interrupted by another command, response ignored!");
						}
					} else {
						// response from another device, ignore it
						DBG_SD_BUS("another device sends response, ignored another response!");
					}
				} else {
					if (dev->sdcmdNo == currentCommandNo) {
						responseDevice = dev;
						sdresp = *resp;
					} else {
						// another command was invoked, ignore response
					}
				}
				break;
			case sd_Data:
				if (responseDevice) {
					if (dev == responseDevice) {
						if (dev->sdcmdNo == currentCommandNo) {
							ExecuteCallback(dev, type, resp);
						} else {
							DBG_SD_BUS("command was interrupted by another command, data ignored!");
						}
					} else {
						DBG_SD_BUS("another device sends response, can not send data!");
					}
				} else {
					DBG_SD_BUS("data sent BEFORE response!");
				}
				break;
			default:
				DBG_SD_BUS("invalid SD response callback type: %u!", type);
				break;
		}
	}
}

void SDBus::SendSDResponseShort(SDDevice* dev, SDRESPONSE* resp)
{
	resp->Short.CRC = crc7(resp, 1 + sizeof(resp->Short.Data));
	resp->Short.End = 1;
	SendSDResponse(dev, sd_Response, resp);
}

void SDBus::SendSDResponseLong(SDDevice* dev, SDRESPONSE* resp)
{
	resp->Long.CRC = crc7(resp, 1 + sizeof(resp->Long.Data));
	resp->Long.End = 1;
	SendSDResponse(dev, sd_Response, resp);
}

void SDBus::SendData(SDDevice* dev, void* data, unsigned int size)
{
	SDDATA d;
	d.data = data;
	d.length = size;
	SendSDResponse(dev, sd_Data, (SDRESPONSE*)&d);
}

SDDevice::SDDevice() : EmulatedComponent()
{
	hSDFile = __null;
	sdcmdNo = 0;
	last_command_index = 0;
	buffer_length = 0;
	active = false;
	buffer_ptr = 0;

	bus = __null;
	thread.Start(SDDevice::SDDeviceThreadProc, this, "dev/SDDevice");
}

SDDevice::~SDDevice()
{
	reqType = reqUndefined;
	hEvent.Signal();
	thread.Terminate();
}

void SDDevice::InitializeDevice()
{
	active = true;
	// set default RCA
	regRCA = 0x0000;
	// reset Card Status
	regCARD_STATUS.Word = 0x00000000;
	memset(&regSD_STATUS, 0, sizeof(regSD_STATUS));
}

void SDDevice::SetCardStatus(CARD_STATUS status)
{
	switch (status) {
		case csInactive:
			active = false;
			break;
		default:
			regCARD_STATUS.CURRENT_STATE = status;
			break;
	}
}

void SDDevice::WaitForIdle()
{
	hIdle.WaitFor();
}

bool SDDevice::SendCommand(SDCOMMAND* cmd, unsigned int cmdNo)
{
	if (active) {
		reqType = reqCommand;
		sdcmd = *cmd;
		sdcmdNo = cmdNo;
		hIdle.Reset();
		hEvent.Signal();
		return true;
	} else {
		// card is inactive
		// do nothing
		return false;
	}
}

bool SDDevice::SDDeviceThreadProc(Thread* t, void* data)
{
	SDDevice* dev = (SDDevice*)data;
	return dev->ThreadFunction();
}

bool SDDevice::ThreadFunction()
{
	hEvent.WaitFor();
	switch (reqType) {
		case reqUndefined:
			// terminate thread
			return false;
		case reqCommand:
			// command
			ExecuteSDCommand();
			break;
		case reqReceiveData:
			// send data to HOST
			DoReceiveData();
			break;
		case reqSendData:
			// receive data from HOST
			DoWriteData();
			break;
		default:
			ASSERT(false);
			break;
	}

	hIdle.Signal();
	return true;
}

void SDDevice::SendData(void* data, unsigned int size)
{
	bus->SendData(this, data, size);
}

bool SDDevice::WriteData(void* data, unsigned int size)
{
	if (active) {
		ASSERT(buffer_ptr + buffer_length + size <= SDDEVICE_BLOCK_BUFFER_CAPACITY);
		DBG_SDv("data write: %u bytes\n", size);
		memcpy(&block_buffer[buffer_ptr + buffer_length], data, size);
		buffer_length += size;
		reqType = reqSendData;
		hIdle.Reset();
		hEvent.Signal();
		return true;
	} else {
		// card is inactive
		// do nothing
		return false;
	}
}

void SDDevice::SendSDResponseR1()
{
	__R1(sdresp).CARD_STATUS = regCARD_STATUS;
	bus->SendSDResponseShort(this, &sdresp);
}

void SDDevice::SendSDResponseR1b()
{
	__R1(sdresp).CARD_STATUS = regCARD_STATUS;
	bus->SendSDResponseShort(this, &sdresp);
}

void SDDevice::SendSDResponseR2()
{
	bus->SendSDResponseLong(this, &sdresp);
}

void SDDevice::SendSDResponseR3()
{
	bus->SendSDResponseShort(this, &sdresp);
}

void SDDevice::SendSDResponseR6()
{
	__R6(sdresp).RCA = regRCA;
	__R6(sdresp).CardStatus = GetCardStatusR6();
	bus->SendSDResponseShort(this, &sdresp);
}

uint16_t SDDevice::GetCardStatusR6()
{
	// card status bits: 23,22,19,12:0
	return (regCARD_STATUS.COM_CRC_ERROR << 15) |
		(regCARD_STATUS.ILLEGAL_COMMAND << 14) |
		(regCARD_STATUS._ERROR << 13) |
		(regCARD_STATUS.HalfWord[0] & 0x1FFF);
}

void SDDevice::ExecuteCallback(SDBUS_CALLBACKTYPE type, SDRESPONSE* resp)
{
	bus->ExecuteCallback(this, type, resp);
}

bool SDDevice::SetBlockAddress(uint32_t address)
{
	bool ret = false;
	if ((address % block_length) != 0) {
		// unaligned address
		regCARD_STATUS.ADDRESS_ERROR = 1;
	} else {
		hSDFile_cs.Lock();
		DBG_SDv("set block address: 0x%08X, block: %u", address, address / block_length);
		if (fseek(hSDFile, address, SEEK_SET) != 0) {
			DBG_SD("can not set block address: 0x%08X, block: %u", address, address / block_length);
		}
		hSDFile_cs.Unlock();
		ret = true;
	}
	return ret;
}

bool SDDevice::ReadCardBlock()
{
	hSDFile_cs.Lock();
	DBG_SDv("--> block read");
	bool ret = fread(block_buffer, block_length, 1, hSDFile) == 1;
	if (ret) {
		buffer_ptr = 0;
		buffer_length = block_length;
	} else {
		buffer_length = 0;
	}
	hSDFile_cs.Unlock();

	return ret;
}

bool SDDevice::WriteCardBlock()
{
	hSDFile_cs.Lock();
	DBG_SDv("--> block write");
	bool ret = fwrite(block_buffer, block_length, 1, hSDFile) == 1;
	if (ret) {
		buffer_length -= block_length;
		if (buffer_length > 0) {
			memmove(block_buffer, block_buffer + block_length, buffer_length);
		}
		buffer_ptr = 0;
	}
	hSDFile_cs.Unlock();

	return ret;
}

bool SDDevice::ReceiveData(unsigned int size)
{
	if (active) {
		reqType = reqReceiveData;
		reqDataLength = size;
		hIdle.Reset();
		hEvent.Signal();
		return true;
	} else {
		// card is inactive
		// do nothing
		return false;
	}
}

void SDDevice::DoWriteData()
{
	switch (last_command_index) {
		case SD_CMD_WRITE_SINGLE_BLOCK:
			switch (regCARD_STATUS.CURRENT_STATE) {
				case csReceiveData:
					if (buffer_length >= block_length) {
						WriteCardBlock();
						SetCardStatus(csProgramming);
						SetCardStatus(csTransfer);
						SendSDResponseR1b();
					}
					break;
				case csTransfer:
					return;
				}
			break;
		case SD_CMD_WRITE_MULTIPLE_BLOCK:
			switch (regCARD_STATUS.CURRENT_STATE) {
				case csReceiveData:
					// write blocks
					while (buffer_length >= block_length) {
						WriteCardBlock();
						SendSDResponseR1b();
					}
					break;
				default:
					DBG_SD("data read request when invalid state after SD_CMD_READ_MULTIPLE_BLOCK");
					break;
			}
			break;
		default:
			DBG_SD("data write with unknown%s command: %u", last_command_index & LAST_CMD_APP ? " APP" : "", sdcmd.Command.Index);
			break;
	}
}

void SDDevice::DoReceiveData()
{
	switch (last_command_index) {
		case SD_CMD_READ_SINGLE_BLOCK:
		case LAST_CMD_APP | SD_ACMD_SEND_SCR:
			switch (regCARD_STATUS.CURRENT_STATE) {
				case csSendingData:
					while (reqDataLength > 0) {
						unsigned int to_read = std::min(reqDataLength, buffer_length);
						SendData(&block_buffer[buffer_ptr], to_read);
						buffer_ptr += to_read;
						reqDataLength -= to_read;
						buffer_length -= to_read;
					}
					SetCardStatus(csTransfer);
					break;
				default:
					DBG_SD("data read request when invalid state, %scommand: %u",
						last_command_index & LAST_CMD_APP ? "APP " : "", last_command_index & 0x3F);
					break;
			}
			break;
		case LAST_CMD_APP | SD_ACMD_SD_STATUS:
			switch (regCARD_STATUS.CURRENT_STATE) {
				case csTransfer:
					while (reqDataLength > 0) {
						uint to_read = std::min(reqDataLength, buffer_length);
						SendData(&block_buffer[buffer_ptr], to_read);
						buffer_ptr += to_read;
						reqDataLength -= to_read;
						buffer_length -= to_read;
					}
					break;
				default:
					DBG_SD("data read request when invalid state, %scommand: %u",
						last_command_index & LAST_CMD_APP ? "APP " : "", last_command_index & 0x3F);
					break;
			}
			break;
		case SD_CMD_READ_MULTIPLE_BLOCK:
			switch (regCARD_STATUS.CURRENT_STATE) {
				case csSendingData:
					// read blocks
					while (reqDataLength > 0) {
						if (buffer_length == 0) {
							// read next block
							if (!ReadCardBlock()) {
								DBG_SD("SDDevice::ReceiveData(): block read failed");
								return;
							}
						}
						uint to_read = std::min(reqDataLength, buffer_length);
						SendData(&block_buffer[buffer_ptr], to_read);
						buffer_ptr += to_read;
						reqDataLength -= to_read;
						buffer_length -= to_read;
					}
					break;
				default:
					DBG_SD("data read request when invalid state after SD_CMD_READ_MULTIPLE_BLOCK");
					break;
			}
			break;
		default:
			DBG_SD("data read with unknown%s command: %u", last_command_index & LAST_CMD_APP ? " APP" : "", sdcmd.Command.Index);
			break;
	}
}

bool SDDevice::ExecuteSDCommand()
{
	// get command type
	if (sdcmd.Command.Start != 0) {
		DBG_SD("Invalid command start bit");
		return false;
	}
	regCARD_STATUS.ILLEGAL_COMMAND = 0;
	switch (sdcmd.Command.Host) {
		case 1:
			// host command
			last_command_index = sdcmd.Command.Index;
			if (regCARD_STATUS.APP_CMD) {
				// application command
				last_command_index |= LAST_CMD_APP;
				regCARD_STATUS.APP_CMD = 0;
				switch (sdcmd.Command.Index) {
					case SD_ACMD_SET_BUS_WIDTH:
						{
							DBG_SD_CMD(SD_ACMD_SET_BUS_WIDTH);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									switch (sdcmd.Argument.Word & 3) {
										case SD_ACMD_ARG_SET_BUS_4BIT:
											regSD_STATUS.DAT_BUS_WIDTH = SD_ACMD_ARG_SET_BUS_4BIT;
											DBG_SD("set SD bus width: 4-bit");
											SendSDResponseR1();
											break;
										case SD_ACMD_ARG_SET_BUS_1BIT:
											regSD_STATUS.DAT_BUS_WIDTH = SD_ACMD_ARG_SET_BUS_1BIT;
											DBG_SD("set SD bus width: 1-bit");
											SendSDResponseR1();
											break;
										default:
											DBG_SD("invalid SD bus width: %u", sdcmd.Argument.Word & 3);
											break;
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_ACMD_SET_BUS_WIDTH);
									break;
							}
						}
						break;
					case SD_ACMD_SD_STATUS:
						{
							DBG_SD_CMD(SD_ACMD_SD_STATUS);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									SendSDResponseR1();
									buffer_length = sizeof(regSD_STATUS);
									memcpy(block_buffer, &regSD_STATUS, sizeof(buffer_length));
									buffer_ptr = 0;
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_ACMD_SD_STATUS);
									break;
							}
						}
						break;
					case SD_ACMD_SD_SEND_OP_COND:
						{
							DBG_SD_CMD(SD_ACMD_SD_SEND_OP_COND);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csIdle:
									if (sdcmd.Argument.Word == 0) {
										// query for OCR
										__R3(sdresp).OCR = regOCR;
										SendSDResponseR3();
									} else {
										OCR_REGISTER ocr = sdcmd.Argument.OCR;
										if ((ocr.Word & regOCR.Word) == 0) {
											// no valid voltage range
											SetCardStatus(csInactive);
										} else {
											// valid voltage range
											regCARD_STATUS.CURRENT_STATE = csReady;
											__R3(sdresp).OCR.Word = regOCR.Word & ocr.Word;
#ifdef DEBUG_SD
											uint32_t val = __R3(sdresp).OCR.Word >> 4;
											uint vmin = 16;
											while ((vmin < 36) && ((val & 1) == 0)) {
												vmin++;
												val >>= 1;
											}
											DBG_SD("set SD card voltage: %d.%dV - %d.%dV",
												vmin / 10, vmin % 10, (vmin + 1) / 10, (vmin + 1) % 10);
#endif
											__R3(sdresp).OCR.PowerStatus = 1;
											SetCardStatus(csReady);
											SendSDResponseR3();
										}
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_ACMD_SD_SEND_OP_COND);
									break;
							}
						}
						break;
					case SD_ACMD_SET_CLR_CARD_DETECT:
						{
							DBG_SD_CMD(SD_ACMD_SET_CLR_CARD_DETECT);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									DBG_SD("turn %s internal pull-up resistor", sdcmd.Argument.Word & 1 ? "ON" : "OFF");
									SendSDResponseR1();
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_ACMD_SET_CLR_CARD_DETECT);
									break;
							}
						}
						break;
					case SD_ACMD_SEND_SCR:
						{
							DBG_SD_CMD(SD_ACMD_SEND_SCR);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									SetCardStatus(csSendingData);
									STRUCT_REVERSE(regSCR);
									buffer_length = sizeof(regSCR);
									memcpy(block_buffer, &regSCR, sizeof(buffer_length));
									buffer_ptr = 0;
									STRUCT_REVERSE(regSCR);
									SendSDResponseR1();
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_ACMD_SEND_SCR);
									break;
							}
						}
						break;
					default:
						// unknown application command
						DBG_SD("unknown APP command: %u:%u, arg: 0x%08X", sdcmd.Command.Host, sdcmd.Command.Index, sdcmd.Argument.Word);
						return false;
				}
			} else {
				switch (sdcmd.Command.Index) {
					case SD_CMD_GO_IDLE_STATE:
						{
							DBG_SD_CMD(SD_CMD_GO_IDLE_STATE);
							// go into idle state
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csIdle:
								case csReady:
								case csIdentification:
								case csStabdby:
								case csTransfer:
								case csSendingData:
								case csReceiveData:
								case csProgramming:
								case csDisconnect:
									SetCardStatus(csIdle);
									InitializeDevice();
									// no response
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_GO_IDLE_STATE);
									break;
							}
						}
						break;
					case SD_CMD_ALL_SEND_CID:
						{
							DBG_SD_CMD(SD_CMD_ALL_SEND_CID);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csReady:
									SetCardStatus(csIdentification);
									STRUCT_REVERSE(regCID);
									__R2(sdresp).CID = regCID;
									STRUCT_REVERSE(regCID);
									SendSDResponseR2();
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_ALL_SEND_CID);
									break;
							}
						}
						break;
					case SD_CMD_SEND_RELATIVE_ADDR:
						{
							DBG_SD_CMD(SD_CMD_SEND_RELATIVE_ADDR);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csIdentification:
									{
										SetCardStatus(csStabdby);
										RCA_REGISTER newRCA = 1 + (rand() & 0xFFFE);
										DBG_SD("asking for card RCA: 0x%04X", newRCA);
										regRCA = newRCA;
										SendSDResponseR6();
									}
									break;
								case csStabdby:
									DBG_SD("setting new card RCA: 0x%04X", __R6(sdresp).RCA);
									regRCA = sdcmd.Argument.RCA.RCA;
									SendSDResponseR6();
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_SEND_RELATIVE_ADDR);
									break;
							}
						}
						break;
					case SD_CMD_SELECT_DESELECT_CARD:
						{
							DBG_SD_CMD(SD_CMD_SELECT_DESELECT_CARD);
							if (sdcmd.Argument.RCA.RCA == regRCA) {
								// select card
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csStabdby:
										SetCardStatus(csTransfer);
										SendSDResponseR1();
										break;
									case csDisconnect:
										SetCardStatus(csProgramming);
										SendSDResponseR1();
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_SELECT_DESELECT_CARD);
										break;
								}
							} else {
								// deselect this card
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csStabdby:
									case csTransfer:
									case csSendingData:
										SetCardStatus(csStabdby);
										break;
									case csProgramming:
										SetCardStatus(csDisconnect);
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_SELECT_DESELECT_CARD);
										break;
								}
							}
						}
						break;
					case SD_CMD_SEND_CSD:
						{
							if (sdcmd.Argument.RCA.RCA == regRCA) {
								DBG_SD_CMD(SD_CMD_SEND_CSD);
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csStabdby:
										STRUCT_REVERSE(regCSD);
										__R2(sdresp).CSD = regCSD;
										STRUCT_REVERSE(regCSD);
										SendSDResponseR2();
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_SEND_CSD);
										break;
								}
							}
						}
						break;
					case SD_CMD_SEND_CID:
						{
							if (sdcmd.Argument.RCA.RCA == regRCA) {
								DBG_SD_CMD(SD_CMD_SEND_CID);
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csStabdby:
										STRUCT_REVERSE(regCID);
										__R2(sdresp).CID = regCID;
										STRUCT_REVERSE(regCID);
										SendSDResponseR2();
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_SEND_CID);
										break;
								}
							}
						}
						break;
					case SD_CMD_STOP_TRANSMISSION:
						{
							DBG_SD_CMD(SD_CMD_STOP_TRANSMISSION);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csSendingData:
									SetCardStatus(csTransfer);
									SendSDResponseR1b();
									break;
								case csReceiveData:
									SetCardStatus(csProgramming);
									SetCardStatus(csTransfer);
									SendSDResponseR1b();
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_STOP_TRANSMISSION);
									break;
							}
						}
						break;
					case SD_CMD_SEND_STATUS:
						{
							if (sdcmd.Argument.RCA.RCA == regRCA) {
								DBG_SD_CMD(SD_CMD_SEND_STATUS);
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csStabdby:
									case csTransfer:
									case csSendingData:
									case csReceiveData:
									case csProgramming:
									case csDisconnect:
										SendSDResponseR1();
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_SEND_STATUS);
										break;
								}
							}
						}
						break;
					case SD_CMD_SET_BLOCKLEN:
						{
							DBG_SD_CMD(SD_CMD_SET_BLOCKLEN);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									if (sdcmd.Argument.Word == block_length) {
										DBG_SD("set block length: %u bytes", sdcmd.Argument.Word);
										SendSDResponseR1();
									} else {
										DBG_SD("invalid block length set: %u bytes (only %u is valid)", sdcmd.Argument.Word, block_length);
										SendSDResponseR1();
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_SET_BLOCKLEN);
									break;
							}
						}
						break;
					case SD_CMD_READ_SINGLE_BLOCK:
						{
							DBG_SD_CMD(SD_CMD_READ_SINGLE_BLOCK);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									if (SetBlockAddress(sdcmd.Argument.Word)) {
										SendSDResponseR1();
										SetCardStatus(csSendingData);
										ReadCardBlock();
									} else {
										DBG_SD("read single block error at 0x%08X", sdcmd.Argument.Word);
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_READ_SINGLE_BLOCK);
									break;
							}
						}
						break;
					case SD_CMD_READ_MULTIPLE_BLOCK:
						{
							DBG_SD_CMD(SD_CMD_READ_MULTIPLE_BLOCK);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									if (SetBlockAddress(sdcmd.Argument.Word)) {
										SendSDResponseR1();
										SetCardStatus(csSendingData);
										ReadCardBlock();
									} else {
										DBG_SD("read multiple block error at 0x%08X", sdcmd.Argument.Word);
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_READ_MULTIPLE_BLOCK);
									break;
							}
						}
						break;
					case SD_CMD_WRITE_SINGLE_BLOCK:
						{
							DBG_SD_CMD(SD_CMD_WRITE_SINGLE_BLOCK);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									if (SetBlockAddress(sdcmd.Argument.Word)) {
										SendSDResponseR1();
										SetCardStatus(csReceiveData);
										bus->SetWriteBufferSizeHint(block_length);
										buffer_ptr = 0;
									} else {
										DBG_SD("write single block error at 0x%08X", sdcmd.Argument.Word);
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_WRITE_SINGLE_BLOCK);
									break;
							}
						}
						break;
					case SD_CMD_WRITE_MULTIPLE_BLOCK:
						{
							DBG_SD_CMD(SD_CMD_WRITE_MULTIPLE_BLOCK);
							switch (regCARD_STATUS.CURRENT_STATE) {
								case csTransfer:
									if (SetBlockAddress(sdcmd.Argument.Word)) {
										SendSDResponseR1();
										SetCardStatus(csReceiveData);
										bus->SetWriteBufferSizeHint(block_length);
										buffer_ptr = 0;
									} else {
										DBG_SD("write multiple block error at 0x%08X", sdcmd.Argument.Word);
									}
									break;
								default:
									DBG_SD_CMD_InvalidState(SD_CMD_WRITE_MULTIPLE_BLOCK);
									break;
							}
						}
						break;
					case SD_CMD_APP_CMD:
						{
							if (sdcmd.Argument.RCA.RCA == regRCA) {
								last_command_index = LAST_CMD_APP;
								DBG_SD_CMD(SD_CMD_APP_CMD);
								switch (regCARD_STATUS.CURRENT_STATE) {
									case csIdle:
									case csStabdby:
									case csTransfer:
									case csSendingData:
									case csReceiveData:
									case csProgramming:
									case csDisconnect:
										if (IsMMCCard()) {
											// MultiMedia card does not respond
										} else {
											regCARD_STATUS.APP_CMD = 1;
											SendSDResponseR1();
										}
										break;
									default:
										DBG_SD_CMD_InvalidState(SD_CMD_APP_CMD);
										break;
								}
							}
						}
						break;
					default:
						// unknown command
						DBG_SD("unknown command: %u:%u, arg: 0x%08X", sdcmd.Command.Host, sdcmd.Command.Index, sdcmd.Argument.Word);
						return false;
				}
				break;
			}
			break;
		default:
			DBG_SD("Invalid command type: %u:%u, arg: %08X", sdcmd.Command.Host, sdcmd.Command.Index, sdcmd.Argument.Word);
			return false;
	}

	return true;
}

bool SDDevice::AttachFile(const char* fileName)
{
	hSDFile = fopen(fileName, "r+b");
	if (hSDFile) {
		fseek(hSDFile, -SDFILE_CONFIG_LENGTH, SEEK_END);
		// read SD registers
		fread(&regCID, sizeof(CID_REGISTER), 1, hSDFile);
		fread(&regCSD, sizeof(CSD_REGISTER), 1, hSDFile);
		fread(&regSCR, sizeof(SCR_REGISTER), 1, hSDFile);
		fread(&regRCA, sizeof(RCA_REGISTER), 1, hSDFile);
		fread(&regOCR, sizeof(OCR_REGISTER), 1, hSDFile);

		STRUCT_REVERSE(regCSD);
		block_length = 1 << regCSD.READ_BL_LEN;
#ifdef DEBUG_SD
		DBG_SD("SD Card inserted");
		STRUCT_REVERSE(regCID);

		char oid[3], pnm[6];
		for (int i = 0; i < 2; i++) oid[i] = regCID.OID[1-i]; oid[2] = 0;
		for (int i = 0; i < 5; i++) pnm[i] = regCID.PNM[4-i]; pnm[5] = 0;
		uint mdt = regCID.MDT;
		uint mdt_year = 2000 + ((mdt & 0xF00) >> 8) * 10 + ((mdt & 0xF0) >> 4);
		uint mdt_mon = mdt & 0xF;
		DBG_SD("MID: 0x%02X, OID: %s, PNM: %s, rev: %X, PSN: 0x%08X, MDT: %02d/%d",
			regCID.MID, oid, pnm, regCID.PRV,
			regCID.PSN, mdt_mon, mdt_year);

		uint MULT = 4 << regCSD.C_SIZE_MULT;
		uint BLOCK_NR = ((uint)regCSD.C_SIZE + 1) * MULT;

		uint trans = -1;
		switch (regCSD.TRAN_SPEED.Unit) {
			case 0:
				trans = 10;
				break;
			case 1:
				trans = 100;
				break;
			case 2:
				trans = 1000;
				break;
			case 3:
				trans = 10000;
				break;
		}
		switch (regCSD.TRAN_SPEED.Value) {
			case 1:
				trans *= 10;
				break;
			case 2:
				trans *= 12;
				break;
			case 3:
				trans *= 13;
				break;
			case 4:
				trans *= 15;
				break;
			case 5:
				trans *= 20;
				break;
			case 6:
				trans *= 25;
				break;
			case 7:
				trans *= 30;
				break;
			case 8:
				trans *= 35;
				break;
			case 9:
				trans *= 40;
				break;
			case 10:
				trans *= 45;
				break;
			case 11:
				trans *= 50;
				break;
			case 12:
				trans *= 50;
				break;
			case 13:
				trans *= 60;
				break;
			case 14:
				trans *= 70;
				break;
			case 15:
				trans *= 80;
				break;
		}

		DBG_SD("capacity: %d * %db = %.2fMB, max speed: %.2fMHz", BLOCK_NR, block_length,
			((double)(BLOCK_NR * block_length)) / (1024.0 * 1024.0), (double)trans / 1000.0);

		STRUCT_REVERSE(regCID);
#endif
		STRUCT_REVERSE(regCSD);
		InitializeDevice();
		return true;
	} else {
		DBG_SD("use file error!");
	}

	return false;
}

bool SDDevice::CloseFile()
{
	if (hSDFile) {
		active = false;
		SetCardStatus(csInactive);
		fclose(hSDFile);
		hSDFile = NULL;
		return true;
	} else {
		// no card was inserted
		return false;
	}
}

/* Table for CRC-7 (polynomial x^7 + x^3 + 1) */
const uint8_t crc7_syndrome_table[256] = {
	0x00, 0x09, 0x12, 0x1b, 0x24, 0x2d, 0x36, 0x3f,
	0x48, 0x41, 0x5a, 0x53, 0x6c, 0x65, 0x7e, 0x77,
	0x19, 0x10, 0x0b, 0x02, 0x3d, 0x34, 0x2f, 0x26,
	0x51, 0x58, 0x43, 0x4a, 0x75, 0x7c, 0x67, 0x6e,
	0x32, 0x3b, 0x20, 0x29, 0x16, 0x1f, 0x04, 0x0d,
	0x7a, 0x73, 0x68, 0x61, 0x5e, 0x57, 0x4c, 0x45,
	0x2b, 0x22, 0x39, 0x30, 0x0f, 0x06, 0x1d, 0x14,
	0x63, 0x6a, 0x71, 0x78, 0x47, 0x4e, 0x55, 0x5c,
	0x64, 0x6d, 0x76, 0x7f, 0x40, 0x49, 0x52, 0x5b,
	0x2c, 0x25, 0x3e, 0x37, 0x08, 0x01, 0x1a, 0x13,
	0x7d, 0x74, 0x6f, 0x66, 0x59, 0x50, 0x4b, 0x42,
	0x35, 0x3c, 0x27, 0x2e, 0x11, 0x18, 0x03, 0x0a,
	0x56, 0x5f, 0x44, 0x4d, 0x72, 0x7b, 0x60, 0x69,
	0x1e, 0x17, 0x0c, 0x05, 0x3a, 0x33, 0x28, 0x21,
	0x4f, 0x46, 0x5d, 0x54, 0x6b, 0x62, 0x79, 0x70,
	0x07, 0x0e, 0x15, 0x1c, 0x23, 0x2a, 0x31, 0x38,
	0x41, 0x48, 0x53, 0x5a, 0x65, 0x6c, 0x77, 0x7e,
	0x09, 0x00, 0x1b, 0x12, 0x2d, 0x24, 0x3f, 0x36,
	0x58, 0x51, 0x4a, 0x43, 0x7c, 0x75, 0x6e, 0x67,
	0x10, 0x19, 0x02, 0x0b, 0x34, 0x3d, 0x26, 0x2f,
	0x73, 0x7a, 0x61, 0x68, 0x57, 0x5e, 0x45, 0x4c,
	0x3b, 0x32, 0x29, 0x20, 0x1f, 0x16, 0x0d, 0x04,
	0x6a, 0x63, 0x78, 0x71, 0x4e, 0x47, 0x5c, 0x55,
	0x22, 0x2b, 0x30, 0x39, 0x06, 0x0f, 0x14, 0x1d,
	0x25, 0x2c, 0x37, 0x3e, 0x01, 0x08, 0x13, 0x1a,
	0x6d, 0x64, 0x7f, 0x76, 0x49, 0x40, 0x5b, 0x52,
	0x3c, 0x35, 0x2e, 0x27, 0x18, 0x11, 0x0a, 0x03,
	0x74, 0x7d, 0x66, 0x6f, 0x50, 0x59, 0x42, 0x4b,
	0x17, 0x1e, 0x05, 0x0c, 0x33, 0x3a, 0x21, 0x28,
	0x5f, 0x56, 0x4d, 0x44, 0x7b, 0x72, 0x69, 0x60,
	0x0e, 0x07, 0x1c, 0x15, 0x2a, 0x23, 0x38, 0x31,
	0x46, 0x4f, 0x54, 0x5d, 0x62, 0x6b, 0x70, 0x79
};

static inline uint8_t crc7_byte(uint8_t crc, uint8_t data)
{
	return crc7_syndrome_table[(crc << 1) ^ data];
}
/**
 * crc7 - update the CRC7 for the data buffer
 * @crc:	 previous CRC7 value
 * @buffer:	 data pointer
 * @len:	 number of bytes in the buffer
 * Context: any
 *
 * Returns the updated CRC7 value.
 */
uint8_t crc7_update(uint8_t	 crc, const void* buffer, size_t len)
{
	const uint8_t* b = (const uint8_t*)buffer;
	while (len--)
		crc = crc7_byte(crc, *b++);
	return crc;
}

uint8_t crc7(const void* data, size_t len)
{
	return crc7_update(0, data, len);
}
