#ifndef SDBUS_H_INCLUDED
#define SDBUS_H_INCLUDED

#include <list>
#include "../component.h"

// SD card bus commands
#define SD_CMD_GO_IDLE_STATE		0		// CMD0
#define SD_CMD_MMC_SEND_OPCOND		1		// CMD1
#define SD_CMD_ALL_SEND_CID			2		// CMD2
#define SD_CMD_MMC_SET_RCA			3		// CMD3
#define SD_CMD_SEND_RELATIVE_ADDR	3		// CMD3
#define SD_CMD_SET_DSR				4		// CMD4
#define SD_CMD_IO_OP_COND			5		// CMD5
#define SD_CMD_SWITCH_FUNCTION		6		// CMD6
#define SD_CMD_SELECT_DESELECT_CARD 7		// CMD7
#define SD_CMD_SEND_IF_COND			8		// CMD8
#define SD_CMD_SEND_CSD				9		// CMD9
#define SD_CMD_SEND_CID				10		 // CMD10
#define SD_CMD_STOP_TRANSMISSION	12		 // CMD12
#define SD_CMD_SEND_STATUS			13		 // CMD13
#define SD_CMD_GO_INACTIVE_STATE	15		 // CMD15
#define SD_CMD_SET_BLOCKLEN			16		 // CMD16
#define SD_CMD_READ_SINGLE_BLOCK	17		 // CMD17
#define SD_CMD_READ_MULTIPLE_BLOCK	18		 // CMD18
#define SD_CMD_WRITE_SINGLE_BLOCK	24		 // CMD24
#define SD_CMD_WRITE_MULTIPLE_BLOCK 25		 // CMD25
#define SD_CMD_PROGRAM_CSD			27		 // CMD27
#define SD_CMD_SET_WRITE_PROT		28		 // CMD28
#define SD_CMD_CLR_WRITE_PROT		29		 // CMD29
#define SD_CMD_SEND_WRITE_PROT		30		 // CMD30
#define SD_CMD_ERASE_WR_BLK_START	32		 // CMD32
#define SD_CMD_ERASE_WR_BLK_END		33		 // CMD33
#define SD_CMD_ERASE				38		 // CMD38
#define SD_CMD_LOCK_UNLOCK			42		 // CMD42
#define SD_CMD_IO_RW_DIRECT			52		 // CMD52
#define SD_CMD_IO_RW_EXTENDED		53		 // CMD53
#define SD_CMD_APP_CMD				55		 // CMD55
#define SD_CMD_GEN_CMD				56		 // CMD56

// application Specific commands
#define SD_ACMD_SET_BUS_WIDTH				6
#define SD_ACMD_SD_STATUS					13
#define SD_ACMD_SEND_NUM_WR_BLOCKS			22
#define SD_ACMD_SET_WR_BLOCK_ERASE_COUNT	23
#define SD_ACMD_SD_SEND_OP_COND				41
#define SD_ACMD_SET_CLR_CARD_DETECT			42
#define SD_ACMD_SEND_SCR					51

// arg definition for ACMD SET_BUS_WIDTH
#define SD_ACMD_ARG_SET_BUS_1BIT			0x00000000
#define SD_ACMD_ARG_SET_BUS_4BIT			0x00000002

#define SDFILE_CONFIG_LENGTH	512

class SDDevice;

enum SDBUS_CALLBACKTYPE {sd_CommandSent, sd_NoResponse, sd_Response, sd_Data, sd_BusyFinish};

enum CARD_STATUS {
	csIdle = 0,
	csReady = 1,
	csIdentification = 2,
	csStabdby = 3,
	csTransfer = 4,
	csSendingData = 5,
	csReceiveData = 6,
	csProgramming = 7,
	csDisconnect = 8,
	csInactive = 100
};

#pragma pack(push, 1)

typedef union CID_REGISTER {
	uint64_t QuadWord[2];
	uint32_t Words[4];
	uint16_t HalfWord[8];
	uint8_t Byte[16];
	struct {
		uint8_t NotUsed:1;
		uint8_t CRC:7;
		uint16_t MDT:12;
		uint16_t :4;
		uint32_t PSN;
		uint8_t PRV;
		char PNM[5];
		char OID[2];
		uint8_t MID;
	};
} CID_REGISTER;

typedef union CSD_REGISTER {
	uint64_t QuadWord[2];
	uint32_t Words[4];
	uint16_t HalfWord[8];
	uint8_t Byte[16];
	struct {
		uint8_t NotUsed:1;
		uint8_t CRC:7; // can change

		uint8_t :2; // can change
		uint8_t FILE_FORMAT:2; // can change
		uint8_t TMP_WRITE_PROTECT:1; // can change
		uint8_t PERM_WRITE_PROTECT:1; // can change
		uint8_t COPY:1; // can change
		uint8_t FILE_FORMAT_GRP:1; // can change

		uint64_t :5;
		uint64_t WRITE_BL_PARTIAL:1;
		uint64_t WRITE_BL_LEN:4;
		uint64_t R2W_FACTOR:3;
		uint64_t :2;
		uint64_t WP_GRP_ENABLE:1;
		uint64_t WP_GRP_SIZE:7;
		uint64_t SECTOR_SIZE:7;
		uint64_t ERASE_BLK_EN:1;
		uint64_t C_SIZE_MULT:3;
		uint64_t VDD_W_CURR_MAX:3;
		uint64_t VDD_W_CURR_MIN:3;
		uint64_t VDD_R_CURR_MAX:3;
		uint64_t VDD_R_CURR_MIN:3;
		uint64_t C_SIZE:12;
		uint64_t :2;
		uint64_t DSR_IMP:1;
		uint64_t READ_BLK_MISALIGN:1;
		uint64_t WRITE_BLK_MISALIGN:1;
		uint64_t READ_BL_PARTIAL:1;

		uint16_t READ_BL_LEN:4;
		uint16_t CCC:12;

		union {
			uint8_t Byte;
			struct {
				uint8_t Unit:3;
				uint8_t Value:4;
				uint8_t :1;
			};
		} TRAN_SPEED;
		uint8_t NSAC;
		union {
			uint8_t Byte;
			struct {
				uint8_t Unit:3;
				uint8_t Value:4;
				uint8_t :1;
			} Bits;
		} TAAC;

		uint8_t :6;
		uint8_t CSD_STRUCTURE:2;
	};
} CSD_REGISTER;

typedef uint16_t RCA_REGISTER;

typedef union OCR_REGISTER {
	uint32_t Word;
	uint16_t HalfWord[2];
	uint8_t Byte[4];
	struct {
		uint32_t :4;
		uint32_t Voltage_16_17:1;
		uint32_t Voltage_17_18:1;
		uint32_t Voltage_18_19:1;
		uint32_t Voltage_19_20:1;
		uint32_t Voltage_20_21:1;
		uint32_t Voltage_21_22:1;
		uint32_t Voltage_22_23:1;
		uint32_t Voltage_23_24:1;
		uint32_t Voltage_24_25:1;
		uint32_t Voltage_25_26:1;
		uint32_t Voltage_26_27:1;
		uint32_t Voltage_27_28:1;
		uint32_t Voltage_28_29:1;
		uint32_t Voltage_29_30:1;
		uint32_t Voltage_30_31:1;
		uint32_t Voltage_31_32:1;
		uint32_t Voltage_32_33:1;
		uint32_t Voltage_33_34:1;
		uint32_t Voltage_34_35:1;
		uint32_t Voltage_35_36:1;
		uint32_t :7;
		uint32_t PowerStatus:1;
	};
} OCR_REGISTER;

typedef union SCR_REGISTER {
	uint64_t QuadWord;
	uint32_t Word[2];
	uint16_t HalfWord[4];
	uint8_t Byte[8];
	struct {
		uint32_t :32;
		uint32_t :16;
		uint32_t SD_BUS_WIDTHS:4;
		uint32_t SD_SECURITY:3;
		uint32_t DATA_STAT_AFTER_ERASE:1;
		uint32_t SD_SPEC:4;
		uint32_t SCR_STRUCTURE:4;
	};
} SCR_REGISTER;

typedef union CARD_STATUS_REGISTER {
	uint32_t Word;
	uint16_t HalfWord[2];
	uint8_t Byte[4];
	struct {
		uint32_t :2;
		uint32_t :1;
		uint32_t AKE_SEQ_ERROR:1;
		uint32_t :1;
		uint32_t APP_CMD:1;
		uint32_t :2;
		uint32_t READY_FOR_DATA:1;
		uint32_t CURRENT_STATE:4;
		uint32_t ERASE_RESET:1;
		uint32_t CARD_ECC_DISABLED:1;
		uint32_t WP_ERASE_SKIP:1;
		uint32_t CIDCSD_OVERWRITE:1;
		uint32_t :1;
		uint32_t :1;
		uint32_t _ERROR:1;
		uint32_t CC_ERROR:1;
		uint32_t CARD_ECC_FAILED:1;
		uint32_t ILLEGAL_COMMAND:1;
		uint32_t COM_CRC_ERROR:1;
		uint32_t LOCK_UNLOCK_FAILED:1;
		uint32_t CARD_IS_LOCKED:1;
		uint32_t WP_VIOLATION:1;
		uint32_t ERASE_PARAM:1;
		uint32_t ERASE_SEQ_ERROR:1;
		uint32_t BLOCK_LEN_ERROR:1;
		uint32_t ADDRESS_ERROR:1;
		uint32_t OUT_OF_RANGE:1;
	};
} CARD_STATUS_REGISTER;

typedef union SD_STATUS_REGISTER {
	uint64_t QuadWord[8];
	uint32_t Word[16];
	uint16_t HalfWord[32];
	uint8_t Byte[64];
	struct {
		uint8_t Reserved_0_311[39];
		uint8_t Reserved_312_447[17];
		uint32_t SIZE_OF_PROTECTED_AREA;
		uint16_t SD_CARD_TYPE;
		uint16_t :13;
		uint16_t SECURED_MODE:1;
		uint16_t DAT_BUS_WIDTH:2;
	};
} SD_STATUS_REGISTER;

typedef struct SDCOMMAND {
	union {
		uint8_t Byte;
		struct {
			uint8_t Index:6;
			uint8_t Host:1;
			uint8_t Start:1;
		};
	} Command;
	union {
		uint32_t Word;
		struct {
			uint16_t Reserved;
			RCA_REGISTER RCA;
		} RCA;
		OCR_REGISTER OCR;
	} Argument;
	uint8_t CRC:7;
	uint8_t End:1;
} SDCOMMAND;

typedef union SDRESPONSE {
	struct {
		uint8_t Start:1;
		uint8_t Transmission:1;
		uint8_t Command:6;
		union {
			uint32_t Word;
			uint16_t HalfWord[2];
			uint8_t Byte[4];
			union {
				CARD_STATUS_REGISTER CARD_STATUS;
			} R1;
			struct {
				OCR_REGISTER OCR;
			} R3;
			struct {
				uint16_t CardStatus;
				RCA_REGISTER RCA;
			} R6;
		} Data;
		uint8_t CRC:7;
		uint8_t End:1;
	} Short;
	struct {
		uint8_t Start:1;
		uint8_t Transmission:1;
		uint8_t Command:6;
		union {
			uint64_t QuadWord[2];
			uint32_t Word[4];
			uint16_t HalfWord[8];
			uint8_t Byte[16];
			union {
				CID_REGISTER CID;
				CSD_REGISTER CSD;
			} R2;
		} Data;
		uint8_t CRC:7;
		uint8_t End:1;
	} Long;
} SDRESPONSE;

typedef struct SDDATA {
	void* data;
	uint length;
} SDDATA;

#pragma pack(pop)

class SDBus;

typedef void (COMPONENT_METHOD *SDBus_callback)(SDBus* bus, SDDevice* device, SDBUS_CALLBACKTYPE type, SDRESPONSE* response);

enum SDBUS_REQUEST_TYPE {reqUndefined, reqCommand, reqSendData, reqReceiveData, reqBusyWait};

#define SDBUS_REQUEST_DATA_CAPACITY		4096
#define SDDEVICE_BLOCK_BUFFER_CAPACITY	(16 * 1024)

typedef struct SDBUS_REQUEST {
	SDBUS_REQUEST_TYPE type;
	SDCOMMAND cmd;
	uint cmdNo;
	void* data;
	uint data_length;
} SDBUS_REQUEST;

typedef class SDBus {
	friend class SDDevice;
private:
	Thread thread;
	Mutex commandSection;
	std::list<SDBUS_REQUEST*> requestList;
	AutoResetEvent hEvent;
	uint currentCommandNo;
	uint lastCommandNo;

	SDBUS_CALLBACKTYPE respType;
	SDRESPONSE sdresp;
	SDDevice* responseDevice;

	SDBus_callback cb;
	SDDevice* devices[256];
	uint devices_count;

	uint write_size_hint;

	COMPONENT_METHOD void SendSDResponse(SDDevice* dev, SDBUS_CALLBACKTYPE type, SDRESPONSE* resp);
	COMPONENT_METHOD void SendSDResponseLong(SDDevice* dev, SDRESPONSE* resp);
	COMPONENT_METHOD void SendSDResponseShort(SDDevice* dev, SDRESPONSE* resp);
	COMPONENT_METHOD void SendData(SDDevice* dev, void* data, uint size);
	COMPONENT_METHOD void SetWriteBufferSizeHint(uint size);
	static CALLBACK bool SDBusThreadProc(Thread* t, void* data);
	COMPONENT_METHOD void ThreadFunction();

public:
	SDBus();
	~SDBus();

	COMPONENT_METHOD void AddDevice(SDDevice* dev);
	COMPONENT_METHOD void SetCallback(SDBus_callback callback_fn);
	COMPONENT_METHOD void ExecuteCallback(SDDevice* dev, SDBUS_CALLBACKTYPE type, SDRESPONSE* resp);

	COMPONENT_METHOD bool SendCommand(SDCOMMAND* cmd);
	COMPONENT_METHOD bool SendData(void* data, uint size);
	COMPONENT_METHOD bool ReceiveData(uint size);
	COMPONENT_METHOD bool BusyWait();

} SDBus;

typedef class SDDevice : public EmulatedComponent {
	friend class SDBus;
private:
	SDBus* bus;
	Thread thread;
	AutoResetEvent hIdle;
	AutoResetEvent hEvent;	// set when command arrives
	SDBUS_REQUEST_TYPE reqType;
	uint reqDataLength;

	FILE* hSDFile;
	Mutex hSDFile_cs;

	uint block_length;
	uint8_t block_buffer[SDDEVICE_BLOCK_BUFFER_CAPACITY];
	uint buffer_ptr;
	uint buffer_length;

	SDCOMMAND sdcmd;
	uint sdcmdNo;
	SDRESPONSE sdresp;

	uint last_command_index;

	bool active;

	static CALLBACK bool SDDeviceThreadProc(Thread* t, void* data);
	COMPONENT_METHOD bool ThreadFunction();

	COMPONENT_METHOD void SetCardStatus(CARD_STATUS status);
	COMPONENT_METHOD uint16_t GetCardStatusR6();

	// internal functions, execute specific actions
	COMPONENT_METHOD bool ExecuteSDCommand();
	COMPONENT_METHOD void DoReceiveData();
	COMPONENT_METHOD void DoWriteData();

	// functions to communicate with BUS, send response, data, etc...
	COMPONENT_METHOD void SendData(void* data, uint size);
	COMPONENT_METHOD void ExecuteCallback(SDBUS_CALLBACKTYPE type, SDRESPONSE* resp);
	COMPONENT_METHOD void SendSDResponseR1();
	COMPONENT_METHOD void SendSDResponseR1b();
	COMPONENT_METHOD void SendSDResponseR2();
	COMPONENT_METHOD void SendSDResponseR3();
	COMPONENT_METHOD void SendSDResponseR6();

	COMPONENT_METHOD bool SetBlockAddress(uint32_t address);
	COMPONENT_METHOD bool ReadCardBlock();
	COMPONENT_METHOD bool WriteCardBlock();

	COMPONENT_METHOD void WaitForIdle();

protected:
	// CID: Card identification number: individual card number for identification.
	CID_REGISTER regCID;
	// CSD: Card specific data: information about the card operation conditions.
	CSD_REGISTER regCSD;
	// RCA: Relative card address: local system address of a card, dynamically
	// suggested by the card and approved by the host during initialization.
	RCA_REGISTER regRCA;
	// OCR: Operation Condition Register
	OCR_REGISTER regOCR;
	// SD Configuration Register: information about the SD Card�s special features capabilities.
	SCR_REGISTER regSCR;
	// Card Status
	CARD_STATUS_REGISTER regCARD_STATUS;
	// SD Status
	SD_STATUS_REGISTER regSD_STATUS;
public:
	SDDevice();
	~SDDevice();
	inline SDBus* GetBus() { return bus; };

	// interface to use from BUS
	COMPONENT_METHOD virtual void InitializeDevice();
	COMPONENT_METHOD virtual bool SendCommand(SDCOMMAND* cmd, uint cmdNo);
	COMPONENT_METHOD virtual bool ReceiveData(uint size);
	COMPONENT_METHOD virtual bool WriteData(void* data, uint size);

	COMPONENT_METHOD virtual bool AttachFile(const char* fileName);
	COMPONENT_METHOD virtual bool CloseFile();

} SDDevice;

uint8_t crc7_update(uint8_t crc, const void* data, size_t len);
uint8_t crc7(const void* data, size_t len);

#endif
