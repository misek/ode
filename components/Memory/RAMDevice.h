/*
 * ram.h
 *
 *  Created on: 16-05-2012
 *      Author: Łukasz Misek
 */

#ifndef RAMDEVICE_H_
#define RAMDEVICE_H_

#include <malloc.h>
#include "../component.h"

class RAMMemory: public EmulatedComponent {
	void* data;
	unsigned int size;
public:
	RAMMemory();
	virtual ~RAMMemory();

	COMPONENT_METHOD void SetSize(unsigned int new_size);
	COMPONENT_METHOD void* GetRAW();
	COMPONENT_METHOD unsigned int GetSize();
};

#endif /* RAMDEVICE_H_ */
