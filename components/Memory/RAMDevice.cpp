/*
 * ram.cpp
 *
 *  Created on: 16-05-2012
 *      Author: Łukasz Misek
 */

#include "RAMDevice.h"

RAMMemory::RAMMemory(): EmulatedComponent()
{
	data = __null;
	size = 0;
}

RAMMemory::~RAMMemory()
{
	SetSize(0);
}

void RAMMemory::SetSize(unsigned int new_size)
{
	if (data) {
		free(data);
	}
	size = new_size;
	if (size) {
		data = memalign(1024 * 1024, size);
	} else {
		data = __null;
	}
}

void* RAMMemory::GetRAW()
{
	return data;
}

unsigned int RAMMemory::GetSize()
{
	return size;
}
