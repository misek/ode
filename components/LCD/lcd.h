/*
 * lcd.h
 *
 *  Created on: 08-06-2012
 *      Author: Łukasz Misek
 */

#ifndef LCD_H_
#define LCD_H_

#include "../component.h"

class LCDDisplay;

enum LCD_BPP {
	bpp_Invalid,
	bpp_R5G6B5
};

#define MAX_LCD_FRAME_BUFFERS	16

enum TouchPanelPenState {
	penUp, penDown
};

struct TouchPanelState {
	TouchPanelPenState state;
	uint x;
	uint y;
};

typedef void (COMPONENT_METHOD *touchpanel_callback)(LCDDisplay* display, TouchPanelState* state);

class LCDDisplay: public EmulatedComponent {
private:
	void* widget_box;
	void* widget_lcd;
	void* frame[MAX_LCD_FRAME_BUFFERS];

	COMPONENT_METHOD void FreeFrameBuffer(uint frame_no);

public:
	LCDDisplay();
	virtual ~LCDDisplay();

	COMPONENT_METHOD void SetFrameBuffer(uint frame_no, void* buffer, uint width, uint height, LCD_BPP bpp);
	COMPONENT_METHOD void RedrawFrame(uint frame_no);
	COMPONENT_METHOD void Redraw();

	COMPONENT_METHOD virtual GtkWidget* GetWidget();

	COMPONENT_METHOD virtual uint GetWidth() = 0;
	COMPONENT_METHOD virtual uint GetHeight() = 0;

	touchpanel_callback TC_callback;
};

class LCD35_320x340: public LCDDisplay {
public:
	LCD35_320x340();

	COMPONENT_METHOD virtual uint GetWidth();
	COMPONENT_METHOD virtual uint GetHeight();
};

#endif /* LCD_H_ */
