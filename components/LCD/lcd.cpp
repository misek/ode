/*
 * lcd.cpp
 *
 *  Created on: 08-06-2012
 *      Author: Łukasz Misek
 */

#include "lcd.h"
#include <cairo.h>
#include <gtk/gtk.h>

struct FRAME_BUFFER_INFO {
	void* data;
	uint width;
	uint height;
	LCD_BPP bpp;
	cairo_format_t format;
	cairo_surface_t* surface;
};

struct LCDInfo {
	LCDDisplay* display;
	FRAME_BUFFER_INFO** fbi;
};

gboolean widget_draw_callback(GtkDrawingArea *widget, cairo_t *cr, LCDInfo* li)
{
	cairo_rectangle(cr, 0, 0, li->display->GetWidth(), li->display->GetHeight());
	cairo_clip(cr);
	// FIXME: only one surface supported
	if (li->fbi[0]->surface) {
		cairo_set_source_surface(cr, li->fbi[0]->surface, 0, 0);
	}

	cairo_paint(cr);
  	return false;
}

gint widget_mouse_motion_callback(GtkDrawingArea *widget, GdkEventMotion *event, LCDDisplay* display)
{
	if (display->TC_callback && (event->state & GDK_BUTTON1_MASK)) {
		TouchPanelState state;
		state.state = penDown;
		if (event->x >= display->GetWidth()) {
			state.x = display->GetWidth() - 1;
		} else
			if (event->x < 0) {
				state.x = 0;
			} else {
				state.x = (uint)event->x;
			}
		if (event->y >= display->GetHeight()) {
			state.y = display->GetHeight() - 1;
		} else
			if (event->y < 0) {
				state.y = 0;
			} else {
				state.y = (uint)event->y;
			}
		display->TC_callback(display, &state);
	}
	return 0;
}

gint widget_button_event_callback(GtkDrawingArea *widget, GdkEventButton *event, LCDDisplay* display)
{
	if (display->TC_callback && (event->button == 1)) {
		TouchPanelState state;

		switch (event->type) {
			case GDK_BUTTON_PRESS:
				state.state = penDown;
				break;
			case GDK_BUTTON_RELEASE:
				state.state = penUp;
				break;
			default:
				return 0;
		}

		state.x = (uint)event->x;
		state.y = (uint)event->y;
		display->TC_callback(display, &state);
	}

	return 0;
}

LCDDisplay::LCDDisplay()
{
	TC_callback = __null;
	LCDInfo* di = new LCDInfo();

	for (uint i = 0; i < MAX_LCD_FRAME_BUFFERS; i++) {
		FRAME_BUFFER_INFO* fbi = new FRAME_BUFFER_INFO;
		memset(fbi, 0, sizeof(FRAME_BUFFER_INFO));
		frame[i] = fbi;
	}
	di->display = this;
	di->fbi = (FRAME_BUFFER_INFO**)frame;

	widget_box = gtk_event_box_new();
	gtk_widget_add_events(GTK_WIDGET(widget_box), GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
	widget_lcd = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(widget_box), GTK_WIDGET(widget_lcd));
	gtk_widget_show(GTK_WIDGET(widget_lcd));

	g_signal_connect(G_OBJECT(widget_lcd), "draw", G_CALLBACK(widget_draw_callback), di);
	g_signal_connect(G_OBJECT(widget_box), "motion-notify-event", G_CALLBACK(widget_mouse_motion_callback), this);
	g_signal_connect(G_OBJECT(widget_box), "button-press-event", G_CALLBACK(widget_button_event_callback), this);
	g_signal_connect(G_OBJECT(widget_box), "button-release-event", G_CALLBACK(widget_button_event_callback), this);
}

LCDDisplay::~LCDDisplay()
{
	for (uint i = 0; i < MAX_LCD_FRAME_BUFFERS; i++) {
		delete (FRAME_BUFFER_INFO*)frame[i];
	}
	gtk_widget_destroy(GTK_WIDGET(widget_lcd));
	gtk_widget_destroy(GTK_WIDGET(widget_box));
}

GtkWidget* LCDDisplay::GetWidget()
{
	return GTK_WIDGET(widget_box);
}

void LCDDisplay::FreeFrameBuffer(uint frame_no)
{
	ASSERT(frame_no < MAX_LCD_FRAME_BUFFERS);
	FRAME_BUFFER_INFO* fbi = (FRAME_BUFFER_INFO*)frame[frame_no];
	if (fbi->surface) {
		cairo_surface_destroy(fbi->surface);
	}
	memset(fbi, 0, sizeof(FRAME_BUFFER_INFO));
}

void LCDDisplay::SetFrameBuffer(uint frame_no, void* buffer, uint width, uint height, LCD_BPP bpp)
{
	FreeFrameBuffer(frame_no);
	FRAME_BUFFER_INFO* fbi = (FRAME_BUFFER_INFO*)frame[frame_no];
	fbi->data = buffer;
	fbi->width = width;
	fbi->height = height;
	fbi->bpp = bpp;

	switch (bpp) {
		case bpp_R5G6B5:
			fbi->format = CAIRO_FORMAT_RGB16_565;
			break;
		default:
			fbi->format = CAIRO_FORMAT_INVALID;
			break;
	}
	if (fbi->format != CAIRO_FORMAT_INVALID) {
		fbi->surface = cairo_image_surface_create_for_data((unsigned char*)fbi->data,
			fbi->format, fbi->width, fbi->height,
			cairo_format_stride_for_width(fbi->format, fbi->width));
	} else {
		FreeFrameBuffer(frame_no);
	}
}

void LCDDisplay::RedrawFrame(uint frame_no)
{
	FRAME_BUFFER_INFO* fbi = (FRAME_BUFFER_INFO*)frame[frame_no];
	if (fbi->surface) {
		//FIXME: implement
	}
}

void LCDDisplay::Redraw()
{
	for (uint i = 0; i < MAX_LCD_FRAME_BUFFERS; i++) {
		FRAME_BUFFER_INFO* fbi = (FRAME_BUFFER_INFO*)frame[i];
		if (fbi->surface) {
			RedrawFrame(i);
		}
	}
	gtk_widget_queue_draw(GTK_WIDGET(widget_lcd));
}

LCD35_320x340::LCD35_320x340(): LCDDisplay()
{

}

uint LCD35_320x340::GetWidth()
{
	return 320;
}

uint LCD35_320x340::GetHeight()
{
	return 240;
}
