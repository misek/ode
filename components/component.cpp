/*
 * component.cpp
 *
 *  Created on: 12-05-2012
 *      Author: Łukasz Misek
 */

#include "component.h"

EmulatedComponent::EmulatedComponent()
{

}

EmulatedComponent::~EmulatedComponent()
{

}

void EmulatedComponent::PowerOn()
{

}

void EmulatedComponent::PowerOff()
{

}

GtkWidget* EmulatedComponent::GetWidget()
{
	// non visual component
	return __null;
}
